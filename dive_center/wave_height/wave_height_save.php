<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>本日の海況</title>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	$dive_center_id = $_SESSION['dive_center_id'];
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	
	/* ダイブセンターに登録されている情報 start */
	$arr_db_field = array("dive_center_id", "wave_height");
	$arr_db_field = array_merge($arr_db_field, array("flag_open"));

	if($dive_center_wave_height_id=="")
	{
		
		//日毎に登録
		$dbup = "delete from dive_center_wave_height where dive_center_id = '$dive_center_id' ";
		$dbup .= " and DATE_FORMAT(regi_date, '%Y%m%d')='".date("Ymd")."' ";
		$db_result = $common_dao->db_update($dbup);
		
		$db_insert = "insert into dive_center_wave_height (dive_center_wave_height_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update dive_center_wave_height set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where dive_center_wave_height_id='".$dive_center_wave_height_id."'";
	}	
	$db_result = $common_dao->db_update($db_insert);
	
	$yyyymm = '0000-00-00';
	//最新の波状況をdive_center へ登録
	$sql = "SELECT DATE_FORMAT(regi_date, '%Y%m%d') as yyyymm FROM dive_center_wave_height where dive_center_id = '$dive_center_id' " ;
	$sql .= " order by regi_date desc ";
	$sql .= " limit 0, 1";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$yyyymm = $db_result[0]["yyyymm"];
	}
	
	if($flag_open==100)
	{
		$datetime = date("Y-m-d 00:00:00");;
	}
	
	$db_insert = "update dive_center set ";
	$db_insert .= " wave_regi_date='".$datetime."' ";
	$db_insert .= " where dive_center_id = '".$dive_center_id."' ";
	$db_result = $common_dao->db_update($db_insert);
		
	$common_connect -> Fn_javascript_move("登録・修正しました", "/dive_center/dashboard/");
?>
</body>
</html>