<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>本日の海況</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	$dive_center_id = $_SESSION['dive_center_id'];
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($dive_center_id == "")
	{
		$common_connect -> Fn_javascript_back("ダイブセンターIDがありません。");
	}
	
	//DBへ保存
	if ($wave_height_id != "" && $dive_center_id != "")
	{
		//商品情報削除
		$dbup = "delete from dive_center_wave_height where dive_center_wave_height_id = '$wave_height_id' and dive_center_id = '$dive_center_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$yyyymm = '0000-00-00';
	//最新の波状況をdive_center へ登録
	$sql = "SELECT DATE_FORMAT(regi_date, '%Y%m%d') as yyyymm FROM dive_center_wave_height where dive_center_id = '$dive_center_id' " ;
	$sql .= " order by regi_date desc ";
	$sql .= " limit 0, 1";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$yyyymm = $db_result[0]["yyyymm"];
	}
		
	$db_insert = "update dive_center set ";
	$db_insert .= " wave_regi_date='".$yyyymm."' ";
	$db_insert .= " where dive_center_id = '".$dive_center_id."' ";
	$db_result = $common_dao->db_update($db_insert);
	
	$common_connect -> Fn_javascript_move("削除しました。", "/dive_center/dashboard/");

?>
</body>
</html>