<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
	$s_dive_center_id = $_SESSION['dive_center_id'];
	
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($s_yyyymm=="")
	{
		$s_yyyymm = date("Ym");
	}
	
	//ステータスをArrayへ
	$sql = "SELECT cate_status_id, cate_status_title FROM cate_status order by view_level";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_status_title[$db_result[$db_loop][cate_status_id]] = $db_result[$db_loop][cate_status_title];
		}
	}
	
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw", "commission"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_id, ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_name[$dive_center_id] = $dive_center_name;
			$arr_dive_center_point[$dive_center_id] = $dive_center_point;
			$arr_dive_center_commission[$dive_center_id] = $commission;
		}
	}
	
header("Content-Type: application/vnd.ms-excel;"); 
header("Content-Disposition: attachment; filename=master_".date("YmdHi").".csv"); 
header("Content-Transfer-Encoding: text/plain"); 
header("Pragma: no-cache"); 
header("Expires: 0"); 

	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id", "member_name_1", "member_name_2");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_name", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price", "used_point"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where ";
	$sql .= " left(yyyymmdd, 7)='".date("Y-m", strtotime($s_yyyymm."01"))."' ";
	$sql .= " and dive_center_id='".$s_dive_center_id."' ";
	
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by dive_center_name desc";
	}
	//$sql .= " and flag_open=1 ";//管理ページは見れる

	$enter = "\r\n";
	$csv_list = "";
	$csv_list .= "日付,センター名,申込者,実施状況,バディ,スタイル,エントリー,タンク1,タンク2,タンク3,その他金額,料金,システム利用料,使用されたポイント".$enter;

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			
			$view_buddy = "";
			$arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id) ;
			if($arr_buddy)
			{
				for($db_loop_buddy=0 ; $db_loop_buddy < count($arr_buddy["member_id"]) ; $db_loop_buddy++)
				{
					//申込者は表示させない
					if($member_id!=$arr_buddy["member_id"][$db_loop_buddy])
					{
						$view_buddy .= $arr_buddy["member_name_1"][$db_loop_buddy]." ".$arr_buddy["member_name_2"][$db_loop_buddy].",";
					}
				}
			}
			if($view_buddy!="") {$view_buddy = substr($view_buddy, 0, strlen($view_buddy)-1); }
			
			$view_reserve_1_tank_name = "";
			if($reserve_1_tank_id!="0")
			{
				$view_reserve_1_tank_name = $reserve_1_tank_name;
			}
			$view_reserve_2_tank_name = "";
			if($reserve_2_tank_id!="0")
			{
				$view_reserve_2_tank_name = $reserve_2_tank_name;
			}
			$view_reserve_3_tank_name = "";
			if($reserve_3_tank_id!="0")
			{
				$view_reserve_3_tank_name = $reserve_3_tank_name;
			}
			
			$used_price = $reserve_all_price*(0.01*$arr_dive_center_commission[$dive_center_id]);
			$regi_date = substr($regi_date, 0, 10);
			
			$csv_list .= "\"".$yyyymmdd."\",\"".$dive_center_name."\",\"".$member_name_1." ".$member_name_2."\",\"".$arr_cate_status_title[$status]."\",\"".$view_buddy."\",\"".$reserve_style_name."\",\"".$reserve_boat_name."\",\"".$view_reserve_1_tank_name."\",\"".$view_reserve_2_tank_name."\",\"".$view_reserve_3_tank_name."\",\"".number_format($sum_etc_price)."円\",\"";
			$csv_list .= number_format($reserve_all_price)."円\",\"".number_format($used_price)."円\",\"".$used_point."pt\"".$enter;

		}
	}

	
	echo mb_convert_encoding($csv_list, 'SJIS', 'UTF-8');
?>
