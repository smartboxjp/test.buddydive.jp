<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
</head>
<body>
<?php
	$common_connect -> Fn_dive_center_check();
	$dive_center_id = $_SESSION['dive_center_id'];
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($s_yyyymmdd=="")
	{
		$common_connect -> Fn_javascript_back("IDがありません。");
	}
	
	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id", "member_name_1", "member_name_2");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_name", "dive_center_point", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where dive_center_id='".$dive_center_id."' and yyyymmdd='".date("Y-m-d", strtotime($s_yyyymmdd))."' ";
	$sql .= " and status<90 ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			
			$view_s_yyyymmdd = date("Y/m/d", strtotime($s_yyyymmdd));

			$db_message_member = $common_member -> Fn_member_info ($common_dao, $member_id);
			$email = $db_message_member[0]["member_email"];
			$dive_center_name = $db_result[$db_loop]["dive_center_name"];
			
			if($email!="")
			{
		$subject = $dive_center_name."より".date("m月d日", strtotime($s_yyyymmdd))."開催中止のお知らせ";
		
		$body = "";
		
$body = <<<EOF
$member_name_1 $member_name_2 様

こんにちは。バディダイブ事務局です。
いつも当サイトのご利用ありがとうございます。

ご予約いただいていたダイビングが開催中止となりました。

====================
■予約日：$view_s_yyyymmdd
■ダイビングポイント：$dive_center_point
====================

本日の海況については、下記URLよりご確認いただけます。
【ダイビングポイントの海況】
https://buddydive.jp/condition/

$global_email_footer


EOF;

	$common_email-> Fn_send_utf($email."<".$email.">",$subject,$body,$global_mail_from,$global_send_mail);

			}
		}
	}

	$db_insert = "update dive_reserve set ";
	$db_insert .= " status='99', "; //中止
	$db_insert .= " up_date='".$datetime."' ";
	$db_insert .= " where dive_center_id='".$dive_center_id."' and yyyymmdd='".date("Y-m-d", strtotime($s_yyyymmdd))."' ";
	$db_insert .= " and status<90 ";

	$db_result = $common_dao->db_update($db_insert);


//管理者へメール
$subject = $dive_center_name."より".date("m月d日", strtotime($s_yyyymmdd))."開催中止のお知らせ";

$body = "";
		
$body = <<<EOF
ご予約いただいていたダイビングが開催中止となりました。

====================
■予約日：$view_s_yyyymmdd
■ダイビングポイント：$dive_center_point
====================

本日の海況については、下記URLよりご確認いただけます。
【ダイビングポイントの海況】
https://buddydive.jp/condition/

$global_email_footer


EOF;

	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);


	$common_connect -> Fn_javascript_move("開催中止を行いました", "day.php?s_yyyymmdd=".str_replace("-", "", $yyyymmdd));
?>
</body>
</html>
