<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	$Common_contents = new CommonContents();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
</head>
<body>
<?php
	$common_connect -> Fn_dive_center_check();
	$dive_center_id = $_SESSION['dive_center_id'];
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($reserve_style_id=="" || $reserve_boat_id=="" || $yyyymmdd=="" || $reserve_1_tank_id=="" || $dive_center_id=="")
	{
		$common_connect -> Fn_javascript_back("すべて必須です。");
	}
	
	$reserve_price = 0;
	$reserve_yyyymm = substr($yyyymmdd, 0, 6);
	$reserve_dd = substr($yyyymmdd, 6, 2);
	
	

	//スタイル
	$sql = "SELECT dive_center_style_id, dive_center_style_title, dive_center_style_price  FROM dive_center_style where dive_center_id='".$dive_center_id."' ";
	$sql .= " and dive_center_style_id='".$reserve_style_id."' ";
	$db_result_style = $common_dao->db_query($sql);
	$reserve_style_id = $db_result_style[0]["dive_center_style_id"];
	$reserve_style_name = $db_result_style[0]["dive_center_style_title"];
	$reserve_style_price = $db_result_style[0]["dive_center_style_price"];
	$reserve_price += $reserve_style_price;
	
	//ボート
	$sql = "SELECT dive_center_boat_id, dive_center_boat_title, dive_center_boat_price FROM dive_center_boat where dive_center_id='".$dive_center_id."' ";
	$sql .= " and dive_center_boat_id='".$reserve_boat_id."' ";
	$db_result_boat = $common_dao->db_query($sql);
	$reserve_boat_id = $db_result_boat[0]["dive_center_boat_id"];
	$reserve_boat_name = $db_result_boat[0]["dive_center_boat_title"];
	$reserve_boat_price = $db_result_boat[0]["dive_center_boat_price"];
	$reserve_price += $reserve_boat_price;
	
	//タンク1
	$sql = "SELECT dive_center_tank_id, dive_center_tank_title, dive_center_tank_price FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
	$sql .= " and dive_center_tank_id='".$reserve_1_tank_id."' ";
	$db_result_tank = $common_dao->db_query($sql);
	$reserve_1_tank_id = $db_result_tank[0]["dive_center_tank_id"];
	$reserve_1_tank_name = $db_result_tank[0]["dive_center_tank_title"];
	$reserve_1_tank_price = $db_result_tank[0]["dive_center_tank_price"];
	$reserve_price += $reserve_1_tank_price;
	
	//タンク2
	if($reserve_2_tank_id!="")
	{
		$sql = "SELECT dive_center_tank_id, dive_center_tank_title, dive_center_tank_price FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
		$sql .= " and dive_center_tank_id='".$reserve_2_tank_id."' ";
		$db_result_tank = $common_dao->db_query($sql);
		$reserve_2_tank_id = $db_result_tank[0]["dive_center_tank_id"];
		$reserve_2_tank_name = $db_result_tank[0]["dive_center_tank_title"];
		$reserve_2_tank_price = $db_result_tank[0]["dive_center_tank_price"];
		$reserve_price += $reserve_2_tank_price;
	}
	
	//タンク3
	if($reserve_3_tank_id!="")
	{
		$sql = "SELECT dive_center_tank_id, dive_center_tank_title, dive_center_tank_price FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
		$sql .= " and dive_center_tank_id='".$reserve_3_tank_id."' ";
		$db_result_tank = $common_dao->db_query($sql);
		$reserve_3_tank_id = $db_result_tank[0]["dive_center_tank_id"];
		$reserve_3_tank_name = $db_result_tank[0]["dive_center_tank_title"];
		$reserve_3_tank_price = $db_result_tank[0]["dive_center_tank_price"];
		$reserve_price += $reserve_3_tank_price;
	}
	
	$reserve_price += $etc_price;
	$reserve_price = $reserve_price-$used_point;
	
	//会員情報
	$datetime = date("Y-m-d H:i:s");
	$reserve_tax = ($reserve_price)*global_tax_percent;
	$reserve_all_price = $reserve_tax+$reserve_price;
	
	$arr_db_field = array("yyyymmdd", "member_id");//, "user_name", "member_name_1", "member_name_2"
	$arr_db_field = array_merge($arr_db_field, array("reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name", "reserve_comment"));
	$arr_db_field = array_merge($arr_db_field, array("used_point"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price", "status"));
	
	$db_insert = "update dive_reserve set ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= $val."='".$$val."', ";
	}
	$db_insert .= " up_date='".$datetime."' ";
	$db_insert .= " where dive_reserve_id='".$dive_reserve_id."' and dive_center_id='".$dive_center_id."' ";
	$db_result = $common_dao->db_update($db_insert);
	
	//清算済みでポイント付与されてなければ追加
	$sql_reserve = "SELECT add_point FROM dive_reserve where dive_center_id='".$dive_center_id."' ";
	$sql_reserve .= " and dive_reserve_id='".$dive_reserve_id."' ";
	$db_result_reserve = $common_dao->db_query($sql_reserve);
	$add_point = $db_result_reserve[0]["add_point"];
	
	$add_point = floor($reserve_all_price*global_point_percent);

	//予約情報ポイントアップ
	$db_insert = "update dive_reserve set ";
	$db_insert .= " add_point='".$add_point."' ";
	$db_insert .= " where dive_reserve_id='".$dive_reserve_id."' ";
	$db_result = $common_dao->db_update($db_insert);


	if($status==2 && ($orignal_add_point != $add_point))
	{
		//会員にポイント付与
		$diff_point = $add_point-$orignal_add_point;
		
		$db_insert = "update member set ";
		$db_insert .= " member_point=member_point+'".$diff_point."' ";
		$db_insert .= " where member_id='".$member_id."' ";
		
		$db_result = $common_dao->db_update($db_insert);

		//ポイント履歴
		$common_member -> Fn_member_point_history ($common_dao, $member_id, $dive_reserve_id, "予約番号：".$dive_reserve_id." 付与", $diff_point, $_SERVER['PHP_SELF']);
			
	}

	$common_connect -> Fn_javascript_move("修正しました", "/dive_center/reserve/day.php?s_yyyymmdd=".str_replace("-", "", $yyyymmdd));
?>
</body>
</html>
