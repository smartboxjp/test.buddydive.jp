<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	$dive_center_id = $_SESSION['dive_center_id'];
	
	$db_result = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	if($db_result)
	{
		$dive_center_name = $db_result[0]["dive_center_name"];
	}
	
  $header_centername = $dive_center_name;
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/header.php'); ?>


<?
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($s_yyyymm=="")
	{
		$s_yyyymm = date("Ym");
	}

	/* ダイブセンターに登録されている情報 start */
	//スタイル
	$arr_db_field = array("dive_center_style_id", "dive_center_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price");
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where dive_center_id='".$dive_center_id."'";
	//$sql .= " and flag_open=1 ";//管理ページは見れる
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_style_title[$dive_center_style_id] = $dive_center_style_title;
		}
	}
	
	//ボート
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
	$arr_db_field_boat = array_merge($arr_db_field_boat, array("view_level", "flag_open", "regi_date", "up_date"));
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	$db_result_boat = $common_dao->db_query($sql);
	if($db_result_boat)
	{
		for($db_loop=0 ; $db_loop < count($db_result_boat) ; $db_loop++)
		{
			$arr_dive_center_boat_title[$db_result_boat[$db_loop][dive_center_boat_id]] = $db_result_boat[$db_loop][dive_center_boat_title];
		}
	}
	
	//タンク
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field_tank = array_merge($arr_db_field_tank, array("view_level", "flag_open", "regi_date", "up_date"));
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	$db_result_tank = $common_dao->db_query($sql);
	if($db_result_tank)
	{
		for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
		{
			$arr_dive_center_tank_title[$db_result_tank[$db_loop][dive_center_tank_id]] = $db_result_tank[$db_loop][dive_center_tank_title];
		}
	}
	/* ダイブセンターに登録されている情報 end */
	
	
	
	
	/*予約情報 start */
	$sql = "SELECT reserve_style_id, reserve_boat_id, ";
	$sql .= " reserve_1_tank_id, reserve_1_tank_name, reserve_2_tank_id, reserve_2_tank_name, reserve_3_tank_id, reserve_3_tank_name, ";
	$sql .= " day(yyyymmdd) as dd, status FROM dive_reserve ";
	$sql .= " where dive_center_id='".$dive_center_id."' and left(yyyymmdd, 7)='".substr($s_yyyymm, 0, 4)."-".substr($s_yyyymm, 4, 2)."' ";
	//$sql .= " and flag_open=1 ";//管理ページは見れる
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_reserve_style[$db_result[$db_loop]["reserve_style_id"]][substr($db_result[$db_loop]["dd"]+100, 1)]++;
			$arr_reserve_boat[$db_result[$db_loop]["reserve_boat_id"]][substr($db_result[$db_loop]["dd"]+100, 1)]++;
			
			$arr_reserve_tank[$db_result[$db_loop]["reserve_1_tank_id"]][substr($db_result[$db_loop]["dd"]+100, 1)]++;
			$arr_reserve_tank[$db_result[$db_loop]["reserve_2_tank_id"]][substr($db_result[$db_loop]["dd"]+100, 1)]++;
			$arr_reserve_tank[$db_result[$db_loop]["reserve_3_tank_id"]][substr($db_result[$db_loop]["dd"]+100, 1)]++;
		}
	}
	/*予約情報 end */
?>
<article>

<div class="listSelect">
  <form action="<? echo $_SERVER['PHP_SELF'];?>">
    <? $var = "s_yyyymm"; ?>
    <select name="<? echo $var;?>" id="<? echo $var;?>">
    <?
    $from_yyyymmdd = date('Y-m-d', strtotime("2015-11-01"));
    $to_yyyymmdd = date('Y-m-d', strtotime("+6 months", strtotime(date("Ymd"))));
    
    for($loop=$from_yyyymmdd ; date('Y-m-d', strtotime($loop)) <= $to_yyyymmdd ; $loop=$loop_yyyymmdd)
    {
      $sql_yyyymm="SELECT '$loop' + INTERVAL 1 MONTH as yyyymm"; 
      $db_result_yyyymm = $common_dao->db_query($sql_yyyymm);
      $loop_yyyymmdd = date('Y-m-d', strtotime($db_result_yyyymm[0]["yyyymm"]));
      $view_loop_yyyymmdd = date('Ym', strtotime($db_result_yyyymm[0]["yyyymm"]));
    ?>
      <option value="<?=$view_loop_yyyymmdd;?>" <? if($view_loop_yyyymmdd==$$var){ echo " selected ";} ?>><?=substr($view_loop_yyyymmdd, 0, 4)."年".substr($view_loop_yyyymmdd, 4, 2)."月";?></option>
    <?
      }
    ?>
    </select>
    <input type="submit" value="表示">
  </form>
</div>

<section class="sectionBox">
  <p class="tit"><? echo substr($s_yyyymm, 0, 4)."年".substr($s_yyyymm, 4, 2)."月"; ?>予約リスト</p>
  <div class="box">
  <table class="listTable">
    <tr>
      <th>日付</th>
      <? foreach($arr_dive_center_style_title as $key=>$value) { ?>
      <th><? echo $value;?></th>
      <? } ?>
      <? foreach($arr_dive_center_boat_title as $key=>$value) { ?>
      <th><? echo $value;?></th>
      <? } ?>
      <? foreach($arr_dive_center_tank_title as $key=>$value) { ?>
      <th><? echo $value;?></th>
      <? } ?>
    </tr>
    <?
		$last_day = date('t', strtotime(substr($s_yyyymm, 0, 4)."-".substr($s_yyyymm, 4, 2)."-01"));
		
    for($loop_day = 1 ; $loop_day <= $last_day ; $loop_day++)
		{
		?>
    <tr>
      <th class="cellLink"><a href="/dive_center/reserve/day.php?s_yyyymmdd=<? echo $s_yyyymm.substr(($loop_day+100), 1, 2);?>"><? echo intval(substr($s_yyyymm, 4, 2));?>月<? echo $loop_day;?>日（<? echo $common_connect -> Fn_date_day($s_yyyymm.substr(($loop_day+100), 1, 2));?>）</a></th>
      <? foreach($arr_dive_center_style_title as $key=>$value) { ?>
      <td>
				<?
					$stock = $arr_reserve_style[$key][substr(($loop_day+100), 1, 2)];
					if($stock!="") { echo $stock."人";}
					$arr_reserve_style_sum[$key] += $stock;
				?>
      </td>
      <? } ?>
      <? foreach($arr_dive_center_boat_title as $key=>$value) { ?>
      <td>
				<?
					$stock = $arr_reserve_boat[$key][substr(($loop_day+100), 1, 2)];
					if($stock!="") { echo $stock."人";}
					$arr_reserve_boat_sum[$key] += $stock;
				?>
      </td>
      <? } ?>
      <? foreach($arr_dive_center_tank_title as $key=>$value) { ?>
      <td>
				<?
					$stock = $arr_reserve_tank[$key][substr(($loop_day+100), 1, 2)];
					if($stock!="") { echo $stock."本";}
					$arr_reserve_tank_sum[$key] += $stock;
				?>
      </td>
      <? } ?>
    </tr>
    <?
		}
    ?>
    <tr class="sum">
      <th>合計</th>
      <? foreach($arr_dive_center_style_title as $key=>$value) { ?>
      <td><? echo $arr_reserve_style_sum[$key];?>人</td>
      <? } ?>
      <? foreach($arr_dive_center_boat_title as $key=>$value) { ?>
      <td><? echo $arr_reserve_boat_sum[$key];?>人</td>
      <? } ?>
      <? foreach($arr_dive_center_tank_title as $key=>$value) { ?>
      <td><? echo $arr_reserve_tank_sum[$key];?>本</td>
      <? } ?>
    </tr>
  </table>
  </div>
</section>


</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/footer.php'); ?>