<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	$dive_center_id = $_SESSION['dive_center_id'];
	
	$db_result = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	if($db_result)
	{
		$dive_center_name = $db_result[0]["dive_center_name"];
	}
	
  $header_centername = $dive_center_name;
	
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/header.php'); ?>


<?
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	
	//javascript 削除 fnReserveDel(i)
	$common_connect->Fn_javascript_delete_reserve("一斉中止メールを送信しますか？", "./day_cancel.php?s_yyyymmdd=".$s_yyyymmdd);
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	//ステータスをArrayへ
	$sql = "SELECT cate_status_id, cate_status_title FROM cate_status order by view_level";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_status_title[$db_result[$db_loop][cate_status_id]] = $db_result[$db_loop][cate_status_title];
		}
	}
	
	
	//スタイル
	$arr_db_field_style = array("dive_center_style_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price", "tank_count");
	$arr_db_field_style = array_merge($arr_db_field_style, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_style_id, ";
	foreach($arr_db_field_style as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	$db_result_style = $common_dao->db_query($sql);
	if($db_result_style)
	{
		for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
		{
			$arr_dive_center_style_title[$db_result_style[$db_loop][dive_center_style_id]] = $db_result_style[$db_loop][dive_center_style_title]."（".number_format($db_result_style[$db_loop][dive_center_style_price])."円）";
		}
	}
	
	
	//ボート
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
	$arr_db_field_boat = array_merge($arr_db_field_boat, array("view_level", "flag_open", "regi_date", "up_date"));
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	$db_result_boat = $common_dao->db_query($sql);
	if($db_result_boat)
	{
		for($db_loop=0 ; $db_loop < count($db_result_boat) ; $db_loop++)
		{
			$arr_dive_center_boat_title[$db_result_boat[$db_loop][dive_center_boat_id]] = $db_result_boat[$db_loop][dive_center_boat_title]."（".number_format($db_result_boat[$db_loop][dive_center_boat_price])."円）";
		}
	}
	
	//タンク
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field_tank = array_merge($arr_db_field_tank, array("view_level", "flag_open", "regi_date", "up_date"));
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	$db_result_tank = $common_dao->db_query($sql);
	if($db_result_tank)
	{
		for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
		{
			$arr_dive_center_tank_title[$db_result_tank[$db_loop][dive_center_tank_id]] = $db_result_tank[$db_loop][dive_center_tank_title]."（".number_format($db_result_tank[$db_loop][dive_center_tank_price])."円）";
		}
	}
	
	
	
	$s_yyyy = substr($s_yyyymmdd, 0, 4);
	$s_mm = substr($s_yyyymmdd, 4, 2);
	$s_dd = substr($s_yyyymmdd, 6, 2);
	
	$sql = "SELECT '".$s_yyyy."-".$s_mm."-".$s_dd."' + INTERVAL -1 DAY as yyyymmdd ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$pre_yyyymmdd = str_replace("-", "", $db_result[0]["yyyymmdd"]);
	}
	
	$sql = "SELECT '".$s_yyyy."-".$s_mm."-".$s_dd."' + INTERVAL 1 DAY as yyyymmdd ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$next_yyyymmdd = str_replace("-", "", $db_result[0]["yyyymmdd"]);
	}
?>
<article>
  <ul class="pankuzu">
    <li><a href="/dive_center/reserve/?s_yyyymm=<? echo $s_yyyy.$s_mm;?>">予約リスト（<? echo $s_yyyy."年".$s_mm."月";?>）</a></li>
    <li>&gt;予約リスト（<? echo $s_mm ;?>月<? echo $s_dd ;?>日）</li>
  </ul>
  
  <?
  if($dive_reserve_id=="")
	{
	?>
  <div class="headBtn">
    <p><a href="#" onClick='fnReserveDel("<? echo $s_yyyymmdd;?>");'>一斉中止メール</a></p>
    <p>予約中のお客さまに開催中止の一斉メールを送信できます</p>
  </div>
  <?
	}
	?>
	
  <?
  if($dive_reserve_id!="")
  {
		$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id", "member_name_1", "member_name_2");
		$arr_db_field = array_merge($arr_db_field, array("dive_center_name", "reserve_style_id", "reserve_style_name"));
		$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
		$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
		$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
		$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
		$arr_db_field = array_merge($arr_db_field, array("used_point", "add_point"));
		$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price", "reserve_comment"));
		$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));
	
		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " up_date FROM dive_reserve where dive_center_id='".$dive_center_id."' and dive_reserve_id='".$dive_reserve_id."' ";
				
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
  ?>
  <section class="sectionBox">
    <p class="tit">予約編集</p>
      <form action="./day_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
				<?php $var = "member_id";?>
        <input name="<? echo $var;?>" type="hidden" value="<?php echo $$var;?>" />
        <table class="editTable">
          <tr>
            <th>予約ID</th>
            <td>
            	<?php $var = "dive_reserve_id";?>
              <?php if($$var=="") {echo "自動生成";} else { echo $$var;}?>
              <input name="<? echo $var;?>" type="hidden" value="<?php echo $$var;?>" />
            </td>
          </tr>
          <tr>
            <th>申込者</th>
            <td>
            	<?php $var = "member_id";?>
              <input type="hidden" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" />
              <?php $var = "member_name_1";?>
              <?php echo $$var;?>
              <?php $var = "member_name_2";?>
              <?php echo $$var;?>
            </td>
          </tr>
          <tr>
            <th>予約日</th>
            <td>
            		<? $var = "yyyymmdd";?>
                <?php echo $$var;?>
              <input type="hidden" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>スタイル</th>
            <td>
            		<? $var = "reserve_style_id";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" />
              	<option value="">---</option>
              <?
							foreach($arr_dive_center_style_title as $key=>$value)
							{
							?>
              	<option value="<?php echo $key;?>" <? if($$var==$key){ echo " selected ";}?>><?php echo $value;?></option>
              <?
							}
              ?>
              </select>
              <? echo $reserve_style_name;?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>エントリー</th>
            <td>
            		<? $var = "reserve_boat_id";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" />
              	<option value="">---</option>
              <?
							foreach($arr_dive_center_boat_title as $key=>$value)
							{
							?>
              	<option value="<?php echo $key;?>" <? if($$var==$key){ echo " selected ";}?>><?php echo $value;?></option>
              <?
							}
              ?>
              </select>
              <? echo $reserve_boat_name;?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>タンク1</th>
            <td>
            		<? $var = "reserve_1_tank_id";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" />
              	<option value="">---</option>
              <?
							foreach($arr_dive_center_tank_title as $key=>$value)
							{
							?>
              	<option value="<?php echo $key;?>" <? if($reserve_1_tank_id==$key){ echo " selected ";}?>><?php echo $value;?></option>
              <?
							}
              ?>
              </select>
              <? echo $reserve_1_tank_name;?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>タンク2</th>
            <td>
            		<? $var = "reserve_2_tank_id";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" />
              	<option value="">---</option>
              <?
							foreach($arr_dive_center_tank_title as $key=>$value)
							{
							?>
              	<option value="<?php echo $key;?>" <? if($reserve_2_tank_id==$key){ echo " selected ";}?>><?php echo $value;?></option>
              <?
							}
              ?>
              </select>
              <? echo $reserve_2_tank_name;?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>タンク3</th>
            <td>
            		<? $var = "reserve_3_tank_id";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" />
              	<option value="">---</option>
              <?
							foreach($arr_dive_center_tank_title as $key=>$value)
							{
							?>
              	<option value="<?php echo $key;?>" <? if($reserve_3_tank_id==$key){ echo " selected ";}?>><?php echo $value;?></option>
              <?
							}
              ?>
              </select>
              <? echo $reserve_3_tank_name;?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>その他金額</th>
            <td>
              <?php $var = "etc_price";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<? echo $$var;?>" />
              <? echo number_format($$var);?>円
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>使用ポイント</th>
            <td>
              <?php $var = "used_point";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<? echo $$var;?>" />
              <? echo number_format($$var);?>pt
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>小計</th>
            <td>
              <? echo number_format(($reserve_all_price)-$reserve_tax);?>円
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>消費税</th>
            <td>
              <?php $var = "reserve_tax";?>
              <? echo number_format($$var);?>円
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>合計</th>
            <td>
              <?php $var = "reserve_all_price";?>
              <span style="color:#F00; font-size:16px;"><? echo number_format($$var);?></span>円
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>加算ポイント</th>
            <td>
              <?php $var = "orignal_add_point";?>
              <input type="hidden" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<? echo $$var;?>" />
              <?php $var = "add_point";?>
              <? echo number_format($$var);?>pt
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>レンタルやその他リクエスト</th>
            <td>
              <?php $var = "reserve_comment";?>
              <textarea name="<?php echo $var;?>" id="<?php echo $var;?>"><? echo $$var;?></textarea>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>実施状況</th>
            <td>
              <?php $var = "status";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" />
              	<option value="">---</option>
              <?
							foreach($arr_cate_status_title as $key=>$value)
							{
							?>
              	<option value="<?php echo $key;?>" <? if($$var==$key){ echo " selected ";}?>><?php echo $value;?></option>
              <?
							}
              ?>
              </select>
              <? echo $arr_cate_status_title[$$var];?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>登録日</th>
            <td>
              <?php $var = "regi_date";?>
              <? echo $$var;?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <td colspan="2" class="tCenter btn"><input type="submit" value="保存する" id="form_confirm"></td>
          </tr>
        </table>
    </form>
  </section>
  <?
	}
  ?>


  <section class="sectionBox mb0">
    <p class="tit">
      <span class="prev">&lt; <a href="?s_yyyymmdd=<? echo $pre_yyyymmdd;?>">前日</a></span>予約リスト（<? echo $s_mm ;?>月<? echo $s_dd ;?>日）<span class="next"><a href="?s_yyyymmdd=<? echo $next_yyyymmdd;?>">翌日</a> &gt;</span>
    </p>
    <div class="box">
      <table class="listTable">
        <tr>
          <th width="150">申込者</th>
          <th>スキル</th>
          <th>メディカル</th>
          <th>バディ</th>
          <th>スタイル</th>
          <th>エントリー</th>
          <th>タンク</th>
          <th>料金</th>
          <th>実施状況</th>
          <th width="50">編集</th>
        </tr>
<?
	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id", "member_name_1", "member_name_2");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_name", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where dive_center_id='".$dive_center_id."' and yyyymmdd='".$s_yyyy."-".$s_mm."-".$s_dd."' ";
	//$sql .= " and flag_open=1 ";//管理ページは見れる
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
?>
        <tr>
          <th class="cellLink"><a href="/dive_center/member/member_detail.php?s_yyyymmdd=<? echo $s_yyyymmdd;?>&dive_reserve_id=<? echo $dive_reserve_id;?>"><? echo $member_name_1." ".$member_name_2;?></a></th>
          <?
						$skill_id = $common_member->Fn_member_skill($common_dao, $member_id);
          ?>
          <td <? if($skill_id!="1") { echo " class='error' ";}?>>
					<?
						if($skill_id=="1") { echo "できる";}
						elseif($skill_id=="2") { echo "できない";}
					?>
          </td>
          <?
						$medical_id = $common_member->Fn_member_medical($common_dao, $member_id);
          ?>
          <td <? if($medical_id!="1") { echo " class='error' ";}?>>
					<?
						if($medical_id=="1") { echo "該当なし";}
						elseif($medical_id=="2") { echo "要診断書";}
					?>
          </td>
          <td>
          	<?
            $arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id) ;
						if($arr_buddy)
						{
							for($db_loop_buddy=0 ; $db_loop_buddy < count($arr_buddy["member_id"]) ; $db_loop_buddy++)
							{
								//申込者は表示させない
								if($member_id!=$arr_buddy["member_id"][$db_loop_buddy])
								{
									echo $arr_buddy["member_name_1"][$db_loop_buddy]." ".$arr_buddy["member_name_2"][$db_loop_buddy]."<br />";
								}
							}
						}
						?>
          </td>
          <td>
						<? echo $reserve_style_name;?>
          </td>
          <td>
						<? echo $reserve_boat_name;?>
          </td>
          <td>
						<?
						if($reserve_1_tank_id!="0")
						{
							echo $reserve_1_tank_name;
						}
						if($reserve_2_tank_id!="0")
						{
							echo "<br />".$reserve_2_tank_name;
						}
						if($reserve_3_tank_id!="0")
						{
							echo "<br />".$reserve_3_tank_name;
						}
						?>
          </td>
          <td><? echo number_format($reserve_all_price);?>円</td>
          <td>
						<?
						if($status==3 || $status==4)
						{
							echo "<span style='color:#00F;'>".$arr_cate_status_title[$status]."</span>";
						}
						else
						{
							echo "<span style='color:#F00;'>".$arr_cate_status_title[$status]."</span>";
						}
						?>
          </td>
          <td><a href="?s_yyyymmdd=<? echo $s_yyyymmdd;?>&dive_reserve_id=<? echo $dive_reserve_id;?>">編集</a></td>
        </tr>
<?
		}
	}
?>
        
      </table>
    </div>
  </section>
  <p class="mt5 mb50">※上から古い順番で掲載しています。<br>
  ※予約がキャンセルになった場合でも削除されることはありません。<br>
  ※同じ方の予約が重複した場合は新しい（下位）の予約を優先して、不自然な場合はお客さまにご確認いただけますようお願いします。</p>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/footer.php'); ?>