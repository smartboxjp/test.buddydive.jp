<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>
<?

	if ($dive_center_email == "" or $dive_center_login_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	else
	{
		$sql = "select dive_center_name, dive_center_id, dive_center_email from dive_center where dive_center_email ='".$dive_center_email."' and dive_center_login_pw='".$dive_center_login_pw."' ";

		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$db_dive_center_id = $db_result[0]["dive_center_id"];
			$db_dive_center_email = $db_result[0]["dive_center_email"];
			$db_dive_center_name = $db_result[0]["dive_center_name"];
		}
		else
		{
			$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
		}
		
		if ($db_dive_center_email == $dive_center_email)
		{
			//管理者の場合
			session_start();
			$_SESSION['dive_center_id']=$db_dive_center_id;
			$_SESSION['dive_center_name']=$db_dive_center_name;
						
			if($db_dive_center_id!="")
			{
				$login_cookie = sha1($db_dive_center_email."_".$dive_center_login_pw);
				$db_insert = "update dive_center set login_cookie='".$login_cookie."' ";
				$db_insert .= " where dive_center_id='".$db_dive_center_id."'";
				$db_result = $common_dao->db_update($db_insert);
		
				setcookie("account", $login_cookie, time()+3600*24*31);//暗号化してクッキーに保存, 31日間
			}
			
			$common_connect->Fn_redirect(global_ssl."/dive_center/dashboard/");

		}
	}
?>
</body>
</html>