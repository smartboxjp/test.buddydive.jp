<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	$dive_center_id = $_SESSION['dive_center_id'];
	
	$db_result = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	if($db_result)
	{
		$dive_center_name = $db_result[0]["dive_center_name"];
	}
	
  $header_centername = $dive_center_name;
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/header.php'); ?>
<script language="javascript"> 
	function fnDel(i) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = '/dive_center/wave_height/wave_height_del.php?wave_height_id='+i;
		} 
	}
</script>

<?
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	
	/* スタイル start */
	$arr_db_field = array("dive_center_style_id", "dive_center_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price");
		
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where dive_center_id='".$dive_center_id."'";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_style_title[$dive_center_style_id] = $dive_center_style_title;
			$arr_dive_center_style_price[$dive_center_style_id] = $dive_center_style_price;
		}
	}
	/* スタイル end */
	
	
	/* ボート start */
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
		
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where dive_center_id='".$dive_center_id."'";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field_boat as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_boat_title[$dive_center_boat_id] = $dive_center_boat_title;
			$arr_dive_center_boat_price[$dive_center_boat_id] = $dive_center_boat_price;
		}
	}
	/* ボート end */
	
	/* タンク start */
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
		
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field_tank as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_tank_title[$dive_center_tank_id] = $dive_center_tank_title;
			$arr_dive_center_tank_price[$dive_center_tank_id] = $dive_center_tank_price;
		}
	}
	/* タンク end */
	

?>
<article>

<section class="sectionBox">
<p class="tit">本日の海況</p>
<div class="box">
  <table class="listTable">
    <tr>
      <th>日付</th>
      <th>時間</th>
      <th>開催状況</th>
      <th>コメント（140文字以内）</th>
      <th width="50">登録</th>
    </tr>
    
    <form action="/dive_center/wave_height/wave_height_save.php" name="form_write" method="post" >
    <tr>
      <td><? echo date("m/d");?></td>
      <td><? echo date("H:i");?></td>
      <td>
      		<?
					$var = "flag_open";
					?>
        <select name="<? echo $var;?>" id="<? echo $var;?>">
          <? foreach($arr_flag_open as $key=>$value) { ?>
          <option value="<?=$key;?>" <? if($$var==$key) { echo " selected ";}?>><?=$value;?></option>
          <? } ?>
        </select>
      </td>
      <td>
      		<? $var = "wave_height";?>
          <textarea rows="3" name="<? echo $var;?>" id="<? echo $var;?>" maxlength="140"><? echo $$var;?></textarea>
      </td>
      <td><button type="submit" class="miniBtn">登録</button></td>
    </tr>
    </form>
    
<?
	$arr_db_field = array("dive_center_wave_height_id", "dive_center_id", "wave_height");
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	//リスト表示
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_wave_height" ;
	$sql .= " where left(regi_date, 10)='".date("Y-m-d")."' and dive_center_id='".$dive_center_id."' " ;
	$sql .= " order by dive_center_wave_height_id desc";
	$sql .= " limit 0, 5";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
		for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
?>
    <tr>
      <td><? echo str_replace("-", "/", substr($regi_date, 0, 10));?></td>
      <td><? echo substr($regi_date, 11, 5);?></td>
      <td><? echo $arr_flag_open[$flag_open];?></td>
      <td><? echo $wave_height;?></td>
      <td><a href="#" onClick='fnDel("<?php echo $dive_center_wave_height_id;?>");'>削除</a></td>
    </tr>
<?
		}
	}
?>
  </table>
</section>

<?
	$sum_etc_price = 0;
	$sum_used_point = 0;
	$sum_reserve_tax = 0;
	$sum_reserve_all_price = 0;
	
	//予約データ
	$sql = "SELECT dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
	$sql .= " ,etc_price, used_point, reserve_tax, reserve_all_price ";
	$sql .= " FROM dive_reserve where dive_center_id='".$dive_center_id."' and yyyymmdd='".date("Y-m-d")."' ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_status[$db_result[$db_loop]["status"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];

		}
	}

?>
<section class="sectionBox">
  <p class="tit">本日の実施状況</p>
  <div class="box">
    <table class="listTable">
      <tr>
        <th width="20%">予約中</th>
        <th width="20%">受付済み</th>
        <th width="20%">精算済み</th>
        <th width="20%">中止</th>
        <th>不参加</th>
      </tr>
      <tr>
        <td class="error">
        <? echo number_format(count($arr_status["0"])); ?>人
        </td>
        <td>
        <? echo number_format(count($arr_status["1"])); ?>人
        </td>
        <td>
        <? echo number_format(count($arr_status["2"])); ?>人
        </td>
        <td>
        <? echo number_format(count($arr_status["99"])); ?>人
        </td>
        <td>
        <? echo number_format(count($arr_status["100"])); ?>人
        </td>
      </tr>
      <tr>
        <td colspan="5" align="center"><a href="/dive_center/reserve/day.php?s_yyyymmdd=<? echo date("Ymd");?>">本日の予約詳細 &gt;</a></td>
      </tr>
    </table>
  </div>
</section>


<?
	$sum_etc_price = 0;
	$sum_used_point = 0;
	$sum_reserve_tax = 0;
	$sum_reserve_all_price = 0;
	
	//予約データ
	$sql = "SELECT dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
	$sql .= " ,etc_price, used_point, reserve_tax, reserve_all_price ";
	$sql .= " FROM dive_reserve where dive_center_id='".$dive_center_id."' and yyyymmdd='".date("Y-m-d")."' ";
	$sql .= " and status<90 ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_reserve_style_id[$db_result[$db_loop]["reserve_style_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_boat_id[$db_result[$db_loop]["reserve_boat_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_tank_id[$db_result[$db_loop]["reserve_1_tank_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_tank_id[$db_result[$db_loop]["reserve_2_tank_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_tank_id[$db_result[$db_loop]["reserve_3_tank_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			
			if($db_result[$db_loop]["reserve_1_tank_id"]!="0")
			{
				$arr_reserve_tank_count[$db_result[$db_loop]["reserve_1_tank_id"]] ++;
			}
			if($db_result[$db_loop]["reserve_2_tank_id"]!="0")
			{
				$arr_reserve_tank_count[$db_result[$db_loop]["reserve_2_tank_id"]] ++;
			}
			if($db_result[$db_loop]["reserve_3_tank_id"]!="0")
			{
				$arr_reserve_tank_count[$db_result[$db_loop]["reserve_3_tank_id"]] ++;
			}
			
			$sum_etc_price += $db_result[$db_loop]["etc_price"];
			$sum_used_point += $db_result[$db_loop]["used_point"];
			$sum_reserve_tax += $db_result[$db_loop]["reserve_tax"];
			$sum_reserve_all_price += $db_result[$db_loop]["reserve_all_price"];
		}
	}
?>
<section class="sectionBox">
  <p class="tit">本日の売上</p>
  <div class="box">
    <table class="listTable">
      <tr>
        <th colspan="2">項目</th>
        <th width="15%">単価</th>
        <th width="15%">数量</th>
        <th width="15%">金額</th>
      </tr>
<?
	//スタイル
	$db_result_style = $common_center->Fn_dive_center_style ($common_dao, $dive_center_id) ;
	if($db_result_style)
	{
		for($db_loop_style=0 ; $db_loop_style < count($db_result_style) ; $db_loop_style++)
		{
			$dive_center_style_id = $db_result_style[$db_loop_style]["dive_center_style_id"];
			$arr_dive_center_style_id[$db_loop_style] = $dive_center_style_id;
			$arr_dive_center_style_title[$dive_center_style_id] = $db_result_style[$db_loop_style]["dive_center_style_title"];
			$arr_dive_center_style_stock[$dive_center_style_id] = $db_result_style[$db_loop_style]["dive_center_style_stock"];
			$arr_dive_center_style_price[$dive_center_style_id] = $db_result_style[$db_loop_style]["dive_center_style_price"];
		}
	}
?>
      <tr>
        <td rowspan="<? echo count($arr_dive_center_style_title);?>">スタイル</td>
        <? $loop = 0;?>
        <td><? echo $arr_dive_center_style_title[$arr_dive_center_style_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($arr_dive_center_style_price[$arr_dive_center_style_id[$loop]]);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($arr_dive_center_style_price[$arr_dive_center_style_id[$loop]]*count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?></td>
      </tr>
      <?
      for($loop = 1 ; $loop<count($arr_dive_center_style_id) ; $loop++)
			{
				$dive_center_style_price = $arr_dive_center_style_price[$arr_dive_center_style_id[$loop]];
			?>
      <tr>
        <td><? echo $arr_dive_center_style_title[$arr_dive_center_style_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_style_price);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($dive_center_style_price*count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?></td>
      </tr>
      <?
			}
			?>
      
      
      
<?
	//エントリー
	$db_result_boat = $common_center->Fn_dive_center_boat ($common_dao, $dive_center_id) ;
	if($db_result_boat)
	{
		for($db_loop_boat=0 ; $db_loop_boat < count($db_result_boat) ; $db_loop_boat++)
		{
			$dive_center_boat_id = $db_result_boat[$db_loop_boat]["dive_center_boat_id"];
			$arr_dive_center_boat_id[$db_loop_boat] = $dive_center_boat_id;
			$arr_dive_center_boat_title[$dive_center_boat_id] = $db_result_boat[$db_loop_boat]["dive_center_boat_title"];
			$arr_dive_center_boat_stock[$dive_center_boat_id] = $db_result_boat[$db_loop_boat]["dive_center_boat_stock"];
			$arr_dive_center_boat_price[$dive_center_boat_id] = $db_result_boat[$db_loop_boat]["dive_center_boat_price"];
		}
	}
?>
      <tr>
        <td rowspan="<? echo count($arr_dive_center_boat_title);?>">エントリー</td>
        <? $loop = 0;?>
        <td><? echo $arr_dive_center_boat_title[$arr_dive_center_boat_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_boat_price);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($dive_center_boat_price*count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?></td>
      </tr>
      <?
      for($loop = 1 ; $loop<count($arr_dive_center_boat_id) ; $loop++)
			{
				$dive_center_boat_price = $arr_dive_center_boat_price[$arr_dive_center_boat_id[$loop]];
			?>
      <tr>
        <td><? echo $arr_dive_center_boat_title[$arr_dive_center_boat_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_boat_price);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($dive_center_boat_price*count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?></td>
      </tr>
      <?
			}
			?>
      
      
      

<?
	//タンク
	$db_result_tank = $common_center->Fn_dive_center_tank ($common_dao, $dive_center_id) ;
	if($db_result_tank)
	{
		for($db_loop_tank=0 ; $db_loop_tank < count($db_result_tank) ; $db_loop_tank++)
		{
			$dive_center_tank_id = $db_result_tank[$db_loop_tank]["dive_center_tank_id"];
			$arr_dive_center_tank_id[$db_loop_tank] = $dive_center_tank_id;
			$arr_dive_center_tank_title[$dive_center_tank_id] = $db_result_tank[$db_loop_tank]["dive_center_tank_title"];
			$arr_dive_center_tank_stock[$dive_center_tank_id] = $db_result_tank[$db_loop_tank]["dive_center_tank_stock"];
			$arr_dive_center_tank_price[$dive_center_tank_id] = $db_result_tank[$db_loop_tank]["dive_center_tank_price"];
		}
	}
?>
      <tr>
        <td rowspan="<? echo count($arr_dive_center_tank_title);?>">タンク</td>
        <?
					$loop = 0;
					$dive_center_tank_price = $arr_dive_center_tank_price[$arr_dive_center_tank_id[$loop]];
				?>
        <td><? echo $arr_dive_center_tank_title[$arr_dive_center_tank_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_tank_price);?></td>
        <td align="right"><? echo number_format(($arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]));?>本</td>
        <td align="right">¥<? echo number_format($dive_center_tank_price*$arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]);?></td>
      </tr>
      <?
      for($loop = 1 ; $loop<count($arr_dive_center_tank_id) ; $loop++)
			{
				$dive_center_tank_price = $arr_dive_center_tank_price[$arr_dive_center_tank_id[$loop]];
			?>
      <tr>
        <td><? echo $arr_dive_center_tank_title[$arr_dive_center_tank_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_tank_price);?></td>
        <td align="right"><? echo number_format($arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]);?>本</td>
        <td align="right">¥<? echo number_format($dive_center_tank_price*$arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]);?></td>
      </tr>
      <?
			}
			?>
      <tr class="sum">
        <td colspan="4" align="center">使用されたポイント</td>
        <td align="right">¥<? echo number_format($sum_used_point);?></td>
      </tr>
      <tr class="sum">
        <td colspan="4" align="center">その他金額</td>
        <td align="right">¥<? echo number_format($sum_etc_price);?></td>
      </tr>
      <tr class="sum">
        <td colspan="4" align="center">小計</td>
        <td align="right">¥<? echo number_format($sum_reserve_all_price-$sum_reserve_tax);?></td>
      </tr>
      <tr>
        <td colspan="4" align="center">消費税</td>
        <td align="right">¥<? echo number_format($sum_reserve_tax);?></td>
      </tr>
      <tr>
        <td colspan="4" align="center">合計</td>
        <td align="right">¥<? echo number_format($sum_reserve_all_price);?></td>
      </tr>
    </table>
  </div>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/footer.php'); ?>