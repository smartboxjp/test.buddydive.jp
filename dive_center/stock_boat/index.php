<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
  
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	$dive_center_id = $_SESSION['dive_center_id'];
	
	$db_result = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	if($db_result)
	{
		$dive_center_name = $db_result[0]["dive_center_name"];
	}
	
  $header_centername = $dive_center_name;
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/header.php'); ?>

<script language="javascript"> 
	function fnUp(yyyymm, lastday, buddy_guide) {
		var s;
		var d;
		
		for (var q=1;q<=lastday;q++) 
		{
			s = new String(q+100);
			d = s.slice(1);
			document.getElementsByName("s_"+buddy_guide+"_"+yyyymm+d)[0].value = document.getElementsByName("num_"+buddy_guide+"_"+yyyymm)[0].value;
			
		}
	} 
</script>

<?
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	/* ダイブセンターに登録されている情報 start */
	$arr_db_field = array("dive_center_boat_id", "dive_center_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where dive_center_id='".$dive_center_id."'";
	//$sql .= " and flag_open=1 ";//管理ページは見れる
	$sql .= " order by view_level ";
	echo $sql;
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_boat_title[$dive_center_boat_id] = $dive_center_boat_title;
			$arr_dive_center_boat_stock[$dive_center_boat_id] = $dive_center_boat_stock;
			$arr_dive_center_boat_price[$dive_center_boat_id] = $dive_center_boat_price;
		}
	}
	/* ダイブセンターに登録されている情報 end */
?>
<article>

<section class="sectionBox">
  <p class="tit">デフォルト在庫</p>
  <div class="box">
    <table class="listTable">
      <tr>
        <th>項目</th>
        <th>金額</th>
        <th>在庫</th>
      </tr>
      <?
      foreach($arr_dive_center_boat_title as $key => $value)
			{
			?>
      <tr>
        <th><? echo $value;?></th>
        <td><? echo number_format($arr_dive_center_boat_price[$key]);?></td>
        <td><? echo number_format($arr_dive_center_boat_stock[$key]);?></td>
      </tr>
      <?
			}
			?>
    </table>
  </div>
</section>



	<?
    $max_month = 3;
		
		if(strlen($yyyymm)==$max_month)
		{
			$yyyymmdd = date($yyyymm."01");
		}
		else
		{
			$yyyymmdd = date("Y-m-01");
		}
		
		for ($i=0 ; $i < $max_month ; $i++)
		 {
			
			$sql = "SELECT '".$yyyymmdd."' + INTERVAL $i MONTH as db_yyyymmdd";
			
			$db_result = $common_dao->db_query($sql);

			
			$yyyy = date("Y", strtotime($db_result[0]["db_yyyymmdd"]));
			$mm = date("m", strtotime($db_result[0]["db_yyyymmdd"]));
			
			//最後の日
			for ($ld = 28; checkdate($mm,$ld,$yyyy); $ld ++); 
			$lastDay = $ld - 1;
			
  ?>
  
<section class="sectionBox">
  <form action="stock_boat_save.php" name="form_write" method="post" boat="margin-top:0px;">
    <input type="hidden" name="yyyymm" value="<?=$yyyy.$mm;?>"  />
    
    <p class="tit"><? echo $yyyy;?>年<? echo $mm	;?>月　<input type="submit" value="変更を保存">
      <br>
      <span>※変更後は必ず保存ボタンを押してください</span>
    </p>
    <?
    
        //DBからarrayへ	
        foreach($arr_dive_center_boat_title as $key => $value)
        {
          $sql = "select * from stock_boat where yyyymm = '".$yyyy.$mm."' and dive_center_id='".$dive_center_id."' and dive_center_boat_id='".$key."'";
          $db_result = $common_dao->db_query($sql);
          if($db_result)
          {
            for($schDay = 1; $schDay <= $lastDay; $schDay ++) {
              $stock_boat[$db_result[0]["yyyymm"]][$db_result[0]["dive_center_boat_id"]][substr(($schDay+100),1,2)] = $db_result[0][s_.substr(($schDay+100),1,2)];
            }
          }
          else
          {
						$defalut_stock = $arr_dive_center_boat_stock[$key];
						
            //DBをチェックして無ければ生成
            $dbup = "insert into stock_boat (yyyymm, dive_center_boat_id, dive_center_id ";
            for($loop=1 ; $loop<=$lastDay ; $loop++)
            {
              $dbup .= " ,s_".substr($loop+100, 1);
            }
            $dbup .= ") values ('".$yyyy.$mm."', '".$key."', '".$dive_center_id."' ";
            for($loop=1 ; $loop<=$lastDay ; $loop++)
            {
              $dbup .= ", '".$defalut_stock."' ";
            }
            $dbup .= ")";
  
            $db_result = $common_dao->db_update($dbup);
            
            //default
            for($schDay = 1; $schDay <= $lastDay; $schDay ++) {
              $stock_boat[$yyyy.$mm][$value][substr(($schDay+100),1,2)] = 1;
            }
          }
        }
        
    ?>
  <div class="box">
    <table class="listTable">
      <tr>
        <th>日付</th>
				<?
        foreach($arr_dive_center_boat_title as $key => $value)
        {
        ?>
        <th><? echo $value;?>の在庫<br>
          <select name="<? echo "num_".$key."_".$yyyy.$mm;?>">
						<? for($loop=0 ; $loop<100 ; $loop++) { ?>
            <option value="<?=$loop;?>"><?=$loop;?></option>
            <? } ?>
            <? $loop = 999;?>
            <option value="<?=$loop;?>"><?=$loop;?></option>
          </select>　
          <input type="button" onClick="fnUp(<?=$yyyy.$mm;?>, <?=$lastDay;?>, <?=$key;?>)" value="一括変更" />
        </th>
        <?
				}
				?>
      </tr>
      <?
			$arr_sum[$yyyy.$mm][$key] = 0;
      for($schDay = 1; $schDay <= $lastDay; $schDay ++) 
			{
				$checkday = mktime(0, 0, 0, $mm, $dd, $yyyy); 
				$week = date("w", $checkday);
				$dd = substr(($schDay+100), 1);
			?>
      <tr <? if($week==5 || $week==6) { echo " class=\"holiday\" "; }?>>
        <th><? echo $mm;?>月<? echo $dd;?>日（<? echo $common_connect->Fn_date_day($yyyy.$mm.$dd); ?>）</th>
				<?
        foreach($arr_dive_center_boat_title as $key => $value)
        {
        ?>
        <td>
          <select name="<? echo "s_".$key."_".$yyyy.$mm.$dd;?>" id="<? echo "s_".$key."_".$yyyy.$mm.$dd;?>">
            <? for($loop=0 ; $loop<100 ; $loop++) { ?>
            <option value="<?=$loop;?>" <? if($stock_boat[$yyyy.$mm][$key][$dd]==$loop) { echo " selected ";}?>><?=$loop;?></option>
            <? } ?>
            <?
						if($stock_boat[$yyyy.$mm][$key][$dd]>=100 &&  $stock_boat[$yyyy.$mm][$key][$dd]!=999)
						{
							$loop = $stock_boat[$yyyy.$mm][$key][$dd];
						?>
            <option value="<?=$loop;?>" <? if($stock_boat[$yyyy.$mm][$key][$dd]==$loop) { echo " selected ";}?>><?=$loop;?></option>
						<?
						}
						?>
            <? $loop = 999;?>
            <option value="<?=$loop;?>" <? if($stock_boat[$yyyy.$mm][$key][$dd]==$loop) { echo " selected ";}?>><?=$loop;?></option>
          </select>人
        </td>
        <?
					$arr_sum[$yyyy.$mm][$key] += $stock_boat[$yyyy.$mm][$key][$dd];
				}
				?>
      </tr>
      <?
			}
			?>
        <tr class="sum">
        <th>合計</th>
				<?
        foreach($arr_dive_center_boat_title as $key => $value)
        {
        ?>
        <td><? echo number_format($arr_sum[$yyyy.$mm][$key])?>人</td>
				<?
				}
        ?>
      </tr>
    </table>
	</div>
  </form>
</section>
  <br />
<?
		 }
?>

  



</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/footer.php'); ?>