<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>在庫修正</title>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	$dive_center_id = $_SESSION['dive_center_id'];
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	
	/* ダイブセンターに登録されている情報 start */
	$arr_db_field = array("dive_center_tank_id", "dive_center_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where dive_center_id='".$dive_center_id."'";
	//$sql .= " and flag_open=1 ";//管理ページは見れる
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_tank_title[$dive_center_tank_id] = $dive_center_tank_title;
			$arr_dive_center_tank_stock[$dive_center_tank_id] = $dive_center_tank_stock;
			$arr_dive_center_tank_price[$dive_center_tank_id] = $dive_center_tank_price;
		}
	}
	/* ダイブセンターに登録されている情報 end */
	
	$yyyy = substr($yyyymm, 0, 4);
	$mm = substr($yyyymm, 4, 2);

	//最初の曜日
	$firstTime = strtotime($yyyy . "-" . $mm . "-01"); 
	$firstWeek = date("w", $firstTime); 

	//最後の日
	for ($ld = 28; checkdate($mm,$ld,$yyyy); $ld++); 
	$lastDay = $ld - 1; 
	
	$db_field = "";
	$db_data = "";
	$db_field_up = "";
		

	foreach($arr_dive_center_tank_title as $key => $value)
	{
		$db_field = "";
		$db_field_up = "";
		for($schDay = 1; $schDay <= $lastDay; $schDay ++) 
		{
			$temp_day = $yyyy.$mm.substr(($schDay+100),1,2);
			$dd = substr(($schDay+100),1,2);
			
			$db_field .= ", "."s_$dd";
			$db_data .= ", '".trim($_POST["s_".$key."_".$temp_day])."'";
			
			$db_field_up .= " , s_$dd='".trim($_POST["s_".$key."_".$temp_day])."'";
		}

		//割当の予約ＩＤがあるため削除禁止
		$sql_count="select count(yyyymm) as all_count FROM stock_tank where yyyymm = '".$yyyy.$mm."' and dive_center_id='".$dive_center_id."' and dive_center_tank_id='".$key."'";

		$db_result_count = $common_dao->db_query($sql_count);
		if($db_result_count)
		{
			$all_count = $db_result_count[0]["all_count"];
			//登録されている在庫が無い場合
			if($all_count==0)
			{
				$sql = "insert into stock_tank (yyyymm, dive_center_id, dive_center_tank_id ".$db_field.",  ) values ('".$yyyy.$mm."', '".$dive_center_id."', '".$key."' ".$db_data." )";
				$db_result = $common_dao->db_update($sql);
			}
			else
			{
				$db_field_up = substr($db_field_up, 2);
				$sql = "update stock_tank set ";
				$sql .= $db_field_up;
				$sql .= " where dive_center_tank_id='".$key."' and dive_center_id='".$dive_center_id."' and yyyymm='".$yyyy.$mm."' ";
				$db_result = $common_dao->db_update($sql);
			}
		}
	}
	
	$common_connect -> Fn_javascript_move("登録・修正しました", "./");
?>
</body>
</html>