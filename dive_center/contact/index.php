<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	$dive_center_id = $_SESSION['dive_center_id'];
	
	$db_result = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	if($db_result)
	{
		$dive_center_name = $db_result[0]["dive_center_name"];
	}
	
  $header_centername = $dive_center_name;
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/header.php'); ?>

<?
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	

?>
<article>

<section class="sectionBox">
<p class="tit">お問い合わせ</p>
<div class="box">
  <table class="listTable">
    <tr>
      <td colspan="2">機能や操作方法のご質問や、ご要望がございましたら下記メールアドレスまでお問い合わせください。<br>弊社担当より折り返しご連絡します。</td>
    </tr>
    <tr>
      <th width="250">バディダイブ事務局プロサポートデスク</th>
      <td class="cellLink"><a href="mailto:prosupport@buddydive.jp">prosupport@buddydive.jp</a></td>
    </tr>
  </table>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/footer.php'); ?>