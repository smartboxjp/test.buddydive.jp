<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	$dive_center_id = $_SESSION['dive_center_id'];
	
	$db_result = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	if($db_result)
	{
		$dive_center_name = $db_result[0]["dive_center_name"];
	}
	
  $header_centername = $dive_center_name;
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/header.php'); ?>

<?
	//管理者チェック
	$common_connect -> Fn_dive_center_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	

?>
<article>

<section class="sectionBox">
<p class="tit">各種資料</p>
<div class="box">
  <table class="listTable">
    <tr>
      <th>資料名</th>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/manual_ver02.pdf" target="_blank">マニュアル ver02 [PDF]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/manual_coupon.pdf" target="_blank">マニュアル追加分（クーポン使用時） [PDF]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/reception.doc" target="_blank">来訪者の受付手順サンプル [Word]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/entry.doc" target="_blank">バディ潜水申込書サンプル [Word]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/accidentreport.doc" target="_blank">事故報告書 [Word]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/insurance_docomo.pdf" target="_blank">保険加入の方法 docomo [PDF]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/insurance_au.pdf" target="_blank">保険加入の方法 au [PDF]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/insurance_softbank.pdf" target="_blank">保険加入の方法 softbank [PDF]</a></td>
    </tr>
    <tr>
      <td class="cellLink"><a href="./files/mail_center_sample.txt" target="_blank">予約の方へ送る案内メールサンプル [txt]</a></td>
    </tr>
  </table>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common_center/include/footer.php'); ?>