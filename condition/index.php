<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>

<?
foreach($_GET as $key => $value)
{
	$$key = $value;
}

if($s_yyyy == "") { $s_yyyy = date("Y");}
if($s_mm == "") { $s_mm = date("m");}
if($s_dd == "") { $s_dd = date("d");}

if (($_SERVER["REQUEST_URI"]) == "/condition/") {
  $meta_title_other = "【毎朝更新】伊豆のダイビングポイントの海況（開催状況・水温・透明度） | BuddyDive（バディダイブ）";
  $meta_title_condition = "【毎朝更新】伊豆のダイビングポイントの海況（開催状況・水温・透明度）";
}else{
  $meta_title_other = $s_yyyy."年".$s_mm."月".$s_dd."日のダイビングポイントの海況 | BuddyDive（バディダイブ）";
  $meta_title_condition = $s_yyyy."年".$s_mm."月".$s_dd."日のダイビングポイントの海況";
}
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>
<?
if(!checkdate($s_mm, $s_dd, $s_yyyy))
{
	$common_connect-> Fn_javascript_back("日付を正しく入力して下さい。");
}

if(strtotime("$s_yyyy/$s_mm/$s_dd")<strtotime(date("2015/12/1")))
{
	$common_connect-> Fn_javascript_back("2015年12月以降の日付を入力してください。");
}
?>
<article>

<div class="map big">
  <div id="mapCanvas"></div>
</div>

  <section class="innerBox">
    <div class="inBox">
      <div class="titBox">
        <h1 class="tit">ダイビングポイントの海況<br><span>[<? echo $s_yyyy."/".$s_mm."/".$s_dd;?>]</span></h1>
      </div>

      <div id="divingPointSNS">
        <ul>
          <li class="fsFacebook"><a class="fb btn" href="http://www.facebook.com/share.php?u=https://buddydive.jp<?php echo $_SERVER["REQUEST_URI"];?>" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false; ga('send', 'event', 'Click', 'SNS', 'Facebook_act');">シェアする</a></li>
          <li class="fsTwitter"><a href="http://twitter.com/share?text=<?php echo $meta_title_condition; ?>&hashtags=バディダイブ&url=https://buddydive.jp<?php echo $_SERVER["REQUEST_URI"];?>" target="_blank" onClick="ga('send', 'event', 'Click', 'SNS', 'Twitter_act');">ツィートする</a></li>
          <li class="fsLine"><a href="http://line.me/R/msg/text/?<?php echo $meta_title; ?>https://buddydive.jp<?php echo $_SERVER["REQUEST_URI"];?>" onClick="ga('send', 'event', 'Click', 'SNS', 'Line_act');">LINEで送る</a></li>
        </ul>
      </div><!--divingPointSNS-->
      <div class="searchBox">
        <p>過去のデータをみる</p>
        <form method="get" action="<? echo $_SERVER['PHP_SELF'];?>">
          <?
            $var = "s_yyyy";
          ?>
          <select id="<? echo $var;?>" name="<? echo $var;?>">
          <? for ($loop=2015; $loop<=date("Y") ; $loop++) { ?>
            <option value="<? echo $loop;?>" <? if($loop==$$var) { echo " selected ";}?>><? echo $loop . "年"; ?></option>
          <? } ?>
          
          </select>
          
          <?
            $var = "s_mm";
          ?>
          <select id="<? echo $var;?>" name="<? echo $var;?>">
          <? for ($loop=1; $loop<=12 ; $loop++) { ?>
            <option value="<? echo substr(($loop+100), 1);?>" <? if(substr(($loop+100), 1)==$$var) { echo " selected ";}?>><? echo substr(($loop+100), 1) . "月";?></option>
          <? } ?>
          </select>
          
          <?
            $var = "s_dd";
          ?>
          <select id="<? echo $var;?>" name="<? echo $var;?>">
          <? for ($loop=1; $loop<=31 ; $loop++) { ?>
            <option value="<? echo substr(($loop+100), 1);?>" <? if(substr(($loop+100), 1)==$$var) { echo " selected ";}?>><? echo substr(($loop+100), 1) . "日";;?></option>
          <? } ?>
          </select>
          <input type="submit" value="表示">
        </form>
      </div>

      <ul class="conditionBox">
<?
	$sql = "SELECT dive_center_id, dive_center_name, dive_center_point, img_1, wave_regi_date FROM dive_center where flag_open = '1' " ;
	$sql .= " order by wave_regi_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$dive_center_id = $db_result[$db_loop]["dive_center_id"];
			$img_1 = $db_result[$db_loop]["img_1"];
			
			$wave_height = "";
			$flag_open = "";
			$regi_date = "";
			//最新の波状況をdive_center へ登録
			$sql_wave = "SELECT wave_height,  flag_open, regi_date FROM dive_center_wave_height where dive_center_id = '".$dive_center_id."' and DATE_FORMAT(regi_date, '%Y-%m-%d')='".$s_yyyy."-".$s_mm."-".$s_dd."' " ;
			$sql_wave .= " order by regi_date desc ";
			$sql_wave .= " limit 0, 1";
			
			$db_result_wave = $common_dao->db_query($sql_wave);
			if($db_result_wave)
			{
				$wave_height = $db_result_wave[0]["wave_height"];
				$flag_open = $db_result_wave[0]["flag_open"];
				$regi_date = $db_result_wave[0]["regi_date"];
			}
?>
        <li>
          <a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>">
            <p class="img"><img src="/<? echo global_dive_center_dir.$dive_center_id."/".$img_1;?>"></p>
            <div class="txtBox">
              <p class="conditionTit"><? echo $db_result[$db_loop]["dive_center_point"];?></p>
              <p class="txt">
  						<?
  						if($flag_open=="")
  						{
  						?>
  						<img src="/common/img/divingpoint/ico_noinfo.png">
  						<?
  						}
  						else
  						{
  						?>
  						<img src="<? echo $arr_flag_open_img[$flag_open]?>">
  						<?
  						}
  						?>
              	
              <span class="data">
  						<?
  						if($flag_open=="")
  						{
  							echo $regi_date;
  						}
  						else
  						{
  							echo date('m/d H:i', strtotime($regi_date));
  						}
  						?>
              </span>
              <?
              if($flag_open=="")
  						{
  							echo "情報はありません。";
  						}
  						else
  						{
  						?>
              <? echo $arr_flag_open[$flag_open];?>
              <?
  						}
  						?>
              </p>
            </div>
            <p class="freeTxt"><? echo $wave_height;?></p>
          </a>
        </li>
<?
		}
	}
?>
        
      </ul><!--conditionBox-->
      <p class="mb20">※バディダイビングを対象とした海況情報を提供しています。</p>
    </div>
  </section><!--contentsBox-->
  
  <div id="pankuzu">
    <ul>
      <li><a href="/">HOME</a>&gt;</li>
      <li>ダイビングポイントの海況</li>
    </ul>
  </div>
</article>

<!--▼▼▼Google Map設定▼▼▼-->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>

//Google Maps
function initialize() {
  var latlng = new google.maps.LatLng(34.968078, 138.931606);
  var opts = {
    zoom: 10,
    center: latlng,
    mapTypeControl: true,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  
  var map = new google.maps.Map(document.getElementById("mapCanvas"), opts);
  var styleOptions = [{
    "featureType":"water","stylers":[{"visibility":"on"},{"color":"#cddcec"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]}
  ]

  var monoType = new google.maps.StyledMapType(styleOptions);
  map.mapTypes.set('mono', monoType);
  map.setMapTypeId('mono');
  
//マーカー設定
//コンディション画像
<?php
		//波状況
		$sql_wave = "SELECT d.dive_center_id, h.wave_height,  h.flag_open, h.regi_date,  google_map_1, google_map_2, ";
		$sql_wave .= " dive_center_name, dive_center_point, img_1, wave_regi_date ";
		$sql_wave .= " FROM dive_center d inner join dive_center_wave_height h ";
		$sql_wave .= " on d.dive_center_id=h.dive_center_id ";
		$sql_wave .= " where d.flag_open and DATE_FORMAT(h.regi_date, '%Y-%m-%d')='".$s_yyyy."-".$s_mm."-".$s_dd."' " ;
		$db_result_wave = $common_dao->db_query($sql_wave);
		if($db_result_wave)
		{
			for($db_loop=0 ; $db_loop < count($db_result_wave) ; $db_loop++)
			{
				$dive_center_id = $db_result_wave[$db_loop]["dive_center_id"];
				$wave_height = $db_result_wave[$db_loop]["wave_height"];
				$flag_open = $db_result_wave[$db_loop]["flag_open"];
				$regi_date = $db_result_wave[$db_loop]["regi_date"];
				$dive_center_name = $db_result_wave[$db_loop]["dive_center_name"];
				$dive_center_point = $db_result_wave[$db_loop]["dive_center_point"];
				$wave_height = $db_result_wave[$db_loop]["wave_height"];
				$google_map_1 = $db_result_wave[$db_loop]["google_map_1"];
				$google_map_2 = $db_result_wave[$db_loop]["google_map_2"];
				
				$icon = str_replace(".png", "_small.png", $arr_flag_open_img[$flag_open]);
				$icon = str_replace("/divingpoint/", "/condition/", $icon);
?>

  var Marker_<? echo $dive_center_id;?> = new google.maps.Marker({
    map: map,
    title: '<? echo $dive_center_point;?>',
    position: new google.maps.LatLng(<? echo $google_map_1;?>, <? echo $google_map_2;?>),
	icon:{
		<? if($dive_center_id=="itadiving") { ?>
			anchor: {x: -15, y: 40},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="itamarine") { ?>
			anchor: {x: 15, y: 40},   /* 場所をずらしてます */
    <? }elseif($dive_center_id=="futo") { ?>
      anchor: {x: 0, y: 50},   /* 場所をずらしてます */
		<? } else { ?>
			//anchor: {x: 0, y: 0},   /* 場所をずらしてます */
		<? } ?>
		url: "<?php echo $icon;?>"
		}
  });
	
	//テキスト
  var Marker_<? echo $dive_center_id;?>_txt = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(<? echo $google_map_1;?>, <? echo $google_map_2;?>),
    icon:{
		<? if($dive_center_id=="itadiving") { ?>
			anchor: {x: -15, y: 0},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="itamarine") { ?>
			anchor: {x: 15, y: 0},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="ishibashi") { ?>
			anchor: {x: -10, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="enoura") { ?>
			anchor: {x: -22, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="fukuura") { ?>
			anchor: {x: -10, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="izusan") { ?>
			anchor: {x: -22, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="ito") { ?>
			anchor: {x: 20, y: 52},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="kawana") { ?>
			anchor: {x: -10, y: 20},   /* 場所をずらしてます */
    <? }elseif($dive_center_id=="futo") { ?>
      anchor: {x: -34, y: 32},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="iop") { ?>
			anchor: {x: -22, y: 14},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="inatori") { ?>
			anchor: {x: -10, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="osezaki") { ?>
			anchor: {x: 20, y: 52},   /* 場所をずらしてます */
		<? } else { ?>
			anchor: {x: 20, y: 0},   /* 場所をずらしてます */
		<? } ?>
    	url:'/common/img/condition/txt_<? echo $dive_center_id;?>.png'
    }
  });

	//リンク設定
	google.maps.event.addListener(Marker_<? echo $dive_center_id;?>, 'click', function() {window.open("/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>" );});

<?
			}
		}
?>


  //マップのタイトル
  var contentString = 'ダイビングポイントの海況（伊豆）';
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  //infowindow.open(map, lopanMarker);//初期状態で吹き出しを表示させる場合は有効にする 
  google.maps.event.addListener(Marker, 'click', function() {
    infowindow.open(map, Marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>

<!--▲▲▲Google Map設定▲▲▲-->
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
