

<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
  $common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<article>
<div id="pankuzuTop" class="mb0">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li>特集記事</li>
</ul>
</div>

<section class="innerBox">
<div class="inBox">
<div class="titBox">
<h1 class="tit">特集記事</h1>
</div>
<ul class="threeList mb30">
<li><a href="/feature/flow.php" style="background-image: url(/common/img/feature/flow/img_thumb.jpg);"><span>バディダイビング<br>1日の流れ</span></a></li>
<li><a href="/feature/merit.php" style="background-image: url(/common/img/feature/merit/img_thumb.jpg);"><span>バディダイビングで得られる<br>5つのメリット</span></a></li>
<li><a href="/feature/qa10.php" style="background-image: url(/common/img/feature/qa10/img_thumb.jpg);"><span>始める前に知っておきたい<br>バディダイビングの疑問10</span></a></li>
</ul>
</div>
</section><!--contentsBox-->

<div id="pankuzu">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li>特集記事</li>
</ul>
</div>
</article>


<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>


