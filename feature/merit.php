<?php
  $meta_title_other = "バディダイビングで得られる5つのメリット";
  $meta_image = "/common/img/feature/merit/img_01.jpg";
?>
<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
  $common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="pankuzuTop">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/feature/">特集記事</a>&gt;</li>
<li><?php echo $meta_title; ?></li>
</ul>
</div>
<!--
-->

<section class="box960 mb50">

<section id="mainContents">

<section class="featureMainBox">
<p><img src="<?php echo $meta_image; ?>" width="640" height="400" alt="<?php echo $meta_title; ?>"></p>
<p class="featureTag">BUDDY DIVING MERIT 5</p>
<h1 class="featureTit"><?php echo $meta_title; ?></h1>

<div id="divingPointSNS">
<ul>
<li class="fsFacebook">
<a class="fb btn" href="http://www.facebook.com/share.php?u=<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false; ga('send', 'event', 'Click', 'SNS', 'Facebook_act');">シェアする</a></li>
<li class="fsTwitter">
<a href="http://twitter.com/share?text=<?php echo $meta_title; ?>&hashtags=バディダイブ&url=<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" target="_blank" onClick="ga('send', 'event', 'Click', 'SNS', 'Twitter_act');">ツィートする</a></li>
<li class="fsLine">
<a href="http://line.me/R/msg/text/?<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" onClick="ga('send', 'event', 'Click', 'SNS', 'Line_act');">LINEで送る</a></li>
</ul>
</div>
</section>

<section class="featureBox">
<h2 class="featureSubTit">1. 新しい価値・ライフスタイル</h2>
<p class="mb20">自分たちで相談しながらダイブプランを立て、水中では自己管理をしつつ、自分たちで決めたコースを通り、生物を探してみたり、のんびりしたり。
誰かに連れていかれるのではなく、海と自分の意思とが向き合えば、海に潜るということが、ただごとではないと肌で感じることでしょう。</p>
<p class="mb20">本来の海の姿と向き合いながら、バディ同士で海中を潜る。
この大いなる探検には、恐怖と喜びの入り混じる感動の世界が待っていますが、ここに新しい価値やライフスタイルにつながるヒントがあるのかもしれません。
<p>新しい価値……いや、これが本来のダイビングの原点とも言えるのかもしれません。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">2. 自立したダイバー = スキルアップ</h2>
<p>自分の頭で考え、自分の責任で海と遊んでみることこそ、一歩上を行く、自立したダイバーへの道でもあり、自然と遊ぶアウトドアの原点。
「自分の頭で考え、実践してみること」はスキルアップの最も近道です。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">3. 低コスト、自分のペース</h2>
<p>「ダイビング=高い」というイメージがありますが、自分のペースで潜ることができ、プロと潜る半額近い料金で潜れることは、単純に大きな魅力。
長くダイビングを趣味として続けるためにも重要なポイントです。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">4. ガイドやイントラと潜る価値が増す!?</h2>
<p class="mb20">増える選択肢=広がる可能性
誤解なきよう言っておきたいのですが、ガイドやインストラクターを付けずに潜るべきだと言っているわけではありません。</p>
<p class="mb20">むしろ、バディダイビングをすることによって、ガイドやインストラクターの価値を実感すると思います。登山で山頂を目指す時、自力登山、ガイド付き登山、ゴンドラで一気に山頂、という選択肢がありますが、同じ山頂でも見えている景色は違うでしょう。</p>
<p>ダイビングでも、バディで気軽に潜れる環境があり、スキルアップしたいときはインストラクターに教わり、その海のことを深く知りたい・ナビゲートして欲しいならガイドをつけて潜る。自分の中で、ダイビングの選択肢を増やしておくことで、ダイビングの可能性も広がると思います。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">5. BuddyDiveで広がる可能性</h2>
<p class="mb20">BuddyDiveでは、スキル、病歴、保険など、登録の一定基準をクリアできれば、AED、酸素、緊急時の対応など、統一した安全レギュレーションをクリアした加盟店で、安心して潜ることができます。</p>
<p class="mb20">また、潜れば潜るほどポイントがたまり、タンクに還元できるのでお得です。さらに、当サイト登録ダイバーを対象としたキャンペーンやイベントで、ダイバー同士がコミュニケーションをできる場を広げていきます。
<p class="mb20">本来、エントリーレベルのCカード講習を受けて取得するCカードとは、バディ同士でダイビングができることの証明。</p>
<p>BuddyDiveは、PC・スマホから手軽に予約ができて、潜れば潜るほどポイントが貯まる予約サイトということになりますが、ダイバーが自己責任で潜ることを表明し、加盟の海は、安全のレギュレーションを備えた施設としてタンクを提供する、アウトドア本来の大人の関係。</p>
</section>


<? if($_SESSION['member_id']=="") {?>
<p class="orangeBtn mt20"><a href="/diver/registration/">無料ダイバー登録</a></p>
<?php }else{ ?>
<p class="blueBtn mt20"><a href="/divingpoint/">ダイビングポイントを探す</a></p>
<?php } ?>
</section><!--mainContents-->

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/feature/side.php'); ?>

</section><!--contentsBox-->

<div id="pankuzu">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/feature/">特集記事</a>&gt;</li>
<li><?php echo $meta_title; ?></li>
</ul>
</div>
</article>


<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>
