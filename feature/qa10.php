<?php
  $meta_title_other = "始める前に知っておきたいバディダイビングの疑問10";
  $meta_image = "/common/img/feature/qa10/img_01.jpg";
?>
<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
  $common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="pankuzuTop">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/feature/">特集記事</a>&gt;</li>
<li><?php echo $meta_title; ?></li>
</ul>
</div>

<section class="box960 mb50">

<section id="mainContents">

<section class="featureMainBox">
<p><img src="<?php echo $meta_image; ?>" width="640" height="400" alt="<?php echo $meta_title; ?>"></p>
<p class="featureTag">BUDDY DIVING Q&amp;A</p>
<h1 class="featureTit"><?php echo $meta_title; ?></h1>

<div id="divingPointSNS">
<ul>
<li class="fsFacebook">
<a class="fb btn" href="http://www.facebook.com/share.php?u=<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false; ga('send', 'event', 'Click', 'SNS', 'Facebook_act');">シェアする</a></li>
<li class="fsTwitter">
<a href="http://twitter.com/share?text=<?php echo $meta_title; ?>&hashtags=バディダイブ&url=<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" target="_blank" onClick="ga('send', 'event', 'Click', 'SNS', 'Twitter_act');">ツィートする</a></li>
<li class="fsLine">
<a href="http://line.me/R/msg/text/?<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" onClick="ga('send', 'event', 'Click', 'SNS', 'Line_act');">LINEで送る</a></li>
</ul>
</div>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q1. そもそも、バディ・ダイビングってなに？</h2>
<p class="mb20"><span class="fs24 bold">A1. </span>世界的な共通ルールとなっている「バディ・システム」
「バディ」と呼ばれるパートナーと一緒に2人1組になって、最初から最後まで互いの近くにいてダイビングをするシステムのことです。</p>
<p class="mb20">“バディダイビング”、“バディ潜水”とは、単に、このバディ・システムでダイビングをすることを指す場合もありますが、ガイドやインストラクターなど、プロのダイバーと一緒に潜らず、自己責任で潜るスタイル、“セルフ・ダイビング”と同義で使われることもあります。</p>
<p>※本サイト『BuddyDive』では、セルフ・ダイビングと同じ意味で使用しています。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q2. インストラクターやガイドなど、プロのダイバーと潜らなくていいの？</h2>
<p class="mb20"><span class="fs24 bold">A2. </span>エントリーレベルのCカードは、バディ同士でダイビングができることの証明ですので、基本的には、Cカードを取得した瞬間から可能です。
ただ、日本ではインストラクターやガイドなど、プロと潜ることの方が一般的なスタイルなので、バディ同士のみで潜る経験をしたことがないダイバーも少ないのかもしれません。</p>
<p class="mb20">海外では、バディダイビングが基本で、ガイドはオプションで付けるという感覚も一般的です。
ですので、ガイド付きでしか潜ったことがない日本人ダイバーが海外で面食らうこともありますが、日本の旅行社を通じて申し込む場合は、たいてい、日本スタイルで潜ることが可能です。</p>
<p>どちらのスタイルが良いというものでもないですが、バディダイビングができるスキル、経験があったほうが、ダイビングの選択肢や幅が広がることは間違いありません。</p>
</section>

<section class="featureBox">
<p class="mb50"><img src="/common/img/feature/qa10/img_02.jpg" width="640" height="400" alt="バディでファンダイビングを楽しむ"></p>
<h2 class="featureSubTit">Q3. バディ・ダイビングの魅力は？ メリットは？</h2>
<p class="mb20"><span class="fs24 bold">A3. </span>自分たちで相談しながらダイブプランを立て、水中では自己管理をしつつ、自分たちで決めたコースを通り、生物を探してみたり、のんびりしたり。
誰かに連れていかれるのではなく、海と自分の意思とが向き合えば、海に潜るということが、ただごとではないと肌で感じることでしょう。</p>
<p class="mb20">この大いなる探検には、恐怖と喜びの入り混じる感動の世界が待っています。
自分の頭で考え、自分の責任で海と遊んでみることこそ、一歩上を行く、自立したダイバーへの道でもあります。</p>
<p>さらに、自分のペースで潜ることができ、プロと潜る場合と比べて半額近い料金で潜れることも大きな魅力です。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q4. バディダイビングのデメリットは？</h2>
<p class="mb20"><span class="fs24 bold">A4. </span>デメリットというと語弊がありますが、基本的に自己責任で潜ることになります。</p>
<p class="mb20">
これまでガイドやインストラクターに頼っていた、計画や管理をすべてバディと実行しなければならず、かける労力はガイドやインストラクターと潜る時より増えるでしょう。
ただ、その労力が楽しみでもあり、充実感にもつながるはず。</p>
<p>さらに、ガイドやインストラクターの凄さ、重要性にも気づかされるでしょう。
ですので、何もバディダイビングだけにこだわる必要もなく、ガイドを頼んだり、ステップアップ講習を受けたりするなど、ダイビングの選択肢を増やし、自分なりに使い分ければよいのです。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q5. どこに行けば、バディダイビングができるの？</h2>
<p class="mb20"><span class="fs24 bold">A5. </span>日本の旅行会社を利用した海外へのダイビング付きのパックツアーに参加すると、日本人スタッフのいるダイビングショップの場合がほとんどで、おおよそガイド付きのダイビングがデフォルトになります。
初めての海の場合とても安心です。</p>
<p class="mb20">現地でダイビングを申し込むと（特に海外資本）、バディダイビングが基本でむしろガイドがオプションというスタイルも珍しくありません。
オプションすらなくて「はい、どうぞ！」と言われ、ガイド付きが当たり前だと思っていた日本人ダイバーが面食らう、なんてことも……。
例えば、バディダイビングといえば、グレートバリアリーフやモルディブのハウスリーフ（ビーチダイビング）などが有名ですね。</p>
<p class="mb20">海外のガイドは文字通り「案内すること」がガイドの役割だという認識で、日本のような手厚い安全管理までしないケースもありますので、いずれにせよ、バディで潜れるスキルがあった方がダイビングの幅は広がり、どこでも安全に楽しめるようになるでしょう。</p>
<p class="mb20">日本の場合、多くのダイビングエリアに、タンクと施設を提供し、緊急時に備えた（緊急連絡網、酸素、AEDなど）「ダイビングセンター」や「ダイビングサービス」と呼ばれる窓口があり、バディダイビングが可能なエリアと不可としているエリアに分かれます。</p>
<p>今までは、各ダイビングエリアによってバディダイビングが可能な基準が異なっていましたが、本サイト『BuddyDive』では、Cカードがあれば、自己責任でバディダイビングが可能な、各ダイビングエリアの窓口をご紹介しています。</p>
<p><a href="/divingpoint/" target="_blank">ダイビングポイント一覧はこちら &gt;</a></p>
</section>


<section class="featureBox">
<p class="mb50"><img src="/common/img/feature/qa10/img_03.jpg" width="640" height="400" alt="バディダイビングで水中カメラ"></p>
<h2 class="featureSubTit">Q6. バディダイビングをする自信がありません……</h2>
<p class="mb20"><span class="fs24 bold">A6. </span>今までガイドやインストラクターとしか潜ったことがないダイバーが、いきなりバディ・ダイビングをするのは確かに不安かもしれません。
ただ、Cカード講習でマスターすべきスキルは、2人でバディ・ダイビングができるレベルの内容のはず。</p>
<p class="mb20">
大事なことは意識。慎重に、自分のできる範囲で潜れば、必要以上に怖がる必要はありません。
岸から真っ直ぐ進んで、180度Uターンして戻って来るだけのダイビングでも、初めて自分たちだけで潜ることは得難い体験になるでしょう。</p>
<p>もし、スキルに不安があれば、スキルアップ講習を開催しているダイビングショップで受講したり、不安がなくなるまで、プロと一緒に潜って、慣れた海から始めることをオススメします。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q7. どれくらいのレベル、スキルが必要？</h2>
<p class="mb20"><span class="fs24 bold">A7. </span>そもそも、バディダイビングができるまでのレベルに達した証がCカード。原則的には、Cカードホルダーであれば、バディダイビングは可能です。</p>
<p class="mb20">まずは、Cカード講習で教わったスキルをチェックしてみてください！<br>
<a href="/about/skill.php" target="_blank">スキルチェックはこちら &gt;</a></p>
<p class="mb20">最低の目安としては、「自分のことは自分でできる」「人に迷惑をかけない」レベルです。
ただ、Cカードを取得していきなりバディダイビングが不安な方は、しばらく講習を受けたダイビングショップで、プロと潜った方が安心。
また、プロのアドバイスや情報が身近にある方が、スキルアップでも、ダイビングを楽しむ上でも効果的かもしれません。</p>
<p>さらに、特に初めて潜る海では、現地の海を一番よく知っているガイドと潜ることをオススメします。
しっかりと海の状況を把握してからバディダイビングをした方が、安全面でも、楽しみの上でもメリットがあるでしょう。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q8. 特別な装備は必要ですか？</h2>
<p class="mb20"><span class="fs24 bold">A8. </span>特別な装備は必要ありませんが、これまでガイドやインストラクターに頼っていた、緊急時の安全対策やバックアップを自分たちで準備しなければなりません。</p>
<p class="mb20">
シグナルフロートやコンパス、ダイブコンピュータは必携でしょうが、ナイフ、ホーン、ベル、予備器材などなど、装備にはゴールはなく、潜る海やコンディションなどなど、いろいろな要素を加味して自分なりに考えて装備は考えます。</p>
<p>
あとは、バディダイビングをするのであれば、安全のためにも（いざという時に使い慣れた器材かどうかは重要）、コストパフォーマンスという意味でも、いずれ、マイギアは持っていた方がよいと思います。</p>
</section>

<section class="featureBox">
<p class="mb50"><img src="/common/img/feature/qa10/img_04.jpg" width="640" height="400" alt="バディダイビングでリラックスしながらバブルリングをつくる"></p>
<h2 class="featureSubTit">Q9. BuddyDiveの仕組みを教えて！</h2>
<p class="mb20"><span class="fs24 bold">A9. </span>本サイト『BuddyDive』は、自己責任で潜るダイバーを応援する予約サイトです。バディダイビングに必要な要件（スキル、病歴、保険）を満たしていることを自己申告＆登録していただければ、加盟店を通じてバディダイビングが可能です。また、加盟店で潜ると、料金の2％がポイントをプレゼント。潜れば潜るほどお得なサービスです。<br>
<a href="/diver/registration/" target="_blank">無料ダイバー登録はこちら &gt;</a></p>
<p class="mb20">加盟店は、受け入れ施設として、緊急時のための統一レギュレーションをクリアしていますので、どの海で潜っても、同様の安全体勢を享受できます。</p>
<p>【加盟店の安全レギュレーション】</p>
<ul class="indent">
<li>&#9679;AEDの設置と利用できるスタッフ</li>
<li>&#9679;酸素の設置と利用できるスタッフ</li>
<li>&#9679;損害賠償保険に加盟</li>
<li>&#9679;応急処置の出来るスタッフの配置</li>
<li>&#9679;1年に1回以上のレスキュートレーニングと応急処置のトレーニングの実施</li>
<li>&#9679;安全に対する注意義務を伝える</li>
<li>&#9679;緊急時の救急隊、保安庁、警察への要請が遅滞なく実施できるように連絡網を構築</li>
<li>&#9679;事前に定めた書式を使って、安全に対する注意義務を漏れなく顧客へ伝える。</li>
</ul>
</section>

<section class="featureBox">
<h2 class="featureSubTit">Q10. 一人でも予約できますか？</h2>
<p class="mb20"><span class="fs24 bold">A10. </span>ダイビングは2人１組の“バディ”単位で潜ることが原則です。
また、バディはお互いのことを理解し、事前にプランを立てる必要があります。そういう意味では、すでに面識のあるバディ同士での予約が理想的です。</p>
<p>BuddyDiveでも、予約時にバディを招待して、2人が予約した時点で潜ることができます。</p>
</section>

<? if($_SESSION['member_id']=="") {?>
<p class="orangeBtn mt20"><a href="/diver/registration/">無料ダイバー登録</a></p>
<?php }else{ ?>
<p class="blueBtn mt20"><a href="/divingpoint/">ダイビングポイントを探す</a></p>
<?php } ?>
</section><!--mainContents-->

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/feature/side.php'); ?>

</section><!--contentsBox-->

<div id="pankuzu">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/feature/">特集記事</a>&gt;</li>
<li><?php echo $meta_title; ?></li>
</ul>
</div>
</article>


<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>
