<?php
  $meta_title_other = "バディダイビング 1日の流れ 体調&amp;海況チェックからディブリーフィングまで";
  $meta_image = "/common/img/feature/flow/img_01.jpg";
?>
<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
  $common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="pankuzuTop">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/feature/">特集記事</a>&gt;</li>
<li><?php echo $meta_title; ?></li>
</ul>
</div>
<!--
-->

<section class="box960 mb50">

<section id="mainContents">

<section class="featureMainBox">
<p><img src="<?php echo $meta_image; ?>" width="640" height="400" alt="<?php echo $meta_title; ?>"></p>
<p class="featureTag">HOW TO BUDDY DIVE</p>
<h1 class="featureTit"><?php echo $meta_title; ?></h1>

<div id="divingPointSNS">
<ul>
<li class="fsFacebook">
<a class="fb btn" href="http://www.facebook.com/share.php?u=<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false; ga('send', 'event', 'Click', 'SNS', 'Facebook_act');">シェアする</a></li>
<li class="fsTwitter">
<a href="http://twitter.com/share?text=<?php echo $meta_title; ?>&hashtags=バディダイブ&url=<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" target="_blank" onClick="ga('send', 'event', 'Click', 'SNS', 'Twitter_act');">ツィートする</a></li>
<li class="fsLine">
<a href="http://line.me/R/msg/text/?<?php echo "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;?>" onClick="ga('send', 'event', 'Click', 'SNS', 'Line_act');">LINEで送る</a></li>
</ul>
</div>
</section>

<section class="featureBox">
<h2 class="featureSubTit">体調&amp;海況情報をチェック</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_02.jpg" width="640" height="400" alt="海況情報をチェック"></p>
<p class="mb20">自分の体調がよいことを確認したら、海況情報をチェック。
当サイトでは、毎朝、ダイビングセンターが海況情報をアップしていますので、ひと目でわかります。<br>
<a href="/condition/index.php?<?php echo 's_yyyy=' . date('Y') . '&s_mm=' .  date('m') . '&s_dd=' . date('d');?>" target="_blank">海況情報はこちら &gt;</a></p>
<p>※ご予約しているダイビングポイントがクローズになった場合は、登録されたメールアドレスにクローズのご連絡が届きます。オープンしているダイビングポイントに予約変更を。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">バディで海況をチェック</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_03.jpg" width="640" height="400" alt="バディで海況をチェック"></p>
<p>到着したら、まずは海況チェック（受付が最初の場合も）。
エントリー口の状況やうねりや波など海のコンディションなど、自分たちで対応できるかどうかをチェックします。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">ダイビングセンターで受付</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_04.jpg" width="640" height="400" alt="ダイビングセンターで受付"></p>
<p>当サイトBuddyDiveをご利用の場合、基本的な情報は事前に伝わっていますので、基本的な申込書などに記入し、Cカードと保険加入の証明を提示。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">ブリーフィング</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_05.jpg" width="640" height="400" alt="ブリーフィング"></p>
<p class="mb20">海の窓口となるダイビングセンターからブリ―フィング。海況や潜る際の注意点や生物などを教えてもらい、疑問や不安があれば、ここで解消しておきましょう。</p>
<p>※ブリーフィングはセンターによって形式が違いますので（例えばペーパーによるリスクの告知など）、各ダイビングエリア情報をチェックしてください。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">ダイブ・プランをチェック</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_01.jpg" width="640" height="400" alt="ダイブ・プランをチェック"></p>
<p class="mb20">潜る前に、ダイブプランを再チェック。コースや申告残圧、水深、時間、サイン、緊急手順など、最中の確認をします。
ダイブプランはブリーフィングで得た情報を活かし、変更することももちろん可能。</p>
<p>例えば、潜るはずだったコースのコンディションが悪いのであれば、コース取りを変更したり、その日に出ていた珍しい生物が深場にいたら2本目の水深を浅くしたり、臨機応変にプランを立てましょう。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">ナビゲーションのチェック</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_06.jpg" width="640" height="400" alt="ナビゲーションのチェック"></p>
<p class="mb20">エントリーすれば、当然、エグジットしますので、エグジットポイントをチェックしておくことは必須です。</p>
<p>ビーチダイビングであれば、岸の方角やエントリー口、ボートダイビングであれば、船が係留してあるロープや根の方角などをチェックしておきます。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">バディ・チェック！</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_07.jpg" width="640" height="400" alt="バディ・チェック！"></p>
<p>潜る前には必ずバディ・チェック！ダイビング器材に不具合がないかはもちろん、バディの体調が万全なことを確認します。</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">海を探検しよう！（ダイビング）</h2>
<p class="mb20">バディで協力しあって計画通りに潜りましょう。最初のうちは、ただ計画通りに潜って帰ってくるだけでも大冒険。真っ直ぐ行って帰ってくるだけでも充実感が待っています。</p>
<p class="mb20">何度も同じ海に潜って慣れてくれば、コンパスを見ずに、ナチュラルナビゲーション（水中の景観、目印を利用したナビゲーション）をメインに潜れるようになってきます。そうすれば、フィッシュウオッチング、撮影など、自分の目的や趣向に合わせて潜れます。</p>
<p class="mb10"><img src="/common/img/feature/flow/img_09.jpg" width="640" height="400" alt="喜びを分かち合い"></p>
<p class="mb30"><span class="bold">喜びを分かち合い</span><br>生物を探したり、撮影したりと、楽しいこと共有できるのもバディ・ダイビングのよいところ。フォト派同士のバディは通常のダイビングとは異なるプランが必要です</p>
<p class="mb10"><img src="/common/img/feature/flow/img_10.jpg" width="640" height="400" alt="苦しみは半減!?"></p>
<p><span class="bold">苦しみは半減!?</span><br>
バディの耳抜きが不調なら体を支えてあげるなど、助け合うことができるのがバディシステムの目的です</p>
</section>

<section class="featureBox">
<h2 class="featureSubTit">ロギング・デブリーフィング</h2>
<p class="mb10"><img src="/common/img/feature/flow/img_11.jpg" width="640" height="400" alt="ロギング・デブリーフィング"></p>
<p class="mb20">ログブックに必要な情報をチェックしつつ、実際のコースをたどりながら、見た生物や体験したシーンなどを振り返ります。</p>
<p class="mb20">この時、楽しみながらも、ダイブプランと照らし合わせて課題や改善点もバディ同士で話し合っておくと、次のダイビングがより充実したものになります。</p>
<p>さあ、次はどこに潜ろうか……。</p>
</section>

<? if($_SESSION['member_id']=="") {?>
<p class="orangeBtn mt20"><a href="/diver/registration/">無料ダイバー登録</a></p>
<?php }else{ ?>
<p class="blueBtn mt20"><a href="/divingpoint/">ダイビングポイントを探す</a></p>
<?php } ?>
</section><!--mainContents-->

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/feature/side.php'); ?>

</section><!--contentsBox-->

<div id="pankuzu">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/feature/">特集記事</a>&gt;</li>
<li><?php echo $meta_title; ?></li>
</ul>
</div>
</article>


<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>
