<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>

<?
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	foreach($_REQUEST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($dive_reserve_id=="")
	{
		$common_connect->Fn_javascript_back("予約IDがありません。");
	}
?>
<script type="text/javascript">
	$(function() {
		
		$('#submit_invite').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("invite_name");
			err_check_count += check_input_email("invite_email");
			
			if(err_check_count!=0)
			{
				swal("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_invite', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<div style='color:#F00;'>正しく入力してください。</div>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<div style='color:#F00;'>メールアドレスは半角英数字で入力してください。</div>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00;'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<div style='color:#F00;'>６文字以上入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						
	});
	
//-->
</script>
<?
	//javascript 削除 fnChangeDel(i)
	$common_connect->Fn_javascript_delete_2("バディを削除しますか？", "./buddy_del.php?dive_reserve_id=", "&buddy_id=")
?>



<article id="diverBox">

<div id="reservationBox">
    <p class="pointName">バディを選択</p>
    <div class="buddySelectBox">
      <? $var = "form_search";?>
      <form action="<? echo $_SERVER['PHP_SELF'];?>" name="<? echo $var;?>" id="<? echo $var;?>" method="post">
      <input type="hidden" name="dive_reserve_id" value="<? echo $dive_reserve_id;?>">
      <div class="buddyUser">
        <p class="tit">ダイバー登録済みのバディを探す</p>
        <? $var = "s_keyword";?>
        <input name="<?=$var;?>" id="<?=$var;?>" value="<? echo $$var;?>" type="text" class="txt" placeholder="バディのお名前 or E-mailで検索">
        <?
        if($s_keyword!="")
				{
					$where = "";
					//既にバディになったリストは省く
					$arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id, $member_id) ;
					if($arr_buddy)
					{
						$where_member_id = "";
						for($db_loop=0 ; $db_loop < count($arr_buddy["member_id"]) ; $db_loop++)
						{
							$where_member_id .= ",". $arr_buddy["member_id"][$db_loop];
						}
						$where .= " and member_id not in (".substr($where_member_id, 1)." )";
					}
					$where .= " and flag_hontouroku=1 ";
					$where .= " and member_id!='".$member_id."' ";
					$where .= " and (user_name like '%".$s_keyword."%' or member_email like '%".$s_keyword."%' or member_name_1 like '%".$s_keyword."%' or member_name_2 like '%".$s_keyword."%' or member_name_kana like '%".$s_keyword."%'  )";
					
					$sql = "SELECT member_id, user_name, member_email, member_login_pw, img_1,member_name_1, member_name_2, member_name_kana FROM member where 1 ".$where ;
					$sql .= " order by up_date desc";
					$db_result = $common_dao->db_query($sql);
				?>
        <ul>
				<?
					if($db_result)
					{
						for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
						{
				?>
          <li>
          <a href="buddy_add.php?buddy_id=<? echo $db_result[$db_loop]["member_id"];?>&dive_reserve_id=<? echo $dive_reserve_id;?>">
          <? if($db_result[$db_loop]["img_1"]!="") { ?>
            <img src="<? echo "/".global_member_dir.$db_result[$db_loop]["member_id"]."/".$db_result[$db_loop]["img_1"];?>" width="50" height="50">
					<? } else { ?>
            <img src="/app_photo/member/default/noimage.gif" width="50" height="50">
          <? } ?>
          <span><? echo $db_result[$db_loop]["member_name_1"]." ".$db_result[$db_loop]["member_name_2"];?>をバディに追加</span>
          </a>
          </li>
        
        <?
						}
					}
					else
					{
						echo "<li>検索結果がありません。</li>";
					}
				?>
        </ul>
				<?
        }
				?>
        <? $var = "submit_search";?>
        <p class="blueBtn small"><button type="submit" name="<? echo $var;?>" id="<? echo $var;?>" class="blueBtn small">検索</button></p>
      </div>
      </form>
      
      <? $var = "form_invite";?>
      <form action="invite_email.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post">
      <input type="hidden" name="dive_reserve_id" value="<? echo $dive_reserve_id;?>">
      <div class="buddyNoUser">
        <p class="tit">ダイバー未登録のバディを招待する</p>
        <? $var = "invited_name";?>
        <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" class="txt" placeholder="バディのお名前">
        <span id="err_<? echo $var;?>"></span>
        <? $var = "invited_email";?>
        <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" class="txt" placeholder="バディのE-mail">
        <span id="err_<? echo $var;?>"></span>
        <? $var = "submit_invite";?>
        <p class="blueBtn small"><button type="submit" name="<? echo $var;?>" id="<? echo $var;?>" class="blueBtn small">招待</button></p>
      </div>
      </form>
    </div>
    
    <dl class="buddyEntry">
      <dt>バディ</dt>
      <?
      $arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id, $member_id) ;
      if($arr_buddy)
      {
        for($db_loop=0 ; $db_loop < count($arr_buddy["member_id"]) ; $db_loop++)
        {
		?>
      <dd>
      <? if($arr_buddy["img_1"][$db_loop]!="") { ?>
      <img src="<? echo global_ssl."/".global_member_dir.$arr_buddy["member_id"][$db_loop]."/".$arr_buddy["img_1"][$db_loop];?>" width="140" height="140">
      <? } else { ?>
      <img src="/app_photo/member/default/noimage.gif" width="140" height="140">
      <? } ?>
      <span><? echo $arr_buddy["member_name_1"][$db_loop]." ".$arr_buddy["member_name_2"][$db_loop];?></span>
      <a href="#" onClick='fnChangeDel("<? echo $dive_reserve_id;?>", "<? echo $arr_buddy["member_id"][$db_loop];?>")'>x</a></dd>
		<?
				}
			}
    ?>
    
    
      <?
      $db_result_inivited = $common_member->Fn_inivited_list ($common_dao, $dive_reserve_id, $member_id);
      if($db_result_inivited)
      {
        for($db_loop=0 ; $db_loop < count($db_result_inivited) ; $db_loop++)
        {
		?>
      <dd>
      <img src="/app_photo/member/default/undiver.gif" width="140" height="140">
      <span><? echo $db_result_inivited[$db_loop]["invited_name"];?></span>
      </dd>
		<?
				}
			}
    ?>
    </dl>
    
    <p class="orangeBtn mt10">
      <?
      if((count($db_result_inivited)+count($arr_buddy["member_id"]))==0)
			{
			?>
      <a href="#" onClick="javascript:swal('バディを1名以上選択してください。');">バディ選択完了</a>
      <?
			}
			else
			{
			?>
      <a href="/diver/">バディ選択完了</a>
      <?
			}
			?>
    </p>
</div>

</article>

<?

	function fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd)
	{
		$return_link = "";
		if($dive_center_id!="") { $return_link.="&dive_center_id=".$dive_center_id;}
		if($s_style_id!="") { $return_link.="&s_style_id=".$s_style_id;}
		if($s_boat_id!="") { $return_link.="&s_boat_id=".$s_boat_id;}
		if($s_yyyymmdd!="") { $return_link.="&s_yyyymmdd=".$s_yyyymmdd;}
		
		return $return_link;
	}
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
