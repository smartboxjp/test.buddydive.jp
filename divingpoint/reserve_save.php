<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	$common_contents = new CommonContents();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
</head>
<body>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/analytics.php'); ?>

<?php

	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($s_style_id=="" || $s_boat_id=="" || $s_yyyymmdd=="" || $s_tank_1=="" || $dive_center_id=="")
	{
		$common_connect -> Fn_javascript_back("すべて必須です。");
	}
	
	$reserve_price = 0;
	$s_yyyymm = substr($s_yyyymmdd, 0, 6);
	$s_dd = substr($s_yyyymmdd, 6, 2);
	
	//_/_/_/_/_/ 在庫チェック start _/_/_/_/_/
	//スタイル
	$result_stock_style = $common_contents -> Fn_stock_style_check($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_style_id);
	if($result_stock_style<1) { $common_connect -> Fn_javascript_back("スタイル在庫がありません。"); }
	
	//ボート
	$result_boat_style = $common_contents -> Fn_boat_style_check($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_boat_id);
	if($result_boat_style<1) { $common_connect -> Fn_javascript_back("ボート在庫がありません。"); }
	
	//タンク	
	$result_tank_style = $common_contents -> Fn_tank_style_check($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_tank_1);
	if($result_tank_style<1) { $common_connect -> Fn_javascript_back("タンク在庫がありません。"); }
	
	if($s_tank_2!="")
	{
		$result_tank_style = $common_contents -> Fn_tank_style_check($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_tank_2);
		if($result_tank_style<1) { $common_connect -> Fn_javascript_back("タンク2在庫がありません。"); }
	}
	
	if($s_tank_3!="")
	{
		$result_tank_style = $common_contents -> Fn_tank_style_check($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_tank_3);
		if($result_tank_style<1) { $common_connect -> Fn_javascript_back("タンク3在庫がありません。"); }
	}
	//_/_/_/_/_/ 在庫チェック end _/_/_/_/_/
	
	//会員情報
	$sql = "SELECT user_name, member_name_1, member_name_2, member_email FROM member where member_id='".$member_id."'";
	$sql .= $where;
	$db_result = $common_dao->db_query($sql);
	$user_name = $db_result[0]["user_name"];
	$member_name_1 = $db_result[0]["member_name_1"];
	$member_name_2 = $db_result[0]["member_name_2"];
	$member_email = $db_result[0]["member_email"];
	
	//ダイビングセンター
	$db_result_dive_center = $common_center -> Fn_center_info ($common_dao, $dive_center_id);
	$dive_center_name = $db_result_dive_center[0]["dive_center_name"];
	$dive_center_point = $db_result_dive_center[0]["dive_center_point"];
	$dive_center_email = $db_result_dive_center[0]["dive_center_email"];

	//スタイル
	$sql = "SELECT dive_center_style_id, dive_center_style_title, dive_center_style_price  FROM dive_center_style where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " and dive_center_style_id='".$s_style_id."' ";
	$db_result_style = $common_dao->db_query($sql);
	$reserve_style_id = $db_result_style[0]["dive_center_style_id"];
	$reserve_style_name = $db_result_style[0]["dive_center_style_title"];
	$reserve_style_price = $db_result_style[0]["dive_center_style_price"];
	$reserve_price += $reserve_style_price;
	
	//ボート
	$sql = "SELECT dive_center_boat_id, dive_center_boat_title, dive_center_boat_price FROM dive_center_boat where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " and dive_center_boat_id='".$s_boat_id."' ";
	$db_result_boat = $common_dao->db_query($sql);
	$reserve_boat_id = $db_result_boat[0]["dive_center_boat_id"];
	$reserve_boat_name = $db_result_boat[0]["dive_center_boat_title"];
	$reserve_boat_price = $db_result_boat[0]["dive_center_boat_price"];
	$reserve_price += $reserve_boat_price;
	
	//タンク1
	$sql = "SELECT dive_center_tank_id, dive_center_tank_title, dive_center_tank_price FROM dive_center_tank where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " and dive_center_tank_id='".$s_tank_1."' ";
	$db_result_tank = $common_dao->db_query($sql);
	$reserve_1_tank_id = $db_result_tank[0]["dive_center_tank_id"];
	$reserve_1_tank_name = $db_result_tank[0]["dive_center_tank_title"];
	$reserve_1_tank_price = $db_result_tank[0]["dive_center_tank_price"];
	$reserve_price += $reserve_1_tank_price;
	
	//タンク2
	if($s_tank_2!="")
	{
		$sql = "SELECT dive_center_tank_id, dive_center_tank_title, dive_center_tank_price FROM dive_center_tank where flag_open='1' and dive_center_id='".$dive_center_id."' ";
		$sql .= " and dive_center_tank_id='".$s_tank_2."' ";
		$db_result_tank = $common_dao->db_query($sql);
		$reserve_2_tank_id = $db_result_tank[0]["dive_center_tank_id"];
		$reserve_2_tank_name = $db_result_tank[0]["dive_center_tank_title"];
		$reserve_2_tank_price = $db_result_tank[0]["dive_center_tank_price"];
		$reserve_price += $reserve_2_tank_price;
	}
	
	//タンク3
	if($s_tank_3!="")
	{
		$sql = "SELECT dive_center_tank_id, dive_center_tank_title, dive_center_tank_price FROM dive_center_tank where flag_open='1' and dive_center_id='".$dive_center_id."' ";
		$sql .= " and dive_center_tank_id='".$s_tank_3."' ";
		$db_result_tank = $common_dao->db_query($sql);
		$reserve_3_tank_id = $db_result_tank[0]["dive_center_tank_id"];
		$reserve_3_tank_name = $db_result_tank[0]["dive_center_tank_title"];
		$reserve_3_tank_price = $db_result_tank[0]["dive_center_tank_price"];
		$reserve_price += $reserve_3_tank_price;
	}
	
	//ポイント
	$point_history = true; //履歴残すため
	$used_point=0;
	if($s_point>0)
	{
		$db_result_point = $common_member -> Fn_member_point($common_dao, $member_id) ;
		if($db_result_point[0]["member_point"]>$arr_dive_center_style_possible_point[$s_style_id])
		{
			$used_point = $s_point;
			$reserve_price -= $s_point;
			
			//ポイント変更
			$db_insert_point = "update member set ";
			$db_insert_point .= " member_point=member_point-".$s_point." ";
			$db_insert_point .= " where member_id='".$member_id."' ";
			$common_dao->db_update($db_insert_point);
			
			$point_history = true;
			
		}
	}

	$datetime = date("Y-m-d H:i:s");
	$status = "0";
	$yyyymmdd = $s_yyyymmdd;
	$reserve_tax = floor($reserve_price*global_tax_percent);
	$reserve_all_price = $reserve_tax+$reserve_price;
	
	$arr_db_field = array("yyyymmdd", "member_id", "user_name", "member_name_1", "member_name_2");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_id", "dive_center_name", "dive_center_point", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("used_point"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_tax", "reserve_all_price", "reserve_comment", "status"));
	
	$db_insert = "insert into dive_reserve ( ";
	$db_insert .= " dive_reserve_id, ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= $val.", ";
	}
	$db_insert .= " regi_date, up_date ";
	$db_insert .= " ) values ( ";
	$db_insert .= " '', ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= " '".$$val."', ";
	}
	$db_insert .= " '$datetime', '$datetime')";
	$db_result = $common_dao->db_update($db_insert);
	
	if ($dive_reserve_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$dive_reserve_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	if($point_history)
	{
		//ポイント履歴
		$common_member -> Fn_member_point_history ($common_dao, $member_id, $dive_reserve_id, $s_yyyymmdd."の予約に使用", "-".$s_point, $_SERVER['PHP_SELF']);
	}
	
	//バディチェックして、データアップ
	$common_member -> Fn_buddy_check_up($common_dao, $member_id, $s_yyyymmdd, $dive_reserve_id) ;

$view_reserve_all_price = number_format($reserve_all_price);

$mail_yyyymmdd = date('Y'.'年'.'m'.'月'.'d'.'日', strtotime($yyyymmdd));
$global_invite_point = number_format(global_invite_point);
$global_invited_point = number_format(global_invited_point);

//Thank youメール
$subject = "【BuddyDive】バディダイブの予約完了しました";

$body = "";
		
$body = <<<EOF
=======================================================
【BuddyDive】バディダイブの予約完了しました
=======================================================

$member_name_1 $member_name_2 様

こんにちは、BuddyDive事務局です。
バディダイブの予約が完了しました。

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
■日付：$mail_yyyymmdd
■ポイント：$dive_center_point （ $dive_center_name ）
■スタイル：$reserve_style_name
■エントリー：$reserve_boat_name
■タンク：$reserve_1_tank_name $reserve_2_tank_name $reserve_3_tank_name
■使用されたポイント：$used_point pt
■料金：$view_reserve_all_price
■レンタルやその他リクエスト：$reserve_comment
＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
ご予約時に入力されたバディの方にご招待のメールをお送りしております。
バディ各自のご予約が必要です。
バディダイビングでは最低2人1組でのエントリーが必須です。
お二人様以上のご予約がない場合は、すでにご予約されたバディの方もダイビングができませんのでご了承ください。

【バディ招待特典】「招待された人」と「招待した人」それぞれにポイントをプレゼントします。
招待された人：$global_invited_point pt
招待した人：$global_invite_point pt

$global_email_footer

EOF;

	if ($member_email != "")
	{
		$common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	}
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
	//ダイビングセンターへ通知メール
$subject = "【BuddyDive】バディダイブの予約が入りました";

$body = "";
		
$body = <<<EOF
=======================================================
【BuddyDive】バディダイブの予約が入りました
=======================================================

$dive_center_name 様

いつもお世話になっております。BuddyDive事務局です。
バディダイブの予約が入りました。

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
■お名前：$member_name_1 $member_name_2 様
■メール：$member_email
■日付：$mail_yyyymmdd
■スタイル：$reserve_style_name
■エントリー：$reserve_boat_name
■タンク：$reserve_1_tank_name $reserve_2_tank_name $reserve_3_tank_name
■使用されたポイント：$used_point pt
■料金：$view_reserve_all_price
■レンタルやその他リクエスト：$reserve_comment
＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

$global_email_footer

EOF;
	if ($dive_center_email != "")
	{
		$common_email-> Fn_send_utf($dive_center_email."<".$dive_center_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	}
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	$common_connect->Fn_redirect("buddy.php?dive_reserve_id=".$dive_reserve_id);
?>
</body>
</html>
