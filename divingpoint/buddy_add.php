<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	$common_member = new CommonMember();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>バディ登録</title>
</head>

<body>
<?
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];

	if ($buddy_id=="" || $dive_reserve_id=="") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	
	//予約したメンバー
	$db_member_info = $common_member -> Fn_member_info($common_dao, $member_id) ;
	if($db_member_info!="")
	{
		$member_email = $db_member_info[0]["member_email"];
		$user_name = $db_member_info[0]["member_name_1"]." ".$db_member_info[0]["member_name_2"];
	}

	//バディ情報
	$db_member_info = $common_member -> Fn_member_info($common_dao, $buddy_id) ;
	if($db_member_info!="")
	{
		$buddy_member_email = $db_member_info[0]["member_email"];
		$buddy_user_name = $db_member_info[0]["member_name_1"]." ".$db_member_info[0]["member_name_2"];
	}
	
	//予約された情報
	$sql = "SELECT  yyyymmdd, dive_center_name, reserve_style_name, reserve_boat_name, reserve_1_tank_name, reserve_2_tank_name FROM dive_reserve where dive_reserve_id='".$dive_reserve_id."' ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$reserve_yyyymmdd = $db_result[0]["yyyymmdd"];
	}
	else
	{
		$common_connect-> Fn_javascript_back("予約情報がありません。");
	}

	//メールチェック
	$sql = "SELECT  member_id FROM member_buddy where buddy_id='".$buddy_id."' and member_id='".$member_id."' and dive_reserve_id='".$dive_reserve_id."' ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$common_connect-> Fn_javascript_move("既に登録されているバディです。", global_ssl."/divingpoint/buddy.php?dive_reserve_id=".$dive_reserve_id);
	}
	
	//予約情報
	$email_detail = "";
	$enter = chr(13)."\n";
	$sql = "SELECT  yyyymmdd, dive_center_id, dive_center_name, dive_center_point, reserve_style_id, reserve_style_name, reserve_boat_id, reserve_boat_name, reserve_1_tank_name, reserve_2_tank_name, reserve_3_tank_name FROM dive_reserve where dive_reserve_id='".$dive_reserve_id."' ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$dive_center_id = $db_result[0]["dive_center_id"];
		$dive_center_point = $db_result[0]["dive_center_point"];
		$reserve_style_id = $db_result[0]["reserve_style_id"];
		$reserve_boat_id = $db_result[0]["reserve_boat_id"];
		$reserve_yyyymmdd = $db_result[0]["yyyymmdd"];
		
		$email_link = global_ssl."/divingpoint/reserve.php?&dive_center_id=".$dive_center_id."&s_yyyymmdd=".str_replace("-", "", $reserve_yyyymmdd)."&s_style_id=".$reserve_style_id."&s_boat_id=".$reserve_boat_id;
		
		$email_detail .= "■日付：".substr($reserve_yyyymmdd, 0, 4)."年".substr($reserve_yyyymmdd, 5, 2)."月".substr($reserve_yyyymmdd, 8, 2)."日".$enter;
		$email_detail .= "■ポイント：".$db_result[0]["dive_center_point"]."（".$db_result[0]["dive_center_name"]."）".$enter;
		if($db_result[0]["reserve_style_name"]!="")
		{
			$email_detail .= "■スタイル：".$db_result[0]["reserve_style_name"].$enter;
		}
		if($db_result[0]["reserve_boat_name"]!="")
		{
			$email_detail .= "■エントリー：".$db_result[0]["reserve_boat_name"].$enter;
		}
		if($db_result[0]["reserve_1_tank_name"]!="")
		{
			$email_detail .= "■タンク1：".$db_result[0]["reserve_1_tank_name"].$enter;
		}
		if($db_result[0]["reserve_2_tank_name"]!="")
		{
			$email_detail .= "■タンク2：".$db_result[0]["reserve_2_tank_name"].$enter;
		}
		if($db_result[0]["reserve_3_tank_name"]!="")
		{
			$email_detail .= "■タンク3：".$db_result[0]["reserve_3_tank_name"].$enter;
		}
	}
	
	//グループIDを探す
	$buddy_group = $common_member -> Fn_buddy_group ($common_dao, $dive_reserve_id) ;
	
	$db_insert = "insert into member_buddy ( ";
	$db_insert .= " buddy_group, buddy_id, dive_reserve_id, member_id, status, regi_date ";
	$db_insert .= " ) values ( ";
	$db_insert .= " '".$buddy_group."','".$buddy_id."','".$dive_reserve_id."','".$member_id."','0', now())";
	$db_result = $common_dao->db_update($db_insert);
	
	//バディへメッセージ
	$db_result_member = $common_member->Fn_member_info ($common_dao, $buddy_id);
	if($db_result_member)
	{
		$db_insert = "insert into member_message ( ";
		$db_insert .= " member_message_id, dive_reserve_id, member_id, from_member_id, regi_date ";
		$db_insert .= " ) values (";
		$db_insert .= "'', '".$dive_reserve_id."','".$buddy_id."','".$member_id."', now())";
		$db_result = $common_dao->db_update($db_insert);
	}
	
	//Thank youメール
	if ($buddy_member_email != "")
	{
		$subject = "【BuddyDive】".$user_name."様よりバディダイブの予約とバディの招待が届きました";
		
		$body = "";
		
$body = <<<EOF
$buddy_user_name 様

こんにちは、BuddyDive事務局です。
$user_name 様よりバディダイブの予約招待が届きました。

内容をご確認のうえ、下記URLよりログインしご予約をお願いします。
$email_link

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
$email_detail
＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
バディ各自のご予約が必要です。
バディダイビングでは最低2人1組でのエントリーが必須です。
お二人様以上おご予約がない場合は、すでにご予約されたバディの方もダイビングができませんのでご了承ください。

$global_email_footer


EOF;

	$common_email-> Fn_send_utf($buddy_user_name."<".$buddy_member_email.">",$subject,$body,$global_mail_from,$global_send_mail);

}
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);


	$common_connect-> Fn_javascript_move("バディを招待しました。", global_ssl."/divingpoint/buddy.php?dive_reserve_id=".$dive_reserve_id);

?>
</body>
</html>