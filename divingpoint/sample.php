
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<meta property="fb:app_id" content="">
<meta property="og:url" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">
<meta property="og:locale" content="">
<meta property="og:type" content="website">
<meta property="og:title" content="">
<meta property="og:image" content="">

<title>ダイビングポイント | - BuddyDive（バディダイブ）</title>

<meta name="description" content="">
<meta name="keywords" content="">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="/common/js/jquery-1.10.2.min.js"><\/script>')</script>

<!--[if lt IE 9]>
<script src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="/common/css/normalize.css">
<link rel="stylesheet" href="/common/css/common.css">
<link rel="stylesheet" href="/common/css/page.css">
<link rel="stylesheet" href="/common/js/slidebars/slidebars.css">
<link rel="stylesheet" href="/common/js/bxslider/jquery.bxslider.css">
<link rel="stylesheet" href="/common/css/sp.css" media="only screen and (max-width:640px)">
</head>
<body>


<div id="inner">

<div class="sb-slidebar sb-left">

<ul>
<li><a href="/diver/login/">ログイン</a><li>
<li><a href="">使い方をみる</a><li>
<li><a href="">ダイビングポイント</a><li>
<li><a href="">今日の海況</a><li>
<li><a href="">特集記事</a><li>
</ul>
<p class="headerBtn"><a href="/diver/registration/?url=/divingpoint/detail.php?dive_center_id=koganezaki">無料ダイバー登録</a></p>
</div>

<div id="sb-site">
<header id="header">
<div class="pc headerIn">
<h1 class="logo"><a href="/"><img src="/common/img/header/logo_blue.png" width="121" height="26" alt="BuddyDive"></a><span class="headTxt">バディダイビングをはじめよう！</span></h1>
<nav>
<ul>
<li class="headerBtn"><a href="/diver/registration/?url=/divingpoint/detail.php?dive_center_id=koganezaki">無料ダイバー登録</a></li>
<li><a href="/diver/login/?url=/divingpoint/detail.php?dive_center_id=koganezaki">ログイン</a><li>
<li><a href="">使い方をみる</a><li>
</ul>
</div>

<div class="navSp sp">
<p class="logoSp"><a href="/"><img src="/common/img/header/logo_blue.png" width="121" height="26" alt="BuddyDive"></a></p>
<p class="menu sb-toggle-left"><img src="/common/img/header/btn_menu.png" width="30" height="34" alt="menu"></p>
</div>
</nav>
</header>

<article>
<ul class="bxslider">
<li><img src="/common/img/divingpoint/koganezaki/img_main01.jpg" alt="" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main02.jpg" alt="施設の目の前がすぐエントリーポイント！　施設を中心に、器材洗い場、干場、ダイビングポイントと、コンパクトにまとまっていて移動が少ないのが嬉しいところ" title="施設の目の前がすぐエントリーポイント！　施設を中心に、器材洗い場、干場、ダイビングポイントと、コンパクトにまとまっていて移動が少ないのが嬉しいところ" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main03.jpg" alt="オーシャンビューのテラス" title="オーシャンビューのテラス" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main04.jpg" alt="ウエットスーツのまま入れる施設内には、お湯やレンジも完備" title="ウエットスーツのまま入れる施設内には、お湯やレンジも完備" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main05.jpg" alt="更衣室" title="更衣室" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main06.jpg" alt="野外シャワーに加えて、室内シャワーも完備" title="野外シャワーに加えて、室内シャワーも完備" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main07.jpg" alt="エントリーポイントの目の前に器材洗い場がある" title="エントリーポイントの目の前に器材洗い場がある" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main08.jpg" alt="スーツを着たまま入れるオーシャナンビューのお風呂" title="スーツを着たまま入れるオーシャナンビューのお風呂" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main09.jpg" alt="水中にケーソンが点在し、ガイドロープでつながれているのでナビゲーションがとてもしやすい" title="水中にケーソンが点在し、ガイドロープでつながれているのでナビゲーションがとてもしやすい" ></li>
<li><img data-layzr="/common/img/divingpoint/koganezaki/img_main10.jpg" alt="水中に持ち込めるマップも配布している" title="水中に持ち込めるマップも配布している" ></li>
</ul>
<style>
.bxlider {
  color: #fff !important;
}
.bxslider img{
  width: 100%;
}

</style>

<div class="box960 mb30">
<section id="mainContents">
<h1 class="divingpointTit">黄金崎公園ビーチ<br><span>Koganezaki Park Beach</span></h1>
<p class="divingpointTxt">西伊豆のほぼ中央に位置し、古くは「新たに開いた里」という意味で「阿羅里」「阿蘭里」と書いた。充実した現地施設や陸上景観の美しさも、多くのダイバーに支持されるポイント。<br><br>
西伊豆の中でも、海況が穏やかな日が多くクローズが少ない。白砂が広がる水中は太陽光が降り注ぐと、とても明るく、潜っているだけで気持ちがいい。普通種からレアものまで豊かな生物相で、少し遠出すると、半水面のホールも楽しめる。<br><br>
水中にケーソンが点在し、ガイドロープでつながれているのでナビゲーションがとてもしやすい。<br>
また、水中に持ち込めるマップも配布している。</p>
<ul class="iconUl">
<tr>
<li><img data-layzr="/common/img/divingpoint/ico_buddy.png" alt="バディ" ><span>バディ</span></li>
<!--<li><img src="/common/img/divingpoint/ico_guide.png" alt="ガイド" ><span>ガイド</span></li>-->
<li><img data-layzr="/common/img/divingpoint/ico_beach.png" alt="ビーチ" ><span>ビーチ</span></li>
<!--<li><img src="/common/img/divingpoint/ico_boat.png" alt="ボート" ><span>ボート</span></li>-->
</ul>
</section><!--mainContents-->

<section id="sideContents">
<div id="divingCenterArea">
<p class="img"><img src="/common/img/divingpoint/koganezaki/img_center.jpg" alt="黄金崎ダイブセンター" ></p>
<h2 class="tit">黄金崎ダイブセンター</h2>
<dl>
<dt>TEL</dt>
<dd>0558-56-1717</dd>
<dt>FAX</dt>
<dd>050-3737-8301</dd>
<dt>Email</dt>
<dd><a href="mailto:dive@arari.co.jp">dive@arari.co.jp</a></dd>
<dt>URL</dt>
<dd><a href="http://www.arari.co.jp" target="_blank">http://www.arari.co.jp</a></dd>
<dt>住所</dt>
<dd>〒410-3501<br>静岡県賀茂郡西伊豆町宇久須2192-1</dd>
<dt>営業時間</dt>
<dd>7:00〜18:00</dd>
</dl>
</div>
<p class="reserveBtn"><a href="/divingpoint/reserve.php?dive_center_id=koganezaki">予約フォームへ</a></p>
<div id="divingPointSNS">
<!--
<ul>
<li class="fsFacebook"><a class="fb btn" href="http://www.facebook.com/share.php?u=http://buddydive.jp/divingpoint/detail.php?dive_center_id=test" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false; ga('send', 'event', 'Click', 'SNS', 'Facebook_act');">シェアする</a></li>
<li class="fsTwitter"><a href="http://twitter.com/share?text=<?php echo タイトル; ?>&hashtags=バディダイブ&url=http://buddydive.jp/divingpoint/detail.php?dive_center_id=test" target="_blank" onClick="ga('send', 'event', 'Click', 'SNS', 'Twitter_act');">ツィートする</a></li>
<li class="fsLine"><a href="http://line.me/R/msg/text/?http://buddydive.jp/divingpoint/detail.php?dive_center_id=test" onClick="ga('send', 'event', 'Click', 'SNS', 'Line_act');">LINEで送る</a></li>
</ul>
-->
</div><!--divingPointSNS-->
</section><!--sideContents-->
</div><!--box960-->

<div id="feeArea">
<div class="box820">
<h2 class="tit">ダイビング料金</h2>
<table class="divingPointTable">
<tr>
<th>スタイル</th>
<td>
<table class="feeTable">
<tr><th>バディダイビング2本</th><td>¥6,500</td></tr>
<tr><th>バディダイビング1本</th><td>¥4,300</td></tr>
</table>
</td>
</tr>
<tr>
<th>エントリー</th>
<td>
<table class="feeTable">
<tr><th>ビーチ</th><td>±¥0</td></tr>
</table>
</td>
</tr>
<tr>
<th>タンク</th>
<td>
<table class="feeTable">
<tr><th>10L</th><td>±¥0</td></tr>
<tr><th>12L</th><td>+¥440</td></tr>
<tr><th>ナイトロックス</th><td>+¥1,000</td></tr>
</table>
</td>
</tr>
<tr>
<th>その他・レンタル等</th>
<td>
駐車場 夏季1,000円、それ以外は無料<br />
【ガイド】<br />
1ダイブ ¥2,700<br />
2ダイブ ¥4,500<br />
【レンタル】<br />
ウエイト1㎏ ¥100<br />
水中ライト ¥800 （電池なし）<br />
３点セット ¥1,500 マスク、フィン、ブーツ、スノーケル<br />
３点セット各一点 ¥500<br />
度付きマスク ¥1,500 ※完全に度が一致しない可能性がございます。<br />
ドライスーツ ¥4,500 ※ドライスーツのレンタルは1週間前までに要予約<br />
ウェットスーツ、レギュ、BC各 ¥2,000<br />
レギュ、ＢＣセット ¥3,000<br />
レギュ、ＢＣ、ウェットセット ¥4,500<br />
レギュ、ＢＣ、ドライスーツセット ¥7,000<br />
フルレンタル（ウェットスーツ） ¥6,000<br />
フルレンタル（ドライスーツ） ¥9,000<br />
【ランチ】<br />
幕の内弁当 ¥800 ※当日8:30までに要予約<br />
黄金膳 ¥1,200 ※２日前までに要予約<br />
鍋焼きうどん（味噌煮込みうどん） ￥1,100 ※当日8:30までに要予約、11月～5月まで      </td>
</tr>
</table>
</div>
</div><!--feeArea-->


<div id="infoArea">
  <div class="box820">
    <h2 class="tit">ダイビングポイント情報</h2>
    <p class="txt">施設から徒歩0分。足場はコンクリートのスロープになっていて、手すりはロープが張られている。ロープがたゆむことがあるので、体重をかけずに体を支える程度に。<br>
また、潮が引くとコンクリートのないゴロタを歩くことになるので、エントリー時は腰のあたりまで水深が来てからフィンを履くか、浅い水深でも早めに横になって泳ぎだそう。いずれにせよ、フィンの着脱はバディで協力するとスムーズ。</p>
<table class="divingPointTable">
<tr>
<th>エントリー／エキジットポイント</th>
<td><img src="/common/img/divingpoint/koganezaki/img_info01.jpg"></td>
</tr>
<tr>
<th>水中マップ <span>[ <a href="/common/img/divingpoint/koganezaki/map.pdf" target="_blank">ダウンロード >></a> ]</span></th>
<td><img src="/common/img/divingpoint/koganezaki/img_info02.jpg"></td>
</tr>
<tr>
<th>エントリー／エキジット時間</th>
<td>4～11月　8：30～16：00<br>
12～3月　8：30～15：30<br>
【ナイトダイビング】<br>
5／15～9／14の土曜日<br>
※20：30が最終EX</td>
</tr>
<tr>
<th>バディダイビングのポイント</th>
<td>水中にケーソンが点在し、ガイドロープでつながれているのでナビゲーションがとてもしやすい。<br>また、水中に持ち込めるマップも配布している。</td>
</tr>
<tr>
<th>推奨スキルや経験</th>
<td>〇オープンウォーターダイバーで履修するスキルが確実にストレスなく行える<br>
〇直線コンパスナビゲーションができる</td>
</tr>
<tr>
<th>必携品</th>
<td>〇コンパス<br>
〇シグナルフロート</td>
</tr>
</table>  </div>
</div><!--infoArea-->


<div id="accessArea">
  <div class="box820">
    <h2 class="tit">アクセス</h2>
    <table class="divingPointTable">
<tr>
<th>①駐車場</th>
<td><p class="mb10">クリスタルパークの角を右に曲がり、小さいトンネルを抜け、すぐ右手に見える広い駐車場が、集
合場所の「根合い」駐車場</p>
<p class="mb10"><img src="/common/img/divingpoint/koganezaki/img_access01.jpg"></p>
<img src="/common/img/divingpoint/koganezaki/img_access02.jpg"></td>
</tr>
<tr>
<th>②電話する</th>
<td>到着し、送迎車への荷物の積み替え準備ができたら0558-56-1717へ電話する。</td>
</tr>
<tr>
<th>③送迎</th>
<td>スタッフが駐車場まで送迎に来る。帰りもスタッフが駐車場まで送迎。<br>
※基本的に、帰るまで車には戻らない</td>
</tr>
</table>  </div>
  <div class="map">
    <div id="mapCanvas"></div>
  </div>
</div><!--accessArea-->


<ul id="pankuzu">
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/divingpoint/">ダイビングポイント</a>&gt;</li>
<li>黄金崎公園ビーチ</li>
</ul>

</article>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>

//Google Maps
function initialize() {
  var latlng = new google.maps.LatLng(34.825031, 138.763651);
  var opts = {
    zoom: 10,
    center: latlng,
    mapTypeControl: true,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  
  var map = new google.maps.Map(document.getElementById("mapCanvas"), opts);
  var styleOptions = [{
    "featureType":"water","stylers":[{"visibility":"on"},{"color":"#cddcec"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]}
  ]

  var monoType = new google.maps.StyledMapType(styleOptions);
  map.mapTypes.set('mono', monoType);
  map.setMapTypeId('mono');
  
  //マーカー
  var image = '/common/img/divingpoint/ico_map.png';
  var Marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: image//デフォルトのマーカーを表示する場合は指定無し
  });
  
  //マップのタイトル
  var contentString = '黄金崎ダイブセンター';
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  //infowindow.open(map, lopanMarker);//初期状態で吹き出しを表示させる場合は有効にする 
  google.maps.event.addListener(Marker, 'click', function() {
    infowindow.open(map, Marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>

<footer>
<div class="footerIn">
<dl class="footerSite">
<dt><a href="">BuddyDiveについて</a></dt>
<dd><a href="">使いかたをみる</a></dd>
<dd><a href="">利用規約</a></dd>
<dd><a href="">プライバシーポリシー</a></dd>
<dd><a href="">安全に対する宣言</a></dd>
<dd><a href="">運営会社</a></dd>
</dl>
<dl class="footerPoint pc">
<dt><a href="">ダイビングポイント</a></dt>
<dd><a href="">菖蒲沢</a></dd>
<dd><a href="">稲取</a></dd>
<dd><a href="">熱川・北川</a></dd>
<dd><a href="">大川</a></dd>
<dd><a href="">赤沢</a></dd>
<dd><a href="">八幡野</a></dd>
<dd><a href="">伊豆海洋公園</a></dd>
<dd><a href="">富戸</a></dd>
<dd><a href="">川奈</a></dd>
<dd><a href="">伊東</a></dd>
<dd><a href="">宇佐美</a></dd>
<dd><a href="">初島</a></dd>
<dd><a href="">網代</a></dd>
<dd><a href="">熱海</a></dd>
<dd><a href="">伊豆山</a></dd>
<dd><a href="">獅子浜</a></dd>
<dd><a href="">大瀬崎</a></dd>
<dd><a href="">井田</a></dd>
<dd><a href="">土肥</a></dd>
<dd><a href="">宇久須</a></dd>
<dd><a href="">安良里</a></dd>
<dd><a href="">田子</a></dd>
<dd><a href="">浮島・堂ヶ島</a></dd>
<dd><a href="">岩地</a></dd>
<dd><a href="">雲見</a></dd>
<dd><a href="">波勝崎</a></dd>
<dd><a href="">子浦妻良</a></dd>
<dd><a href="">中木</a></dd>
<dd><a href="">神子元島</a></dd>
<dd><a href="">下田大浦</a></dd>
<dd><a href="">須崎</a></dd>
</dl>
</div>
<p class="copy">Copyright&copy;2015 EAST BLUE All Rights Reserved.</p>
</footer>

</div><!--sb-site-->
</div><!--wrap-->

<script src="/common/js/script.js"></script>
<!-- bgswitcher -->
<script src="/common/js/jquery.bgswitcher.js"></script>
<script>
  $("#main").bgswitcher({
  images: ["/common/img/index/img_main01.jpg", "/common/img/index/img_main02.jpg", "/common/img/index/img_main03.jpg", "/common/img/index/img_main04.jpg"],
  duration: 3000,
});
</script>
<!-- Slidebars -->
<script src="/common/js/slidebars/slidebars.js"></script>
<script>
  (function($) {
    $(document).ready(function() {
      $.slidebars();
    });
  }) (jQuery);
</script>
<!-- bxslider -->
<script src="/common/js/bxslider/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
  $('.bxslider').bxSlider({
    captions: true,
    auto: true,
    speed: 800,
    mode: 'fade',
    autoHover: true
  });
});
</script>
<script>var layzr = new Layzr({});</script>
</body>
</html>
