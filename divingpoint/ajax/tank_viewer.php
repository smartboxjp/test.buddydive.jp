<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember(); //DB関連
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="/common/js/jquery-1.10.2.min.js"><\/script>')</script>

<link rel="stylesheet" href="/common/css/normalize.css">
<link rel="stylesheet" href="/common/css/common.css">
<link rel="stylesheet" href="/common/css/page.css">
<link rel="stylesheet" href="/common/css/sp.css" media="only screen and (max-width:780px)">
<link rel="stylesheet" href="/common/js/slidebars/slidebars.css">
<script type="text/javascript">
	function fnChangeCal(i) {
		$.ajax({
			type:"GET",
			url:"./ajax/tank_viewer.php?"+i,
			dataType:"html",
			success: function(retun_data){
				$('#label_tank_viewer').html(retun_data);
			},
			error:function(){
				$('#label_data_'+i).html("<br /><font color=red>もう一度選択してください。</font>");
			}
		});
	}
	

</script>
<?
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	//スタイル
	$arr_db_field_style = array("dive_center_style_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price", "tank_count");
	$arr_db_field_style = array_merge($arr_db_field_style, array("possible_point"));
	$arr_db_field_style = array_merge($arr_db_field_style, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_style_id, ";
	foreach($arr_db_field_style as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	$db_result_style = $common_dao->db_query($sql);

	$var = "style";
	for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
	{
		foreach($arr_db_field_style as $val)
		{
			$$val = $db_result_style[$db_loop][$val];
		}
		$arr_tank_count[$dive_center_style_id] = $tank_count;
		$arr_dive_center_style_title[$dive_center_style_id] = $dive_center_style_title;
		$arr_dive_center_style_price[$dive_center_style_id] = $dive_center_style_price;
		$arr_dive_center_style_possible_point[$dive_center_style_id] = $possible_point;
	}
	
	
	//ボート
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
	$arr_db_field_boat = array_merge($arr_db_field_boat, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_boat = $common_dao->db_query($sql);

	$var = "boat";
	for($db_loop=0 ; $db_loop < count($db_result_boat) ; $db_loop++)
	{
		foreach($arr_db_field_boat as $val)
		{
			$$val = $db_result_boat[$db_loop][$val];
		}
		$arr_dive_center_boat_title[$dive_center_boat_id] = $dive_center_boat_title;
		$arr_dive_center_boat_price[$dive_center_boat_id] = $dive_center_boat_price;
	}
	
	//タンク
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field_tank = array_merge($arr_db_field_tank, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_tank = $common_dao->db_query($sql);

	$var = "tank";
	for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
	{
		foreach($arr_db_field_tank as $val)
		{
			$$val = $db_result_tank[$db_loop][$val];
		}
		$arr_dive_center_tank_title[$dive_center_tank_id] = $dive_center_tank_title;
		$arr_dive_center_tank_price[$dive_center_tank_id] = $dive_center_tank_price;
	}
	
	//「スキルできない」場合　当日有効な保険に加入します」をチェック必要
	$sql = "SELECT yes_no FROM member_skill where member_id='".$member_id."' and skill_id=1 ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		//出来ないは$skill_yes_no=="2"
		$skill_yes_no = $db_result[0]["yes_no"];
	}
?>

<script type="text/javascript">
	
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#fbeaea";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			//err_check_count += check_radio("medical");
			
			

			<?
			$var = "flag_hoken";
			?>
			$("#err_<?=$var;?>").html(err_default);
			$("#"+"label_<?=$var;?>").css(background,bgcolor_default);
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				$("#"+"label_<?=$var;?>").css(background,bgcolor_err);
				err ="<p class='error tCenter'>「当日有効な保険に加入」が必要です</p>";
			}
			else
			{
				count++;
			}
			if(err!="")
			{
				$("#err_<?=$var;?>").html(err);
			}
			<? /* 保険加入 end*/ ?>
			
			if(err!="")
			{
				swal("「当日有効な保険に加入」が必要です");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		
	});
	
//-->
</script>
    <form action="/divingpoint/reserve_save.php" method="post">
    	<? $var = "dive_center_id";?>
    <input type="hidden" name="<? echo $var;?>" value=<? echo $$var;?>>
    	<? $var = "s_style_id";?>
    <input type="hidden" name="<? echo $var;?>" value=<? echo $$var;?>>
    	<? $var = "s_boat_id";?>
    <input type="hidden" name="<? echo $var;?>" value=<? echo $$var;?>>
    	<? $var = "s_yyyymmdd";?>
    <input type="hidden" name="<? echo $var;?>" value=<? echo $$var;?>>
    <dl>
        <p>
<table class="tankTable">
<?
	
			//タンク在庫チェック
			if($s_style_id!="" && $s_boat_id!="" && strlen($s_yyyymmdd)==8)
			{
				$sql = "select s_".substr($s_yyyymmdd, 6, 2)." as stock_tank, dive_center_tank_id from stock_tank where yyyymm = '".date('Ym', strtotime($s_yyyymmdd))."' and dive_center_id='".$dive_center_id."' ";

				$db_result = $common_dao->db_query($sql);
				if($db_result)
				{
					for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
					{
						$arr_stock_tank[$db_result[$db_loop]["dive_center_tank_id"]] = $db_result[$db_loop]["stock_tank"];
					}
				}
			}
			
			for($db_loop_tank=1 ; $db_loop_tank <= $arr_tank_count[$s_style_id] ; $db_loop_tank++)
			{
				$var = "s_tank_".$db_loop_tank;
?>
        <tr><th><?=$db_loop_tank;?>本目</th>
        <td>
<?
				for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
				{
					foreach($arr_db_field_tank as $val)
					{
						$$val = $db_result_tank[$db_loop][$val];
					}
?>
					<?
					//在庫がある場合クリック可能
					if($arr_stock_tank[$dive_center_tank_id]>=$db_loop_tank) 
					{
					?>
					<label id="label_<?=$var;?>_<?=$dive_center_tank_id;?>">
          		<?
              if($db_loop_tank==1)
							{
								//デフォルト設定
								if($s_tank_1=="") {$s_tank_1 = $db_result_tank[$db_loop]["dive_center_tank_id"];}
						?>
          		<input type="radio" name="<? echo $var;?>" id="<? echo $var;?>_<? echo $db_loop;?>"  onClick="fnChangeCal('<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, $dive_center_tank_id, $s_tank_2, $s_tank_3, $s_point);?>')" <? if($dive_center_tank_id==$s_tank_1){ echo " checked ";}?> value="<? echo $dive_center_tank_id;?>">
              <?
							}
              elseif($db_loop_tank==2)
							{
								//デフォルト設定
								if($s_tank_2=="") {$s_tank_2 = $db_result_tank[$db_loop]["dive_center_tank_id"];}
						?>
          		<input type="radio" name="<? echo $var;?>" id="<? echo $var;?>_<? echo $db_loop;?>"  onClick="fnChangeCal('<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, $s_tank_1, $dive_center_tank_id, $s_tank_3, $s_point);?>')" <? if($dive_center_tank_id==$s_tank_2){ echo " checked ";}?> value="<? echo $dive_center_tank_id;?>">
              <?
							}
							else
							{
								//デフォルト設定
								if($s_tank_3=="") {$s_tank_3 = $db_result_tank[$db_loop]["dive_center_tank_id"];}
							?>
          		<input type="radio" name="<? echo $var;?>" id="<? echo $var;?>_<? echo $db_loop;?>"  onClick="fnChangeCal('<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, $s_tank_1, $s_tank_2, $dive_center_tank_id, $s_point);?>')" <? if($dive_center_tank_id==$s_tank_3){ echo " checked ";}?> value="<? echo $dive_center_tank_id;?>">
              <?
							}
							?>
              
						<? echo $dive_center_tank_title;?>
						<? if($dive_center_tank_price!=0) { echo "（+".$dive_center_tank_price."）";}?>
					</label>
					<?
					}
					//在庫が無い場合クリック不可
					else 
					{ 
					?>
					<label id="label_<?=$var;?>_<?=$dive_center_tank_id;?>" class="full"><? echo $dive_center_tank_title;?>
							<? if($dive_center_tank_price!=0) { echo "（+".$dive_center_tank_price."）";}?>
					</label>
					<?
					} 
					?>
<?
				}
?>
					<br />
<?
			}
?>
				<span id="err_tank"></span>
          </td></tr>
        </table>
        </p>
      
      <?
				$db_result_point = $common_member -> Fn_member_point($common_dao, $member_id) ;
				if($arr_dive_center_style_possible_point[$s_style_id]!=0)
				{
			?>
      <dt>ポイント</dt>
      <dd class="select">
        <? 
				if($db_result_point[0]["member_point"]>$arr_dive_center_style_possible_point[$s_style_id])
				{
				?>
			<!--▼ポイントが多くて使える-->
				<? $var = "s_point"?>
				<label id="label_<?=$var;?>" class="pointYN">
        		<?
            if($$var>0)
						{
							$point_onclick = 0;
						}
						else
						{
							$point_onclick = $arr_dive_center_style_possible_point[$s_style_id];
						}
					?>
          <input type="checkbox" name="<? echo $var;?>" id="<? echo $var;?>" onClick="fnChangeCal('<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, $s_tank_1, $s_tank_2, $s_tank_3, $point_onclick);?>')" <? if($$var>0){ echo " checked ";}?> value="<? echo $arr_dive_center_style_possible_point[$s_style_id];?>">ポイントを使う
        </label>
				<p><span class="cPink"><? echo number_format($arr_dive_center_style_possible_point[$s_style_id]);?>pt</span>を使って&yen;<? echo number_format($arr_dive_center_style_possible_point[$s_style_id]);?>値引きすることができます。<br>
				<? echo $db_result_point[0]["member_name_1"]." ".$db_result_point[0]["member_name_2"];?>さんの現在のポイントは<span class="cPink"><? echo number_format($db_result_point[0]["member_point"])?>pt</span>です。</p>
				<?
				}
				else
				{
				?>
			<!--▼ポイントが少なくて使えない--> 
				<p><span class="cPink"><? echo number_format($arr_dive_center_style_possible_point[$s_style_id]);?>pt</span>以上のポイントが必要です。<br>
				<? echo $db_result_point[0]["member_name_1"]." ".$db_result_point[0]["member_name_2"];?>さんの現在のポイントは<span class="cPink"><? echo number_format($db_result_point[0]["member_point"])?>pt</span>ですので、使用することはできません。</p>
				<?
				}
				?>
      </dd>
      <?
      }
			?>
        
      <? $sum_price = 0;?>
      <dt>料金</dt>
      <dd>
        <table class="pirceTable">
          <tr>
            <th><? echo $arr_dive_center_style_title[$s_style_id];?></th>
            <td>
            	¥ <? echo number_format($arr_dive_center_style_price[$s_style_id]);?>
              <? $sum_price += $arr_dive_center_style_price[$s_style_id];?>
            </td>
          </tr>
          <tr>
            <th><? echo $arr_dive_center_boat_title[$s_boat_id];?></th>
            <td>
           	 ¥ <? echo number_format($arr_dive_center_boat_price[$s_boat_id]);?>
              <? $sum_price += $arr_dive_center_boat_price[$s_boat_id];?>
            </td>
          </tr>
          <?
					for($db_loop_tank=1 ; $db_loop_tank <= $arr_tank_count[$s_style_id] ; $db_loop_tank++)
					{
						$var = "s_tank_".$db_loop_tank;
					 if($$var!="") 
					 { 
					?>
          <tr>
            <th><?=$db_loop_tank;?>本目：<? echo $arr_dive_center_tank_title[$$var] ;?></th>
            <td>
           	 ¥ <? echo number_format($arr_dive_center_tank_price[$$var]);?>
              <? $sum_price += $arr_dive_center_tank_price[$$var];?>
            </td>
          </tr>
          <?
					 }
					}
					?>
          <tr>
            <th>ポイント</th>
            <td>
            <span class="cPink">- ¥ <? echo number_format($s_point);?></span>
              <? $sum_price -= $s_point;?>
            </td>
          </tr>
          <tr>
            <th>消費税</th>
            <td>
            <? $tax = floor($sum_price*global_tax_percent);?>
            	¥ <? echo number_format($tax);?>
              <? $sum_price += $tax;?>
            </td>
          </tr>
          <tr class="sum">
            <th>合計</th><td>¥ <? echo number_format($sum_price);?></td>
          </tr>
        </table>
      </dd>
      
      <dt>レンタル器材、ガイド、ランチなどのリクエスト</dt>
      <dd>
          <?php $var = "reserve_comment";?>
        <textarea class="txt" id="<?=$var;?>" name="<?=$var;?>"></textarea>
        <span id="err_<?=$var;?>"></span>
      </dd>

      <p class="insuranceCheck">
				<? $var = "flag_hoken";?>
        <label id="label_<?=$var;?>">
          <input type="checkbox" name="<? echo $var;?>" id="<? echo $var;?>" value="1">当日までに有効な保険に加入します
        </label>
        <span id="err_<?=$var;?>" />
      </p>
      <p class="tCenter"><a href="/about/insurance.php" target="_blank">（加入が必要な保険について）</a></p>
      <?
			if($skill_yes_no=="1")
			{
			?>
        <? $var = "form_confirm";?>
      <p class="orangeBtn mt10">
        <input type="submit" value="上記の内容で予約する" name="<? echo $var;?>" id="<? echo $var;?>">
      </p>
      <?
			}
			else
			{
			?>
      		<p class="tCenter mt10">できないスキルがある方はバディダイビングに参加できません<br>
      		<a href="/diver/profile/skill.php?return_url=/divingpoint/reserve.php?dive_center_id=<?php echo $dive_center_id; ?>">（スキルチェックを見直す）</a></p>
		 <?
			}
			?>
    </dl>
  </form>
    
<script src="/common/js/script.js"></script>

<?

	function fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, $s_tank_1, $s_tank_2, $s_tank_3, $s_point)
	{
		$return_link = "";
		if($dive_center_id!="") { $return_link.="&dive_center_id=".$dive_center_id;}
		if($s_style_id!="") { $return_link.="&s_style_id=".$s_style_id;}
		if($s_boat_id!="") { $return_link.="&s_boat_id=".$s_boat_id;}
		if($s_yyyymmdd!="") { $return_link.="&s_yyyymmdd=".$s_yyyymmdd;}
		if($s_tank_1!="") { $return_link.="&s_tank_1=".$s_tank_1;}
		if($s_tank_2!="") { $return_link.="&s_tank_2=".$s_tank_2;}
		if($s_tank_3!="") { $return_link.="&s_tank_3=".$s_tank_3;}
		if($s_point!="") { $return_link.="&s_point=".$s_point;}
		
		return $return_link;
	}
?>