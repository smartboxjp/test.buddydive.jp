<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	$admin_id = $_SESSION['admin_id'];
		
	if($dive_center_id!="")
	{
		$where = " ";
		
		if($admin_id=="")
		{
			$where .= " and flag_open=1 ";
		}

		$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "img_1");
		$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw"));
		$arr_db_field = array_merge($arr_db_field, array("comment_1", "comment_2", "comment_3", "comment_4", "google_map_1", "google_map_2"));
		$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT dive_center_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM dive_center where dive_center_id='".$dive_center_id."'";
		$sql .= $where;
		
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	$meta_title_etc = $dive_center_point. ' | ';
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>


<?
	

	/* スタイル start */
	$arr_db_field = array("dive_center_style_id", "dive_center_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price", "possible_point");
		
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where dive_center_id='".$dive_center_id."'";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_style_title[$dive_center_style_id] = $dive_center_style_title;
			$arr_dive_center_style_price[$dive_center_style_id] = $dive_center_style_price;
			$arr_dive_center_possible_point[$dive_center_style_id] = $possible_point;
			
		}
	}
	/* スタイル end */
	
	
	/* ボート start */
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
		
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where dive_center_id='".$dive_center_id."'";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field_boat as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_boat_title[$dive_center_boat_id] = $dive_center_boat_title;
			$arr_dive_center_boat_price[$dive_center_boat_id] = $dive_center_boat_price;
		}
	}
	/* ボート end */
	
	/* タンク start */
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
		
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field_tank as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_tank_title[$dive_center_tank_id] = $dive_center_tank_title;
			$arr_dive_center_tank_price[$dive_center_tank_id] = $dive_center_tank_price;
		}
	}
	/* タンク end */
	
	
	
?>
<article>

<?
	if($dive_center_name=="")
	{
		$common_connect->Fn_javascript_back("登録されてないIDです。");
	}
	if($comment_1!="")
	{
		echo $comment_1;
	}
?>


<div id="feeArea">
  <div class="box820">
    <h2 class="tit">ダイビング料金</h2>
    <table class="divingPointTable">
    <tr>
    <th>スタイル</th>
    <td>
      <table class="feeTable">
      <?
      foreach($arr_dive_center_style_title as $key=>$value)
			{
			?>
        <tr>
          <th>
						<? echo $value;?>
            <? if($arr_dive_center_possible_point[$key]!=0) { ?>
            <p class="pointTxt"><span class="pointIcon">P</span><? echo number_format($arr_dive_center_possible_point[$key])?>pt使えます</p>
            	<? } ?>
          </th>
          <td>¥<? echo number_format($arr_dive_center_style_price[$key]);?></td>
        </tr>
      <?
			}
      ?>
      </table>
    </td>
    </tr>
    <tr>
    <th>エントリー</th>
    <td>
      <table class="feeTable">
      <?
      foreach($arr_dive_center_boat_title as $key=>$value)
			{
			?>
        <tr><th><? echo $value;?></th><td><? if($arr_dive_center_boat_price[$key]=="0"){ echo "±";} else { echo "+";}?>¥<? echo number_format($arr_dive_center_boat_price[$key]);?></td></tr>
      <?
			}
      ?>
      </table>
    </td>
    </tr>
    <tr>
    <th>タンク</th>
    <td>
      <table class="feeTable">
      <?
      foreach($arr_dive_center_tank_title as $key=>$value)
			{
			?>
        <tr><th><? echo $value;?></th><td><? if($arr_dive_center_tank_price[$key]=="0"){ echo "±";} else { echo "+";}?>¥<? echo number_format($arr_dive_center_tank_price[$key]);?></td></tr>
      <?
			}
      ?>
      </table>
    </td>
    </tr>
<?
	if($comment_2!="")
	{
?>
    <tr>
      <th>その他・レンタル等</th>
      <td>
<?
		echo nl2br($comment_2);
?>
			</td>
    </tr>
<?
	}
?>
    </table>
  </div>
</div><!--feeArea-->


<?
if($comment_3!="")
{
?>
<div id="infoArea">
  <div class="box820">
    <h2 class="tit">ダイビングポイント情報</h2>
    <? echo $comment_3;?>
  </div>
</div><!--infoArea-->
<?
}
?>


<div id="accessArea">
  <div class="box820">
    <h2 class="tit">アクセス</h2>
    <? echo $comment_4;?>
  </div>
<?
	if($google_map_1!="" && $google_map_2!="")
	{
?>
  <div class="map">
    <div id="mapCanvas"></div>
  </div>
<?
	}
?>
</div><!--accessArea-->

<div id="pankuzu">
<ul>
<li><a href="/">HOME</a>&gt;</li>
<li><a href="/divingpoint/">ダイビングポイント</a>&gt;</li>
<li><? echo $dive_center_point;?></li>
</ul>
</div>

</article>


<?
	if($google_map_1!="" && $google_map_2!="")
	{
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>

//Google Maps
function initialize() {
  var latlng = new google.maps.LatLng(<? echo $google_map_1;?>, <? echo $google_map_2;?>);
  var opts = {
    zoom: 10,
    center: latlng,
    mapTypeControl: true,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  
  var map = new google.maps.Map(document.getElementById("mapCanvas"), opts);
  var styleOptions = [{
    "featureType":"water","stylers":[{"visibility":"on"},{"color":"#cddcec"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]}
  ]

  var monoType = new google.maps.StyledMapType(styleOptions);
  map.mapTypes.set('mono', monoType);
  map.setMapTypeId('mono');
  
  //マーカー
  var image = '/common/img/divingpoint/ico_map.png';
  var Marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: image
  });
  
  //マップのタイトル
  var contentString = '<? echo $dive_center_point;?>';
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  //infowindow.open(map, lopanMarker);//初期状態で吹き出しを表示させる場合は有効にする 
  google.maps.event.addListener(Marker, 'click', function() {
    infowindow.open(map, Marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>
<?
	}
?>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
