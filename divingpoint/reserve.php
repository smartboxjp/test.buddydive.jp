<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>

<?
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	//ログイン情報がなければURLを渡す
	if(isset($_SESSION['member_id'])!=true || $_SESSION['member_id'] == '')
	{
		//?を「&&&」に変換して送る
		$common_connect -> Fn_javascript_move("ログインが必要です。", "/diver/login/?url=/divingpoint/reserve.php&&&dive_center_id=".$dive_center_id."&s_yyyymmdd=".$s_yyyymmdd."&s_style_id=".$s_style_id."&s_boat_id=".$s_boat_id);
	}

	if(trim($_SESSION['user_name']) == '')
	{
		$common_connect -> Fn_javascript_move("プロフィール登録が必要です。", "/diver/profile/?url=/divingpoint/reserve.php?dive_center_id=".$dive_center_id);
	}

	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	

	//ログインチェック
	$common_connect -> Fn_member_check_return($_SERVER['PHP_SELF']."?".fn_link($dive_center_id, $s_style_id, $s_boat_id, $pre_yyyymm, "", "", ""));
	
		
	if($dive_center_id!="")
	{
		$where = " ";
		
		if($_SESSION['admin_id']=="")
		{
			$where .= " and flag_open=1 ";
		}

		$arr_db_field = array("dive_center_id", "dive_center_name");
		$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw"));
		$arr_db_field = array_merge($arr_db_field, array("img_1"));
		$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT dive_center_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM dive_center where dive_center_id='".$dive_center_id."'";
		$sql .= $where;
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
		else
		{
			$common_connect->Fn_javascript_back("公開しているダイブセンターがありません。");
		}
	}
	
	//スタイル
	$arr_db_field_style = array("dive_center_style_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price", "tank_count");
	$arr_db_field_style = array_merge($arr_db_field_style, array("possible_point"));
	$arr_db_field_style = array_merge($arr_db_field_style, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_style_id, ";
	foreach($arr_db_field_style as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_style = $common_dao->db_query($sql);
	
	
	//ボート
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
	$arr_db_field_boat = array_merge($arr_db_field_boat, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_boat = $common_dao->db_query($sql);
	
	//タンク
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field_tank = array_merge($arr_db_field_tank, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_tank = $common_dao->db_query($sql);
	
	if($s_yyyymmdd=="")
	{
		$s_yyyymmdd = date('Ymd');
	}
	elseif(strlen($s_yyyymmdd)=="6")
	{
		$s_yyyymmdd = $s_yyyymmdd.date("d");
	}
?>
<script type="text/javascript">
	function fnChangeCal(i) {
		$.ajax({
			type:"GET",
			url:"./ajax/tank_viewer.php?"+i,
			dataType:"html",
			success: function(retun_data){
				$('#label_tank_viewer').html(retun_data);
			},
			error:function(){
				$('#label_data_'+i).html("<br /><font color=red>もう一度選択してください。</font>");
			}
		});
	}
	
	$(function() {
		
		//初期化
		fnChangeCal("<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, "", "", "");?>");
		
		$('input[name=style]').click(function() {
			fn_style_boat();
		});
		
		$('input[name=boat]').click(function() {
			fn_style_boat();
		});

		function fn_style_boat() {
			if($('input[name=style]:checked').val()!=undefined && $('input[name=boat]:checked').val()!=undefined)
			{
				document.location.href = "?<? echo fn_link($dive_center_id, "", "", $s_yyyymmdd, "", "", "");?>&s_style_id="+$('input[name=style]:checked').val()+"&s_boat_id="+$('input[name=boat]:checked').val()+"#reservationBox";
			}
		}
	
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#fbeaea";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			//err_check_count += check_radio("medical");
			
			<? /* style start */ ?>
			<?
			$var = "style";
			?>
			$("#err_<?=$var;?>").html(err_default);
			<?
			for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
			{
			?>
			$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_default);
			<?
			}
			?>
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				<?
				for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
				{
				?>
				$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_err);
				<?
				}
				?>
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			else
			{
				count++;
			}
			if(err!="")
			{
				$("#err_<?=$var;?>").html(err);
			}
			<? /* style end */ ?>
			
			
			<? /* boat start */ ?>
			<?
			$var = "boat";
			?>
			$("#err_<?=$var;?>").html(err_default);
			<?
			for($db_loop=0 ; $db_loop < count($db_result_boat) ; $db_loop++)
			{
			?>
			$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_default);
			<?
			}
			?>
			
			
			<? /* tank start */ ?>
			<?
			$var = "tank";
			?>
			$("#err_<?=$var;?>").html(err_default);
			<?
			for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
			{
			?>
			$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_default);
			<?
			}
			?>
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				<?
				for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
				{
				?>
				$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_err);
				<?
				}
				?>
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			else
			{
				count++;
			}
			if(err!="")
			{
				$("#err_<?=$var;?>").html(err);
			}
			<? /* tank end */ ?>
			
			
			if(err!="")
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		

		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div class='error'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		//全角
		function checkIsZenkaku(value) {
			for (var i = 0; i < value.length; ++i) {
			var c = value.charCodeAt(i);
			//  半角カタカナは不許可
			if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
				return false;
			}
			}
			return true;
		}
		
		// 半角数字チェック
		function checkNumber(value) {
			 if(value.match( /[^0-9]+/ ) ) {
				return false;
			 }
			 return true;
		}
	
		//全角カタカナチェック
		function checkKatakana(value){
			for(i=0; i<value.length; i++){
				if(value.charAt(i) != 'ー')	{
					if(value.charAt(i) < 'ア' || value.charAt(i) > 'ン'){
						return false;
					}
				}
			}
			return true;
		}
	
		//全角ひらがなチェック
		function checkHirakana(value){
			if (value.match(/^[\u3040-\u309F]+$/)) {
				return true;
			} else {
				return false;
			}
		}
		
		//英数半角
		function checkHankaku(value){
			if(value.match(/[^0-9A-Za-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 

	});
	
//-->
</script>



<article id="diverBox">

<div id="reservationBox">
    <p class="pointName"><? echo $dive_center_name;?>予約フォーム</p>
    <dl>

      <dt>スタイル</dt>
      <dd class="select">
<?
			$check_possible_point = false;
			$var = "style";
			for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
			{
				foreach($arr_db_field_style as $val)
				{
					$$val = $db_result_style[$db_loop][$val];
				}
				$arr_tank_count[$dive_center_style_id] = $tank_count;
?>
        <label id="label_<?=$var;?>_<?=$db_loop;?>"><input type="radio" name="<? echo $var;?>" id="<? echo $var;?>_<?=$db_loop;?>" value="<? echo $dive_center_style_id;?>" <? if($s_style_id==$dive_center_style_id) { echo " checked ";}?>>
        <?
        if($possible_point!=0)
				{
					$check_possible_point = true;
				?>
        	<span class="pointYNicon">P</span>
        <?
				}
				?>
				<? echo $dive_center_style_title;?>
        </label>
<?
			}
?>
				<span id="err_<?=$var;?>"></span>
        <? if($check_possible_point) { ?>
				<p><span class="pointYNicon">P</span>ポイントを使用できます。</p>
        <? } ?>
      </dd>

      <dt>エントリー</dt>
      <dd class="select">
<?
			$var = "boat";
			for($db_loop=0 ; $db_loop < count($db_result_boat) ; $db_loop++)
			{
				foreach($arr_db_field_boat as $val)
				{
					$$val = $db_result_boat[$db_loop][$val];
				}
?>
        <label id="label_<?=$var;?>_<?=$db_loop;?>"><input type="radio" name="<? echo $var;?>" id="<? echo $var;?>_<?=$db_loop;?>" value="<? echo $dive_center_boat_id;?>" <? if($s_boat_id==$dive_center_boat_id) { echo " checked ";}?>><? echo $dive_center_boat_title;?></label>
<?
			}
?>
				<span id="err_<?=$var;?>"></span>
      </dd>

<? /*カレンダー start */ ?>
<?
	
	// 現在の年月を取得
	$selected_yyyymm01 = substr($s_yyyymmdd, 0, 6)."01";
	$year = date('Y', strtotime($selected_yyyymm01));
	$month = date('n', strtotime($selected_yyyymm01));
	// 月末日を取得
	$last_day = date('j', mktime(0, 0, 0, $month + 1, 0, $year));
	
	$calendar = array();
	$j = 0;
	 
	// 月末日までループ
	for ($i = 1; $i < $last_day + 1; $i++) {
    // 曜日を取得
    $week = date('w', mktime(0, 0, 0, $month, $i, $year));
    // 1日の場合
    if ($i == 1) {
			// 1日目の曜日までをループ
			for ($s = 1; $s <= $week; $s++) {
				// 前半に空文字をセット
				$calendar[$j]['day'] = '';
				$j++;
			}
    }
 
    // 配列に日付をセット
    $calendar[$j]['day'] = $i;
    $j++;
 
    // 月末日の場合
    if ($i == $last_day) {
			// 月末日から残りをループ
			for ($e = 1; $e <= 6 - $week; $e++) {
				// 後半に空文字をセット
				$calendar[$j]['day'] = '';
				$j++;
			}
    }
	}
	
	if($s_yyyymmdd=="")
	{
		$s_yyyymmdd = date('Ym01');
	}
	
	//前月
	$sql = "SELECT '".$selected_yyyymm01."' - INTERVAL 1 MONTH as yyyymmdd ";
	$db_result = $common_dao->db_query($sql);
	$pre_yyyymm = date('Ym', strtotime($db_result[0]["yyyymmdd"]));
	
	//翌月
	$sql = "SELECT '".$selected_yyyymm01."' + INTERVAL 1 MONTH as yyyymmdd ";
	$db_result = $common_dao->db_query($sql);
	$next_yyyymm = date('Ym', strtotime($db_result[0]["yyyymmdd"]));
	
	//在庫チェック
	if($s_style_id!="" && $s_boat_id!="")
	{
		$sql = "select * from stock_style where yyyymm = '".date('Ym', strtotime($selected_yyyymm01))."' and dive_center_id='".$dive_center_id."' and dive_center_style_id='".$s_style_id."'";
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			for($schDay = 1; $schDay <= $last_day; $schDay ++) {
				$stock_style[$db_result[0]["yyyymm"]][$db_result[0]["dive_center_style_id"]][substr(($schDay+100),1,2)] = $db_result[0][s_.substr(($schDay+100),1,2)];
			}
		}
		
		$sql = "select * from stock_boat where yyyymm = '".date('Ym', strtotime($selected_yyyymm01))."' and dive_center_id='".$dive_center_id."' and dive_center_boat_id='".$s_boat_id."'";
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			for($schDay = 1; $schDay <= $last_day; $schDay ++) {
				$stock_boat[$db_result[0]["yyyymm"]][$db_result[0]["dive_center_boat_id"]][substr(($schDay+100),1,2)] = $db_result[0][s_.substr(($schDay+100),1,2)];
			}
		}
	}
?>
      <? $var = "dd";?>
      <dt id="calendar">日付</dt>
      <dd>
        <table class="calenderTable <? if($s_style_id=="" || $s_boat_id=="") { echo " off ";}?>">
          <tr>
            <th colspan="2" class="brNone"><a href="?<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $pre_yyyymm, "", "", "");?>#calendar">&lt; 前月</a></th>
            <th colspan="3" class="month"><?php echo $month; ?>月</th>
            <th colspan="2"><a href="?<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, $next_yyyymm, "", "", "");?>#calendar">翌月 &gt;</a></th>
          </tr>
          <tr>
            <th>SUN</th>
            <th>MON</th>
            <th>TUE</th>
            <th>WED</th>
            <th>THU</th>
            <th>FRI</th>
            <th>SAT</th>
          </tr>
          <tr>
          <? // class="full" ?>
					<?php $cnt = 0; ?>
					<?php foreach ($calendar as $key => $value): ?>
       
              <td>
              <?php $cnt++; ?>
              <?
							if($value['day']!="")
							{ 
							?>
								<?
                  if(
									$stock_style[date('Ym', strtotime($selected_yyyymm01))][$s_style_id][substr(($value['day']+100), 1, 2)]>0 && 
									$stock_boat[date('Ym', strtotime($selected_yyyymm01))][$s_boat_id][substr(($value['day']+100), 1, 2)]>0 && 
									date('Ym', strtotime($selected_yyyymm01)).substr(($value['day']+100), 1, 2)>=date("Ymd")
									)
                  {
                ?>
                  <label>
										<?php echo $value['day']; ?>
                    <input type="radio" name="<? echo $var;?>" value="<? echo substr(($value['day']+100), 1, 2);?>" onClick="fnChangeCal('<? echo fn_link($dive_center_id, $s_style_id, $s_boat_id, date('Ym', strtotime($selected_yyyymm01)).substr(($value['day']+100), 1, 2), "", "", "");?>')" <? if(date('Ym', strtotime($selected_yyyymm01)).substr(($value['day']+100), 1, 2)==$s_yyyymmdd){ echo " checked ";} ?>>
                  </label>
								<?
									}
									else
									{
										echo "<span>".$value['day']."</span>";
									}
							}
								?>
              </td>
       
          <?php if ($cnt == 7): ?>
          </tr>
          <tr>
          <?php $cnt = 0; ?>
          <?php endif; ?>
       
          <?php endforeach; ?>
          </tr>
        </table>
      </dd>
<? /*カレンダー end */ ?>
      
      
      <dt>タンク</dt>
      <dd class="select">
        <span id="label_tank_viewer">スタイル、エントリー、日付を選択してください。</span>
      </dd>
    </dl>
      
      
	<p class="tCenter">※ダイビング料金はダイビング終了後に現地にてご精算となります。</p>
</div>

</article>

<?

	function fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd, $s_tank_1, $s_tank_2, $s_tank_3)
	{
		$return_link = "";
		if($dive_center_id!="") { $return_link.="&dive_center_id=".$dive_center_id;}
		if($s_style_id!="") { $return_link.="&s_style_id=".$s_style_id;}
		if($s_boat_id!="") { $return_link.="&s_boat_id=".$s_boat_id;}
		if($s_yyyymmdd!="") { $return_link.="&s_yyyymmdd=".$s_yyyymmdd;}
		if($s_tank_1!="") { $return_link.="&s_tank_1=".$s_tank_1;}
		if($s_tank_2!="") { $return_link.="&s_tank_2=".$s_tank_2;}
		if($s_tank_3!="") { $return_link.="&s_tank_3=".$s_tank_3;}
		
		return $return_link;
	}
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
