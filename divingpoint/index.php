<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>

<article>
  
  <section class="innerBox">
    <div class="inBox">
      <div class="titBox">
        <h1 class="tit">ダイビングポイント</h1>
      </div>

<?php
	$s_cate_area_id = $common_connect->h($_GET["s_cate_area_id"]);

	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
    if($s_cate_area_id != "")
    {
        $where .= " and cate_area_id='".$s_cate_area_id."' ";
    }

	$where .= " and flag_open='1' ";
	
	//合計
	$sql_count = "SELECT count(dive_center_id) as all_count FROM dive_center where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "img_1");
	$arr_db_field = array_merge($arr_db_field, array("comment_list"));
	$arr_db_field = array_merge($arr_db_field, array("flag_boat", "flag_beach", "flag_guide"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by rand() ";
	}
	$sql .= " limit $offset,$view_count";
	
?>

      <ul class="twoTxtList">
<?php
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
		for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
?>
        <li><a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>"><img src="/<? echo global_dive_center_dir.$dive_center_id."/".$img_1;?>" alt="<? echo $dive_center_point;?>">
            <ul class="divingpointIcon">
            <!--▼ガイド ビーチ ボート有無-->
							<? if($flag_boat=="1") { ?>
              <li><img src="/common/img/divingpoint/ico_boat.png" alt="ボート" ></li>
              <? } ?>
							<? if($flag_beach=="1") { ?>
              <li><img src="/common/img/divingpoint/ico_beach.png" alt="ビーチ" ></li>
              <? } ?>
							<? if($flag_guide=="1") { ?>
              <li><img src="/common/img/divingpoint/ico_guide.png" alt="ガイド" ></li>
              <? } ?>
            <!--▲ガイド ビーチ ボート有無-->
              <li><img src="/common/img/divingpoint/ico_buddy.png" alt="バディ" ></li>
            </ul>
            <span><? echo $dive_center_point;?></span> <span class="subTxt"><? echo $dive_center_name;?></span><br>
            <!--▼デフォルト料金-->
            <? echo $comment_list;?>
            <!--▲デフォルト料金--></a>
          </li>
<?
		}
?>
      </ul>
<?
	}
?>
    </div>
  </section><!--contentsBox-->
  
  <div id="pankuzu">
    <ul>
      <li><a href="/">HOME</a>&gt;</li>
      <li>ダイビングポイント</li>
    </ul>
  </div>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
