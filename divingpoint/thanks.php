<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>

<?
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
		
	if($dive_center_id!="")
	{
		$where = " ";
		
		if($admin_id=="")
		{
			$where .= " and flag_open=1 ";
		}

		$arr_db_field = array("dive_center_id", "dive_center_name");
		$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw"));
		$arr_db_field = array_merge($arr_db_field, array("img_1"));
		$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT dive_center_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM dive_center where dive_center_id='".$dive_center_id."'";
		$sql .= $where;
		
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	//スタイル
	$arr_db_field_style = array("dive_center_style_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price", "tank_count");
	$arr_db_field_style = array_merge($arr_db_field_style, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_style_id, ";
	foreach($arr_db_field_style as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_style = $common_dao->db_query($sql);
	
	
	//ボート
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
	$arr_db_field_boat = array_merge($arr_db_field_boat, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_boat = $common_dao->db_query($sql);
	
	//タンク
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field_tank = array_merge($arr_db_field_tank, array("view_level", "flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where flag_open='1' and dive_center_id='".$dive_center_id."' ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result_tank = $common_dao->db_query($sql);
?>
<script type="text/javascript">
	function fnChangeCal(i) {
		$.ajax({
			type:"GET",
			url:"./ajax/tank_viewer.php?"+i,
			dataType:"html",
			success: function(retun_data){
				$('#label_tank_viewer').html(retun_data);
			},
			error:function(){
				$('#label_data_'+i).html("<br /><font color=red>もう一度選択してください。</font>");
			}
		});
	}
	
	$(function() {
		
		$('#user_name_email_check_1').click(function() {
			if($('#user_name_email_1').val()=="")
			{
				$("#user_name_email_1_viewer").html("<span style='background-color:#FFCCCC;'>姓名 or Emailを入力してください。</span>");
			}
			else
			{
				$.ajax({
					type:"GET",
					url:"user_name_email_check.php?user_name_email=" + $('#user_name_email_1').val(),
					dataType:"html",
					success: function(retun_data){
						$("#user_name_email_1_viewer").html(retun_data);
					},
					error:function(){
						//alert("error");
						$("#user_name_email_1_viewer").html("検索したバディのダイバー登録がありません。お名前とメールアドレスを入力して招待してください。");
					}
				});
			}
		});
		
		
		$('#user_name_email_check_2').click(function() {
			if($('#user_name_email_2').val()=="")
			{
				$("#user_name_email_2_viewer").html("<span style='background-color:#FFCCCC;'>姓名 or Emailを入力してください。</span>");
			}
			else
			{
				$.ajax({
					type:"GET",
					url:"user_name_email_check.php?user_name_email=" + $('#user_name_email_2').val(),
					dataType:"html",
					success: function(retun_data){
						$("#user_name_email_2_viewer").html(retun_data);
					},
					error:function(){
						//alert("error");
						$("#user_name_email_2_viewer").html("検索したバディのダイバー登録がありません。お名前とメールアドレスを入力して招待してください。");
					}
				});
			}
		});
		
		
		$('#user_name_email_check_3').click(function() {
			if($('#user_name_email_3').val()=="")
			{
				$("#user_name_email_3_viewer").html("<span style='background-color:#FFCCCC;'>姓名 or Emailを入力してください。</span>");
			}
			else
			{
				$.ajax({
					type:"GET",
					url:"user_name_email_check.php?user_name_email=" + $('#user_name_email_3').val(),
					dataType:"html",
					success: function(retun_data){
						$("#user_name_email_3_viewer").html(retun_data);
					},
					error:function(){
						//alert("error");
						$("#user_name_email_3_viewer").html("検索したバディのダイバー登録がありません。お名前とメールアドレスを入力して招待してください。");
					}
				});
			}
		});
		
		
		$('input[name=style]').click(function() {
			fn_style_boat();
		});
		
		$('input[name=boat]').click(function() {
			fn_style_boat();
		});
		
		function fn_style_boat() {
			if($('input[name=style]:checked').val()!=undefined && $('input[name=boat]:checked').val()!=undefined)
			{
				document.location.href = "?<? echo fn_link($dive_center_id, "", "", $s_yyyymmdd);?>&s_style_id="+$('input[name=style]:checked').val()+"&s_boat_id="+$('input[name=boat]:checked').val()+"#reservationBox";
			}
		}
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#fbeaea";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			//err_check_count += check_radio("medical");
			
			<? /* style start */ ?>
			<?
			$var = "style";
			?>
			$("#err_<?=$var;?>").html(err_default);
			<?
			for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
			{
			?>
			$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_default);
			<?
			}
			?>
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				<?
				for($db_loop=0 ; $db_loop < count($db_result_style) ; $db_loop++)
				{
				?>
				$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_err);
				<?
				}
				?>
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			else
			{
				count++;
			}
			if(err!="")
			{
				$("#err_<?=$var;?>").html(err);
			}
			<? /* style end */ ?>
			
			
			<? /* boat start */ ?>
			<?
			$var = "boat";
			?>
			$("#err_<?=$var;?>").html(err_default);
			<?
			for($db_loop=0 ; $db_loop < count($db_result_boat) ; $db_loop++)
			{
			?>
			$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_default);
			<?
			}
			?>
			
			
			<? /* tank start */ ?>
			<?
			$var = "tank";
			?>
			$("#err_<?=$var;?>").html(err_default);
			<?
			for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
			{
			?>
			$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_default);
			<?
			}
			?>
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				<?
				for($db_loop=0 ; $db_loop < count($db_result_tank) ; $db_loop++)
				{
				?>
				$("#"+"label_<?=$var;?>_<?=$db_loop;?>").css(background,bgcolor_err);
				<?
				}
				?>
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			else
			{
				count++;
			}
			if(err!="")
			{
				$("#err_<?=$var;?>").html(err);
			}
			<? /* tank end */ ?>
			
			
			
			if(err!="")
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		

		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div class='error'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		//全角
		function checkIsZenkaku(value) {
			for (var i = 0; i < value.length; ++i) {
			var c = value.charCodeAt(i);
			//  半角カタカナは不許可
			if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
				return false;
			}
			}
			return true;
		}
		
		// 半角数字チェック
		function checkNumber(value) {
			 if(value.match( /[^0-9]+/ ) ) {
				return false;
			 }
			 return true;
		}
	
		//全角カタカナチェック
		function checkKatakana(value){
			for(i=0; i<value.length; i++){
				if(value.charAt(i) != 'ー')	{
					if(value.charAt(i) < 'ア' || value.charAt(i) > 'ン'){
						return false;
					}
				}
			}
			return true;
		}
	
		//全角ひらがなチェック
		function checkHirakana(value){
			if (value.match(/^[\u3040-\u309F]+$/)) {
				return true;
			} else {
				return false;
			}
		}
		
		//英数半角
		function checkHankaku(value){
			if(value.match(/[^0-9A-Za-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 

	});
	
//-->
</script>

<?
	if($s_yyyymmdd=="")
	{
		$s_yyyymmdd = date('Ymd');
	}
	elseif(strlen($s_yyyymmdd)=="6")
	{
		$s_yyyymmdd = $s_yyyymmdd.date("d");
	}
	
?>


<article>

<div id="reservationBox">
  <form action="/diver/reserved.php">
    <p class="pointName"><? echo $dive_center_name;?>黄金崎公園ビーチへの予約完了しました</p>
		<table class="thanksTable">
		<tr>
			<th>ダイビングポイント</th>
			<td>黄金崎公園ビーチ<br><img src="/common/img/divingpoint/0_img_center.png" width="300"></td>
		</tr>
		<tr>
			<th>スタイル</th>
			<td>バディダイビング2本</td>
		</tr>
		<tr>
			<th>エントリー</th>
			<td>ビーチ2本</td>
		</tr>
		<tr>
			<th>日付</th>
			<td>2015年9月1日（火）</td>
		</tr>
		<tr>
			<th>タンク</th>
			<td>14L , 10L</td>
		</tr>
		<tr>
			<th>レンタルやその他リクエスト</th>
			<td>レギュ、BCセットのレンタルお願いします。<br>身長170cm、体重60kg</td>
		</tr>
		<tr>
			<th>バディ</th>
			<td>
				<ul class="buddyEntry">
				<li><img src="/app_photo/member/4/4__1.jpg" width="140" height="140"><span>秋元 智道</span></li>
				<li><img src="/app_photo/member/4/4__1.jpg" width="140" height="140"><span>秋元 智道</span></li>
				</ul>
				<p>※バディの方もご自身での登録とご予約が必要です。</p>
			</td>
		</tr>
		</table>
    <p class="blueBtn mt10">
    	<a href="/diver/">マイページへ</a>
    </p>
  </form>
</div>

</article>

<?

	function fn_link($dive_center_id, $s_style_id, $s_boat_id, $s_yyyymmdd)
	{
		$return_link = "";
		if($dive_center_id!="") { $return_link.="&dive_center_id=".$dive_center_id;}
		if($s_style_id!="") { $return_link.="&s_style_id=".$s_style_id;}
		if($s_boat_id!="") { $return_link.="&s_boat_id=".$s_boat_id;}
		if($s_yyyymmdd!="") { $return_link.="&s_yyyymmdd=".$s_yyyymmdd;}
		
		return $return_link;
	}
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
