<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	if($member_id!="")
	{
		$arr_db_field = array("user_name", "member_email", "member_login_pw");
		$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana", "member_point"));
		$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
		$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
			
		$sql = "SELECT member_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " regi_date FROM member where member_id='".$member_id."'";
		$sql .= " and flag_hontouroku='1'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	//ステータスをArrayへ
	$sql = "SELECT cate_status_id, cate_status_title FROM cate_status order by view_level";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_status_title[$db_result[$db_loop][cate_status_id]] = $db_result[$db_loop][cate_status_title];
		}
	}
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>


<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("name");
			err_check_count += check_input_email("email");
			err_check_count += check_input("comment");
			
			if(err_check_count!=0)
			{
				swal("error","入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span style='color:#cb112b;'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						
	});
	
//-->
</script>

<article>
<div id="diverBox">
<section class="mypageCont">
<p class="tit">ご予約やバディについての変更・ご要望・ご質問<br>
<span class="fs14">ご予約の変更やバディ同士の予約連携などのご要望・ご質問がございましたら下記フォームよりご連絡ください。</p>
	<? $var = "form_regist";?>
  <form action="index_save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" enctype="multipart/form-data">
	<input name="name" id="name" type="hidden" value="<? echo $member_name_1 . $member_name_2 ;?>">
	<input name="email" id="email" type="hidden" value="<? echo $member_email;?>">
    <table class="formTable">
      <tr>
        <th>ご要望・ご質問</th>
        <td>
          <? $var = "comment";?>
          <textarea name="<?=$var;?>" id="<?=$var;?>" type="text" class="textInput" rows="10"></textarea>
          <label id="err_<?=$var;?>"></label>
        </td>
      </tr>
    </table>

    <? $var = "form_confirm";?>
    <p class="blueBtn mt20"><input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="送信する"></p>
    <? $var = "return_url";?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
  </form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>