<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<article>
<div id="diverBox">
<section class="accountBox">
<p class="tit">ご予約やバディについての変更・ご要望・ご質問</p>
<p class="mt20">ご予約やバディについての変更・ご要望・ご質問を受け付けました。<br>サポートデスクよりご連絡させていただきます。</p>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>