<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<article>
<div id="diverBox">
<section class="accountBox">
<p class="tit">お問い合せ完了</p>
<p class="mt20">お問い合せありがとうございました。返信を希望されている方には弊社よりご連絡いたします。</p>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>