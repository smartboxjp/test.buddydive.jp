<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>


<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("name");
			err_check_count += check_input_email("email");
			err_check_count += check_input("comment");
			
			if(err_check_count!=0)
			{
				swal("error","入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span style='color:#cb112b;'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span style='color:#cb112b;'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span style='color:#cb112b;'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
						
	});
	
//-->
</script>

<article>
<div id="diverBox">
<section class="mypageCont">
<p class="tit">お問い合わせ<br>
<span class="fs14">お客様よりよくいただく質問を掲載しています。ご参照ください。<br><a href="/about/faq/" class="borderLink mt10">よくある質問 &gt;&gt;</a></span></p>
	<? $var = "form_regist";?>
  <form action="index_save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" enctype="multipart/form-data">
    <table class="formTable">
      <tr>
        <th>お名前</th>
        <td>
          <? $var = "name";?>
          <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="お名前" class="textInput" value="<? echo $$var;?>">
          <label id="err_<?=$var;?>"></label>
        </td>
      </tr>
      <tr>
        <th>メールアドレス</th>
        <td>
          <? $var = "email";?>
          <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="メールアドレス" class="textInput" value="<? echo $$var;?>">
          <label id="err_<?=$var;?>"></label>
        </td>
      </tr>
      <tr>
        <th>お問い合わせ内容</th>
        <td>
          <? $var = "comment";?>
          <textarea name="<?=$var;?>" id="<?=$var;?>" type="text" class="textInput" rows="10"></textarea>
          <label id="err_<?=$var;?>"></label>
        </td>
      </tr>
    </table>
    <? $var = "form_confirm";?>
    <p class="blueBtn mt20"><input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="問い合わせする"></p>
    
		<? $var = "return_url";?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
  </form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>