<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>


<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($name=="" || $email=="" || $comment=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
//ユーザー
if($email!="")
{
	$subject = "『BuddyDive』にお問い合わせありがとうございました。";
	
	$body = "";
		
$body = <<<EOF
$name 様

『Buddy Dive』にお問い合わせがありがとうございました。
下記の内容でお問い合わせが送信されています。

──────────────────────────────

--------------------------------------------------
お問い合わせ内容
--------------------------------------------------
【お名前】 $name
【メールアドレス】 $email
【お問い合わせ内容】
$comment


【受 付 日】$datetime

──────────────────────────────

$global_email_footer


EOF;
	$common_email-> Fn_send_utf($email."<".$email.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
}

//管理者
	$subject = "『BuddyDive』にお問い合わせがありました。";
	
	$body = "";
		
$body = <<<EOF
『Buddy Dive』にお問い合わせがありました。

──────────────────────────────

--------------------------------------------------
お問い合わせ内容
--------------------------------------------------
【お名前】 $name
【メールアドレス】 $email
【お問い合わせ内容】
$comment


【受 付 日】$datetime

──────────────────────────────

$global_email_footer
EOF;
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
	$global_bcc_mail = "support@buddydive.jp";
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	
	$common_connect-> Fn_redirect("./thankyou.php");
?>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>