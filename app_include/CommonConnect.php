<?php

class CommonConnect{ // extends CommonDao

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	public function Fn_admin_check()
	{
		if(isset($_SESSION['admin_id'])!=true || $_SESSION['admin_id'] == '')
		{
			$this -> Fn_javascript_move("管理者専用ページです", "/master/login/");
		}
	}
	
	public function Fn_dive_center_check()
	{
		if(isset($_SESSION['dive_center_id'])!=true || $_SESSION['dive_center_id'] == '')
		{
			$this -> Fn_javascript_move("管理者専用ページです", "/dive_center/login/");
		}
	}
	
	public function Fn_member_check()
	{
		if(isset($_SESSION['member_id'])!=true || $_SESSION['member_id'] == '')
		{
			$this -> Fn_javascript_move("ログインが必要です。", "/diver/login/");
		}
	}
	
	public function Fn_member_check_return($return_url)
	{
		if(isset($_SESSION['member_id'])!=true || $_SESSION['member_id'] == '')
		{
			$this -> Fn_javascript_move("ログインが必要です。", "/diver/login/?url=".$return_url);
		}
	}
	
	public function Fn_javascript_delete($err, $return_url)
	{
?>
<script language="javascript"> 
	function fnChangeDel(i) { 
	swal({
            title:"<? echo $err;?>", // タイトル文
            //text:"Your will not be able to recover this imaginary file!", //説明文
            type:"warning", // default:null "warning","error","success","info"
            allowOutsideClick:false, // default:false アラートの外を画面クリックでアラート削除
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#42bfe8", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: false // default:true 確認ボタンを押したらアラートが削除される
            },
            function(){
                document.location.href = '<? echo $return_url;?>'+i;
            }
        );
	}
</script>
<?
	}
	
	
	public function Fn_javascript_delete_reserve($err, $return_url)
	{
?>
<script language="javascript"> 
	function fnReserveDel(i) { 
	swal({
            title:"<? echo $err;?>", // タイトル文
            //text:"Your will not be able to recover this imaginary file!", //説明文
            type:"warning", // default:null "warning","error","success","info"
            allowOutsideClick:false, // default:false アラートの外を画面クリックでアラート削除
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#42bfe8", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: false // default:true 確認ボタンを押したらアラートが削除される
            },
            function(){
                document.location.href = '<? echo $return_url;?>'+i;
            }
        );
	}
</script>
<?
	}
	

	public function Fn_javascript_delete_2($err, $return_url_1, $return_url_2)
	{
?>
<script language="javascript"> 
	function fnChangeDel(i, j) { 
	swal({
            title:"<? echo $err;?>", // タイトル文
            //text:"Your will not be able to recover this imaginary file!", //説明文
            type:"warning", // default:null "warning","error","success","info"
            allowOutsideClick:false, // default:false アラートの外を画面クリックでアラート削除
            showCancelButton: true, // default:false キャンセルボタンの有無
            confirmButtonText:"", // default:"OK" 確認ボタンの文言
            confirmButtonColor: "#42bfe8", // default:"#AEDEF4" 確認ボタンの色
            cancelButtonText:"キャンセル", // キャンセルボタンの文言
            closeOnConfirm: false // default:true 確認ボタンを押したらアラートが削除される
            },
            function(){
                document.location.href = '<? echo $return_url_1;?>'+i+'&<? echo $return_url_2;?>'+j;
            }
        );
	}
</script>
<?
	}

	/* すべて半角に変換 */
	public function Fn_shiftjis($str)
	{
		$str= mb_convert_kana($str,"rnask","UTF-8");
	
		return $str;
		/* すべて全角に変換 */
		//$str = mb_convert_kana($str,"RNASKV","EUC-JP");
	}

	//メールチェック
	public function Fn_valid_email($mail_address)
	{
		if (!preg_match('/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$mail_address))
			return false;
		else
			return true;
	}
	
	/*
	//Javascriptのhistoryback
	public function Fn_javascript_back($message)
	{
		echo ("
			<script src=\"/common/js/sweetalert/sweetalert.min.js\"></script>
			<link rel=\"stylesheet\" href=\"/common/js/sweetalert/sweetalert.css\">

			<SCRIPT LANGUAGE=JavaScript>
			<!--
			swal('$message');
			history.back();
			//-->
			</SCRIPT>
		");
		exit;
	}
	*/
	
	//Javascriptのhistoryback
	public function Fn_javascript_back($message)
	{
		echo ("
			<script src=\"/common/js/sweetalert/sweetalert.min.js\"></script>
			<link rel=\"stylesheet\" href=\"/common/js/sweetalert/sweetalert.css\">

			<SCRIPT LANGUAGE=JavaScript>
			<!--
			swal({title:'$message', closeOnConfirm: false}, 
				function(){
					history.back();
				}
			);
			//-->
			</SCRIPT>
		");
		exit;
	}
	
	//Javascriptのmove
	public function Fn_javascript_move($message1, $message2)
	{
		echo ("
			<script src=\"/common/js/sweetalert/sweetalert.min.js\"></script>
			<link rel=\"stylesheet\" href=\"/common/js/sweetalert/sweetalert.css\">
			<SCRIPT LANGUAGE=JavaScript>
			<!--
			swal({title:'$message1', closeOnConfirm: false}, 
				function(){
					document.location.href = '$message2';
				}
			);
			//-->
			</SCRIPT>
		");
		exit;
	}

	//URL移動
	public function Fn_redirect($url)
	{
		echo ("<meta http-equiv='Refresh' content='0; URL=$url'>");
		exit;
	}
	
	//文字化け対策
	//input [real_escape_string] -> output[htmlspecialchars]
  public function str_htmlspecialchars($string){
  	return htmlspecialchars($string);
  }
  
	public function h($string){
		return htmlspecialchars(trim($string), ENT_QUOTES);
	}

	//SQL injection対策
	public function Fn_filter($str) 
	{
		$str=htmlspecialchars($str); //特殊文字を HTML エンティティに変換 例）&→&amp;
		$str=strip_tags($str); //html tag delete
		$str=addslashes($str);
		$str=mysql_real_escape_string($str);//文字列の特殊文字をエスケープ
		return $str;
	}

	
	//時間変更
	public function Fn_date($str1)
	{
		return substr($str1,0,4)."年".substr($str1,5,2)."月".substr($str1,8,2)."日(".substr($str1,11,2).":".substr($str1,17,2).")";
	}
	
	//時間変更
	public function Fn_date_day($str1)
	{
		switch(date("w", strtotime($str1))){ 
			case "0": 
			$view_day="日"; 
			break; 
			case "1": 
			$view_day="月"; 
			break; 
			case "2": 
			$view_day="火"; 
			break; 
			case "3": 
			$view_day="水"; 
			break; 
			case "4": 
			$view_day="木"; 
			break; 
			case "5": 
			$view_day="金"; 
			break; 
			case "6": 
			$view_day="土"; 
			break; 
		}
		return $view_day;
	}
	
	//paging
	public function Fn_paging($view_count, $all_count)
	{
      if ($view_count < $all_count)
      {
        foreach($_GET as $key => $value){ 
          if($key!="page")
          {
            $query .= "&".$key."=".trim($value);
          }
        }
  
        for ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
        {
          if ($i == $page)
          {
            echo "[ ".$i." ]";
          }
          else
          {
            echo "[ <a href=".$_SERVER["PHP_SELF"]."?page=".$i.$query."#form_search>".$i."</a> ]";
          }
        }
      }
			
	}
	
	
	//paging
	public function Fn_paging_10($view_count, $all_count)
	{
		

        foreach($_GET as $key => $value){ 
          if($key!="page")
          {
            $query .= "&".$key."=".trim($value);
          }
        }
		$page = $_GET["page"];
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		
		echo "<div class=\"paging\">";
		echo "<ul>";

		If ($page!="1")
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page-1).$query."#search_list\">←前</a></li>";
		}
	
		If ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page+1).$query."#search_list\">次→</a></li>";
		}
		echo "</ul>";
		echo "</div><!-- /.paging -->";
	}
	
	//paging 一覧
	public function Fn_paging_10_list($view_count, $all_count, $query)
	{
		foreach($_GET as $key => $value){ 
			if($key!="page" && $key != "submit_button" && $key != "arr_genre" && $key != "arr_area_m")
			{
				$query .= "&".$key."=".trim($value);
			}
		}
		$page = $_GET["page"];
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		
		echo "<div class=\"paging\">";
		echo "<ul>";

		If ($page!="1")
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page-1).$query."#search_list\">←前</a></li>";
		}
	
		If ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page+1).$query."#search_list\">次→</a></li>";
		}
		echo "</ul>";
		echo "</div><!-- /.paging -->";
	}

	//フォルダ全て削除
	public function Fn_deldir($dir)
	{
		$handle = opendir($dir);
		while (false!==($FolderOrFile = readdir($handle)))
		{
			if($FolderOrFile != "." && $FolderOrFile != "..") 
			{ 
		
				if(is_dir("$dir/$FolderOrFile")) 
				{ $this -> Fn_deldir("$dir/$FolderOrFile"); } // recursive
					else
				{ unlink("$dir/$FolderOrFile"); }
			} 
		}
		closedir($handle);
		if(rmdir($dir))
		{ $success = true; }
		return $success; 
		
	} 
	
	//フォルダ内古いファイル削除
	//$deadline = 24*60*60;  //削除期限（指定秒数以上経過で削除）
	//$save_dir = $global_path.global_temp_img."/";
	public function Fn_old_delete($path, $deadline)
	{
		
		$count = 0;
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if (is_file($path."/".$file)) {
					if((time() - filemtime($path."/".$file) > $deadline)){
						if(unlink($path."/".$file)){
							//echo("{$file}を削除しました。<br />\n");
							//$count += 1;
						}else{
							//echo("{$file}の削除に失敗しました。<br />\n");
						}
					}else{
						//echo "{$file}は削除しませんでした。<br />\n";
					}
				}else{
					//echo("{$file}はファイルではありません。<br />\n");
				}
			}
			closedir($handle);
		}
		
		//echo("{$count}ファイル削除しました。");
	}
	
	//文字制限
	public function Fn_shot_string($str, $len)
	{
		if(mb_strlen($str, "UTF-8")>=$len)
		{
			$return_str = mb_substr($str, 0, $len, "UTF-8")."...";
		}
		else 
		{
			$return_str = $str;
		}
		
		return $return_str;
	}
	
		
	//directory内ファイルコピー
	public function copyDirectory($imageDir, $destDir)
	{
		$handle=opendir($imageDir);   
		while($filename=readdir($handle))
		{       
			if(strcmp($filename,".")!=0	&& strcmp($filename,"..")!=0)
		 {
			 if(is_dir("$imageDir/$filename"))
			 {
				if(!empty($filename) && !file_exists("$destDir/$filename"))
				mkdir("$destDir/$filename");
				copyDirectory("$imageDir/$filename","$destDir/$filename");
			 }
		 else
			 {
				if(file_exists("$destDir/$filename"))
				unlink("$destDir/$filename");
				copy("$imageDir/$filename","$destDir/$filename");
			 }
		 }
		}     
	}
	
	//ランダム生成
	public function Fn_random_password($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyz1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}
	
	//ショップPV
	public function Fn_shop_pv($shop_id)
	{	
		global $common_dao;
		
		if ($shop_id!="")
		{
			if (ereg("iPhone|iPod|Android.*Mobile|Windows.*Phone", $_SERVER['HTTP_USER_AGENT'])) {
				$web_mobile="s";
			}
			else
			{
				$web_mobile="w";
			}
			
			if($_GET["f_qr"]!="")
			{
				$f_qr = "1";
			}
			else
			{
				$f_qr = "0";
			}
			
			$sql = "select shop_id from shop_pv where shop_id='$shop_id' and yyyymm='".date("Ym")."' and web_mobile='".$web_mobile."' and f_qr='".$f_qr."'";
			$db_result = $common_dao->db_query($sql);
			if($db_result)
			{
				$shop_id_pv = $db_result[0]["shop_id"];
			}
		
		
			if ($shop_id_pv != "")
			{
				$db_insert = "update shop_pv set p_".date("d")."=p_".date("d")."+1  ";
				$db_insert .= " where shop_id='$shop_id' and yyyymm='".date("Ym")."' and web_mobile='".$web_mobile."' and f_qr='".$f_qr."'";
			}
			else
			{
				$db_insert = "insert into shop_pv (shop_id, yyyymm, web_mobile, f_qr, p_".date("d").") values ('$shop_id','".date("Ym")."','".$web_mobile."','".$f_qr."',1)";
				
			}
			$db_result = $common_dao->db_update($db_insert);
		}
		
	}
	
	
	//コンテンツPV
	public function Fn_contents_pv($contents_id)
	{	
		global $common_dao;
		
		if ($contents_id!="")
		{
			if (ereg("iPhone|iPod|Android.*Mobile|Windows.*Phone", $_SERVER['HTTP_USER_AGENT'])) {
				$web_mobile="s";
			}
			else
			{
				$web_mobile="w";
			}
		
			$sql = "select contents_id from contents_pv where contents_id='$contents_id' and yyyymm='".date("Ym")."' and web_mobile='".$web_mobile."'";
			$db_result = $common_dao->db_query($sql);
			if($db_result)
			{
				$contents_id_pv = $db_result[0]["contents_id"];
			}
		
		
			if ($contents_id_pv != "")
			{
				$db_insert = "update contents_pv set p_".date("d")."=p_".date("d")."+1  ";
				$db_insert .= " where contents_id='$contents_id' and yyyymm='".date("Ym")."' and web_mobile='".$web_mobile."'";
			}
			else
			{
				$db_insert = "insert into contents_pv (contents_id, yyyymm, web_mobile, p_".date("d").") values ('$contents_id','".date("Ym")."','".$web_mobile."',1)";
				
			}
			$db_result = $common_dao->db_update($db_insert);
		}
		
	}
	
	//ランダム
	public function Fn_random_data($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}

}


?>
