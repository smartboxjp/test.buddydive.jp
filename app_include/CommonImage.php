<?php 
class CommonImage{ // extends CommonDao

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	public function create_folder($save_dir) {
		if (!is_dir($save_dir)) { 
			@mkdir($save_dir,0777);  
			chmod($save_dir, 0777);
		} 
	}
	
	public function img_save($filebox_name, $common_connect, $save_dir, $new_end_name, $name_id, $text_img_1, $max_size=false) {

		$fname1 = $_FILES[$filebox_name]['tmp_name'];
		$fname_name1 = $_FILES[$filebox_name]['name'];
		$fname_size1 = $_FILES[$filebox_name]['size'];
		$fname_type1 = $_FILES[$filebox_name]['type'];
		$fname_error1 = $_FILES[$filebox_name]['error'];
		
		$fname_ext1=explode(".",$fname_name1); //拡張子

		if ($fname_name1 != "" && 2>count($fname_ext1))
		{
			$common_connect -> Fn_javascript_back("ファイル名前を確認ください。");
		}
		else
		{
			if ($fname_name1 != "")
			{
				$file_rand_name = "_";
				/*
				$chars = "abcdefghijklmnopqrstuvwxyz1234567890";
				for($i = 0; $i < 20; $i++){
					$file_rand_name .= $chars{mt_rand(0, strlen($chars)-1)};
				}
				*/
				
				$fname_new_name1 = strtolower($name_id.$file_rand_name.$new_end_name.".".$fname_ext1[count($fname_ext1)-1]);

				$input = $save_dir.$fname_new_name1;
				
				
				$result_img1 = $this -> func_upload ($fname1, $fname_name1, $fname_new_name1, $fname_size1, $fname_type1, $fname_error1, $save_dir);
				//角度を先に把握
				$return_angle = $this -> orientationFixedImage_angle($input);
				
				//Thumnail
				if($max_size != "")
				{
					$imagethumb = $this-> create_thumbnail($input, "", $save_dir."", $max_size); 
				}
				
				//角度通りrotation
				if($return_angle!="")
				{
					$this -> GDImageResize_Rotate($input, $input, $width = NULL, $height = NULL, $type = NULL, $quality = 100, $return_angle);
				}
				
			}
		}
		
		//修正の場合
		if ($fname_name1 == "")
		{
			$fname_new_name1=$text_img_1;
		}
		
		return $fname_new_name1;
	}

	//サイズrolation
	public function GDImageResize_Rotate($src_file, $dst_file, $width = NULL, $height = NULL, $type = NULL, $quality=100, $rotate=0)
	 { 
		  global $IsTrueColor, $Extension; 
	
		  $im = $this -> GDImageLoad($src_file); 
		  if( !$im ) return false; 
	
		  if( !$width ) $width = imagesx($im); 
		  if( !$height ) $height = imagesy($im); 
	
		  if( $IsTrueColor && $type != "gif" ) $im2 = imagecreatetruecolor($width, $height);
		   else $im2 = imagecreate($width, $height); 
	
		  if( !$type ) $type = $Extension; 
	
		  imagecopyresampled($im2, $im, 0, 0, 0, 0, $width, $height, imagesx($im), imagesy($im));
	 
		  //全て回転させる
			  $im = $this -> rotateImage_Rotate($im, $rotate);
			  $im2 = $this -> rotateImage_Rotate($im2, $rotate);
	
		  if( $type == "gif" ) { 
			imagegif($im2, $dst_file); 
		  } 
		  else if( $type == "jpg" || $type == "jpeg" ) { 
			imagejpeg($im2, $dst_file, $quality); 
		  } 
		  else if( $type == "png" ) { 
			imagepng($im2, $dst_file); 
		  } 
	
		  imagedestroy($im); 
		  imagedestroy($im2); 
	
		  return true; 
	} 
	

	//イメージloading
	public function GDImageLoad($filename) 
	{ 
		  global $IsTrueColor, $Extension; 
	
		  if( !file_exists($filename) ) return false; 
	
		  $image_type = @exif_imagetype($filename); 
	
		  switch( $image_type ) { 
			  case IMAGETYPE_JPEG:
					$im = imagecreatefromjpeg($filename); 
					$Extension = "jpg"; 
					break; 
			  case IMAGETYPE_GIF:
					$im = imagecreatefromgif($filename); 
					$Extension = "gif"; 
					break; 
			  case IMAGETYPE_PNG:
					$im = imagecreatefrompng($filename); 
					$Extension = "png"; 
					break; 
			  default: 
					break; 
		  } 
	
		  $IsTrueColor = @imageistruecolor($im); 
	
		  return $im; 
	} 
	
	

	//回転
	public function rotateImage_Rotate($img, $rotation) {
	   $width = imagesx($img);
	   $height = imagesy($img);
	   switch($rotation) {
		 case 90: $newimg= @imagecreatetruecolor($height , $width );break;
		 case 180: $newimg= @imagecreatetruecolor($width , $height );break;
		 case 270: $newimg= @imagecreatetruecolor($height , $width );break;
		 case 0: return $img;break;
		 case 360: return $img;break;
	   }
	   if($newimg) { 
		for($i = 0;$i < $width ; $i++) { 
		  for($j = 0;$j < $height ; $j++) {
			 $reference = imagecolorat($img,$i,$j);
			 switch($rotation) {
			   case 90: if(!@imagesetpixel($newimg, ($height - 1) - $j, $i, $reference )){return false;}break;
			   case 180: if(!@imagesetpixel($newimg, $width - $i, ($height - 1) - $j, $reference )){return false;}break;
			   case 270: if(!@imagesetpixel($newimg, $j, ($width - 1) - $i, $reference )){return false;}break;
			 }
		   } 
		} return $newimg; 
	  } 
	  return false;
	 }
	 
	 
	// 画像の方向を正す
	public function orientationFixedImage_angle($input)
	{
		$image = ImageCreateFromJPEG($input);
		$exif_datas = @exif_read_data($input);
		
		if(isset($exif_datas['Orientation'])){
			  $orientation = $exif_datas['Orientation'];
			  if($image){
				  // 未定義
				  if($orientation == 0){
				  // 通常
				  }else if($orientation == 1){
				  // 左右反転
				  }else if($orientation == 2){
					//$this-> image_flop($image);
				  // 180°回転
				  }else if($orientation == 3){
					$return_angle = 180;
				  // 上下反転
				  }else if($orientation == 4){
					//$this-> image_Flip($image);
				  // 反時計回りに90°回転 上下反転
				  }else if($orientation == 5){
					$return_angle = 270;
					//$this-> image_flip($image);
				  // 時計回りに90°回転
				  }else if($orientation == 6){
					$return_angle = 90;
				  // 時計回りに90°回転 上下反転
				  }else if($orientation == 7){
					$return_angle = 90;
					//$this-> image_flip($image);
				  // 反時計回りに90°回転
				  }else if($orientation == 8){
					$return_angle = 270;
				  }
			  }
		}
		return $return_angle;
	}

	
	// 画像の左右反転
	public function image_flop($image){
		// 画像の幅を取得
		$w = imagesx($image);
		// 画像の高さを取得
		$h = imagesy($image);
		// 変換後の画像の生成（元の画像と同じサイズ）
		$destImage = @imagecreatetruecolor($w,$h);
		// 逆側から色を取得
		for($i=($w-1);$i>=0;$i--){
			for($j=0;$j<$h;$j++){
				$color_index = imagecolorat($image,$i,$j);
				$colors = imagecolorsforindex($image,$color_index);
				imagesetpixel($destImage,abs($i-$w+1),$j,imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
			}
		}
		return $destImage;
	}
	
	// 上下反転
	public function image_flip($image){
		// 画像の幅を取得
		$w = imagesx($image);
		// 画像の高さを取得
		$h = imagesy($image);
		// 変換後の画像の生成（元の画像と同じサイズ）
		$destImage = @imagecreatetruecolor($w,$h);
		// 逆側から色を取得
		for($i=0;$i<$w;$i++){
			for($j=($h-1);$j>=0;$j--){
				$color_index = imagecolorat($image,$i,$j);
				$colors = imagecolorsforindex($image,$color_index);
				imagesetpixel($destImage,$i,abs($j-$h+1),imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
			}
		}
		return $destImage;
	}
	
//_/_/_/_/_/_/ Thumnail Function _/_/_/_/_/_/
	//$imagethumb = create_thumbnail("イメージURL", "ファイル後ろに追加する文字", "保存するフォルダ", Widthのサイズ); 
	//$imagethumb = create_thumbnail("img/2006-08-04-018.JPG", "", "../img_l/", 500); 
	public function create_thumbnail($file, $add_name, $save_folder, $max_side = false, $fixed = false) { 
		// 1 = GIF, 2 = JPEG 
		if(!$max_side) $max_side = 100; 
		
		if(file_exists($file)) { 
			$image_attr = getimagesize($file); 
		
			//widthが320以上の場合だけ縮小する
			if ($image_attr[0]> $max_side)
			{
				$type = getimagesize($file); 
			 
				if(!function_exists('imagegif') && $type[2] == 1) { 
					$error = 'Filetype not supported. Thumbnail not created.'; 
				} 
				elseif (!function_exists('imagejpeg') && $type[2] == 2) { 
					$error = 'Filetype not supported. Thumbnail not created.'; 
				} 
				else {     
					// create the initial copy from the original file 
					if($type[2] == 1) { 
						$image = imagecreatefromgif($file); 
					} 
					elseif($type[2] == 2) { 
						$image = imagecreatefromjpeg($file); 
					} 
					elseif($type[2] == 3) { 
						$image = imagecreatefrompng($file); 
					} 
					 
					if(function_exists('imageantialias')) 
						imageantialias($image, TRUE); 
		 
					// figure out the longest side 
					if($image_attr[0] > $image_attr[1]): 
						$image_width = $image_attr[0]; 
						$image_height = $image_attr[1]; 
											
						if($fixed) { 
							$image_new_width  = $max_side; 
							$image_new_height = (int)($max_side * 3 / 4); 
							// 4:3 ratio 
						} else { 
							$image_new_width = $max_side; 
			 
							$image_ratio = $image_width / $image_new_width; 
							$image_new_height = (int) ($image_height / $image_ratio); 
						} 
						//width > height 
					else: 
						$image_width = $image_attr[0]; 
						$image_height = $image_attr[1]; 
						if($fixed) { 
							$image_new_height = $max_side; 
							$image_new_width  = (int)($max_side * 3 / 4); 
							// 3:4 ratio 
						} else { 
							$image_new_height = $max_side; 
			 
							$image_ratio = $image_height / $image_new_height; 
							$image_new_width = (int) ($image_width / $image_ratio); 
						} 
						//height > width 
					endif; 
		 
					$thumbnail = imagecreatetruecolor($image_new_width, $image_new_height); 
					@ imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $image_new_width, $image_new_height, $image_attr[0], $image_attr[1]); 
					 
					$thumb = preg_replace('!(\.[^.]+)?$!', $add_name.'$1', $save_folder.basename($file), 1); 
					//$thumbpath = str_replace(basename($file), $thumb, $file); 
					$thumbpath = $thumb;
	
					// move the thumbnail to it's final destination 
					if($type[2] == 1) { 
						if (!imagegif($thumbnail, $thumbpath, 100)) { 
							$error = 'Thumbnail path invalid'; 
						} 
					} 
					elseif($type[2] == 2) { 
						if (!imagejpeg($thumbnail, $thumbpath, 100)) { 
							$error = 'Thumbnail path invalid'; 
						} 
					}
					elseif($type[2] == 3) { 
						if (!imagepng($thumbnail, $thumbpath, 100)) { 
							$error = 'Thumbnail path invalid'; 
						} 
					}    

   				}
			////widthが320以上
			}
		
		} else { 
			$error = 'File not found'; 
		} 
 
		if(!empty($error)) { 
			die($error); 
		} else { 
			return $thumbpath; 
		} 
			 
	} 
	
	
	//_/_/_/_/_/_/ IMAGE Function _/_/_/_/_/_/
	
	public function func_upload ($fname, $fname_name, $fname_new_name, $fname_size, $fname_type, $fname_error, $save_dir)
	{
		if ($fname_error > 0)
		{
			echo 'Problem: ';
			switch ($fname_error)
			{
				case 1:  echo 'File exceeded upload_max_filesize';  break;
				case 2:  echo 'File exceeded max_file_size';  break;
				case 3:  echo 'File only partially uploaded';  break;
				case 4:  echo 'No file uploaded';  break;
	
			}
			exit;
		}
	
	
		//name check
		if($fname_name!=none)
		{
			//upload_file_name_ck($rename);
	
			//file sizecheck
			if($fname_size>5000000)
			{
				echo 'Over 5MB';
			}
	
			$DEST=$save_dir.$fname_new_name;
	
			/*
			while(file_exists($DEST))
			{ 
				$mark += 1;
				$DEST=$save_dir.$mark.$fname_new_name; 
			}
			*/
	
			// SAVE Dir
			$upfile = $save_dir.$fname_name;

			if (is_uploaded_file($fname)) 
			{
				if (!move_uploaded_file($fname, $DEST))
				{
					echo 'Problem: Could not move file to destination directory';
					exit;
				}
			} 
			else 
			{
				echo 'Problem: Possible file upload attack. Filename: '.$fname_name;
				exit;
			}
			//echo '<br>File uploaded successfully : <b>'.$fname_name.'</b>'; 
			return $mark.$fname_name;
			
		}
	}
	
	public function upload_file_name_ck($uploadfilename){
		if(eregi("\.php|\.php3|\.html|\.htm|\.phtml|\.com|\.bat|\.exe|\.inc|\.js|\.ph|\.asp|\.jsp|\.cgi|\.pl",$uploadfilename))
			echo '<br> UpLoad ERROR : <font color=red>'.$uploadfilename.'</font><br>';
		return;
	}
	

	//フォルダ全て削除
	public function deldir($dir)
	{
		$handle = opendir($dir);
		while (false!==($FolderOrFile = readdir($handle)))
		{
			if($FolderOrFile != "." && $FolderOrFile != "..") 
			{ 
				if(is_dir("$dir/$FolderOrFile")) 
				{ deldir("$dir/$FolderOrFile"); } // recursive
				else
				{ unlink("$dir/$FolderOrFile"); }
			} 
		}
		
		closedir($handle);
		
		if(rmdir($dir))
		{ $success = true; }
		
		return $success; 
	} 
}


?>
