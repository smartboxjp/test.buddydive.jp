
<?
	$where = "";
	if($s_member_id!= "")
	{
		$where .= " and member_id = '".$s_member_id."'";
	}
	
	$arr_db_field = array("member_id", "user_name", "member_email", "member_login_pw", "temp_key");
	$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana"));
	$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
	$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship"));
	$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM member where 1 ".$where ;
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[0][$val];
		}
	}


	$sql = "SELECT cate_medical_id, cate_medical_title ";
	$sql .= " FROM cate_medical where flag_open='1' ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$cate_medical_title[$db_result[$db_loop]["cate_medical_id"]] = $db_result[$db_loop]["cate_medical_title"];
	}
	
	$sql = "SELECT medical_id, yes_no ";
	$sql .= " FROM member_medical where member_id='".$member_id."' ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$arr_member_medical[$db_result[$db_loop]["medical_id"]] = $db_result[$db_loop]["yes_no"];
	}
?>

<article>
  <ul class="pankuzu">
    <li><? echo $pankuzu;?></li>
    <li>&gt;<a href="member_detail.php?s_member_id=<? echo $s_member_id;?>">個人情報（<? echo $member_name_1;?> <? echo $member_name_2;?>）</a></li>
    <li>&gt;メディカルチェック</li>
  </ul>
  <section class="sectionBox">
    <p class="tit">メディカルチェック（<? echo $member_name_1;?> <? echo $member_name_2;?>）</p>
    <div class="box">
      <table class="listTable">
        <tr>
          <th class="tCenter bgLightGray">項目</th>
          <th width="70" class="tCenter bgLightGray">はい・いいえ</th>
        </tr>
        <?
				foreach($cate_medical_title as $key => $value)
				{
					$var = "medical_".$key;
				?>
        <tr>
          <th><? echo nl2br($value);?></th>
          <td class="tCenter <? if($arr_member_medical[$key]==1) { echo " error ";}?>">
          <?
          if($arr_member_medical[$key]==1)
					{
					?>
          	<label>はい</label>
          <?
					}
					elseif($arr_member_medical[$key]==2)
					{
					?><label>いいえ</label>
          <?
					}
					?>
          </td>
        </tr>
        <?
				}
				?>
        
        <?
					$medical_id = 1;
        ?>
        <tr class="sum">
          <td colspan="2" class="tCenter <? if($arr_member_medical[$medical_id]=="2") { echo " error ";}?>">
          <?
					if($arr_member_medical[$medical_id]=="1")
					{
						echo "私は上記項目に該当する事項は一つもありません。<br />また今後、上記項目に該当する事項が発生した場合、医師の診断書を持参してサービスを受けることを約束します。";
					}
					elseif($arr_member_medical[$medical_id]=="2")
					{
						echo "私は上記に該当する項目があるが、医師の診断許可書を持参しサービスを受けます。<br />診断書を持参しない場合は当日のダイビングが出来ないことも了解いたします。";
					}
					?>
          </td>
        </tr>
      </table>
    </div>
  </section>

</article>