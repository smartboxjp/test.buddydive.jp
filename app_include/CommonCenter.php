<?php

class CommonCenter{
	
	//本登録済みのダイバー情報
	public function Fn_center_info ($common_dao, $dive_center_id) 
	{ 
		$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "img_1", "commission");
		$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw"));
		$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT dive_center_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM dive_center where dive_center_id='".$dive_center_id."'";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result;
	} 
	
	//スタイル
	public function Fn_dive_center_style ($common_dao, $dive_center_id) 
	{ 
		$sql = "SELECT ";
		$sql.= " dive_center_style_id, dive_center_style_title, dive_center_style_price, dive_center_style_stock, possible_point ";
		$sql .= " FROM dive_center_style where dive_center_id='".$dive_center_id."'";
		//$sql .= " and flag_open=1 ";//管理ページは見れる
		$sql .= " order by view_level ";
		$db_result = $common_dao->db_query($sql);
		return $db_result;
	} 
	
	//エントリー
	public function Fn_dive_center_boat ($common_dao, $dive_center_id) 
	{ 
		$sql = "SELECT ";
		$sql.= " dive_center_boat_id, dive_center_boat_title, dive_center_boat_price, dive_center_boat_stock ";
		$sql .= " FROM dive_center_boat where dive_center_id='".$dive_center_id."'";
		//$sql .= " and flag_open=1 ";//管理ページは見れる
		$sql .= " order by view_level ";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result;
	} 
	
	//タンク
	public function Fn_dive_center_tank ($common_dao, $dive_center_id) 
	{ 
		$sql = "SELECT ";
		$sql.= " dive_center_tank_id, dive_center_tank_title, dive_center_tank_price, dive_center_tank_stock ";
		$sql .= " FROM dive_center_tank where dive_center_id='".$dive_center_id."'";
		//$sql .= " and flag_open=1 ";//管理ページは見れる
		$sql .= " order by view_level ";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result;
	} 
	
}


?>
