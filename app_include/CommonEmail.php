<?php

class CommonEmail{ // extends CommonDao

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	//メール送信する
	public function Fn_send_utf ($email, $subject, $body, $mail_from1 = null, $mail_from2) { 

		if ($email != "") 
		{ 
			mb_language('Japanese');
			mb_internal_encoding("UTF-8");
			if(!mb_send_mail($email, $subject, $body, "From: ".mb_encode_mimeheader($mail_from1)."<".$mail_from2.">", "-f " .$mail_from2)) { 
				return false; 
			} else { 
				return true; 
			} 
		}
} 
	
	//メール送信する
	public function Fn_send_utf_test ($email, $subject, $body, $mail_from1 = null, $mail_from2) { 

		if ($email != "") 
		{ 
			mb_language('Japanese');
			mb_internal_encoding("UTF-8");
			if(!mb_send_mail($email, $subject, $body, "From: ".mb_encode_mimeheader($mail_from1)."<".$mail_from2.">", "-f " .$mail_from2)) { 
			echo $mail_from1;
			echo "<hr />エラー";
				return false; 
			} else { 
			echo "<hr />成功";
				return true; 
			} 
			exit;
		}
	} 

	//htmlメール送信する
	public function Fn_send_html_mailing_utf ($to, $subject, $body, $fromname = null, $from, $arr_BCC) 
	{ 
		require_once($_SERVER['DOCUMENT_ROOT']."/app_system/PHPMailer-master/class.phpmailer.php");

		if ($to != "") 
		{ 
			mb_language("japanese");           //言語(日本語)
			mb_internal_encoding("UTF-8");     //内部エンコーディング(UTF-8)
			
			/*
			$to = "click55@msn.com";      //宛先
			$subject = "メールの件名ﾃｽﾄ";         //件名
			$body = "<img src=\"https://www.okinawapaper.com/img/common/logo.png\" width='100'>メールの本文です。ﾃｽﾄ<a href=\"http://www.okinawapaper.com/test/test_mail.php\">てすと</a>";      //本文
			$from = "info@okinawapaper.com";      //差出人
			$fromname = "ほげほげ　太郎ﾃｽﾄ";      //差し出し人名
			$attachfile = "./logo.png";        //添付ファイルパス
			*/
			
			$mail = new PHPMailer();           //PHPMailerのインスタンス生成
			$mail->CharSet = "iso-2022-jp";    //文字コード設定
			$mail->Encoding = "7bit";          //エンコーディング
			

			$mail->AddAddress($to);//宛先(To)をセット
			$mail->From = $from;                                                               //差出人(From)をセット
			$mail->FromName = mb_encode_mimeheader(mb_convert_encoding($fromname,"JIS","UTF-8")); //差出人(From名)をセット
			$mail->Subject = mb_encode_mimeheader(mb_convert_encoding($subject,"JIS","UTF-8"));   //件名(Subject)をセット
			$mail->Body  = mb_convert_encoding($body,"JIS","UTF-8");                              //本文(Body)をセット
			//$mail->AddAttachment($attachfile);    
			$mail->isHTML(true);//HTML形式
			
			foreach($arr_BCC as $key => $value)
			{
				if(trim($value)!="")
				{
					$mail->AddBCC($value);
					echo $value."<hr />";
				}
			}
			
			//メールを送信
			if (!$mail->Send()){
				return false;
				//echo("Failed to send mail. Error:".$mail->ErrorInfo);
			}else{
				return true;
			}
		}
	} 

	//メール送信する
	public function Fn_send_sjis ($email, $subject, $body, $mail_from1 = null, $mail_from2) { 

		if ($email != "") 
		{ 
			mb_language('Japanese');
			mb_internal_encoding("sjis");
			if(!mb_send_mail($email, $subject, $body, "From: ".mb_encode_mimeheader($mail_from1)."<".$mail_from2.">", "-f " .$mail_from2)) { 
				return false; 
			} else { 
				return true; 
			} 
		}
	} 
	
	//ファイル添付してメール送信
	public function Fn_send_file ($mailTo, $subject, $body, $from = null, $mail_send, $returnMail = null) { 
		
		$boundary = md5(uniqid(rand(),1)); //境界バウンダリー文字生成

		mb_language("ja");
		mb_internal_encoding("utf8"); //sjis
		header("Content-Type:text/html; charset=utf8"); //Shift-JIS
	
		$boundary = md5(uniqid(rand(),1)); //境界バウンダリー文字生成
		
	
		$header = "From: ".$from."<".$mail_send.">\n";
		$header .= "X-Mailer: PHP/".phpversion()."\n";
		$header .= "MIME-version: 1.0\n";
	
		$body_contents = "";
	
		if (!$_FILES) { //添付がない場合
			$header .= "Content-Type: text/plain; charset=ISO-2022-JP\n";
			$header .= "Content-Transfer-Encoding: 7bit\n";
		}
		else{ //添付がある場合
			$header .= "Content-Type: multipart/mixed;\n";
			$header .= "\tboundary=\"".$boundary."\"\n";
			$body_contents .= "This is a multi-part message in MIME format.\n\n";
			$body_contents .= "--".$boundary."\n";
			$body_contents .= "Content-Type: text/plain; charset=ISO-2022-JP\n";
			$body_contents .= "Content-Transfer-Encoding: 7bit\n\n";
		}
		$name = mb_convert_encoding($name, "JIS", "auto"); //JISに変換
		//$subject = mb_convert_encoding($subject, "JIS", "auto"); //JISに変換
		$body = mb_convert_encoding($body, "JIS", "auto"); //JISに変換
		$body_contents .= $body . "\n\n";
	
		$attachsize = 0;
		for ($i=0;$i<count($_FILES["attach"]["tmp_name"]);$i++) {
			if ($_FILES["attach"]["tmp_name"][$i]) { //添付がある場合
				$upattach = $_FILES["attach"]["tmp_name"][$i];
				$upattachname = $_FILES["attach"]["name"][$i];
				$upattachtype = $_FILES["attach"]["type"][$i];
				$upattachsize = $_FILES["attach"]["size"][$i];
				$attachsize = $attachsize + $upattachsize;
				$fp = fopen($_FILES["attach"]["tmp_name"][$i], "r");
				$attachcon = fread($fp, filesize($_FILES["attach"]["tmp_name"][$i]));
				fclose($fp);
				$attachenco = chunk_split(base64_encode($attachcon)); //エンコードして分割
				
				$body_contents .= "--".$boundary."\n";
				$body_contents .= "Content-Type: " . $upattachtype . ";\n";
				$body_contents .= "\tname=\"".$upattachname."\"\n";
				$body_contents .= "Content-Transfer-Encoding: base64\n";
				$body_contents .= "Content-Disposition: attachment;\n";
				$body_contents .= "\tfilename=\"".$upattachname."\"\n\n";
				$body_contents .= "$attachenco\n";
			}
		}
		$body_contents .= "--$boundary--\n\n";
		$body_contents = mb_convert_encoding($body_contents, "JIS", "auto"); //JISに変換
		
		if (ini_get('safe_mode')) {
			$result = mb_send_mail($mailTo, $subject, $body_contents, $header);
		} else {
			$result = mb_send_mail($mailTo, $subject, $body_contents, $header,'-f' . $returnMail);
		}
		
	}

}


?>
