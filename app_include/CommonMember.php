<?php

class CommonMember{
	
	//本登録済みのダイバー情報
	public function Fn_member_info ($common_dao, $member_id) 
	{ 
		$sql = "select member_id, user_name, member_email, img_1, member_name_1, member_name_2 from member where member_id ='".$member_id."' and flag_hontouroku=1 ";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result;
	} 
	
	//ダイバーのメディカル
	public function Fn_member_medical ($common_dao, $member_id) 
	{ 
		$sql = "select yes_no from member_medical where member_id ='".$member_id."' and medical_id=1 ";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result[0]["yes_no"];
	} 
	
	//バディのグループID
	public function Fn_buddy_group ($common_dao, $dive_reserve_id)
	{
		$sql = "SELECT buddy_group FROM member_buddy where buddy_reserve_id='".$dive_reserve_id."' or dive_reserve_id='".$dive_reserve_id."' ";
		$sql .= " group by buddy_group ";
		$db_result = $common_dao->db_query($sql);
	
		if($db_result)
		{
			$buddy_group= $db_result[0]["buddy_group"];
		}
		else
		{
			$buddy_group = $dive_reserve_id;
		}
		
		return $buddy_group;
	}
	
	//ダイバーのスキール
	public function Fn_member_skill ($common_dao, $member_id) 
	{ 
		$sql = "select yes_no from member_skill where member_id ='".$member_id."' and skill_id=1 ";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result[0]["yes_no"];
	} 
	
	
	public function Fn_buddy_list ($common_dao, $dive_reserve_id, $except_member_id) 
	{ 
		//予約IDからグループIDを探す
		if($dive_reserve_id!="")
		{
			$buddy_group = $this->Fn_buddy_group ($common_dao, $dive_reserve_id);
			if($buddy_group!="" && $buddy_group!="0")
			{
				$sql = "select buddy_id, member_id, buddy_reserve_id, dive_reserve_id ";
				$sql .= " from member_buddy where 1 ";
				$sql .= " and buddy_group='".$buddy_group."' ";
				$db_result = $common_dao->db_query($sql);
				if($db_result)
				{
					for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
					{
						$arr_buddy_id[$db_result[$db_loop]["member_id"]] = $db_result[$db_loop]["dive_reserve_id"];
						$arr_buddy_id[$db_result[$db_loop]["buddy_id"]] = $db_result[$db_loop]["buddy_reserve_id"];
					}
				}
				
				foreach($arr_buddy_id as $key=>$value)
				{
					if($key != $except_member_id)
					{
						//招待したリスト
						$sql = "SELECT member_id, img_1, user_name, member_name_1, member_name_2, member_email FROM member where 1 ";
						$sql .= " and member_id='".$key."' ";
						$db_result = $common_dao->db_query($sql);
					
						if($db_result)
						{
							for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
							{
								$arr_buddy["reserve_id"][] = $value;
								$arr_buddy["buddy_group"][] = $buddy_group;
								$arr_buddy["member_id"][] = $db_result[$db_loop]["member_id"];
								$arr_buddy["img_1"][] = $db_result[$db_loop]["img_1"];
								$arr_buddy["user_name"][] = $db_result[$db_loop]["user_name"];
								$arr_buddy["member_name_1"][] = $db_result[$db_loop]["member_name_1"];
								$arr_buddy["member_name_2"][] = $db_result[$db_loop]["member_name_2"];
								$arr_buddy["member_email"][] = $db_result[$db_loop]["member_email"];
							}
						}
					}
				}
				
			}
		}
		return $arr_buddy;
	} 
	
	public function Fn_buddy_status($common_dao, $member_id, $dive_reserve_id, $buddy_id) 
	{ 
		$sql_reserve = "SELECT status from dive_reserve where dive_reserve_id='".$dive_reserve_id."' ";
		
		$db_result_reserve = $common_dao->db_query($sql_reserve);
		if($db_result_reserve)
		{
			$result = $db_result_reserve[0]["status"];
		}
		else
		{
			$result = "未予約";
		}
		/*
		$buddy_group = $this->Fn_buddy_group ($common_dao, $dive_reserve_id);
		
		$sql = "SELECT buddy_reserve_id FROM member_buddy ";
		$sql .= " where buddy_group='".$buddy_group."' and (buddy_id='".$buddy_id."' ";
		$sql .= " or member_id='".$buddy_id."' ) ";
		$db_result = $common_dao->db_query($sql);
		
		$result = "未予約";
		if($db_result)
		{
			if($db_result[0]["buddy_reserve_id"]!="0")
			{
				$sql_reserve = "SELECT status from dive_reserve where dive_reserve_id='".$db_result[0]["buddy_reserve_id"]."' "
				
				$db_result_reserve = $common_dao->db_query($sql_reserve);
				$result = $db_result_reserve[0]["status"];
			}
		}
		*/
		return $result;
	} 
	
	//バディがいるかをチェック
	public function Fn_buddy_check_up($common_dao, $member_id, $s_yyyymmdd, $dive_reserve_id)
	{ 
		$sql = "SELECT d.dive_reserve_id FROM member_buddy m inner join dive_reserve d on m.dive_reserve_id=d.dive_reserve_id";
		$sql .= " where m.buddy_id='".$member_id."' and d.yyyymmdd='".date("Y-m-d", strtotime($s_yyyymmdd))."' and m.member_id=d.member_id and m.buddy_reserve_id=0 ";
		$db_result = $common_dao->db_query($sql);
		
		if($db_result)
		{
				$db_up = "Update member_buddy set buddy_reserve_id= '".$dive_reserve_id."' where dive_reserve_id= '".$db_result[0]["dive_reserve_id"]."' and buddy_id='".$member_id."'";
				$db_result_up = $common_dao->db_update($db_up);
				
				//一致しているメッセージあれば削除
				$db_up = "Delete from member_message where member_id= '".$member_id."' and dive_reserve_id= '".$db_result[0]["dive_reserve_id"]."' ";
				$db_result_del = $common_dao->db_update($db_up);
		}
	} 
	
	
	public function Fn_inivited_list ($common_dao, $dive_reserve_id, $member_id) 
	{ 
		if($dive_reserve_id!="")
		{
			$sql = "SELECT invited_name FROM invite where dive_reserve_id='".$dive_reserve_id."' and member_id='".$member_id."' ";
			$sql .= " order by up_date desc";
			$db_result = $common_dao->db_query($sql);
		
			return $db_result;
		}
	}
	
	//ダイバーのメディカル
	public function Fn_member_point ($common_dao, $member_id) 
	{ 
		$sql = "select member_point, member_name_1, member_name_2 from member where member_id ='".$member_id."' ";
		$db_result = $common_dao->db_query($sql);
		
		return $db_result;
	} 
	
	//ポイント履歴
	public function Fn_member_point_history ($common_dao, $member_id, $dive_reserve_id, $point_comment, $point, $url) 
	{ 
		$db_insert = "insert into member_point (member_point_id, member_id, dive_reserve_id, point_comment, point, url, regi_date, up_date) VALUES ('', '".$member_id."', '".$dive_reserve_id."', '".$point_comment."', '".$point."', '".$url."', now(), now()) ";
		$db_result = $common_dao->db_update($db_insert);
	} 
	
	//ポイント履歴
	public function Fn_member_last_dive ($common_dao, $member_id) 
	{ 
		$sql = "select yyyymmdd from dive_reserve where member_id ='".$member_id."' ";
		$sql .= " and status=2 ";
		$sql .= " order by yyyymmdd desc limit 0, 1";
		$db_result = $common_dao->db_query($sql);
		
		if($db_result)
		{
			$last_reserve = $db_result[0]["yyyymmdd"];
		}
		return $last_reserve;
	} 
	
	
}


?>
