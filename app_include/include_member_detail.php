<?
	/*
		下記のリストで共有
		/master/member/member_detail.php
		/dive_center/member/member_detail.php
	*/
  
	//ステータスをArrayへ
	$sql = "SELECT cate_status_id, cate_status_title FROM cate_status order by view_level";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_status_title[$db_result[$db_loop][cate_status_id]] = $db_result[$db_loop][cate_status_title];
		}
	}
	
	
	$where = "";
	$where .= " and member_id = '".$s_member_id."'";
	
	$arr_db_field = array("member_id", "user_name", "member_email", "member_login_pw", "temp_key");
	$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana"));
	$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
	$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
	$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
	$arr_db_field = array_merge($arr_db_field, array("member_point"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM member where 1 ".$where ;
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[0][$val];
		}
	}
?>

<article>
  <ul class="pankuzu">
    <li><? echo $pankuzu;?></li>
    <li>&gt; 個人情報（<? echo $user_name."：".$member_name_1;?> <? echo $member_name_2;?>）</li>
  </ul>
  <section class="sectionBox">
    <p class="tit">個人情報</p>
    <div class="box">
      <table class="listTable">
        <tr>
          <th>ユーザー名</th>
          <td colspan="2"><? echo $user_name;?></td>
        </tr>
        <tr>
          <th>メールアドレス</th>
          <td colspan="2"><? echo $member_email;?></td>
        </tr>
        <tr>
          <th>名前</th>
          <td colspan="2"><? echo $member_name_1;?> <? echo $member_name_2;?></td>
        </tr>
        <tr>
          <th>フリガナ</th>
          <td colspan="2"><? echo $member_name_kana;?></td>
        </tr>
        <tr>
          <th>生年月日</th>
          <td colspan="2"><? echo $birth_yyyy;?>年<? echo $birth_mm;?>月<? echo $birth_dd;?>日</td>
        </tr>
        <tr>
          <th>保有ポイント</th>
          <td colspan="2"><? echo number_format($member_point);?>pt</td>
        </tr>
        <tr>
          <th>血液型</th>
          <td colspan="2"><? echo $blood;?></td>
        </tr>
        <tr>
          <th>住所</th>
          <td colspan="2"><? echo $pref;?> <? echo $address;?></td>
        </tr>
        <tr>
          <th>携帯電話番号</th>
          <td colspan="2"><? echo $phone;?></td>
        </tr>
        <tr>
        <th>自宅電話番号</th>
        <td colspan="2"><? echo $tel;?></td>
        </tr>
        <tr>
          <th>緊急連絡先 名前</th>
          <td colspan="2"><? echo $emergency_name;?></td>
        </tr>
        <tr>
          <th>緊急連絡先 間柄</th>
          <td colspan="2"><? echo $emergency_relationship;?></td>
        </tr>
        <tr>
          <th>緊急連絡先 電話番号</th>
          <td colspan="2"><? echo $emergency_tel;?></td>
        </tr>
        <tr>
          <th>最終認定ランク</th>
          <td colspan="2"><? echo $diving_rank;?></td>
        </tr>
        <tr>
          <th>指導団体</th>
          <td colspan="2"><? echo $diving_group;?></td>
        </tr>
        
        
        <tr>
          <th>写真</th>
          <td colspan="2">
          <?
						if($img_1!="")
						{
							echo "<img src='/".global_member_dir.$member_id."/".$img_1."' width=100 border=0>";
						}
					?>
          </td>
          </tr>
        <tr>
          <th>スキルチェック</th>
          <td>
          <?
					$skill_id = 1;
          	$sql = "SELECT skill_id, yes_no ";
					$sql .= " FROM member_skill where member_id='".$s_member_id."' and skill_id='".$skill_id."'";
					
					$db_result = $common_dao->db_query($sql);
					for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
					{
						$arr_member_skill[$db_result[$db_loop]["skill_id"]] = $db_result[$db_loop]["yes_no"];
					}
					
					if($arr_member_skill[$skill_id]=="1")
					{
						echo "私は上記項目すべてのスキルができます";
					}
					elseif($arr_member_skill[$skill_id]=="2")
					{
						echo "私はできないスキルがあります";
					}
					?>
          </td>
          <td>
          	<a href="member_skill.php?s_member_id=<? echo $s_member_id;?>">確認 &gt;&gt;</a>
          </td>
        </tr>
        <tr>
          <th>メディカルチェック</th>
          <td>
          <?
					$medical_id = 1;
          	$sql = "SELECT medical_id, yes_no ";
					$sql .= " FROM member_medical where member_id='".$s_member_id."' and medical_id='".$medical_id."'";
					
					$db_result = $common_dao->db_query($sql);
					for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
					{
						$arr_member_medical[$db_result[$db_loop]["medical_id"]] = $db_result[$db_loop]["yes_no"];
					}
					
					if($arr_member_medical[$medical_id]=="1")
					{
						echo "私は上記項目に該当する事項は一つもありません。<br />また今後、上記項目に該当する事項が発生した場合、医師の診断書を持参してサービスを受けることを約束します。";
					}
					elseif($arr_member_medical[$medical_id]=="2")
					{
						echo "私は上記に該当する項目があるが、医師の診断許可書を持参しサービスを受けます。<br />診断書を持参しない場合は当日のダイビングが出来ないことも了解いたします。";
					}
					?>
          </td>
          <td>
          	<a href="member_medical.php?s_member_id=<? echo $s_member_id;?>">確認 &gt;&gt;</a>
          </td>
        </tr>
      </table>
    </div>
  </section>

  <section class="sectionBox">
    <p class="tit">ダイビングログ</p>
    <div class="box">
      <table class="listTable">
        <tr>
          <th>実施状況</th>
          <th>日付</th>
          <th>ポイント</th>
          <th>スタイル</th>
          <th>エントリー</th>
          <th>タンク</th>
          <th>金額</th>
          <th>バディ</th>
        </tr>
<?		
	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_point", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price", "used_point"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where member_id='".$member_id."' and status<90 and yyyymmdd<'".date("Y-m-d")."' ";
	$sql .= " order by yyyymmdd desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}

?>
        <tr>
          <th><? echo $arr_cate_status_title[$status];?></th>
          <td><? echo date('Y/m/d', strtotime($yyyymmdd))?></td>
          <td><? echo $dive_center_point;?></td>
          <td><? echo $reserve_style_name;?></td>
          <td><? echo $reserve_boat_name;?></td>
          <td>
						<?
							echo $reserve_1_tank_name;
							if($reserve_2_tank_name!="")
							{
								echo " , ".$reserve_2_tank_name;
							}
							if($reserve_3_tank_name!="")
							{
								echo " , ".$reserve_3_tank_name;
							}
						?>
					</td>
          <td>¥<? echo number_format($reserve_all_price);?>
					<?
						if($used_point!="0")
						{
							echo " <br>（".number_format($used_point)."pt）";
						}
					?>
					</td>
          <td>
					<?
						$arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id) ;
						if($arr_buddy)
						{
							for($db_loop_buddy=0 ; $db_loop_buddy < count($arr_buddy["member_id"]) ; $db_loop_buddy++)
							{
					?>
					<a href="./member_detail.php?s_member_id=<? echo $arr_buddy["member_id"][$db_loop_buddy];?>"><? echo $arr_buddy["member_name_1"][$db_loop_buddy]." ".$arr_buddy["member_name_2"][$db_loop_buddy];?></a><br />
					
					<?
							}
						}
					?>
					</td>
        </tr>
<?
		}
	}
?>
      </table>
    </div>
  </section>
</article>