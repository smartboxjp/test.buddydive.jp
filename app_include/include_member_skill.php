
<?
	$where = "";
	if($s_member_id!= "")
	{
		$where .= " and member_id = '".$s_member_id."'";
	}
	
	$arr_db_field = array("member_id", "user_name", "member_email", "member_login_pw", "temp_key");
	$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana"));
	$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
	$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship"));
	$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM member where 1 ".$where ;
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[0][$val];
		}
	}


	$sql = "SELECT cate_skill_id, cate_skill_title ";
	$sql .= " FROM cate_skill where flag_open='1' ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$cate_skill_title[$db_result[$db_loop]["cate_skill_id"]] = $db_result[$db_loop]["cate_skill_title"];
	}
	
	$sql = "SELECT skill_id, yes_no ";
	$sql .= " FROM member_skill where member_id='".$member_id."' ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$arr_member_skill[$db_result[$db_loop]["skill_id"]] = $db_result[$db_loop]["yes_no"];
	}
?>

<article>
  <ul class="pankuzu">
    <li><? echo $pankuzu;?></li>
    <li>&gt;<a href="member_detail.php?s_member_id=<? echo $s_member_id;?>">個人情報（<? echo $member_name_1;?> <? echo $member_name_2;?>）</a></li>
    <li>&gt;スキルチェック</li>
  </ul>
  <section class="sectionBox">
    <p class="tit">スキルチェック（<? echo $member_name_1;?> <? echo $member_name_2;?>）</p>
    <div class="box">
      <table class="listTable">
        <tr>
          <th class="tCenter bgLightGray">項目</th>
          <th width="70" class="tCenter bgLightGray">できる</th>
        </tr>
        <?
				foreach($cate_skill_title as $key => $value)
				{
					$var = "skill_".$key;
				?>
        <tr>
          <th><? echo nl2br($value);?></th>
          <td class="tCenter <? if($arr_member_skill[$key]==2) { echo " error ";}?>">
          <?
          if($arr_member_skill[$key]==1)
					{
					?>
          	<label>できる</label>
          <?
					}
					elseif($arr_member_skill[$key]==2)
					{
					?><label>できない</label>
          <?
					}
					?>
          </td>
        </tr>
        <?
				}
				?>
        
        <?
					$skill_id = 1;
        ?>
        <tr class="sum">
          <td colspan="2" class="tCenter <? if($arr_member_skill[$skill_id]=="2") { echo " error ";}?>">
          <?
					if($arr_member_skill[$skill_id]=="1")
					{
						echo "私は上記項目すべてのスキルができます";
					}
					elseif($arr_member_skill[$skill_id]=="2")
					{
						echo "私はできないスキルがあります";
					}
					?>
          </td>
        </tr>
      </table>
    </div>
  </section>

</article>