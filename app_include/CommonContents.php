<?php

class CommonContents{
	
	//スタイル在庫
	public function Fn_dive_center_info ($common_dao, $admin_id, $dive_center_id) 
	{ 
		$where = " ";

		$sql = "SELECT dive_center_name, dive_center_point, dive_center_email FROM dive_center where dive_center_id='".$dive_center_id."'";
		$sql .= $where;
		$db_result = $common_dao->db_query($sql);
		
		return $db_result;
	} 
	
	
	//スタイル在庫
	public function Fn_stock_style_check ($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_id) 
	{ 

		$result_style_style = 0;
		$sql = "select s_".$s_dd." as result_stock_style from stock_style where yyyymm = '".$s_yyyymm."' and dive_center_id='".$dive_center_id."' and dive_center_style_id='".$s_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$result_stock_style = $db_result[0]["result_stock_style"];
		}
		
		return $result_stock_style;
	} 
	//ボート在庫
	public function Fn_boat_style_check ($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_id) 
	{ 

		$result_boat_style = 0;
		$sql = "select s_".$s_dd." as result_boat_style from stock_boat where yyyymm = '".$s_yyyymm."' and dive_center_id='".$dive_center_id."' and dive_center_boat_id='".$s_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$result_boat_style = $db_result[0]["result_boat_style"];
		}
		
		return $result_boat_style;
	} 
	
	//タンク在庫
	public function Fn_tank_style_check ($common_dao, $s_dd, $s_yyyymm, $dive_center_id, $s_id) 
	{ 

		$result_tank_style = 0;
		$sql = "select s_".$s_dd." as result_tank_style from stock_tank where yyyymm = '".$s_yyyymm."' and dive_center_id='".$dive_center_id."' and dive_center_tank_id='".$s_id."'";
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$result_tank_style = $db_result[0]["result_tank_style"];
		}
		
		return $result_tank_style;
	} 
	
}


?>
