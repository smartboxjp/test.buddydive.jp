<?php 
session_start();
$global_path = $_SERVER['DOCUMENT_ROOT']."/";
require_once $global_path."app_include/connect_db.php";

require_once $global_path."/app_include/CommonConnect.php";
require_once $global_path."/app_include/CommonDao.php";

if($_SERVER['HTTP_HOST']=="test.buddydive.jp")
{
	//テストサーバー
	$testserver = true;
	define('global_service_name', "buddydive");
	define('global_no_ssl', "http://test.buddydive.jp");//www.
	define('global_ssl', "http://test.buddydive.jp");//www.
	define('global_site', "test.buddydive.jp");
}
else
{
	//本番
	define('global_service_name', "buddydive");
	define('global_no_ssl', "http://buddydive.jp");//www.
	define('global_ssl', "https://buddydive.jp");//www.
	define('global_site', "buddydive.jp");
}

define('global_copyright', "Copyright (C) buddydive.jp. All Rights Reserved.");
define('global_dive_center_dir', "app_photo/dive_center/");
define('global_member_dir', "app_photo/member/");

define('global_tax_percent', "0.08");
define('global_point_percent', "0.02");
define('global_member_regist', "500"); //会員登録のとき /diver/registration/regist_temp.php
define('global_invite_point', "500");
define('global_invited_point', "500");

$global_send_mail = "sendonly@buddydive.jp";//送信先
$global_mail_from = "buddydive";
$global_bcc_mail = "system@buddydive.jp";//受信
$global_support_mail = "support@buddydive.jp";

$arr_flag_open[1] = "開催";
$arr_flag_open[2] = "条件付き";
$arr_flag_open[99] = "開催中止";
$arr_flag_open[100] = "休み";
	
$arr_flag_open_img[1] = "/common/img/divingpoint/ico_good.png";
$arr_flag_open_img[2] = "/common/img/divingpoint/ico_attention.png";
$arr_flag_open_img[99] = "/common/img/divingpoint/ico_bad.png";
$arr_flag_open_img[100] = "/common/img/divingpoint/ico_holiday.png";
	

$global_email_footer = <<<EOF
――――――――――――――――――――――――――――――――
BuddyDive バディダイブ
「バディダイビングをはじめよう！」
https://buddydive.jp

お問い合せ
support@buddydive.jp
――――――――――――――――――――――――――――――――
※このメールにお心当たりのない場合は、上記メールアドレスまでご連絡ください。
※このメールは送信専用メールアドレスから配信されています。
EOF;

/*
$arr_education[1] = "学歴不問";
$arr_education[2] = "高卒以上";
$arr_education[3] = "大卒以上";
*/
?>