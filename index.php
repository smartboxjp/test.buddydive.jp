<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
?>
<?php $meta_home = true; ?>
<?
//海況
foreach($_GET as $key => $value)
{
	$$key = $value;
}
if($s_yyyy == "") { $s_yyyy = date("Y");}
if($s_mm == "") { $s_mm = date("m");}
if($s_dd == "") { $s_dd = date("d");}
?>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/header.php"); ?>

<article>

<section id="main">
<p class="mainTit"><img src="/common/img/index/img_maintit.png" alt="BuddyDive"></p>
<p class="mainTxt">バディダイビングをはじめよう！</p>

<? if($_SESSION['member_id']==""){ 
//ログアウト ?>
<p class="mainBtn"><a href="/diver/registration/">無料ダイバー登録</a></p>
<? }else{ 
//ログイン ?>
<p class="mainBtn"><a href="/diver/invitation/">バディを招待する</a></p>
<? } ?>
</section><!--main-->

<section class="contentsBox">
<div class="inBox">
  <div class="titBox">
    <h2 class="tit">ダイビングポイント</h2>
    <p class="titLink"><a href="/divingpoint/">ダイビングポイント一覧から探す &gt;</a></p>
    </div>
    <ul class="twoList">
<?
	//ポイントリスト表示
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "img_1");
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center where flag_open=1 ";
	$sql .= " order by rand() ";
	$sql .= " limit 0, 4";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
?>
      <li><a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>" style="background-image: url(/<? echo global_dive_center_dir.$dive_center_id."/".$img_1;?>);"><span><? echo $dive_center_point;?></span></a></li>
<?
		}
	}
?>
    </ul>
  </div>

  <p class="blueBtn mt40"><a href="/divingpoint/">もっと見る &gt;</a></p>
</section><!--contentsBox-->


<!--▼▼▼Google Map設定▼▼▼-->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>

//Google Maps
function initialize() {
  var latlng = new google.maps.LatLng(34.968078, 138.931606);
  var opts = {
    zoom: 10,
    center: latlng,
    mapTypeControl: true,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  
  var map = new google.maps.Map(document.getElementById("mapCanvas"), opts);
  var styleOptions = [{
    "featureType":"water","stylers":[{"visibility":"on"},{"color":"#cddcec"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c3bdb0"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]}
  ]

  var monoType = new google.maps.StyledMapType(styleOptions);
  map.mapTypes.set('mono', monoType);
  map.setMapTypeId('mono');
  
//マーカー設定
//コンディション画像
<?php
		//波状況
		$sql_wave = "SELECT dive_center_id, dive_center_point, google_map_1, google_map_2 ";
		$sql_wave .= " FROM dive_center  ";
		$sql_wave .= " where flag_open=1 ";
		$db_result_wave = $common_dao->db_query($sql_wave);
		if($db_result_wave)
		{
			for($db_loop=0 ; $db_loop < count($db_result_wave) ; $db_loop++)
			{
				$dive_center_id = $db_result_wave[$db_loop]["dive_center_id"];
				$dive_center_point = $db_result_wave[$db_loop]["dive_center_point"];
				$google_map_1 = $db_result_wave[$db_loop]["google_map_1"];
				$google_map_2 = $db_result_wave[$db_loop]["google_map_2"];
				
				$icon = "/common/img/divingpoint/".$dive_center_id."/img_icon.png";
?>

  var Marker_<? echo $dive_center_id;?> = new google.maps.Marker({
    map: map,
    title: '<? echo $dive_center_point;?>',
    position: new google.maps.LatLng(<? echo $google_map_1;?>, <? echo $google_map_2;?>),
	icon:{
		<? if($dive_center_id=="itadiving") { ?>
			anchor: {x: 0, y: 40},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="itamarine") { ?>
			anchor: {x: 50, y: 40},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="futo") { ?>
			anchor: {x: 0, y: 50},   /* 場所をずらしてます */
		<? } else { ?>
			//anchor: {x: 0, y: 0},   /* 場所をずらしてます */
		<? } ?>
		url: "<?php echo $icon;?>"
		}
  });
	
	//テキスト
  var Marker_<? echo $dive_center_id;?>_txt = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(<? echo $google_map_1;?>, <? echo $google_map_2;?>),
    icon:{
		<? if($dive_center_id=="itadiving") { ?>
			anchor: {x: -5, y: 0},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="itamarine") { ?>
			anchor: {x: 45, y: 0},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="ishibashi") { ?>
			anchor: {x: -18, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="enoura") { ?>
			anchor: {x: -22, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="fukuura") { ?>
			anchor: {x: -18, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="izusan") { ?>
			anchor: {x: -22, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="ito") { ?>
			anchor: {x: 20, y: 52},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="kawana") { ?>
			anchor: {x: -18, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="futo") { ?>
			anchor: {x: -44, y: 28},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="iop") { ?>
			anchor: {x: -26, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="inatori") { ?>
			anchor: {x: -18, y: 20},   /* 場所をずらしてます */
		<? }elseif($dive_center_id=="osezaki") { ?>
			anchor: {x: 20, y: 52},   /* 場所をずらしてます */
		<? } else { ?>
			anchor: {x: 20, y: 0},   /* 場所をずらしてます */
		<? } ?>
    	url:'/common/img/condition/txt_<? echo $dive_center_id;?>.png'
    }
  });

	//リンク設定
	google.maps.event.addListener(Marker_<? echo $dive_center_id;?>, 'click', function() {window.open("/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>" );});

<?
			}
		}
?>

  //マップのタイトル
  var contentString = 'ダイビングポイント';
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  //infowindow.open(map, lopanMarker);//初期状態で吹き出しを表示させる場合は有効にする 
  google.maps.event.addListener(Marker, 'click', function() {
    infowindow.open(map, Marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>

<!--▲▲▲Google Map設定▲▲▲-->
<div class="map big">
  <div id="mapCanvas"></div>
</div>

<section class="contentsBox gray">
  <div class="inBox">
    <div class="titBox">
      <h2 class="tit">ダイビングポイントの海況</h2>
      <p class="titLink"><a href="/condition/">海況一覧をみる &gt;</a></p>
    </div>
    <ul class="fiveCircleList">
<?
	$sql = "SELECT dive_center_id, dive_center_name, dive_center_point, wave_regi_date FROM dive_center where flag_open = '1' " ;
	$sql .= " order by wave_regi_date desc ";
	$sql .= " limit 0, 5";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$wave_regi_date = date('Y-m-d', strtotime($db_result[$db_loop]["wave_regi_date"]));
			$dive_center_id = $db_result[$db_loop]["dive_center_id"];
			
				//最新の波状況をdive_center へ登録
				$sql_wave = "SELECT wave_height,  flag_open, regi_date FROM dive_center_wave_height where dive_center_id = '".$dive_center_id."' and DATE_FORMAT(regi_date, '%Y-%m-%d')='".$wave_regi_date."' " ;
				$sql_wave .= " order by regi_date desc ";
				$sql_wave .= " limit 0, 1";
			
				$db_result_wave = $common_dao->db_query($sql_wave);
				if($db_result_wave)
				{
					$wave_height = $db_result_wave[0]["wave_height"];
					$flag_open = $db_result_wave[0]["flag_open"];
					$regi_date = $db_result_wave[0]["regi_date"];

?>
      <li><a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>"><img src="<? echo $arr_flag_open_img[$flag_open]?>"><span><? echo $db_result[$db_loop]["dive_center_point"];?></span><? echo $arr_flag_open[$flag_open];?><br>[<? echo date('m/d H:i', strtotime($regi_date));?>]</a></li>
<?
				}
		}
	}

?>

    </ul>
    <p class="blueBtn mt40"><a href="/condition/index.php?<?php echo "s_yyyy=" . date("Y") . "&s_mm=" .  date("m") . "&s_dd=" . date("d");?>">もっと見る &gt;</a></p>
  </div>
</section><!--contentsBox-->

<?
//海況
	$sql = "SELECT dive_center_id, dive_center_name, dive_center_point, img_1, wave_regi_date FROM dive_center where flag_open = '1' " ;
	$sql .= " order by wave_regi_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$dive_center_id = $db_result[$db_loop]["dive_center_id"];
			$img_1 = $db_result[$db_loop]["img_1"];
			
			$wave_height = "";
			$flag_open = "";
			$regi_date = "";
			//最新の波状況をdive_center へ登録
			$sql_wave = "SELECT wave_height,  flag_open, regi_date FROM dive_center_wave_height where dive_center_id = '".$dive_center_id."' and DATE_FORMAT(regi_date, '%Y-%m-%d')='".$s_yyyy."-".$s_mm."-".$s_dd."' " ;
			$sql_wave .= " order by regi_date desc ";
			$sql_wave .= " limit 0, 1";
			
			$db_result_wave = $common_dao->db_query($sql_wave);
			if($db_result_wave)
			{
				$wave_height = $db_result_wave[0]["wave_height"];
				$flag_open = $db_result_wave[0]["flag_open"];
				$regi_date = $db_result_wave[0]["regi_date"];
			}
		}
	}
?>

<section class="contentsBox">
<div class="inBox">
<div class="titBox">
<h2 class="tit">特集記事</h2>
<p class="titLink"><a href="/feature/">特集記事一覧をみる &gt;</a></p>
</div>
<ul class="threeList">
<li><a href="/feature/flow.php" style="background-image: url(/common/img/feature/flow/img_thumb.jpg);"><span>バディダイビング<br>1日の流れ</span></a></li>
<li><a href="/feature/merit.php" style="background-image: url(/common/img/feature/merit/img_thumb.jpg);"><span>バディダイビングで得られる<br>5つのメリット</span></a></li>
<li class="pc"><a href="/feature/qa10.php" style="background-image: url(/common/img/feature/qa10/img_thumb.jpg);"><span>始める前に知っておきたい<br>バディダイビングの疑問10</span></a></li>
</ul>
<p class="blueBtn mt40"><a href="/feature/">もっと見る &gt;</a></p>
</div>
</section><!--contentsBox-->

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/footer.php"); ?>
