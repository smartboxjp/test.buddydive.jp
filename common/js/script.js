// JavaScript Document

//ページ内スクロール
$(function(){
  $("a[href^=#]").click(function(){
    var Hash = $(this.hash);
    var HashOffset = $(Hash).offset().top;
    $("html,body").animate({
      scrollTop: HashOffset
    }, 'fast');
    return false;
  });
});

//ロールオーバー
$(function($) {
  var postfix = '_o';
  $('.over img,.over input').not('[src*="'+ postfix +'."]').each(function() {
    var img = $(this);
    var src = img.attr('src');
    var src_on = src.substr(0, src.lastIndexOf('.'))
      + postfix
      + src.substring(src.lastIndexOf('.'));
    $('<img>').attr('src', src_on);
    img.hover(function() {
      img.attr('src', src_on);
    }, function() {
      img.attr('src', src);
    });
  });
});

//スマホでTELリンク
$(function(){
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0){
        $('.telLink').each(function(){
            var str = $(this).text();
            $(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        });
    }
});

//私はできないスキルがあります
$('#label_skill_1_2').click(function(){
  swal('できないスキルがある方は、バディダイビングに参加できません<br><a href="/diver/profile/skill_course.php" target="_blank">スキル講習に興味がある &gt;</a>');
});
//私は上記に該当する項目があるが、医師の診断許可書を持参しサービスを受けます。
$('#label_medical_1_2').click(function(){
  swal('ダイビング当日に医師の診断許可書を持参してください');
});

//登録フォームの入力確認
$(function() {
  // 背景色制御
  $('.checkTable input').change(   // ←(1)
    function() {
      $('input').closest('td').addClass('required');   // ←(2)
      $(':checked').closest('td').removeClass('required');   // ←(3)
    }
  ).trigger('change');    // ←(4)
});
//予約フォーム
$(function() {
  // 背景色制御
  $('input').change(   // ←(1)
    function() {
      $('input').closest('label').removeClass('checked');   // ←(2)
      $(':checked').closest('label').addClass('checked');   // ←(3)
    }
  ).trigger('change');    // ←(4)
});

