<footer>
<div class="footerIn">
<ul class="footerList">
<?php if($_SESSION['member_id']==""){ ?>
<li><a href="/diver/registration/?url=<? echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>">無料ダイバー登録</a></li>
<li><a href="/diver/login/?url=<? echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>">ログイン</a></li>
<?php }else{ ?>
<li><a href="/diver/invitation/">バディを招待する</a><li>
<li><a href="/diver/login/logout.php">ログアウト</a></li>
<?php } ?>
<li><a href="/condition/index.php?<?php echo "s_yyyy=" . date("Y") . "&s_mm=" .  date("m") . "&s_dd=" . date("d");?>">今日の海況</a><li>
<li><a href="/feature/">特集記事</a></li>
</ul>
<dl class="footerSite">
<dt><a href="/about/">BuddyDiveについて</a></dt>
<dd><a href="/about/guide.php">使いかたをみる</a></dd>
<dd><a href="/about/skill.php">スキルチェック</a></dd>
<dd><a href="/about/faq/">よくある質問</a></dd>
<dd><a href="/about/medical.php">メディカルチェック</a></dd>
<dd><a href="/about/rule.php">利用規約</a></dd>
<dd><a href="/about/checklist.php">バディ潜水確認事項</a></dd>
<dd><a href="/about/privacy.php">個人情報保護方針</a></dd>
<dd><a href="/about/insurance.php">保険について</a></dd>
<dd><a href="/about/company.php">運営会社</a></dd>
<dd><a href="/contact/">お問い合わせ</a></dd>
</dl>
<dl class="footerPoint pc">
<dt><a href="/divingpoint/">ダイビングポイント</a></dt>
<dd><a href="/divingpoint/detail.php?dive_center_id=ishibashi">石橋</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=enoura">江之浦</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=fukuura">福浦</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=izusan">伊豆山</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=atami">熱海</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=ito">伊東</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=futo">富戸</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=iop">伊豆海洋公園</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=inatori">稲取</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=shoubusawa">菖蒲沢</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=kumomi">雲見</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=koganezaki">黄金崎公園ビーチ</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=toi">土肥</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=itamarine">井田 [Marine]</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=itadiving">井田 [Diving]</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=shishihama">獅子浜</a></dd>
<dd><a href="/divingpoint/detail.php?dive_center_id=osezaki">大瀬崎</a></dd>
</dl>
</div>
<p class="copy">Copyright&copy;2015 EAST BLUE All Rights Reserved.</p>
</footer>

</div><!--sb-site-->
</div><!--wrap-->

<script src="/common/js/script.js"></script>
<!-- bgswitcher -->
<script src="/common/js/jquery.bgswitcher.js"></script>
<script>
  $("#main").bgswitcher({
  images: ["/common/img/index/img_main01.jpg", "/common/img/index/img_main02.jpg", "/common/img/index/img_main03.jpg", "/common/img/index/img_main04.jpg"],
  duration: 3000,
});
</script>
<!-- Slidebars -->
<script src="/common/js/slidebars/slidebars.js"></script>
<script>
  (function($) {
    $(document).ready(function() {
      $.slidebars();
    });
  }) (jQuery);
</script>
<script>var layzr = new Layzr({});</script>
</body>
</html>
