<?php 
	$url = explode('/', $_SERVER["REQUEST_URI"]);//urlを'/'で分割
  $category_file = $url[1];//カテゴリーファイル
  $page_file = $url[2];//ページファイル
	$url_parts = pathinfo($_SERVER["SCRIPT_NAME"]);//urlを名称で分割
	$last_file = $url_parts['filename'];//最終ファイル名の拡張子抜き
	
	//各ページタイトル
	if($category_file == 'divingpoint'){ $category_title = 'ダイビングポイント';
    if($last_file == 'reserve'){ $page_title = '予約フォーム';
    }
  }elseif($category_file == 'about'){ $category_title = '';
    if($page_file == 'faq'){ $category_title = 'よくある質問';
    }else{
      if($last_file == 'index'){ $page_title = 'BuddyDiveについて';
      }elseif($last_file == 'guide'){ $page_title = 'BuddyDiveの使い方';
      }elseif($last_file == 'guide_reserve'){ $page_title = 'BuddyDiveのご予約方法';
      }elseif($last_file == 'rule'){ $page_title = '利用規約';
      }elseif($last_file == 'privacy'){ $page_title = '個人情報保護方針';
      }elseif($last_file == 'checklist'){ $page_title = 'バディ潜水確認事項と傷害保険について';
      }elseif($last_file == 'company'){ $page_title = '運営会社';
      }elseif($last_file == 'skill'){ $page_title = 'スキルチェック';
      }elseif($last_file == 'medical'){ $page_title = 'メディカルチェック';
      }elseif($last_file == 'insurance'){ $page_title = 'バディダイビング時に加入が必要な保険';
      }
    }
  }elseif($category_file == 'diver'){ $category_title = 'ダイバーページ';
    if($page_file == 'registration'){ $category_title = ''; $page_title = 'ダイバー登録';
      if($last_file == 'thankyou'){ $category_title = ''; $page_title = '仮登録メール送信完了';
      }
    }elseif($page_file == 'login'){ $category_title = ''; $page_title = 'ログイン';
      if($last_file == 'password_forget'){ $category_title = ''; $page_title = 'パスワード再発行';
      }
    }elseif($page_file == 'invitation'){ $category_title = ''; $page_title = 'バディ招待';
      if($last_file == 'thankyou'){ $page_title = ''; $page_title = 'バディ招待メール送信完了';
      }
    }elseif($page_file == 'profile'){ $page_title = 'プロフィール';
      if($last_file == 'info'){ $page_title = ''; $page_title = 'メールアドレス・パスワード';
      }elseif($last_file == 'skill'){ $page_title = ''; $page_title = 'スキルチェック';
      }elseif($last_file == 'skill_course'){ $page_title = ''; $page_title = 'スキル講習';
      }elseif($last_file == 'medical'){ $page_title = ''; $page_title = 'メディカルチェック';
      }elseif($last_file == 'thankyou'){ $page_title = ''; $page_title = 'ダイバー登録完了';
      }
    }
  }elseif($category_file == 'contact'){ $category_title = 'お問い合せ';
    if($page_file == 'thankyou'){ $page_title = 'お問い合せ完了';
    }
  }elseif($last_file == '404'){ 
    $page_title = 'Not Found';
	}
	
	//サイトタイトル
	if($meta_home){
		$site_title = 'バディダイビング予約サイト - BuddyDive（バディダイブ）';
	}else{
		$site_title = 'BuddyDive（バディダイブ）';
	}
	
	if($category_title && !$page_title) { //カテゴリータイトルはあるが、ページタイトルはない
		$meta_title = $category_title . ' | ' . $site_title;
	}elseif(!$category_title && $page_title) { //カテゴリータイトルはないが、ページタイトルはある
		$meta_title = $page_title . ' | ' . $site_title;
	}elseif($category_title && $page_title) { //カテゴリータイトルとページタイトルがある
		$meta_title = $page_title . ' | ' . $category_title . ' | ' . $site_title;
	}else{
		$meta_title = $site_title;
	}

	$meta_title = $meta_title_etc.$meta_title;

?>