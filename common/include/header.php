<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common/include/common.php"); ?>
<?
	if($meta_title_other!="")
	{
		$meta_title = $meta_title_other;
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<meta name="format-detection" content="telephone=no">
<link rel="apple-touch-icon-precomposed" href="/common/img/share/apple-touch-icon-precomposed.png"/>
<meta property="fb:app_id" content="1730078240558379" />
<meta property="og:type"   content="website" />
<meta property="og:url"    content="https://<? echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
<? if($last_file == detail){?>
<meta property="og:image"  content="https://buddydive.jp/common/img/divingpoint/<? echo $dive_center_id;?>/img_main01.jpg" />
<? }elseif($category_file == condition){?>
<meta property="og:image"  content="https://buddydive.jp/common/img/condition/staticmap.png" />
<? }elseif($page_file == faq){?>
<meta property="og:image"  content="https://buddydive.jp/common/img/about/faq/img_blackboard.jpg" />
<? }elseif($meta_image){ ?>
<meta property="og:image"  content="https://buddydive.jp<?php echo $meta_image; ?>" />
<? }else{ ?>
<meta property="og:image"  content="https://buddydive.jp/common/img/index/img_main01.jpg" />
<? } ?>
<meta property="og:title"  content="<?php echo $meta_title; ?>" />
<meta property="og:description"  content="Buddy Diveは、自己責任で潜るダイバーを応援する予約サイトです。自分の意思と海が向き合った瞬間、海に潜るということが、ただごとではないと肌で感じることでしょう。" />
<meta property="og:site_name"  content="バディダイビング予約サイト - BuddyDive（バディダイブ）" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="<?php echo $meta_title; ?>">
<meta name="twitter:description" content="Buddy Diveは、自己責任で潜るダイバーを応援する予約サイトです。自分の意思と海が向き合った瞬間、海に潜るということが、ただごとではないと肌で感じることでしょう。">

<? if($last_file == detail){?>
<meta name="twitter:image"  content="https://buddydive.jp/common/img/divingpoint/<? echo $dive_center_id;?>/img_main01.jpg" />
<? }elseif($category_file == condition){?>
<meta name="twitter:image"  content="https://buddydive.jp/common/img/condition/staticmap.png" />
<? }elseif($page_file == faq){?>
<meta name="twitter:image"  content="https://buddydive.jp/common/img/about/faq/img_blackboard.jpg" />
<? }elseif($meta_image){ ?>
<meta name="twitter:image"  content="https://buddydive.jp<?php echo $meta_image; ?>" />
<? }else{ ?>
<meta name="twitter:image" content="https://buddydive.jp/common/img/index/img_main01.jpg">
<? } ?>
<link rel="Shortcut Icon" href="https://buddydive.jp/common/img/share/favicon.ico" type="image/x-icon" />
<title><?php echo $meta_title; ?></title>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="/common/js/jquery-1.10.2.min.js"><\/script>')</script>
<? if($category_file == divingpoint){ ?>
<script src="/common/js/layzr.js"></script>
<!-- bxslider -->
<script src="/common/js/bxslider/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
  $('.bxslider').bxSlider({
    captions: true,
    auto: true,
    speed: 800,
    mode: 'fade',
    autoHover: true
  });
});
</script>
<? } ?>
<script src="/common/js/sweetalert/sweetalert-dev.js"></script>
<link rel="stylesheet" href="/common/js/sweetalert/sweetalert.css">
<!--[if lt IE 9]>
<script src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="/common/css/normalize.css">
<link rel="stylesheet" href="/common/css/common.css">
<link rel="stylesheet" href="/common/css/page.css">
<link rel="stylesheet" href="/common/js/slidebars/slidebars.css">
<link rel="stylesheet" href="/common/js/bxslider/jquery.bxslider.css">
<link rel="stylesheet" href="/common/css/sp.css" media="only screen and (max-width:780px)">
</head>
<body>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/analytics.php'); ?>
<?php if($testserver) { echo '<div style="background:rgba(255,0,0,0.5); color:#fff; font-size:15px; text-align:center; position:fixed; z-index:9999; top:0; width:100%;">テストサーバーです。</div>' ; } ?>

<script language="javascript"> 
  $(function () {
    // プルダウン変更時に遷移
    $('select[name=sel_divepoint]').change(function() {
      if ($(this).val() != '') {
        window.location.href = "/divingpoint/reserve.php?dive_center_id="+$(this).val();
      }
    });
  });
</script>

<div id="<?php if($meta_home) { echo home; }else{echo inner;}?>">

<!--▼スマホメニュー-->
<div class="sb-slidebar sb-left">

<? if($_SESSION['member_id']=="") {
//ログアウト ?>
<ul>
<li><a href="/diver/login/?url=/index.php?">ログイン</a></li>
<li><a href="/divingpoint/">ダイビングポイント</a></li>
<li><a href="/condition/index.php?<?php echo "s_yyyy=" . date("Y") . "&s_mm=" .  date("m") . "&s_dd=" . date("d");?>">今日の海況</a></li>
<li><a href="/feature/">特集記事</a></li>
<li><a href="/about/guide.php">使い方をみる</a></li>
<li><a href="/about/faq/">よくある質問</a></li>
</ul>
<p class="headerBtn"><a href="/diver/registration/?url=<? echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>">無料ダイバー登録</a></p>
<? } else {
//ログイン ?>
<ul>
<li><a href="/diver/">ダイバーページ</a></li>
<li><a href="/divingpoint/">ダイビングポイント</a></li>
<li><a href="/condition/index.php?<?php echo "s_yyyy=" . date("Y") . "&s_mm=" .  date("m") . "&s_dd=" . date("d");?>">今日の海況</a></li>
<li><a href="/feature/">特集記事</a></li>
<li><a href="/about/guide.php">使い方をみる</a></li>
<li><a href="/about/faq/">よくある質問</a></li>
<li><a href="/diver/login/logout.php">ログアウト</a></li>
</ul>
<div class="headerCustomSp">
  <p>
    <select name="sel_divepoint" id="sel_divepoint">
      <option value="">予約する</option>
      <?
        //ポイントリスト表示
        $arr_db_field = array("dive_center_name", "dive_center_point", "img_1");
        
        $sql = "SELECT dive_center_id, ";
        foreach($arr_db_field as $val)
        {
          $sql .= $val.", ";
        }
        $sql .= " 1 FROM dive_center where flag_open=1 ";
        $sql .= " order by view_level ";

        $db_result = $common_dao->db_query($sql);
        if($db_result)
        {
          for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
          {
      ?>
            <option value="<? echo $db_result[$db_loop]["dive_center_id"];?>"><? echo $db_result[$db_loop]["dive_center_point"];?></option>
      <?
          }
        }
      ?>
    </select>
  </p>
</div>
<p class="headerBtn"><a href="/diver/invitation/">バディを招待する</a><p>
<? } ?>
</div>
<!--▲スマホメニュー-->

<div id="sb-site">
<header id="header">
<div class="pc">
<?php if($home) { ?>
<h1 class="logo"><a href="/"><img src="/common/img/header/logo.png" alt="BuddyDive"></a></h1>
<?php }else{ ?>
<h1 class="logo"><a href="/"><img src="/common/img/header/logo_blue.png" width="121" height="26" alt="BuddyDive"></a><span class="headTxt">バディダイビングをはじめよう！</span></h1>
<?php } ?>

<nav>
<!--▼PCヘッダー-->
  <ul>
  <? if($_SESSION['member_id']==""){ 
  //ログアウト ?>
    <li class="headerBtn"><a href="/diver/registration/?url=<? echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>">無料ダイバー登録</a></li>
    <li><a href="/diver/login/?url=<? echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>">ログイン</a></li>
    <li><a href="/about/faq/">よくある質問</a></li>
    <li><a href="/about/guide.php">使い方をみる</a></li>
  <? }else{ 
  //ログイン ?>
    <li class="headerBtn"><a href="/diver/invitation/">バディを招待する</a></li>
    <li class="headerCustom">
      <select name="sel_divepoint" id="sel_divepoint">
        <option value="">予約する</option>
        <?
          //ポイントリスト表示
          $arr_db_field = array("dive_center_name", "dive_center_point", "img_1");
          
          $sql = "SELECT dive_center_id, ";
          foreach($arr_db_field as $val)
          {
            $sql .= $val.", ";
          }
          $sql .= " 1 FROM dive_center where flag_open=1 ";
          $sql .= " order by view_level ";

          $db_result = $common_dao->db_query($sql);
          if($db_result)
          {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
        ?>
              <option value="<? echo $db_result[$db_loop]["dive_center_id"]?>"><? echo $db_result[$db_loop]["dive_center_point"]?></option>
        <?
            }
          }
        ?>
      </select>
    </li>
    <li><a href="/diver/login/logout.php">ログアウト</a></li>
    <li class="circleImg">
    		<a href="/diver/">
        <?
				if($_SESSION["member_img"]!="") 
				{ 
				?>
        	<span class="img" style="background:url(<? echo global_ssl."/".global_member_dir.$_SESSION["member_id"]."/".$_SESSION["member_img"];?>);"></span>
				<? 
				} 
				else
				{ 
				?>
        	<span class="img" style="background:url(/app_photo/member/default/noimage.gif);"></span>
				<? 
				} 
				
					//メッセージ
					$sql = "SELECT count(member_message_id) as message_count FROM member_message ";
					$sql .= " where member_id='".$_SESSION["member_id"]."' ";
					$db_result = $common_dao->db_query($sql);
				?>
          <? if($db_result[0]["message_count"]!="0") { ?>
          <span class="noticeIcon"><? echo $db_result[0]["message_count"];?></span>
          <? } ?>
          
				<? echo $_SESSION['user_name'];?>さん
        </a>
    </li>
  <? } ?>
  </ul>
</div>
<!--▲PCヘッダー-->
<!--▼スマホヘッダー-->
<div class="navSp sp">
<p class="logoSp"><a href="/"><img src="/common/img/header/logo_blue.png" width="121" height="26" alt="BuddyDive"></a></p>
<p class="menu sb-toggle-left"><img src="/common/img/header/btn_menu.png" width="30" height="34" alt="menu"></p>

<? if($_SESSION['member_id']==""){ 
  //ログアウト ?>
<p class="menberImg"><a href="/diver/login/?url=/index.php?"><img src="/common/img/header/btn_login.png" width="34" height="34"></a></p>
<? }else{ 
  //ログイン?>
<p class="menberImg"><a href="/diver/">
        <?
        if($_SESSION["member_img"]!="") 
        { 
        ?>
          <span class="img" style="background:url(<? echo global_ssl."/".global_member_dir.$_SESSION["member_id"]."/".$_SESSION["member_img"];?>);"></span>
        <? 
        } 
        else
        { 
        ?>
          <span class="img" style="background:url(/app_photo/member/default/noimage.gif);"></span>
        <? 
        } 
          //メッセージ
          $sql = "SELECT count(member_message_id) as message_count FROM member_message ";
          $sql .= " where member_id='".$_SESSION["member_id"]."' ";
          $db_result = $common_dao->db_query($sql);
        ?>
          <? if($db_result[0]["message_count"]!="0") { ?>
          <span class="noticeIcon"><? echo $db_result[0]["message_count"];?></span>
          <? } ?>
</a></p>
<? } ?>
</div>
<!--▲スマホヘッダー-->
</nav>
</header>