<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<?
	//javascript 削除 fnChangeDel(i)
	$common_connect->Fn_javascript_delete("削除しますか？", "./message_del.php?member_message_id=");
	
	//javascript 削除 fnReserveDel(i)
	$common_connect->Fn_javascript_delete_reserve("取消しますか？", "./reserve_del.php?dive_reserve_id=");
	
?>

<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	if($member_id!="")
	{
		$arr_db_field = array("user_name", "member_email", "member_login_pw");
		$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana", "member_point"));
		$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
		$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
			
		$sql = "SELECT member_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " regi_date FROM member where member_id='".$member_id."'";
		$sql .= " and flag_hontouroku='1'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	//ステータスをArrayへ
	$sql = "SELECT cate_status_id, cate_status_title FROM cate_status order by view_level";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_status_title[$db_result[$db_loop][cate_status_id]] = $db_result[$db_loop][cate_status_title];
		}
	}
?>
<article>
<div id="diverBox">


<?
//メッセージ
$sql = "SELECT member_message_id, dive_reserve_id, member_id, from_member_id, message, regi_date FROM member_message ";
$sql .= " where member_id='".$member_id."' ";
$sql .= " order by member_message_id desc";

$db_result = $common_dao->db_query($sql);
if($db_result)
{
?>
  <section class="mypageCont notice">
    <p class="tit">通知</p>
    <ul class="noticeUl">
    <?
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			if($db_result[$db_loop]["dive_reserve_id"]!="0")
			{
				//予約データ
				$sql_reserve = "SELECT ";
				$sql_reserve .= " yyyymmdd, member_id, dive_center_id, dive_center_point, reserve_style_id, reserve_boat_id, yyyymmdd, ";
				$sql_reserve .= " member_name_1, member_name_2, ";
				$sql_reserve .= " up_date FROM dive_reserve where dive_reserve_id='".$db_result[$db_loop]["dive_reserve_id"]."' ";
				
				$db_result_reserve = $common_dao->db_query($sql_reserve);
				if($db_result_reserve)
				{
					$db_message_member = $common_member -> Fn_member_info ($common_dao, $db_result_reserve[0]["member_id"]);
					
					 if($db_message_member[0]["img_1"]!="")
					 {
						 $message_member_img_1 = "/".global_member_dir.$db_result_reserve[0]["member_id"]."/".$db_message_member[0]["img_1"];
					 }
					 else
					 {
						 $message_member_img_1 = "/app_photo/member/default/noimage.gif";
					 }
    ?>
      <li>
        <span class="circleImg" style="background:url(<? echo $message_member_img_1;?>);"></span>
        <a href="<? echo "/divingpoint/reserve.php?&dive_center_id=".$db_result_reserve[0]["dive_center_id"]."&s_yyyymmdd=".str_replace("-", "", $db_result_reserve[0]["yyyymmdd"])."&s_style_id=".$db_result_reserve[0]["reserve_style_id"]."&s_boat_id=".$db_result_reserve[0]["reserve_boat_id"];?>">「<? echo $db_result_reserve[0]["member_name_1"]." ".$db_result_reserve[0]["member_name_2"];?>」さんから「<? echo date("Y/m/d", strtotime($db_result_reserve[0]["yyyymmdd"]));?> <? echo $db_result_reserve[0]["dive_center_point"];?>」のダイビングに招待があります。このリンクから予約してください。</a>
        <br>※心当たりがなければ<a href="#" onClick='fnChangeDel("<?php echo $db_result[$db_loop]["member_message_id"];?>");'>削除</a>してください。
      </li>
    <?
				}
				else
				{
					//招待があったが、予約が削除された場合通知削除
					$dbup = "delete from member_message where member_id = '".$member_id."' and member_message_id = '".$db_result[$db_loop]["member_message_id"]."'";
					$db_result = $common_dao->db_update($dbup);
				}
			}
			else
			{
    ?>
      <li>
        <span class="circleImg" style="background:url(http://buddydive.jp/app_photo/member/5/5__1.jpg);"></span>
        <? echo $db_result[$db_loop]["message"];?>（<? echo date("Y年m月d日", strtotime($db_result[$db_loop]["regi_date"]));?>）
        <br>※心当たりがなければ<a href="#" onClick='fnChangeDel("<?php echo $db_result[$db_loop]["member_message_id"];?>");'>削除</a>してください。
      </li>
    <?
			}
		}
		?>
    </ul>
  </section>
<?
}
?>

<section class="mypageCont">
  <p class="tit">個人情報</p>
  <table class="formTable">
    <tr>
    <th class="vaTop">
    	<p class="diverImg">
    		<?
       if($img_1!="")
			 {
				 echo "<img src=\"/".global_member_dir.$member_id."/".$img_1."\" width=\"180px\">";
			 }
			 else
			 {
				 echo "<img src=\"/app_photo/member/default/noimage.gif\">";
			 }
			?>
     </p>

      <p class="myPoint"><span class="pointIcon">P</span>ポイント<span class="pointNumber"><? echo number_format($member_point);?></span>pt</p>
    </th>
    <td>
      <table class="txtTable">
        <tr><th>ユーザー名</th><td><? echo $user_name;?></td></tr>
        <tr><th>メールアドレス</th><td><? echo $member_email;?><br>[ <a href="/diver/profile/info.php?return_url=<? echo $_SERVER['PHP_SELF'];?>">メールアドレスとパスワード変更</a> ]</td></tr>
        <tr><th>姓名</th><td><? echo $member_name_1." ".$member_name_2;?><br>[ <a href="/diver/profile/?return_url=<? echo $_SERVER['PHP_SELF'];?>">プロフィール編集</a> ]</td></tr>
        <tr><th>最終認定ランク</th><td><? echo $diving_rank;?></td></tr>
        <tr><th>ダイビング本数</th><td><? echo number_format($diving_count);?>本</td></tr>
        <tr>
          <th>スキルチェック</th>
          <td>
          <?						
					$sql = "SELECT yes_no FROM member_skill where member_id='".$member_id."' and skill_id=1 ";
					
					$db_result = $common_dao->db_query($sql);
					if($db_result)
					{
							$skill_yes_no = $db_result[0]["yes_no"];
					}
					
					if($skill_yes_no=="1")
					{
						echo "すべてのスキルができます";
					}
					elseif($skill_yes_no=="2")
					{
						echo "できないスキルがあります<br><span class='cRed'>※バディダイビングに参加できません</span> <a href='/diver/profile/skill_course.php' target='_blank'>スキル講習に興味がある &gt;</a>";
					}
					else
					{
						echo "<span class='cRed'>登録が必要です。</span> ";
					}
					?>
          <br>[ <a href="/diver/profile/skill.php?return_url=<? echo $_SERVER['PHP_SELF'];?>">スキルチェック編集</a> ]
          </td>
        </tr>
        <tr>
          <th>メディカルチェック</th>
          <td>
          <?	
					$sql = "SELECT yes_no FROM member_medical where member_id='".$member_id."' and medical_id=1 ";
					
					$db_result = $common_dao->db_query($sql);
					if($db_result)
					{
							$medical_yes_no = $db_result[0]["yes_no"];
					}
					
					if($medical_yes_no=="1")
					{
						echo "該当する項目はありません";
					}
					elseif($medical_yes_no=="2")
					{
						echo "該当する項目があるが、医師の診断許可書を持参します ";
					}
					else
					{
						echo "<span class='cRed'>登録が必要です。</span> ";
					}
					?>
          <br>[ <a href="/diver/profile/medical.php?return_url=<? echo $_SERVER['PHP_SELF'];?>">メディカルチェック編集</a> ]
          </td>
        </tr>
      </table>
    </td>
    </tr>
  </table>
</section>


<?		
	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id", "member_name_1", "member_name_2");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_point", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where member_id='".$member_id."' and status<90 and yyyymmdd>='".date("Y-m-d")."' ";
	$sql .= " order by yyyymmdd";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
?>
<section class="mypageCont">
	
  <p class="tit">予約中</p>
<?
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			
			$db_result_dive_center = $common_center->Fn_center_info ($common_dao, $dive_center_id);
			$img_1 = $db_result_dive_center[0]["img_1"];
?>
<div class="reservationBox">
  <table class="formTable">
    <tr>
      <th class="vaTop" align="left" rowspan="2">
        <p class="diverImg"><a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>"><img src="/<? echo global_dive_center_dir.$dive_center_id."/".$img_1;?>" width="180" alt="<? echo $dive_center_point;?>"></a></p>
      </th>
      <td rowspan="2">
        <table class="txtTable">
          <tr>
            <th>ポイント</th>
            <td><a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>"><? echo $dive_center_point;?></a></td>
          </tr>
          <tr>
            <th>日付</th>
            <td><? echo date('Y/m/d', strtotime($yyyymmdd))?></td>
          </tr>
          <tr>
            <th>バディ</th>
            <td>
            <ul class="buddyUl">
							<?
              $arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id, $member_id) ;
              if($arr_buddy)
              {
                for($db_loop_buddy=0 ; $db_loop_buddy < count($arr_buddy["member_id"]) ; $db_loop_buddy++)
                {
            ?>
            <li>
            <? if($arr_buddy["img_1"][$db_loop_buddy]!="") { ?>
            <p class="circleImg" style="background:url(<? echo global_ssl."/".global_member_dir.$arr_buddy["member_id"][$db_loop_buddy]."/".$arr_buddy["img_1"][$db_loop_buddy];?>);"></p>
            <? } else { ?>
            <p class="circleImg" style="background:url(/app_photo/member/default/noimage.gif)"></p>
            <? } ?>
            <span><? echo $arr_buddy["member_name_1"][$db_loop_buddy]." ".$arr_buddy["member_name_2"][$db_loop_buddy];?></span>
            
						<?
							$result_status=$common_member->Fn_buddy_status($common_dao, $member_id, $arr_buddy["reserve_id"][$db_loop_buddy], $arr_buddy["member_id"][$db_loop_buddy]);
							if($result_status=="未予約")
							{
								echo "<span class='error'>（未予約）</span>";
							}
							else
							{
								echo "<span>".$arr_cate_status_title[$result_status]."</span>";
							}
            ?>
            </li>
            
            <?
                }
              }
            ?>
          
          
					<?
					
            $db_result_invited = $common_member->Fn_inivited_list($common_dao, $dive_reserve_id, $member_id) ;
            if($db_result_invited)
            {
              for($db_loop_invited=0 ; $db_loop_invited < count($db_result_invited) ; $db_loop_invited++)
              {
          ?>
            <li>
            <p class="circleImg" style="background:url(/app_photo/member/default/undiver.gif)"><!--<img src="/app_photo/member/default/undiver.gif">--></p>
            <span><? echo $db_result_invited[$db_loop_invited]["invited_name"];?></span>
            <span class='error'>（未予約）</span>
            </li>
          <?
              }
            }
          ?>
            </ul>
            <a href="/divingpoint/buddy.php?dive_reserve_id=<? echo $dive_reserve_id;?>">バディ変更・追加</a>
            </td>
          </tr>
          <tr>
            <th>スタイル</th>
            <td><? echo $reserve_style_name;?></td>
          </tr>
          <tr>
            <th>エントリー</th>
            <td><? echo $reserve_boat_name;?></td>
          </tr>
          <tr>
            <th>タンク</th>
            <td><? echo $reserve_1_tank_name;?>　<? echo $reserve_2_tank_name;?>　<? echo $reserve_3_tank_name;?></td>
          </tr>
          <tr>
            <th>金額</th>
            <td>¥<? echo number_format($reserve_all_price);?></td>
          </tr>
        </table>
      </td>
      <td class="formTableBtn" valign="top">バディダイビングの参加には保険の加入が必要です。1日掛け捨て保険の場合、ご自宅出発前までの加入が適用条件となるものもあります。<br>前日までのお申し込みをお願いします。<br>
      <p class="blueBtn small mt10 mb20"><a href="/about/insurance.php">保険の申込</a></p></td>
    </tr>
    </tr>
      <td valign="bottom">
      	<p class="mb10">ご予約やバディについての変更・ご要望・ご質問がございましたら、下記よりご連絡ください。</p>
      	<ul class="changCancelUl">
      	  <li class="blueBtn small"><a href="/divingpoint/contact/");'>予約の変更など</a></li>
      	  <li class="grayBtn"><a href="#" onClick='fnReserveDel("<? echo $dive_reserve_id;?>");'>予約取消</a></li>
        </ul>
	  </td>
    </tr>
  </table>
</div>
<?
		}
?>
</section>
<?
	}
?>


<?		
	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "dive_center_id");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_point", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where member_id='".$member_id."' and status<90 and yyyymmdd<'".date("Y-m-d")."' ";
	$sql .= " order by yyyymmdd desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
?>

<section class="mypageCont">
  <p class="tit">ダイビングログ</p>
  <table class="logTable">

<?
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
?>
    <tr>
      <th><? echo date('Y/m/d', strtotime($yyyymmdd))?></th>
      <td class="pointImg">
      <?
				$db_result_dive_center = $common_center->Fn_center_info ($common_dao, $dive_center_id);
				$img_1 = $db_result_dive_center[0]["img_1"];
      ?>
        <a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>">
          <img src="/<? echo global_dive_center_dir.$dive_center_id."/".$img_1;?>" width="180" alt="<? echo $dive_center_point;?>">
        </a>
      </td>
      <td><a href="/divingpoint/detail.php?dive_center_id=<? echo $dive_center_id;?>"><? echo $dive_center_point;?></a><br><? echo $reserve_style_name;?></td>
      <td>
      <ul class="buddyUl">
				<?
          $arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id, $member_id) ;
          if($arr_buddy)
          {
            for($db_loop_buddy=0 ; $db_loop_buddy < count($arr_buddy["member_id"]) ; $db_loop_buddy++)
            {
        ?>
        <li>
        <? if($arr_buddy["img_1"][$db_loop_buddy]!="") { ?>
        <p class="circleImg" style="background:url(<? echo "/".global_member_dir.$arr_buddy["member_id"][$db_loop_buddy]."/".$arr_buddy["img_1"][$db_loop_buddy];?>);"></p>
        <? } else { ?>
        <p class="circleImg" style="background:url(/app_photo/member/default/noimage.gif)"></p>
        <? } ?>
        <span><? echo $arr_buddy["member_name_1"][$db_loop_buddy]." ".$arr_buddy["member_name_2"][$db_loop_buddy];?></span>
        
        
        </li>
        
        <?
            }
          }
        ?>
      </ul>
      </td>
    </tr>
<?
		}
?>

  </table>
</section>
<?
	}
?>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>