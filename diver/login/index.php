<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input_email("member_email");
			err_check_count += check_input_pw("member_login_pw");
			
			if(err_check_count!=0)
			{
				swal("error","入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<span class='errorForm'>６文字以上入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						
	});
	
//-->
</script>

<article>
<div id="diverBox">
<section class="accountBox">
	<form action="/diver/login/login_check.php" name="form_regist" id="form_regist" method="post">
		<? $var = "url";?>
    <input name="<?=$var;?>" id="<?=$var;?>" type="hidden" value="<? echo str_replace("&&&", "?", str_replace("url=", "", $_SERVER['QUERY_STRING']));?>">
    <p class="tit">ログイン</p>
    <div>
      <? $var = "member_email";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="メールアドレス" class="textInput">
      <label id="err_<?=$var;?>"></label>
    </div>
    <div>
      <? $var = "member_login_pw";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="password" placeholder="パスワード" class="textInput">
      <label id="err_<?=$var;?>"></label>
    </div>
    <p class="blueBtn mt30">
			<? $var = "form_confirm";?>
      <input name="<?=$var;?>" id="<?=$var;?>"  type="submit" value="ログイン" />
    </p>
    <p class="userBtn"><a href="/diver/login/password_forget.php">パスワードをお忘れた場合 &gt;&gt;</a></p>
	</form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>