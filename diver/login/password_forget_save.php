<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>パスワード再発行チェック</title>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");

	if ($member_email == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}

		$sql = "select member_id from member where member_email ='".$member_email."' and flag_hontouroku=1 ";


		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$member_id = $db_result[0]["member_id"];
		}
		else
		{
			$common_connect->Fn_javascript_back("登録されてないメールです。");
		}
	
	$member_login_pw = $common_connect-> Fn_random_password(10);
	$db_insert = "update member set  member_login_pw='".$member_login_pw."' ";
	$db_insert .= " where member_id='".$member_id."' ";
	
	$db_result = $common_dao->db_update($db_insert);
	
	$temp_url = global_ssl."/diver/login/";

	//Thank youメール
	if ($member_email != "")
	{
		$subject = "『Buddy Diving』パスワードを再発行しました。";
		
		$body = "";
		
$body = <<<EOF

この度は『Buddy Diving』のご利用頂き、

誠にありがとうございます。

──────────────────────────────

--------------------------------------------------
再発行内容
--------------------------------------------------
【メールアドレス】 $member_email
【パスワード】 $member_login_pw

【ログインURL】
$temp_url

【受 付 日】$datetime

──────────────────────────────

※なお、このメールに憶えのない方は、
下記メールアドレスまでご連絡ください。
info@buddydiving.jp

$global_email_footer

EOF;

}

	$common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	$common_connect-> Fn_javascript_move("パスワード再発行完了しました。メールを確認してください。", global_ssl."/diver/login/");
?>
</body>
</html>