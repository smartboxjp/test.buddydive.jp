<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>
<?

	if ($member_email == "" or $member_login_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	else
	{
		$sql = "select member_id, user_name, member_email, member_name_1, member_name_2, img_1 from member where member_email ='".$member_email."' and member_login_pw='".$member_login_pw."' and flag_hontouroku=1 ";


		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$db_member_id = $db_result[0]["member_id"];
			$db_member_email = $db_result[0]["member_email"];
			$db_user_name = $db_result[0]["user_name"];
			$db_member_name_1 = $db_result[0]["member_name_1"];
			$db_member_name_2 = $db_result[0]["member_name_2"];
			$member_img = $db_result[0]["img_1"];
		}
		else
		{
			$common_connect->Fn_javascript_back("メールアドレスとパスワードを確認してください。");
		}
		
		if ($db_member_email == $member_email)
		{
			//管理者の場合
			session_start();
			$_SESSION['member_id']=$db_member_id;
			$_SESSION['user_name']=$db_member_name_1." ".$db_member_name_2;
			$_SESSION['member_img']=$member_img;
			
			if(trim($db_member_name_1)=="")
			{
				$common_connect->Fn_javascript_move("プロフィール登録が必要です。", global_ssl."/diver/profile/");
			}
			
			
			$sql = "SELECT yes_no FROM member_skill where member_id='".$db_member_id."' and skill_id=1 ";
			$db_result = $common_dao->db_query($sql);
			if(!$db_result)
			{
				$common_connect->Fn_javascript_move("スキルチェックが必要です。", global_ssl."/diver/profile/skill.php");
			}
			
			$sql = "SELECT yes_no FROM member_medical where member_id='".$db_member_id."' and medical_id=1 ";
			$db_result = $common_dao->db_query($sql);
			if(!$db_result)
			{
				$common_connect->Fn_javascript_move("メディカルチェックが必要です。", global_ssl."/diver/profile/medical.php");
			}
			

			$arr_data = preg_split('/\?/', $url);

			$url_check = $arr_data[0];

			if($url_check!="" && $url_check!="/diver/login/index.php" && $url_check!="/diver/registration/index.php")
			{
				$common_connect->Fn_redirect($url);
			}
			else
			{
				$common_connect->Fn_redirect(global_ssl."/diver/profile/");
			}

		}
	}
?>
</body>
</html>