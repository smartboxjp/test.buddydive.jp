<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	$common_member = new CommonMember();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>バディ招待</title>
</head>

<body>
<?
	
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	if ($invited_name == "" or $invited_email == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}

	//予約したメンバー
	$db_member_info = $common_member -> Fn_member_info($common_dao, $member_id) ;
	if($db_member_info!="")
	{
		$member_email = $db_member_info[0]["member_email"];
		$member_name_1 = $db_member_info[0]["member_name_1"];
		$member_name_2 = $db_member_info[0]["member_name_2"];
	}
	
	//メールチェック
	$sql = "SELECT  member_id FROM member where member_email='".$invited_email."' and flag_hontouroku=1 ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		if($db_result[0]["member_id"]!="")
		{
			$common_connect-> Fn_javascript_back("既に登録されているメールです。");
		}
	}
		//招待したことがあるかチェック
	$sql = "SELECT  invite_id FROM invite where invited_email='".$invited_email."' and member_id='".$member_id."' and status=1 ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$common_connect-> Fn_javascript_back("招待したことがあるメンバーです。");
	}
	
	//使用されてない招待キーは削除
	$db_insert .= " Delete from invite where invited_email='".$invited_email."' and member_id='".$member_id."'";
	$db_result = $common_dao->db_update($db_insert);
	
	$datetime = date("Y-m-d H:i:s");
	
	$invited_key = date("Ymd").$common_connect-> Fn_random_password(20);
	$status = 0;
	
	$arr_db_field = array("invited_key", "member_id", "dive_reserve_id", "status", "reserve_yyyymmdd", "invited_name", "invited_email");
	
	$db_insert = "insert into invite ( ";
	$db_insert .= " invite_id, invite_point, invited_point, ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= $val.", ";
	}
	$db_insert .= " regi_date, up_date ";
	$db_insert .= " ) values ( ";
	$db_insert .= " '', '".global_invite_point."','".global_invited_point."',";
	foreach($arr_db_field as $val)
	{
		$db_insert .= " '".$$val."', ";
	}
	$db_insert .= " '$datetime', '$datetime')";
	$db_result = $common_dao->db_update($db_insert);
	


	$temp_url = global_ssl."/diver/registration/?invited_key=".$invited_key;
	$invite_point = "招待した人：".number_format(global_invite_point)."pt";
	$invited_point = "招待された人：招待".number_format(global_invited_point)."pt + 新規登録" .number_format(global_invited_point)."pt";

	//Thank youメール
	if ($invited_email != "")
	{
		$subject = "【BuddyDive】".$member_name_1." ".$member_name_2."様よりダイバー登録の招待が届きました";
		
		$body = "";
		
$body = <<<EOF
$invited_name 様

こんにちは、BuddyDive事務局です。
$member_name_1 $member_name_2 様よりバディダイブの招待が届きました。

内容をご確認のうえ、下記URLよりダイバー登録をお願いします。
$temp_url

【バディ招待特典】「招待された人」と「招待した人」それぞれにポイントをプレゼントします。
$invited_point
$invite_point

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
バディ各自のご予約が必要です。
バディダイビングでは最低2人1組でのエントリーが必須です。
お二人様以上おご予約がない場合は、すでにご予約されたバディの方もダイビングができませんのでご了承ください。

$global_email_footer


EOF;

}

	$common_email-> Fn_send_utf($invited_name."<".$invited_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);


	$common_connect-> Fn_redirect("./thankyou.php");

?>
</body>
</html>