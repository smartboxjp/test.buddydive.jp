<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$url = $_GET["url"];

?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("invited_name");
			err_check_count += check_input_email("invited_email");
			
			if(err_check_count!=0)
			{
				swal("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
	});
	
//-->
</script>
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
?>

<article>
<div id="diverBox">
<section class="accountBox">
	<form action="/diver/invitation/index_save.php" name="form_regist" id="form_regist" method="post">
    <p class="tit">バディを招待する</p>
    <p class="importantTxt mt20"><span class="line">「招待した人」に<span>500pt</span>。<br>「招待された人」に招待<span>500pt</span>+新規登録<span>500pt</span>がプレゼントされます。</span></p>
    <div>
      <? $var = "invited_name";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="お名前" class="textInput">
      <label id="err_<?=$var;?>"></label>
    </div>
    <div>
      <? $var = "invited_email";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="メールアドレス" class="textInput">
      <label id="err_<?=$var;?>"></label>
    </div>
    <p class="blueBtn mt20">
			<? $var = "form_confirm";?>
      <input name="<?=$var;?>" id="<?=$var;?>"  type="submit" value="バディ招待" />
    </p>
	</form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>