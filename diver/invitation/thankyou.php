<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<article>
<div id="diverBox">
<section class="accountBox">
<p class="tit">バディ招待メール送信完了</p>
<p class="mt20">ご入力いただいたバディのメールアドレスに招待メールを送信しました。バディの方のご登録が完了しますと、招待した方、招待された方それぞれに<span class="pointIcon">P</span><span class="cPink">500pt</span>のプレゼントしています。<br>
<span class="cRed">※メールが届いていない場合は迷惑メールフォルダもご確認ください。</span></p>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>