<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>取消</title>
</head>

<body>
<?
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];

	if ($dive_reserve_id=="") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	
	
	$arr_db_field = array("dive_reserve_id", "yyyymmdd", "member_id", "user_name", "member_name_1", "member_name_2", "dive_center_id");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_point", "reserve_style_id", "reserve_style_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_boat_id", "reserve_boat_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_1_tank_id", "reserve_1_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_2_tank_id", "reserve_2_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("reserve_3_tank_id", "reserve_3_tank_name"));
	$arr_db_field = array_merge($arr_db_field, array("etc_price", "reserve_tax", "reserve_all_price"));
	$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));

	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " up_date FROM dive_reserve where member_id='".$member_id."' and dive_reserve_id='".$dive_reserve_id."' ";
	$sql .= " order by yyyymmdd desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[0][$val];
		}
		
		//会員情報
		$db_result_member = $common_member->Fn_member_info ($common_dao, $member_id);
		$member_email = $db_result_member[0]["member_email"];

		$db_result_dive_center = $common_center->Fn_center_info ($common_dao, $dive_center_id);
		$dive_center_name = $db_result_dive_center[0]["dive_center_name"];
		$dive_center_point = $db_result_dive_center[0]["dive_center_point"];
		$dive_center_email = $db_result_dive_center[0]["dive_center_email"];

		
	$mail_yyyymmdd = date('Y'.'年'.'m'.'月'.'d'.'日', strtotime($yyyymmdd));

//Thank youメール
$subject = "【BuddyDive】バディダイブの予約が取消されました";

$body = "";
		
$body = <<<EOF
=======================================================
【BuddyDive】バディダイブの予約が取消されました
=======================================================

$member_name_1 $member_name_2 様

こんにちは、BuddyDive事務局です。
バディダイブの予約が取消されました。

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
■日付：$mail_yyyymmdd
■ポイント：$dive_center_name （ $dive_center_point ）
■スタイル：$reserve_style_name
■エントリー：$reserve_boat_name
＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
ご予約時に入力されたバディの方に予約取消のメールをお送りしております。
バディ各自ご予約の取消が必要です。
バディダイビングでは最低2人1組でのエントリーが必須です。
お二人様以上おご予約がない場合は、すでにご予約されたバディの方もダイビングができませんのでご了承ください。


$global_email_footer

EOF;


		if ($member_email != "")
		{
			$common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
		}
		$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
		
		if ($dive_center_email != "")
		{
			$common_email-> Fn_send_utf($dive_center_email."<".$dive_center_email.">",$subject,$body,$global_mail_from,$global_send_mail);
		}
		$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

		//buddyにメール
		$arr_buddy = $common_member->Fn_buddy_list ($common_dao, $dive_reserve_id, $member_id) ;
		if($arr_buddy)
		{
			for($db_loop_buddy=0 ; $db_loop_buddy < count($arr_buddy["member_id"]) ; $db_loop_buddy++)
			{
				$result_status=$common_member->Fn_buddy_status($common_dao, $member_id, $dive_reserve_id, $arr_buddy["member_id"][$db_loop_buddy]);
				
			
				if($result_status=="0")
				{
					$buddy_member_name_1 = $arr_buddy["member_name_1"][$db_loop_buddy];
					$buddy_member_name_2 = $arr_buddy["member_name_2"][$db_loop_buddy];
					$buddy_member_email = $arr_buddy["member_email"][$db_loop_buddy];
					
//キャンセルされた場合buddyへメール
$buddy_subject = "【BuddyDive】バディの予約が取消されました";

$buddy_body = "";
		
$buddy_body = <<<EOF
=======================================================
【BuddyDive】バディの予約が取消されました
=======================================================

$buddy_member_name_1 $buddy_member_name_2 様

こんにちは、BuddyDive事務局です。
$member_name_1 $member_name_2 様のバディの予約が取消されました。

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
■日付：$mail_yyyymmdd
■ポイント：$dive_center_name （ $dive_center_point ）
■スタイル：$reserve_style_name
■エントリー：$reserve_boat_name
＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
バディダイビングでは最低2人1組でのエントリーが必須です。
お二人様以上おご予約がない場合は、すでにご予約されたバディの方もダイビングができませんのでご了承ください。


$global_email_footer

EOF;
					if ($buddy_member_email != "")
					{
						$common_email-> Fn_send_utf($buddy_member_email."<".$buddy_member_email.">",$buddy_subject,$buddy_body,$global_mail_from,$global_send_mail);
					}
				}
			}
		}//buddyにメール
		
	} //if($db_result)
		
	//$dbup = "delete from dive_reserve where dive_reserve_id = '$dive_reserve_id' and member_id = '$member_id'";
	//20160118 キャンセル：98
	$dbup = "update dive_reserve set status='98' where dive_reserve_id = '$dive_reserve_id' and member_id = '$member_id'";
	$common_dao->db_update($dbup);
	
	$dbup = "delete from member_message where member_id = '".$member_id."' and member_message_id = '".$db_result[$db_loop]["member_message_id"]."'";
	$common_dao->db_update($dbup);

	$common_connect -> Fn_javascript_move("取消を行いました。", "./");

?>
</body>
</html>