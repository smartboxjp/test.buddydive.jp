<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<script language="javascript"> 
	function fnImgDel(j, k) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './img_one_del.php?img='+j+'&img_name='+k;
		} 
	}
</script>

<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input_email("member_email");
			err_check_count += check_input_pw("member_login_pw");
			
			if(err_check_count!=0)
			{
				swal("error","入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span style='color:#cb112b;'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span style='color:#cb112b;'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span style='color:#cb112b;'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span style='color:#cb112b;'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<span style='color:#cb112b;'>６文字以上入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						
	});
	
//-->
</script>
<?

	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	$return_url = $_GET["return_url"];
	
	if($member_id!="")
	{
		$arr_db_field = array("user_name", "member_email", "member_login_pw", "temp_key");
		$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana"));
		$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
		$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
			
		$sql = "SELECT member_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " regi_date FROM member where member_id='".$member_id."'";
		//$sql .= " and flag_open='1'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
?>
<article>
<div id="diverBox">
<section class="mypageCont">
<p class="tit">メールアドレス・パスワード</p>
	<? $var = "form_regist";?>
  <form action="info_save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" enctype="multipart/form-data">
    <table class="formTable">
      <tr>
        <th>メールアドレス</th>
        <td>
          <? $var = "member_email";?>
          <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="メールアドレス" class="textInput" value="<? echo $$var;?>">
          <label id="err_<?=$var;?>"></label>
        </td>
      </tr>
      <tr>
        <th>パスワード</th>
        <td>
          <? $var = "member_login_pw";?>
          <input name="<?=$var;?>" id="<?=$var;?>" type="password" placeholder="パスワード" class="textInput">
          <label id="err_<?=$var;?>"></label>
        </td>
      </tr>
    </table>
    <? $var = "form_confirm";?>
    <p class="blueBtn mt20"><input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="変更する"></p>
    
		<? $var = "return_url";?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
  </form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>