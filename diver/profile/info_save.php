<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>


<?

	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($member_email=="" || $member_login_pw=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//メールチェック
	$sql = "SELECT  member_id FROM member where member_email='".$member_email."' and flag_hontouroku=1 and member_id!='".$member_id."' ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		if($db_result[0]["member_id"]!="")
		{
			$common_connect-> Fn_javascript_back("既に登録されているメールです。");
		}
	}
	
	
	//メール送信のため
	$sql = "SELECT member_email FROM member where member_id='".$member_id."' ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$orignal_member_email = $db_result[0]["member_email"];
	}
	
	
	$arr_db_field = array("member_email", "member_login_pw");
		
	$db_insert = "update member set ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= $val."='".$$val."', ";
	}
	$db_insert .= " up_date='".$datetime."' ";
	$db_insert .= " where member_id='".$member_id."'";
	$db_result = $common_dao->db_update($db_insert);
	

if($orignal_member_email!="")
{
	$subject = "『Buddy Dive』メールまたはパスワードが変更されました。";
	
	$body = "";
		
$body = <<<EOF
こんにちは、BuddyDive事務局です。
メールまたはパスワードが変更されました。

※なお、このメールに憶えのない方は、
下記メールアドレスまでご連絡ください。
info@buddydive.jp

$global_email_footer


EOF;
	$common_email-> Fn_send_utf($orignal_member_email."<".$orignal_member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
}

		
	if($return_url=="")
	{
		$return_url = global_ssl."/diver/";
	}
	$common_connect-> Fn_javascript_move("メールアドレスまたはパスワードの変更完了しました。", $return_url);
?>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>