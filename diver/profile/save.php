<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	$common_image = new CommonImage(); //画像
	
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>


<?

	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($member_name_1=="" || $member_name_2=="" || $member_name_kana=="" || $emergency_tel=="" || $diving_rank=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//$arr_db_field = array("user_name", "member_email", "member_login_pw", "temp_key");
	$arr_db_field = array("member_name_1", "member_name_2", "member_name_kana");
	$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
	$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship"));
	$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count"));
	//$arr_db_field = array_merge($arr_db_field, array("flag_hontouroku"));
		
	$db_insert = "update member set ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= $val."='".$$val."', ";
	}
	$db_insert .= " up_date='".$datetime."' ";
	$db_insert .= " where member_id='".$member_id."'";
	$db_result = $common_dao->db_update($db_insert);
	

	//Folder生成
	$save_dir = $global_path.global_member_dir.$member_id."/";
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $member_id, $text_img_1, "400");//400
	$dbup = "update member set ";
	for($loop = 1 ; $loop<2 ; $loop++)
	{
		$dbup .= " img_".$loop."='".$fname_new_name[$loop]."',";
	}
	$dbup .= " up_date='$datetime' where member_id='".$member_id."'";
	
	$db_result = $common_dao->db_update($dbup);
	
	//セッション渡す
	$sql = "select member_id, user_name, member_email, member_name_1, member_name_2, img_1, temp_key from member where member_id ='".$member_id."' ";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$_SESSION['user_name']=$db_result[0]["member_name_1"]." ".$db_result[0]["member_name_2"];
		$_SESSION['member_img']=$db_result[0]["img_1"];
		$temp_key=$db_result[0]["temp_key"];
	}
	
	
	//temp_keyから不正なアクセスなくす
	if($temp_key!="")
	{		
		$dbup = "update member set ";
		$dbup .= " temp_key='".substr($temp_key, 0, 20)."',";
		$dbup .= " up_date='$datetime' where member_id='".$member_id."'";
		
		$db_result = $common_dao->db_update($dbup);
	}

	$fname_type1 = $_FILES["img_1"]['type'];
	if($fname_type1 == "image/png")
	{
		$javascript_alert = "ファイル形式がpngの場合登録出来ないこともあります。\\n";
	}
		
	if($return_url=="")
	{
		$return_url = global_ssl."/diver/profile/skill.php";
	}
	$common_connect-> Fn_javascript_move($javascript_alert."プロフィール登録完了しました。", $return_url);
?>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>