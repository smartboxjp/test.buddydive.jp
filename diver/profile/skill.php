<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<?
	$sql = "SELECT cate_skill_id, cate_skill_title ";
	$sql .= " FROM cate_skill where flag_open='1' ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$cate_skill_title[$db_result[$db_loop]["cate_skill_id"]] = $db_result[$db_loop]["cate_skill_title"];
	}
?>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#fbeaea";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			//err_check_count += check_radio("skill");
			
			<? /* skill start */ ?>
			count = 0;
			count_false = 0;
			err = "";
			$("#err_skill").html(err_default);
			<?
			foreach($cate_skill_title as $key => $value)
			{
				$var = "skill_".$key;
			?>
			$("#"+"label_<?=$var;?>_1").css(background,bgcolor_default);
			$("#"+"label_<?=$var;?>_2").css(background,bgcolor_default);
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				$("#"+"label_<?=$var;?>_1").css(background,bgcolor_err);
				$("#"+"label_<?=$var;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			else
			{
				count++;
			}
			
			if($('input[name=<?=$var;?>]:checked').val() == "2")
			{
				count_false++;
			}
			<?
			}
			?>
			
			<? $key=1; ?>
			$("#label_skill_<? echo $key;?>_1").css(background,bgcolor_default);
			$("#label_skill_<? echo $key;?>_2").css(background,bgcolor_default);
			
			
			if($('input[name=skill_<? echo $key;?>]:checked').val() == undefined)
			{
				$("#"+"label_skill_<? echo $key;?>_1").css(background,bgcolor_err);
				$("#"+"label_skill_<? echo $key;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			
			if((count_false>0) && ($('input[name=skill_<? echo $key;?>]:checked').val() == "1"))
			{
				$("#label_skill_<? echo $key;?>_1").css(background,bgcolor_err);
				$("#label_skill_<? echo $key;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて「できる」場合選択ができます。</div>";
			}
			
			if((count_false==0) && ($('input[name=skill_<? echo $key;?>]:checked').val() == "2"))
			{
				$("#label_skill_<? echo $key;?>_1").css(background,bgcolor_err);
				$("#label_skill_<? echo $key;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて「できる」を選択してください。</div>";
			}
			
			<? /* skill end */ ?>
			
			if(err!="")
			{
				$("#err_skill").html(err);
			}
			
			
			if(err!="")
			{
				swal("error","入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		

		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div class='error'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		//全角
		function checkIsZenkaku(value) {
			for (var i = 0; i < value.length; ++i) {
			var c = value.charCodeAt(i);
			//  半角カタカナは不許可
			if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
				return false;
			}
			}
			return true;
		}
		
		// 半角数字チェック
		function checkNumber(value) {
			 if(value.match( /[^0-9]+/ ) ) {
				return false;
			 }
			 return true;
		}
	
		//全角カタカナチェック
		function checkKatakana(value){
			for(i=0; i<value.length; i++){
				if(value.charAt(i) != 'ー')	{
					if(value.charAt(i) < 'ア' || value.charAt(i) > 'ン'){
						return false;
					}
				}
			}
			return true;
		}
	
		//全角ひらがなチェック
		function checkHirakana(value){
			if (value.match(/^[\u3040-\u309F]+$/)) {
				return true;
			} else {
				return false;
			}
		}
		
		//英数半角
		function checkHankaku(value){
			if(value.match(/[^0-9A-Za-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 
		
		

	});
	
//-->
</script>

<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	$return_url = $_GET["return_url"];
	
	
	$sql = "SELECT skill_id, yes_no ";
	$sql .= " FROM member_skill where member_id='".$member_id."' ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$arr_member_skill[$db_result[$db_loop]["skill_id"]] = $db_result[$db_loop]["yes_no"];
	}
?>
<article>
<div id="diverBox">
<section class="mypageCont">
<p class="tit">スキルチェック</p>
<p>安全にバディ潜水ができるためには、下記すべてのスキル項目に「できる」のチェックが必要です。ブランクがあり、できないスキルがある場合は、必ず下記スキルができるようになってからバディダイビングにご参加ください。<br>
できないスキルがある場合でもダイバー登録を完了すれば<span class="pointIcon">P</span>ポイントがプレゼントされます。</p>
	<? $var = "form_regist";?>
  <form action="skill_save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" >
    <table class="checkTable">
      <tr>
        <th class="tCenter bgLightGray">項目</th>
        <th class="tCenter bgLightGray answer">できる</th>
      </tr>
  <?
			foreach($cate_skill_title as $key => $value)
			{
				$var = "skill_".$key;
  ?>
      <tr>
        <th><? echo $value;?></th>
        <td class="tCenter required">
          <label id="label_<?=$var;?>_1"><input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="1" <? if ($arr_member_skill[$key]!="" && $arr_member_skill[$key] == "1") { echo "checked";}?> />できる</label> 
          <label id="label_<?=$var;?>_2"><input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="2" <? if ($arr_member_skill[$key]!="" && $arr_member_skill[$key]=="2") { echo "checked";}?> />できない</label>
        </td>
      </tr>
  <?
      }
  ?>
      <tr>
        <td colspan="2" class="lastCheck required">
					<?
					$key = 1;
          $var = "skill_".$key;
          ?>
          <label id="label_<?=$var;?>_1">
            <input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="1" <? if ($arr_member_skill[$key]!="" && $arr_member_skill[$key]=="1") { echo "checked";}?> />
						<span>私はすべてのスキルができます。</span>
          </label>
          <label id="label_<?=$var;?>_2">
            <input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="2" <? if ($arr_member_skill[$key]!="" && $arr_member_skill[$key]=="2") { echo "checked";}?> />
						<span>私はできないスキルがあります。</span>
          </label>
        </td>
      </tr>
    </table>
    <p class="blueBtn mt20">
      <label id="err_skill"></label>
			<? $var = "form_confirm";?>
      <input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="<? if($return_url=="") { echo "メディカルチェックへ";} else {echo "保存する";}?>">
      
			<? $var = "return_url";?>
      <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
    </p>
    <p class="tCenter mt20">チェックするか否かの疑問がある方は、お気軽にサポートディスクまで<a href="/contact/">お問い合わせ</a>ください。</p>
  </form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>