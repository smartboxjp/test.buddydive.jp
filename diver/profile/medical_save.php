<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	$datetime = date("Y-m-d H:i:s");
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	$sql = "SELECT cate_medical_id, cate_medical_title ";
	$sql .= " FROM cate_medical where flag_open='1' ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$cate_medical_title[$db_result[$db_loop]["cate_medical_id"]] = $db_result[$db_loop]["cate_medical_title"];
	}
	
	//最初の登録の場合thankyou.php ページへ
	$sql = "SELECT member_id ";
	$sql .= " FROM member_medical where member_id='".$member_id."' ";
	$db_result = $common_dao->db_query($sql);
	if(!$db_result)
	{
		$return_url = "./thankyou.php";
	}

	$db_insert = "Delete from member_medical where member_id='".$member_id."' ";
	$db_result = $common_dao->db_update($db_insert);
	
	$db_insert = "INSERT INTO member_medical (member_id, medical_id, yes_no, regi_date) VALUES ";
	foreach($cate_medical_title as $key => $value)
	{
		$var = "medical_".$key;
		$db_result_medical .= " ('".$member_id."', '".$key."', '".$$var."', '".$datetime."'), ";
	}
	
	if($db_result_medical!="")
	{
		$db_insert = $db_insert.substr($db_result_medical, 0, strlen($db_result_medical)-2).";";
		$db_result = $common_dao->db_update($db_insert);
	}
	
	//私は上記項目に該当する事項は一つもありません。...
	$db_insert = "INSERT INTO member_medical (member_id, medical_id, yes_no, regi_date) VALUES ";
	$key = "1";
	$var = "medical_".$key;
	$db_result_medical = " ('".$member_id."', '".$key."', '".$$var."', '".$datetime."'), ";
	
	if($db_result_medical!="")
	{
		$db_insert = $db_insert.substr($db_result_medical, 0, strlen($db_result_medical)-2).";";
		$db_result = $common_dao->db_update($db_insert);
	}
	
	//私は上記項目に該当する項目がある場合は、...
	$db_insert = "INSERT INTO member_medical (member_id, medical_id, yes_no, regi_date) VALUES ";
	$key = "2";
	$var = "medical_".$key;
	$db_result_medical = " ('".$member_id."', '".$key."', '".$$var."', '".$datetime."'), ";
	
	if($db_result_medical!="")
	{
		$db_insert = $db_insert.substr($db_result_medical, 0, strlen($db_result_medical)-2).";";
		$db_result = $common_dao->db_update($db_insert);
	}
	
	//登録完了日
	$db_insert = "update member set hontouroku_finish_date=now() where member_id='".$member_id."' and hontouroku_finish_date='0000-00-00 00:00:00' and hontouroku_start_date!='0000-00-00 00:00:00' ";
	$db_result = $common_dao->db_update($db_insert);
	
	//修正日
	$db_insert = "update member set up_date=now() where member_id='".$member_id."' ";
	$db_result = $common_dao->db_update($db_insert);
	
	/* point 招待された場合 start*/
	if($_COOKIE['invited_key'] != '')
	{
		$sql = "select invite_id, member_id, invite_point, invited_point from invite where invited_key ='".$_COOKIE['invited_key']."' and status!=1 ";

		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$invite_member_id = $db_result[0]["member_id"];
			$invite_point = $db_result[0]["invite_point"];
			$invited_point = $db_result[0]["invited_point"];
		}
		
		if($invite_member_id!="")
		{
			//招待されたメンバーにポイント追加
			$db_insert = "update member set member_point=member_point+".$invited_point." where member_id='".$member_id."' ";
			$db_result = $common_dao->db_update($db_insert);
			
			$db_insert = "insert into member_point (member_point_id, member_id, point_comment, point, url, regi_date, up_date) VALUES ('', '".$member_id."', '招待されたメンバー', '".$invited_point."', '".$_SERVER['PHP_SELF']."', now(), now()) ";
			$db_result = $common_dao->db_update($db_insert);
			

			
			//招待メンバーにポイント追加
			$db_insert = "update member set member_point=member_point+".$invite_point." where member_id='".$invite_member_id."' ";
			$db_result = $common_dao->db_update($db_insert);
			$db_insert = "insert into member_point (member_point_id, member_id, point_comment, point, url, regi_date, up_date) VALUES ('', '".$invite_member_id."', '招待メンバー', '".$invite_point."', '".$_SERVER['PHP_SELF']."', now(), now()) ";
			$db_result = $common_dao->db_update($db_insert);
			
		}
		
		//招待
		$db_insert = "update invite set status=1 where invited_key ='".$_COOKIE['invited_key']."'";
		$db_result = $common_dao->db_update($db_insert);
		$_COOKIE['invited_key'] = "";
	}
	/* point 招待された場合 end*/
	
	
	if($return_url=="")
	{
		$return_url = global_ssl."/";
	}
	$common_connect-> Fn_javascript_move("メディカル登録完了しました。", $return_url);
?>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>