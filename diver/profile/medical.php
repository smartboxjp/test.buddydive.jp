<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<?
	$sql = "SELECT cate_medical_id, cate_medical_title ";
	$sql .= " FROM cate_medical where flag_open='1' ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$cate_medical_title[$db_result[$db_loop]["cate_medical_id"]] = $db_result[$db_loop]["cate_medical_title"];
	}
?>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#fbeaea";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			//err_check_count += check_radio("medical");
			
			<? /* medical start */ ?>
			count = 0;
			count_false = 0;
			err = "";
			$("#err_medical").html(err_default);
			<?
			foreach($cate_medical_title as $key => $value)
			{
				$var = "medical_".$key;
			?>
			$("#"+"label_<?=$var;?>_1").css(background,bgcolor_default);
			$("#"+"label_<?=$var;?>_2").css(background,bgcolor_default);
			
			if($('input[name=<?=$var;?>]:checked').val() == undefined)
			{
				$("#"+"label_<?=$var;?>_1").css(background,bgcolor_err);
				$("#"+"label_<?=$var;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			else
			{
				count++;
			}
			
			if($('input[name=<?=$var;?>]:checked').val() == "1")
			{
				count_false++;
			}
			<?
			}
			?>
			
			<? $key=1; ?>
			$("#label_medical_<? echo $key;?>_1").css(background,bgcolor_default);
			$("#label_medical_<? echo $key;?>_2").css(background,bgcolor_default);
			
			
			if($('input[name=medical_<? echo $key;?>]:checked').val() == undefined)
			{
				$("#"+"label_medical_<? echo $key;?>_1").css(background,bgcolor_err);
				$("#"+"label_medical_<? echo $key;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			
			if((count_false>0) && ($('input[name=medical_<? echo $key;?>]:checked').val() == "1"))
			{
				$("#label_medical_<? echo $key;?>_1").css(background,bgcolor_err);
				$("#label_medical_<? echo $key;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて「いいえ」場合選択ができます。</div>";
			}
			
			if((count_false==0) && ($('input[name=medical_<? echo $key;?>]:checked').val() == "2"))
			{
				$("#label_medical_<? echo $key;?>_1").css(background,bgcolor_err);
				$("#label_medical_<? echo $key;?>_2").css(background,bgcolor_err);
				err ="<div class='error'>すべて「いいえ」を選択してください。</div>";
			}
			
			<? $key=2; ?>
			$("#label_medical_<? echo $key;?>_1").css(background,bgcolor_default);
			
			if($('input[name=medical_<? echo $key;?>]:checked').val() == undefined)
			{
				$("#"+"label_medical_<? echo $key;?>_1").css(background,bgcolor_err);
				err ="<div class='error'>すべて正しく選択してください。</div>";
			}
			
			<? /* medical end */ ?>
			
			if(err!="")
			{
				$("#err_medical").html(err);
			}
			
			
			if(err!="")
			{
				swal("error","入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		

		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div class='error'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		//全角
		function checkIsZenkaku(value) {
			for (var i = 0; i < value.length; ++i) {
			var c = value.charCodeAt(i);
			//  半角カタカナは不許可
			if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
				return false;
			}
			}
			return true;
		}
		
		// 半角数字チェック
		function checkNumber(value) {
			 if(value.match( /[^0-9]+/ ) ) {
				return false;
			 }
			 return true;
		}
	
		//全角カタカナチェック
		function checkKatakana(value){
			for(i=0; i<value.length; i++){
				if(value.charAt(i) != 'ー')	{
					if(value.charAt(i) < 'ア' || value.charAt(i) > 'ン'){
						return false;
					}
				}
			}
			return true;
		}
	
		//全角ひらがなチェック
		function checkHirakana(value){
			if (value.match(/^[\u3040-\u309F]+$/)) {
				return true;
			} else {
				return false;
			}
		}
		
		//英数半角
		function checkHankaku(value){
			if(value.match(/[^0-9A-Za-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 
		
		

	});
	
//-->
</script>

<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	$return_url = $_GET["return_url"];
	
	
	$sql = "SELECT medical_id, yes_no ";
	$sql .= " FROM member_medical where member_id='".$member_id."' ";
	
	$db_result = $common_dao->db_query($sql);
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$arr_member_medical[$db_result[$db_loop]["medical_id"]] = $db_result[$db_loop]["yes_no"];
	}
?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">メディカルチェック</p>
<p>スクーバダイビングにおけるすべての行為は、自己責任において行われるものです。<br>
バディ潜水中に体調不良によって起こったトラブルについては、バディの責任において対処せねばなりません。また下記の疾患により救助を要する事故が発生した場合に、救助にかかる費用はもちろんのこと、救助活動においてダイビングサービスが受けた損害についても貴方へ損害賠償の請求が発生しうる可能性もあります。<br>
上記のことを考慮し、慎重かつ控えめに判断をしてください。</p>

	<? $var = "form_regist";?>
  <form action="medical_save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" >
    <table class="checkTable">
      <tr>
        <th colspan="2" class="tCenter bgLightGray">慢性的疾患に関する了解事項<br>
        <span class="notes">※下記の項目をすべて確認し、該当する項目には必ずチェックを入れてください。<br>また最終項目のいずれかにもチェックを入れてください。</span></th>
      </tr>
      <tr>
        <th class="tCenter bgLightGray">項目</th>
        <th class="tCenter bgLightGray answer">該当する</th>
      </tr>
      
  <?
			foreach($cate_medical_title as $key => $value)
			{
				$var = "medical_".$key;
  ?>
      <tr>
        <th><? echo nl2br($value);?></th>
        <td class="tCenter required">
          <label id="label_<?=$var;?>_1"><input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="1" <? if ($arr_member_medical[$key]!="" && $arr_member_medical[$key] == "1") { echo "checked";}?> />はい</label> 
          <label id="label_<?=$var;?>_2"><input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="2" <? if ($arr_member_medical[$key]!="" && $arr_member_medical[$key]=="2") { echo "checked";}?> />いいえ</label>
        </td>
      </tr>
  <?
      }
  ?>
      <tr>
        <td colspan="2" class="lastCheck required">
					<?
					$key = 1;
          $var = "medical_".$key;
          ?>
          <label id="label_<?=$var;?>_1">
            <input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="1" <? if ($arr_member_medical[$key]!="" && $arr_member_medical[$key]=="1") { echo "checked";}?> />
						<span>私は上記項目に該当する項目は一つもありません。<br>
        また今後、上記項目に該当する事項が発生した場合、医師の診断書を持参してサービスを受けることを約束します。</span>
          </label>
          <label id="label_<?=$var;?>_2">
            <input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="2" <? if ($arr_member_medical[$key]!="" && $arr_member_medical[$key]=="2") { echo "checked";}?> />
						<span>私は上記に該当する項目があるが、医師の診断許可書を持参しサービスを受けます。<br>診断書を持参しない場合は当日のダイビングが出来ないことも了解いたします。</span>
          </label>
        </td>
      </tr>
    </table>
    <table class="checkTable mt20">
      <tr>
      <th colspan="2" class="tCenter bgLightGray">一時的疾患に関する了解事項<br>
      <span class="notes">※慢性的疾患以外に一時的疾患（下記に該当する）によるダイビングへの参加を拒否される可能性があります。<br>
      不安のある方は医師へ御相談してご参加ください。<br>
      下記の項目をすべて確認し、必ずチェックを入れてください。</span></th>
      </tr>
      <tr>
      <th class="tCenter bgLightGray">項目</th>
      </tr>
      <tr>
      <th>中程度の運動（たとえば12分以内に1.6Kmを走る）ができない。</th>
      </tr>
      <tr>
      <th>過去に胃や腸に潰瘍ができたことはある。</th>
      </tr>
      <tr>
      <th>首、背中、腰、または四肢に痛みがある。</th>
      </tr>
      <tr>
      <th>首、背中、腰、または四肢に異常があって、治療を受けたことがある。</th>
      </tr>
      <tr>
      <th>骨折、捻挫、脱臼の経験があり、現在もその後遺症（痛み・関節の不安定感など）が残っている。</th>
      </tr>
      <tr>
      <th>副鼻腔炎（蓄膿症も含む）、気管支炎（急性、慢性、アレルギー性）、または風邪をひきやすい。</th>
      </tr>
      <tr>
      <th>現在、飲んでいる薬がある。または．過去3ヶ月以内に何らかの医療を受けたことがある。</th>
      </tr>
      <tr>
      <th>45歳以上の方で、以下の項目が一つ以上あてはまる<br>
      ・パイプ、葉巻、煙草を喫煙している。<br>
      ・コレステロール値レベルが高い<br>
      ・家族に心臓発作や脳卒中の病歴がある方がいる。<br>
      ・現在診療を受けている。<br>
      ・高血圧である。<br>
      ・食事療法で調整しているが糖尿病である。</th>
      </tr>
      <tr>
        <td class="lastCheck required">
					<?
					$key = 2;
          $var = "medical_".$key;
          ?>
          <label id="label_<?=$var;?>_1">
            <input name="<?=$var;?>" id="<?=$var;?>" type="checkbox" value="1" <? if ($arr_member_medical[$key]!="" && $arr_member_medical[$key]=="1") { echo "checked";}?> />
						<span>私は上記項目に該当する項目がある場合は、ダイビングへの参加を拒否される可能性があり、また医師に相談する必要があることを認識したうえで、自己責任においてダイビング活動に参加することを了承します</span>
          </label>
        </td>
      </tr>
    </table>
    <p class="blueBtn mt20">
      <label id="err_medical"></label>
			<? $var = "form_confirm";?>
      <input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="<? if($return_url=="") { echo "ダイバー登録完了";} else {echo "保存する";}?>">
      
			<? $var = "return_url";?>
      <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
    </p>
    <p class="tCenter mt20">チェックするか否かの疑問がある方は、お気軽にサポートディスクまで<a href="/contact/">お問い合わせ</a>ください。</p>
	</form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>