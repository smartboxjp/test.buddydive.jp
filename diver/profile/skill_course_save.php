<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>スキル講習</title>
</head>

<body>
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	
	//ユーザー名チェック
	$arr_db_field = array("member_name_1", "member_name_2", "pref", "address", "member_email", "tel", "phone");
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " member_id ";
	$sql .= " FROM member where member_id='".$member_id."' ";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
		}
	}
	
	$datetime = date("Y-m-d H:i:s");
	


	//Thank youメール
	if ($member_email != "")
	{
		$subject = "『Buddy Dive』スキル講習に興味があると回答がありました。";
		
		$body = "";
		
$body = <<<EOF
スキル講習に興味があると回答がありました。

──────────────────────────────

--------------------------------------------------
内容
--------------------------------------------------
【お名前】 $member_name_1 $member_name_2
【メールアドレス】 $member_email
【住所】 $pref $address
【携帯電話番号】 $phone

【受 付 日】$datetime

──────────────────────────────


$global_email_footer


EOF;

}

	//$common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_support_mail."<".$global_support_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
	
	$common_connect-> Fn_redirect("skill_course_thankyou.php");

?>
</body>
</html>