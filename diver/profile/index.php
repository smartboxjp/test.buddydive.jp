<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<script language="javascript"> 
	function fnImgDel(j, k) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './img_one_del.php?img='+j+'&img_name='+k;
		} 
	}
</script>

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("member_name_1");
			err_check_count += check_input("member_name_2");
			err_check_count += check_input_nama_kana("member_name_kana");
			err_check_count += check_input_birth("birth", "birth_yyyy", "birth_mm", "birth_dd");
			err_check_count += check_radio_2("sex", "label_sex_0", "label_sex_1");
			err_check_count += check_radio_4("blood", "label_blood_0", "label_blood_1", "label_blood_2", "label_blood_3");
			
			//err_check_count += check_input("tel");
			err_check_count += check_input("phone");
			err_check_count += check_input("pref");
			err_check_count += check_input("address");
			err_check_count += check_input("emergency_name");
			err_check_count += check_input("emergency_relationship");
			err_check_count += check_input("emergency_tel");
			err_check_count += check_input("diving_rank");
			err_check_count += check_input("diving_group");
			err_check_count += check_input("diving_count");
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		
		function check_radio_2($str, $l_str_1, $l_str_2) 
		{
			err_check = false;
			$("#err_"+$str).html(err_default);
			$("#"+$l_str_1).css(background,bgcolor_default);
			$("#"+$l_str_2).css(background,bgcolor_default);
			
			if($('input[name="'+$str+'"]:checked').val() == undefined)
			{
				$("#"+$l_str_1).css(background,bgcolor_err);
				$("#"+$l_str_2).css(background,bgcolor_err);
				err ="<div class='error'>正しく選択してください。</div>";
				$("#err_"+$str).html(err);
				
				return 1;
			}
			return 0;
		}
		
		function check_radio_4($str, $l_str_1, $l_str_2, $l_str_3, $l_str_4) 
		{
			err_check = false;
			$("#err_"+$str).html(err_default);
			$("#"+$l_str_1).css(background,bgcolor_default);
			$("#"+$l_str_2).css(background,bgcolor_default);
			$("#"+$l_str_3).css(background,bgcolor_default);
			$("#"+$l_str_4).css(background,bgcolor_default);
			
			if($('input[name="'+$str+'"]:checked').val() == undefined)
			{
				$("#"+$l_str_1).css(background,bgcolor_err);
				$("#"+$l_str_2).css(background,bgcolor_err);
				$("#"+$l_str_3).css(background,bgcolor_err);
				$("#"+$l_str_4).css(background,bgcolor_err);
				err ="<div class='error'>正しく選択してください。</div>";
				$("#err_"+$str).html(err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_2($str, $str_1, $str_2) 
		{
			err = "";
			$("#err_"+$str).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			$("#"+$str_2).css(background,bgcolor_default);
			
			if($('#'+$str_1).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#"+$str_1).css(background,bgcolor_err);
			}
			
			if($('#'+$str_2).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#"+$str_2).css(background,bgcolor_err);
			}

			if(err!="")
			{
				$("#err_"+$str).html(err);
				
				return 1;
			}
			return 0;
		}
		
		
		function check_input_nama_kana($str_1) 
		{
			err = "";
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			
			check_str = $('#'+$str_1).val().replace(" ", "").replace("　", "");
			
			if(check_str=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#"+$str_1).css(background,bgcolor_err);
			}
			else if(checkKatakana(check_str) == false)
			{
				err ="<div class='error'>全角で正しく入力してください。</div>";
				$("#"+$str_1).css(background,bgcolor_err);
			}
			if(err!="")
			{
				$("#err_"+$str_1).html(err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_birth($str, $str_1, $str_2, $str_3) 
		{
			err = "";
			$("#err_"+$str).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			$("#"+$str_2).css(background,bgcolor_default);
			$("#"+$str_3).css(background,bgcolor_default);
			
			if($('#'+$str_1).val()=="")
			{
				err ="<div class='error'>正しく選択してください。</div>";
				$("#"+$str_1).css(background,bgcolor_err);
			}
			
			if($('#'+$str_2).val()=="")
			{
				err ="<div class='error'>正しく選択してください。</div>";
				$("#"+$str_2).css(background,bgcolor_err);
			}
			
			if($('#'+$str_3).val()=="")
			{
				err ="<div class='error'>正しく選択してください。</div>";
				$("#"+$str_3).css(background,bgcolor_err);
			}

			if(err!="")
			{
				$("#err_"+$str).html(err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div class='error'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div class='error'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		//全角
		function checkIsZenkaku(value) {
			for (var i = 0; i < value.length; ++i) {
			var c = value.charCodeAt(i);
			//  半角カタカナは不許可
			if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
				return false;
			}
			}
			return true;
		}
		
		// 半角数字チェック
		function checkNumber(value) {
			 if(value.match( /[^0-9]+/ ) ) {
				return false;
			 }
			 return true;
		}
	
		//全角カタカナチェック
		function checkKatakana(value){
			for(i=0; i<value.length; i++){
				if(value.charAt(i) != 'ー')	{
					if(value.charAt(i) < 'ア' || value.charAt(i) > 'ン'){
						return false;
					}
				}
			}
			return true;
		}
	
		//全角ひらがなチェック
		function checkHirakana(value){
			if (value.match(/^[\u3040-\u309F]+$/)) {
				return true;
			} else {
				return false;
			}
		}
		
		//英数半角
		function checkHankaku(value){
			if(value.match(/[^0-9A-Za-z]+/) == null){
				return true;
			}else{
				return false;
			}
		}		
		

	});
	
//-->
</script>
<?

	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	$return_url = $_GET["return_url"];
	
	if($member_id!="")
	{
		$arr_db_field = array("user_name", "member_email", "member_login_pw", "temp_key");
		$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana"));
		$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
		$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
			
		$sql = "SELECT member_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " regi_date FROM member where member_id='".$member_id."'";
		//$sql .= " and flag_open='1'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
?>
<article>
<div id="diverBox">
<section class="mypageCont">
<p class="tit">プロフィール</p>
	<? $var = "form_regist";?>
  <form action="save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" enctype="multipart/form-data">
    <table class="formTable">
      <tr>
      <th>姓</th>
      <td>
				<? $var = "member_name_1"; ?>
        <input type="text" placeholder="山田" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<?=$var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>名</th>
      <td>
				<? $var = "member_name_2"; ?>
        <input type="text" placeholder="太郎" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<?=$var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>フリガナ</th>
      <td>
				<? $var = "member_name_kana"; ?>
        <input type="text" placeholder="ヤマダ　タロウ" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<?=$var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>生年月日</th>
      <td>
				<? $var = "birth_yyyy";?>
        <select name="<? echo $var;?>" id="<? echo $var;?>" class="date" >
        <option value="">----</option>
        <?
        for($loop=date("Y")-5 ; $loop > 1930 ; $loop--)
        {
        ?>
        <option value="<? echo $loop;?>" <? if($loop==$$var) { echo " selected ";}?>><? echo $loop;?></option>
        <?
        }
        ?>
        </select>
         年
				<? $var = "birth_mm";?>
        <select name="<? echo $var;?>" id="<? echo $var;?>" class="date" >
        <option value="">----</option>
        <?
        for($loop=1 ; $loop < 13 ; $loop++)
        {
        ?>
          <option value="<? echo $loop;?>" <? if($loop==$$var) { echo " selected ";}?>><? echo $loop;?></option>
        <?
        }
        ?>
        </select>
         月
			<? $var = "birth_dd";?>
      <select name="<? echo $var;?>" id="<? echo $var;?>" class="date" >
      <option value="">--</option>
      <?
      for($loop=1 ; $loop < 32 ; $loop++)
      {
      ?>
        <option value="<? echo $loop;?>" <? if($loop==$$var) { echo " selected ";}?>><? echo $loop;?></option>
      <?
      }
      ?>
      </select>
      日
        <label id="err_birth"></label>
      </td>
      </tr>
      <tr>
      <th>性別</th>
      <td>
          <? $var = "sex";?>
					<?
					$arr_sex[] = "男性";
					$arr_sex[] = "女性";
					
          foreach($arr_sex as $key=>$value)
          {
          ?>
            <label id="label_<?=$var;?>_<? echo $key;?>"><input name="<?=$var;?>" id="<?=$var;?>_<? echo $key;?>" type="radio" value="<? echo $value;?>" <? if ($$var == $value) { echo "checked";}?> /><? echo $value;?></label>　
          <?
          }
          ?>
          <label id="err_sex"></label>
      </td>
      </tr>
      <tr>
      <th>血液型</th>
      <td>
          <? $var = "blood";?>
					<?
					$arr_blood[] = "A型";
					$arr_blood[] = "AB型";
					$arr_blood[] = "B型";
					$arr_blood[] = "O型";
					
          foreach($arr_blood as $key=>$value)
          {
          ?>
            <label id="label_<?=$var;?>_<? echo $key;?>"><input name="<?=$var;?>" id="<?=$var;?>" type="radio" value="<? echo $value;?>" <? if ($$var == $value) { echo "checked";}?> /><? echo $value;?></label>　
          <?
          }
          ?>
          <label id="err_blood"></label>
      </td>
      </tr>
      <tr>
      <th>住所</th>
      <td>
				<? $var = "pref";?>
				<?
        $arr_pref[] = "北海道";
        $arr_pref[] = "青森県";
        $arr_pref[] = "岩手県";
        $arr_pref[] = "宮城県";
        $arr_pref[] = "秋田県";
        $arr_pref[] = "山形県";
        $arr_pref[] = "福島県";
        $arr_pref[] = "茨城県";
        $arr_pref[] = "栃木県";
        $arr_pref[] = "群馬県";
        $arr_pref[] = "埼玉県";
        $arr_pref[] = "千葉県";
        $arr_pref[] = "東京都";
        $arr_pref[] = "神奈川県";
        $arr_pref[] = "新潟県";
        $arr_pref[] = "富山県";
        $arr_pref[] = "石川県";
        $arr_pref[] = "福井県";
        $arr_pref[] = "山梨県";
        $arr_pref[] = "長野県";
        $arr_pref[] = "岐阜県";
        $arr_pref[] = "静岡県";
        $arr_pref[] = "愛知県";
        $arr_pref[] = "三重県";
        $arr_pref[] = "滋賀県";
        $arr_pref[] = "京都府";
        $arr_pref[] = "大阪府";
        $arr_pref[] = "兵庫県";
        $arr_pref[] = "奈良県";
        $arr_pref[] = "和歌山県";
        $arr_pref[] = "鳥取県";
        $arr_pref[] = "島根県";
        $arr_pref[] = "岡山県";
        $arr_pref[] = "広島県";
        $arr_pref[] = "山口県";
        $arr_pref[] = "徳島県";
        $arr_pref[] = "香川県";
        $arr_pref[] = "愛媛県";
        $arr_pref[] = "高知県";
        $arr_pref[] = "福岡県";
        $arr_pref[] = "佐賀県";
        $arr_pref[] = "長崎県";
        $arr_pref[] = "熊本県";
        $arr_pref[] = "大分県";
        $arr_pref[] = "宮崎県";
        $arr_pref[] = "鹿児島県";
        $arr_pref[] = "沖縄県";
        ?>
        <select name="<? echo $var;?>" id="<? echo $var;?>" class="date" >
        <option value="">都道府県</option>
        <?
        foreach($arr_pref as $key => $value)
        {
        ?>
          <option value="<? echo $value;?>" <? if($$var==$value) { echo " selected ";}?>><? echo $value;?></option>
        <?
        }
        ?>
        </select>
        <label id="err_<? echo $var;?>"></label>
      
        <br>
				<? $var = "address";?>
        <input type="text" placeholder="港区○○10-20　□□マンション502" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
        </td>
      </tr>
      <tr>
      <th>携帯電話番号</th>
      <td>
				<? $var = "phone";?>
        <input type="text" placeholder="09012345678" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>自宅電話番号<span class="tDeepBlue">[任意]</span></th>
      <td>
				<? $var = "tel";?>
        <input type="text" placeholder="0312345678" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>緊急連絡先 名前</th>
      <td>
				<? $var = "emergency_name";?>
        <input type="text" placeholder="山田 太一" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>緊急連絡先 間柄</th>
      <td>
				<? $var = "emergency_relationship";?>
        <input type="text" placeholder="父" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>緊急連絡先 電話番号</th>
      <td>
				<? $var = "emergency_tel";?>
        <input type="text" placeholder="0398765432" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>最終認定ランク</th>
      <td>
				<? $var = "diving_rank";?>
        <input type="text" placeholder="レスキューダイバー" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
      <th>指導団体</th>
      <td>
				<? $var = "diving_group";?>
        <input type="text" placeholder="PADI" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
        <label id="err_<? echo $var;?>"></label>
      </td>
      </tr>
      <tr>
        <th>ダイビング本数</th>
        <td>
          <? $var = "diving_count";?>
          <input type="text" placeholder="300" class="textInput" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var; ?>">
          <label id="err_<? echo $var;?>"></label>
        </td>
      </tr>
      <tr>
        <th>プロフィール画像</th>
        <td>
            <? 
                $var = "img_1";
            ?>
                <input type="file" name="<?=$var;?>" size="50"> 
                <input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>"><br>
            <?
                if ($$var != "")
                {
                    //echo "<a href='".global_ssl."/".global_member_dir.$member_id."/".$$var."' target='_blank'>".global_ssl."/".global_member_dir.$member_id."/".$$var."</a><br />";
                    echo "<a href='/".global_member_dir.$member_id."/".$$var."' target='_blank'><img src='/".global_member_dir.$member_id."/".$$var."?d=".date(his)."' width=250 border=0></a>";
                    echo "<br /><a href=\"#\" onClick='fnImgDel(\"".$$var."\", \"".$var."\");'>上のファイル削除</a>";
                }
            ?>
        </td>
      </tr>
    </table>
    <? $var = "form_confirm";?>
    <p class="blueBtn mt20"><input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="<? if($return_url=="") { echo "スキルチェックへ";} else {echo "保存する";}?>"></p>
    
		<? $var = "return_url";?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
  </form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>