<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$url = $_GET["url"];

?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
?>

<article>
<div id="diverBox">
<section class="accountBox">
	<form action="/diver/profile/skill_course_save.php" name="form_regist" id="form_regist" method="post">
    <p class="tit">スキル講習</p>
    <p class="importantTxt mt20">スキルチェックで「できないスキルがあります」になった方を対象としたスキル講習を行っています。ご興味があるかたは下記ボタンをクリックしてください。</p>
    
    <p class="blueBtn mt20">
			<? $var = "form_confirm";?>
      <input name="<?=$var;?>" id="<?=$var;?>"  type="submit" value="スキル講習に興味がある" />
    </p>
	</form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>