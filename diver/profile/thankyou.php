<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<article>
<div id="diverBox">
<section class="accountBox">
<p class="tit">ダイバー登録完了</p>
<p class="mt20">ダイバー登録ありがとうございました。新規登録キャンペーンとして<span class="pointIcon">P</span><span class="cPink">500pt</span>のポイントをプレゼントしました。ご自身の<a href="/diver/">ダイバーページ</a>からご確認していただけます。<br>
またバディをご招待して登録されて場合は、招待した方、招待された方それぞれに<span class="pointIcon">P</span><span class="cPink">500pt</span>のプレゼントしています。</p>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>