<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//このページに入ったらログアウトされる
	$_SESSION['member_id']="";
	$_SESSION['member_img']="";
	
	$url = $_GET["url"];
	
	//招待キー
	$invited_key = $_GET["invited_key"];
	if($invited_key!="")
	{
		setcookie("invited_key", $invited_key, time()+3600*24*365, '/');//クッキーに保存
	}
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<script type="text/javascript">
	$(function() {
		
		$('#user_name_check').click(function() {
			$.ajax({
				type:"GET",
				url:"user_name_check.php?user_name=" + $('#user_name').val(),
				dataType:"html",
				success: function(retun_data){
					swal(retun_data);
				},
				error:function(){
					//swal("error");
				}
			});
			//window.open("./user_name_check.php?user_name=" + $('#user_name').val(), "OpenWin",  "resizable=yes,menubar=no,directories=no,scrollbars=yes,status=no,location=no,width=670,height=380");
		});
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("user_name");
			err_check_count += check_input_email("member_email");
			err_check_count += check_input_pw("member_login_pw");
			err_check_count += check_checkbox_agree("agree");
			
			if(err_check_count!=0)
			{
				swal("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<span class='errorForm'>６文字以上入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						
		function check_checkbox_agree($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			$("#"+$str+"_label").css(background,bgcolor_default);
			
			if($("#"+$str).is(':checked')==false)
			{
				err ="<span class='errorForm'>※選択必須項目です。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str+"_label").css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
	});
	
//-->
</script>

<article>
<div id="diverBox">
<section class="accountBox">
	<form action="/diver/registration/index_save.php" name="form_regist" id="form_regist" method="post">
    <p class="tit">ダイバー登録</p>
    <div class="username">
      <? $var = "user_name";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="ユーザー名" class="textInput">
      <label id="err_<?=$var;?>"></label>
      
      <? $var = "user_name_check";?>
      <a href="#" name="<?=$var;?>" id="<?=$var;?>" class="check">チェック</a>
    </div>
    <div>
      <? $var = "member_email";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="メールアドレス" class="textInput">
      <label id="err_<?=$var;?>"></label>
    </div>
    <div>
      <? $var = "member_login_pw";?>
      <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="パスワード" class="textInput">
      <label id="err_<?=$var;?>"></label>
    </div>
    <p class="agree">
    <label>
			<? $var = "agree";?>
      <label id="<? echo $var;?>_label">
        <input type="checkbox" name="<? echo $var;?>" id="<? echo $var;?>" value="1"> 
        <a href="/about/rule.php" target="_blank">利用規約</a>と<a href="/about/privacy.php" target="_blank">個人情報保護方針</a>と<a href="/about/checklist.php" target="_blank">バディ潜水確認事項</a>に同意します
      </label>
      <label id="err_<?=$var;?>"></label>
    </label>
    </p>
    <p class="blueBtn mt20">
			<? $var = "form_confirm";?>
      <input name="<?=$var;?>" id="<?=$var;?>"  type="submit" value="ダイバー登録" />
    </p>
    <p class="userBtn"><a href="<? echo global_ssl."/diver/login/";?>">すでにダイバー登録をしている方はこちら</a></p>
	</form>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>