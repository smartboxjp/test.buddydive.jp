<?php	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>ダイバー登録開始</title>
</head>

<body>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/analytics.php'); ?>

<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($temp_key=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	

	$sql = "SELECT member_id, member_email FROM member where temp_key = '".$temp_key."'  ";//and flag_hontouroku=0
	
	$db_result = $common_dao->db_query($sql);
	if(!$db_result)
	{
		$common_connect-> Fn_javascript_back("既に登録されている可能性があります。");
	}
	else
	{
		$invited_member_id = $db_result[0]["member_id"];	
		$db_insert = "update member set flag_hontouroku=1, up_date='".$datetime."', hontouroku_start_date='".$datetime."', member_point='".global_member_regist."' ";
		$db_insert .= " where temp_key = '".$temp_key."' ";
		$db_result_up = $common_dao->db_update($db_insert);
		
		/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ start */
		//招待されていたら、バディ追加、招待データ削除、通知追加
		$sql_invite = "SELECT dive_reserve_id, member_id FROM invite where invited_email = '".$db_result[0]["member_email"]."' ";
		$sql_invite .= " order by invite_id desc limit 0, 1 ";
		
		$db_result_invite = $common_dao->db_query($sql_invite);
		if($db_result_invite)
		{
			$dive_reserve_id = $db_result_invite[0]["dive_reserve_id"];
			$invite_member_id = $db_result_invite[0]["member_id"];
			//バディ追加
			$db_insert_member_buddy = "insert into member_buddy ( ";
			$db_insert_member_buddy .= " buddy_id, buddy_reserve_id, dive_reserve_id, member_id, status, regi_date ";
			$db_insert_member_buddy .= " ) values ( ";
			$db_insert_member_buddy .= "'".$invited_member_id."','0', '".$dive_reserve_id."','".$invite_member_id."',0, now())";
			$db_result_member_buddy = $common_dao->db_update($db_insert_member_buddy);

			
			/*
			//招待データ削除
			$db_insert_invite_del = "Delete from invite ";
			$db_insert_invite_del .= " where invited_email = '".$db_result[0]["member_email"]."' ";
			$db_result_invite_del= $common_dao->db_update($db_insert_invite_del);
			
			//通知追加
			$db_insert_message = "insert into member_message ( ";
			$db_insert_message .= " member_message_id, dive_reserve_id, member_id, from_member_id, regi_date ";
			$db_insert_message .= " ) values (";
			$db_insert_message .= "'', '".$dive_reserve_id."','".$invited_member_id."','".$invite_member_id."', now())";
			$db_result_message = $common_dao->db_update($db_insert_message);
			*/
		}
		/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ end */
		
		session_start();
		$_SESSION['member_id']=$invited_member_id;
		$_SESSION['user_name']="";
		$_SESSION['member_img']="";
		
		$common_connect-> Fn_redirect(global_ssl."/diver/profile/");
	}
	
?>

</body>

</html>
