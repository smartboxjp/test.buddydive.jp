<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>仮会員登録</title>
</head>

<body>
<?

	if ($user_name == "" or $member_email == "" or $member_login_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	
	//ユーザー名チェック
	$sql = "SELECT  member_id FROM member where user_name='".$user_name."' and member_email!='".$member_email."'";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		if($db_result[0]["member_id"]!="")
		{
			$common_connect-> Fn_javascript_back("既に登録されているユーザー名です。");
		}
	}
	
	//メールチェック
	$sql = "SELECT  member_id FROM member where member_email='".$member_email."' and flag_hontouroku=1 ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		if($db_result[0]["member_id"]!="")
		{
			$common_connect-> Fn_javascript_back("既に登録されているメールです。");
		}
	}
	
	$datetime = date("Y-m-d H:i:s");
	//既存会員データがあれば削除
	$db_insert = "Delete from member ";
	$db_insert .= " where member_email = '".$member_email."' ";
	$db_result = $common_dao->db_update($db_insert);
	
	$temp_key = date("Ymd").$common_connect-> Fn_random_password(20);
	
	$flag_open =0;
	$arr_db_field = array("member_email", "member_login_pw", "user_name", "temp_key");
	
	$db_insert = "insert into member ( ";
	$db_insert .= " member_id, ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= $val.", ";
	}
	$db_insert .= " regi_date, up_date ";
	$db_insert .= " ) values ( ";
	$db_insert .= " '', ";
	foreach($arr_db_field as $val)
	{
		$db_insert .= " '".$$val."', ";
	}
	$db_insert .= " '$datetime', '$datetime')";
	$db_result = $common_dao->db_update($db_insert);
	

	$view_member_login_pw = substr($member_login_pw, 0, 2)."＊＊＊＊＊";
	$temp_url = global_ssl."/diver/registration/regist_temp.php?temp_key=".$temp_key;

	//Thank youメール
	if ($member_email != "")
	{
		$subject = "『Buddy Dive』仮登録ありがとうございます。";
		
		$body = "";
		
$body = <<<EOF
この度は『Buddy Dive』に仮登録頂き、誠にありがとうございます。
まだ登録は完了していません。
以下の本登録用URLより本登録手続きを行ってください。

──────────────────────────────

--------------------------------------------------
仮登録内容
--------------------------------------------------
【ユーザー名】 $user_name
【メールアドレス】 $member_email
【パスワード】 $view_member_login_pw

【本登録用URL】
$temp_url

【受 付 日】$datetime

──────────────────────────────

※なお、このメールに憶えのない方は、
下記メールアドレスまでご連絡ください。
info@buddydive.jp

$global_email_footer


EOF;

}

	$common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	$common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
	
	$common_connect-> Fn_redirect("thankyou.php");

?>
</body>
</html>