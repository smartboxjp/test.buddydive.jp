<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>

<article>
<div id="diverBox">
<section class="accountBox">
<p class="tit">ダイバー仮登録メール送信完了</p>
<p class="mt20">ご登録いただいたアドレスに仮登録メールを送信しました。<br>メールに記載しているURLよりログインしてください。<br>
<span class="cRed">※メール届いていない場合は迷惑メールフォルダもご確認ください。<br>
hotmailをご利用の方は特に届きにくくなっております。</span><br>
よくある質問<a href="/about/faq/20151203.php">「仮登録メールが届きません」</a>をご参照ください。</p>
</section>
</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>