<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">Buddy Diveについて<br>
<span class="fs14">Buddy Diveは、自己責任で潜るダイバーを応援する予約サイトです。</span></p>
<p class="pTit">自己責任で潜るダイバーと安全レギュレーションを持つ加盟施設</p>
<p class="pTxt">バディ・ダイビングに必要な要件(スキル、病歴、保険)を満たしていることを申告＆登録し、自己責任で潜る意思を表明したダイバーは、加盟店を通じてバディ・ダイビング(ガイドやインストラクター無しのダイビング。セルフダイビング)をすることが可能になります。<br>
加盟店は、当該サイトでスキルチェック・メディカルチェック項目を満たし「バディ潜水確認事項」「利用規約」「個人情報保護方針」についての同意登録された方（以下 登録会員）へタンク（シリンダー）と施設サービスを提供します。</p>
<p class="pTit">Buddy Diveが安全のためにダイバーに約束する7つのこと</p>
<p class="pTxt">ダイビング施設として、ルールを守れる登録会員の安全のために、ダイバーの皆さんに以下の7つのことを約束します。</p>
<ul class="pList ml20">
<li>1．AEDの設置と利用できるスタッフを配置</li>
<li>2．酸素の設置と利用できるスタッフ配置</li>
<li>3．損害賠償保険に加盟</li>
<li>4．応急処置の出来るスタッフ配置</li>
<li>5．スタッフへレスキュートレーニングと応急処置のトレーニングの実施</li>
<li>6．各エリアのリスクや安全に対する情報注意を伝える</li>
<li>7．緊急時の救急隊、保安庁、警察への要請が遅滞なく実施できるように連絡網を構築</li>
</ul>
<p class="pTit">共有できる情報は登録サービスで利用できる</p>
<p class="pTxt">加盟店で潜る予約の際には、登録した情報（個人の情報、スキル、病歴）は再利用できるため、利用サービスが変わるごとに毎回書き込む手間が省けます。</p>
<p class="pTit">潜れば潜るほどお得なポイント制</p>
<p class="pTxt">加盟店で潜ると、タンク（シリンダー）と施設を含む御利用料金（施設によって一部違いがあります）の2％をポイントとしてプレゼント。<br>
ポイントをためて、タンク（シリンダー）と還元することが可能なお得なサービスです。</p>
<p class="pTit">バディで潜るための情報を提供します</p>
<p class="pTxt">自分たちでプランを立てるバディ・ダイビングでは、情報収集が要。
そこで、Buddy Diveでは、日々の海況情報をはじめ、初めてバディ・ダイビングをするダイバーのための情報を提供します
。</p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>