<?php
  $page_title = "インストラクターでも保険の加入が必要でしょうか。";
?>

<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">よくある質問<br>
<span class="fs14">お客様よりよくいただくご質問を掲載しております。</p>
<dl class="faqList">
<dt><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><?php echo $page_title; ?></p></dt>
<dd><p class="qaIcon"><span class="aIcon">A</span></p>
<div class="txt">
<p class="mb10"><span class="bold">BuddyDiveでは、「遭難対策費用（救援者費用）」が含まれた保険に加入していただくことを必ずお願いしております。</span></p>
<p class="mb20">なぜなら、バディダイビングは自分のペースで自由に潜れる半面、責任は各ダイバーにかかってきます。<br>
もし事故が起きたら、バディダイバーの救助は周りのインストラクターや現地サービスの方の助けを借りなければなりません。<br>
その際、周りの救助することへのリスクや、救助することによる失われる利益を含めて、補償する準備が必要だと我々は考えているからです。</p>

<p class="mb20">万全を期していても、事故を絶対に防ぐことはできません。オープンウォーターでもインストラクターでも事故のリスクがありますので、利用者すべての方に保険への加入を必ずお願いしております。<br>
インストラクターの場合、引率したゲストを守るための賠償保険と、自分自身の事故の際に遭難対策費用が含まれた保険がありますので、保険内容を確認ください。<br>
<a href="/about/insurance.php" target="_blank">遭難対策費用の含まれた保険についてはこちら&gt;&gt;</a></p>
</div></dd>
</dl>
<p class="tCenter"><a href="/about/faq/" class="borderLink">よくある質問TOP &gt;&gt;</a></p>
<p class="faqContact">該当する項目がない。または読んでも解決しない場合はお手数ですが下記フォームよりお問い合わせください。<br><a href="/contact/" class="borderLink mt10">お問い合わせフォーム &gt;&gt;</a></span></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>