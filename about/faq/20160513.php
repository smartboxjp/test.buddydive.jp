<?php
  $page_title = "貯まったポイントは何に使えますか？";
?>

<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">よくある質問<br>
<span class="fs14">お客様よりよくいただくご質問を掲載しております。</p>
<dl class="faqList">
<dt><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><?php echo $page_title; ?></p></dt>
<dd><p class="qaIcon"><span class="aIcon">A</span></p>
<div class="txt">
<p>ダイバー登録、ダイバー紹介、ダイビング予約などで貯まるポイントは、「バディダイビング2本」または「バディダイビング3本」を予約した際に、ポイントを使用することで割引を受けることができます。<br>
使用可能ポイントは各ダイビングセンターによって異なりますが、おおむね1本目と2本目のタンク差額分をポイント利用できます。<br>
※ポイントは現地で使用を申し込むことはできません。必ず予約時にお申し込みください。また予約がキャンセルまたは海況などで中止した場合は使用されません。</p>
</div></dd>
</dl>

<div class="guideBox">
<p class="guideTit"><span>01</span>『Pマーク』を選択</p>
<p class="img"><img src="/common/img/about/faq/img_20160513_01.jpg" alt="ログイン"></p>
<p class="txt">『Pマーク』のついたスタイルでポイントを使用できます。</p>
</div>

<div class="guideBox">
<p class="guideTit"><span>02</span>『ポイントを使う』を選択</p>
<p class="img"><img src="/common/img/about/faq/img_20160513_02.jpg" alt="ログイン"></p>
<p class="txt">『ポイントを使う』を選択すると、値引きされた金額が表示されます。<br>
※ポイントが不足している場合には、選択することはできません。</p>
</div>

<p class="tCenter"><a href="/about/faq/" class="borderLink">よくある質問TOP &gt;&gt;</a></p>
<p class="faqContact">該当する項目がない。または読んでも解決しない場合はお手数ですが下記フォームよりお問い合わせください。<br><a href="/contact/" class="borderLink mt10">お問い合わせフォーム &gt;&gt;</a></span></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>