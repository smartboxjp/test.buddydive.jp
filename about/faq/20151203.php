<?php
  $page_title = "ダイバー登録をしましたが仮登録メールが届きません。";
?>

<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">よくある質問<br>
<span class="fs14">お客様よりよくいただくご質問を掲載しております。</p>
<dl class="faqList">
<dt><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><?php echo $page_title; ?></p></dt>
<dd><p class="qaIcon"><span class="aIcon">A</span></p>
<div class="txt">
<p class="mb20">仮登録メールが届かない場合は、次のいずれかの可能性が考えられます。</p>
<p class="mb20"><span class="bold">1. 迷惑メールフォルダまたはゴミ箱に振り分けられている。</span><br>
ご利用のセキュリティソフトやメールサービスの迷惑メール機能で、迷惑メールとして扱われてしまっています。迷惑メールの解除をお願いします。<br>
・Gmailをご利用の場合：<a href="https://support.google.com/mail/answer/9008?hl=ja" target="_blank">Gmailヘルプ</a>をご参照ください。<br>
・Yahoo!メールをご利用の場合：<a href="http://www.yahoo-help.jp/app/answers/detail/p/622/a_id/47924" target="_blank">Yahoo!メールヘルプ</a>をご参照ください。</p>

<p class="mb20"><span class="bold">2. ドメイン指定受信等の受信拒否設定になっている。</span><br>
ご利用の携帯端末やメールサービスの設定で指定されたドメインからのみを受信許可になっています。下記ドメインを受信許可に設定お願いします。<br>
<span class="cRed">@buddydive.jp</span></p>

<p class="mb20"><span class="bold">3. 受信フォルダの容量が一杯になっている。</span><br>
受信フォルダ内を整理して容量に空きをつくってから、再度ダイバー登録をお願いします。<br>
<a href="/diver/registration/?url=/about/faq/20151203.php">無料ダイバー登録 &gt;&gt;</a></p>

<p class="mb20"><span class="bold">4. メールサービスのセキュリティ設定が高くブロックされている。</span><br>
メールサービスによっては、ユーザー側で操作できずブロックされてしまいます。お手数ですが別のメールアドレスで再度ダイバー登録をお願いします。<br>
<a href="/diver/registration/?url=/about/faq/20151203.php">無料ダイバー登録 &gt;&gt;</a></p>

<p class="mb20"><span class="bold">5. 登録時のメールアドレスが間違っている。</span><br>
登録メールアドレスの入力間違いである可能性があります。メールアドレスをご確認のうえ、再度ダイバー登録をお願いします。<br>
<a href="/diver/registration/?url=/about/faq/20151203.php">無料ダイバー登録 &gt;&gt;</a></p>

<p class="mb20"><span class="bold">6. サーバー、プロバイダーによって消去されている。</span><br>
上記のどれにも該当しない場合、弊社サーバーより送られたメールがお客様に届く途中で消去されている可能性があります。<br>
特に「フリーメール（yahoo!メール、Gmail、Hotmailなど）」では迷惑メールにも入らずに消去されてしまうことがあります。<br>
お手数ですが、他のメールアドレスにて再度ダイバー登録をお願いします。<br>
<a href="/diver/registration/?url=/about/faq/20151203.php">無料ダイバー登録 &gt;&gt;</a></p>

</div></dd>
</dl>
<p class="tCenter"><a href="/about/faq/" class="borderLink">よくある質問TOP &gt;&gt;</a></p>
<p class="faqContact">該当する項目がない。または読んでも解決しない場合はお手数ですが下記フォームよりお問い合わせください。<br><a href="/contact/" class="borderLink mt10">お問い合わせフォーム &gt;&gt;</a></span></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>