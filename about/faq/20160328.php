<?php
  $page_title = "フィーチャーフォン（ガラケー）でサービスを利用することができるでしょうか。";
?>

<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">よくある質問<br>
<span class="fs14">お客様よりよくいただくご質問を掲載しております。</p>
<dl class="faqList">
<dt><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><?php echo $page_title; ?></p></dt>
<dd><p class="qaIcon"><span class="aIcon">A</span></p>
<div class="txt">
<p>当サイトの閲覧環境にフィーチャーフォンは含まれておりません。ご登録やご予約などのサイト利用はPCまたはスマートフォンからお願いします。<br>
ご登録は、フィーチャーフォンのキャリアメール（docomo,au,softbankなど）でもご利用いただけます。<br>
メールが届かない場合は<a href="/about/faq/20151203.php">【メールが届きません】</a>をご参照ください。</p>
</div></dd>
</dl>

<p class="tCenter"><a href="/about/faq/" class="borderLink">よくある質問TOP &gt;&gt;</a></p>
<p class="faqContact">該当する項目がない。または読んでも解決しない場合はお手数ですが下記フォームよりお問い合わせください。<br><a href="/contact/" class="borderLink mt10">お問い合わせフォーム &gt;&gt;</a></span></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>