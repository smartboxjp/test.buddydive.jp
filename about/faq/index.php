<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">よくある質問<br>
<span class="fs14">お客様よりよくいただくご質問を掲載しております。</span></p>
<ul class="faqList">
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20151203.php">ダイバー登録をしましたが仮登録メールが届きません。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20151204.php">問い合わせをしましたがメールが返ってきません。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20151222.php">パスワードを忘れてしまいました。再発行はどこからすればよいでしょうか。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20160328.php">フィーチャーフォン（ガラケー）でサービスを利用することができるでしょうか。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20160329.php">予約したダイビングポイントの変更方法を教えてください。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20160513.php">貯まったポイントは何に使えますか。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20161014.php">経験本数やダイビングレベルなど、どの程度であればバディダイビングができるでしょうか。</a></p></li>
<li><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><a href="/about/faq/20161018.php">インストラクターでも保険の加入が必要でしょうか。</a></p></li>
</ul>
<p class="faqContact">該当する項目がない。または読んでも解決しない場合はお手数ですが下記フォームよりお問い合わせください。<br><a href="/contact/" class="borderLink mt10">お問い合わせフォーム &gt;&gt;</a></span></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>