<?php
  $page_title = "経験本数やダイビングレベルなど、どの程度であればバディダイビングができるでしょうか。";
?>

<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">よくある質問<br>
<span class="fs14">お客様よりよくいただくご質問を掲載しております。</p>
<dl class="faqList">
<dt><p class="qaIcon"><span class="qIcon">Q</span></p><p class="txt"><?php echo $page_title; ?></p></dt>
<dd><p class="qaIcon"><span class="aIcon">A</span></p>
<div class="txt">
<p class="mb10"><span class="bold">経験本数について</span><br>
Cカードを保持されている方であれば、経験本数は特に関係ありません。<br>
最低限のスキルと健康状態維持し、事故後の自己責任体制を整えていただければ、経験が少ないなりの潜り方でダイビングは楽しめるであろうと考えております。すこしずつ各バディのペースで経験を増やしていただければとも考えております。</p>
<p class="mb20">（例）穏やかな海況で、ナビゲーションがしやすく、軽いトラブルで水面へ浮上するのに問題がない（船舶の往来がないなど）
海域でしたら、経験が少なくとも当システムで提示としているスキルと万全な体調があれば問題なくダイビング楽しめると考えております。</p>

<p class="mb10"><span class="bold">ダイビングレベルについて</span><br>システムへの登録の際に「スキルチェック」項目を全部クリア（できる）ことを表明いただくことが条件となります。<br>
<a href="/about/skill.php" target="_blank">スキルチェック項目はこちら&gt;&gt;</a></p>
<p class="mb20">スキルチェック項目はOW（オープン・ウォーター）で習得されているスキルです。もしできないスキルがあればインストラクターから再トレーニングを受けてください。今後はトレーニングシステムも提供していく予定です。</p>
</div></dd>
</dl>
<p class="tCenter"><a href="/about/faq/" class="borderLink">よくある質問TOP &gt;&gt;</a></p>
<p class="faqContact">該当する項目がない。または読んでも解決しない場合はお手数ですが下記フォームよりお問い合わせください。<br><a href="/contact/" class="borderLink mt10">お問い合わせフォーム &gt;&gt;</a></span></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>