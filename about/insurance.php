<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<h1 class="tit">バディダイビング時に加入が必要な保険</h1>
<h2 class="pTit">遭難対策費用が含まれる保険とは</h2>
<p class="pTxt">バディダイビングは自分のペースで自由に潜れる楽しさがキーポイントです。
しかし、自由な半面、責任は各ダイバーにかかってきます。もし事故が起きたらバディダイバーの救助は周りのインストラクターや現地サービスの方の助けを借りなければなりません。その際に周りの救助することへのリスクや救助することによる、失われる利益を含めて、保障する準備が必要だと我々は考えます。</p>
<h2 class="pTit">1日から入れる保険</h2>
<ul class="guideUl mt20">
<li class="pc"><p class="guideImg"><a href="https://www.nttdocomo.co.jp/service/convenience/insurance/onetime_insurance/sports/"
 target="_blank"><img src="/common/img/about/insurance/img_docomo.png" width="200" height="200" alt="ドコモ ワンタイム保険"></a></p>
 <p class="tCenter"><span class="bold">ドコモ ワンタイム保険<br>『スポーツ・レジャー保険』</span><br>プラン：しっかり<br>保険料（1日）：590円</p>
 <p class="blueBtn small mt10"><a href="https://www.nttdocomo.co.jp/service/convenience/insurance/onetime_insurance/sports/
" target="_blank">詳細・申込</a></p></li>
<li class="sp"><p class="guideImg"><a href="http://onetime.nttdocomo.co.jp/n/sp/onetime/sports/sports.html"
 target="_blank"><img src="/common/img/about/insurance/img_docomo.png" width="200" height="200" alt="ドコモ ワンタイム保険"></a></p>
 <p class="tCenter"><span class="bold">ドコモ ワンタイム保険<br>『スポーツ・レジャー保険』</span><br>プラン：しっかり<br>保険料（1日）：590円</p>
<p class="blueBtn small mt10 sp"><a href="http://onetime.nttdocomo.co.jp/n/sp/onetime/sports/sports.html
" target="_blank">詳細・申込</a></p></li>
<li><p class="guideImg"><a href="http://www.au-sonpo.co.jp/pc/kokunai/#premium"
 target="_blank"><img src="/common/img/about/insurance/img_au.png" width="200" height="200" alt="au損保"></a></p>
 <p class="tCenter"><span class="bold">au損保<br>『国内旅行の保険』</span><br>プラン：ブロンズ<br>保険料（1日）：262円</p>
 <p class="blueBtn small mt10"><a href="http://www.au-sonpo.co.jp/pc/kokunai/#premium"
 target="_blank">詳細・申込</a></p></li>
<li><p class="guideImg"><a href="http://www.softbank.jp/mobile/service/insurance-sports-leisure/" target="_blank"><img src="/common/img/about/insurance/img_softbank.png" width="200" height="200" alt="ソフトバンクかんたん保険"></a></p>
<p class="tCenter"><span class="bold">ソフトバンクかんたん保険<br>『スポーツ・レジャー保険』</span><br>プラン：ちょこっと<br>保険料（1日）：300円</p>
<p class="blueBtn small mt10"><a href="http://www.softbank.jp/mobile/service/insurance-sports-leisure/
" target="_blank">詳細・申込</a></p></li>
</ul>
<p class="mt20">※プランによっては『救援者費用』が含まれていないことがあります。申込前にご確認ください。</p>
<div class="faqContact mt20 mb30">
<h3 class="bold mb10">保険加入方法のご案内</h3>
<ul>
<li>・<a href="/common/img/about/insurance/insurance_docomo.pdf" target="_blank">ドコモ ワンタイム保険『スポーツ・レジャー保険』の加入方法 [PDF]<br></a></li>
<li>・<a href="/common/img/about/insurance/insurance_au.pdf" target="_blank">au損保『国内旅行の保険』の加入方法 [PDF]<br></a></li>
<li>・<a href="/common/img/about/insurance/insurance_softbank.pdf" target="_blank">ソフトバンクかんたん保険『スポーツ・レジャー保険』の加入方法 [PDF]<br></a></li>
</ul>
</div>
<div class="mb30">
<h2 class="pTit">DANジャパンの会員</h2>
<div class="pTxt">
<dl class="l200Dl mt20">
<dt><a href="http://www.danjapan.gr.jp/join/index.html" target="_blank"><img src="/common/img/about/insurance/img_dan.png" width="200" height="200" alt="DANジャパン"></a></dt>
<dd>1年間有効なDANジャパンの会員には「遭難対策費用」が自動的に付加されています。さらに減圧症対応などのDANの医療ネットワークの利用や海外での事故にも対応しています。<p class="blueBtn small mt10"><a href="http://www.danjapan.gr.jp/join/index.html" target="_blank">詳細・申込</a></p></dd>
</dl>
</div>
</div>
<h2 class="pTit">PADIダイバーズ保険（傷害総合保険）</h2>
<div class="pTxt">
<dl class="l200Dl mt20">
<dt><a href="http://www.padi.co.jp/visitors/insure/index.asp" target="_blank"><img src="/common/img/about/insurance/img_padi.png" width="200" height="200" alt="PADIダイバーズ保険（傷害総合保険）"></a></dt>
<dd>保険期間1年間のPADIダイバーズ保険は、月々910円からのスタンダードプランがあります。カメラ派ダイバーに嬉しいカメラ水没事故の補償もついています。<p class="blueBtn small mt10"><a href="http://www.padi.co.jp/visitors/insure/index.asp" target="_blank">詳細・申込</a></p></dd>
</dl>
</div>
<p class="tRight">（2016年2月現在）</p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>