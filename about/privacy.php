<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">個人情報保護方針</p>
<p class="pTxt">「バディダイブ」（以下「当サイト」といいます）は、本サイト（https://buddydive.jp）において、個人情報を保護するために細心の注意を払っています。
ここでご案内する当サイトの個人情報保護方針（以下「本個人情報保護方針」といいます）は、お客様に当サイトが運営する本サイトを安心してご利用いただくために、お客様の個人情報をどの様に取り扱っているかを説明しております。
当サイトが掲載した本個人情報保護方針を守っていないと感じた場合には、当サイトお問い合わせ先にご連絡ください。</p>
<p class="pTit">個人情報の収集と利用目的について</p>
<p class="pTxt">当サイトが収集した個人情報は、お客様への当サイトサービスの提供、商品購入時の決済の他、下記の目的にのみ利用し、下記目的外での利用はいたしません。</p>
<ul class="pList">
<li>・当サイトの商品のご案内、その他第三者が本システム登録のお客様向けに企画した特別プロモーション(以下「会員様向特別プロモーション企画」といいます。）に関するご案内など、当サイトからお客様へのダイレクトメール、電子メールによる情報提供（なお、会員様向特別プロモーション企画のご案内については、当該企画を行った第三者からの依頼を受けて、あくまでも当サイトからお客様宛に当該企画についての情報を提供させていただくものであり、当サイトから当該企画を行った第三者に対してお客様の個人情報を提供することはありません。）</li>
<li>・お客様との連絡</li>
<li>・その他お客様に有益で便利なサービスをご提供するため</li>
</ul>
<p class="pTxt">【登録情報について】<br>
プロフィール、スキル、メディカル等の登録情報は、予約業務の遂行ならびに安全確認のため提携ダイビング業者に開示します。また、事故などのトラブルが発生した場合は登録情報を救命、捜索、調査に関わる各機関との連携のため開示します。</p>
<p class="pTxt">【ダイバー登録済みのバディを探すについて】<br>
登録ダイバーのお名前とメールアドレスは、バディ検索を行うために利用します。</p>
<p class="pTxt">【アクセスログについて】<br>
本サイトでは、アクセスされた方の情報をアクセスログという形で記録しています。アクセスログには、アクセスされた方のIPアドレス、ブラウザの種類、ドメイン名、アクセス時間、アクセス回数などが含まれます。アクセスログは、本サイトの保守管理や利用状況に関する統計分析のために活用いたしますが、上記以外の目的では利用はいたしません。</p>
<p class="pTxt">【ご友人招待について】<br>
お客様が本サイトのを利用して、ご友人を招待する場合、当サイトはお客様よりご友人のメールアドレスを収集いたします。その後、当サイトは自動的に本サービスを紹介したメールをご友人に送付します。なお、本サービスで収集したご友人のメールアドレスは、上記以外の目的では利用いたしません。</p>
<p class="pTit">クッキー、ウェブビーコンの使用について</p>
<p class="pTxt">本サイトでは、多くのウェブサイトと同様にクッキーおよびウェブビーコンを使用しております。クッキーとは、お客様が本サイトに訪問したときに、サーバーがお客様のパソコンにデータを送るために利用する小さなデータファイルで、サービスを充実させ、本サイトをお客様により便利に利用していただくために使用しています。本サイトでは、ショッピングカートやログイン管理、統計情報の収集のために、クッキーを使用しております。
お客様は、クッキーを受け取る際に警告を表示するようにブラウザを設定することにより、クッキーを受け取るか否かを決めることができます。クッキーを拒否した場合には、本サイトのサービスが一部ご利用できない場合がございます。クッキーを利用することによって、本サイトの機能をより便利にご利用していただくことができるようになるため、クッキーを受け取る設定にしておくことをおすすめいたします。
また、本サイトでは広告配信、効果測定、利便性の向上などのために広告を表示している会社や第三者企業において、クッキーもしくはウェブビーコンなどを使用することがございます。この場合のクッキーもしくはウェブビーコンなどの利用は、各会社のプライバシーの考え方に従って行われます。</p>
<p class="pTit">個人情報の業務提携に伴う提供および管理について</p>
<p class="pTxt">当サイトは、その他提携会社と共同で業務を行うために必要となる場合に、利用規約においてお客様の同意を得た上で、提携会社に対して個人情報の開示を行うことがあります。この場合にも、当サイトは、個人情報の安全管理が図られるよう、他提携会社に対し、当該業務目的以外での使用を禁止し、当該業務終了後には、他提携会社が、当該個人情報を適切な処置で廃棄または当サイトに返還することを義務づけています。</p>
<p class="pTit">個人情報の第三者への開示について</p>
<p class="pTxt">当サイトが収集した個人情報については、以下の場合を除き、本人（当該個人情報により識別される特定の個人をいう。以下、同じ。）の許可なく第三者への開示はいたしません。また、当ウェブサイト上で皆様からいただいた個人情報は、その他情報源（名簿、住所録、第三者など）から間接的に収集した情報で統合、加工されることは一切ありません。</p>
<ul class="pList">
<li>・法令に基づく場合</li>
<li>・人の生命、身体または財産の保護のために必要がある場合であって、お客様の同意を得ることが困難であるとき</li>
<li>・公衆衛生の向上または児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難であるとき</li>
<li>・国の機関又、地方公共団体またはその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合で、お客様の同意を得ることにより当該事務の遂行に支障を及ぼすおそれがあるとき</li>
</ul>
<p class="pTit">情報の管理およびセキュリティについて</p>
<p class="pTxt">当サイトでは、個人情報の管理については、合理的な技術的方策を採ることにより、個人情報の紛失、改ざん、漏洩などのないよう努めております。また、当サイトでは、個人情報を扱う社員に対して、教育啓蒙活動を実施し、個人情報の安全管理を義務づけています。お客様の登録情報はプライバシー保護およびセキュリティのため、パスワードで保護されています。また、お客様の特に重要な情報が送信される際、セキュリティのために、本サイトでは、SSL（Secure Socket Layer）というソフトウェアを使用しています。SSLを用いることにより、お客様の個人情報やクレジットカード番号などの情報を暗号化してお客様のパソコンからサーバーまで送信することができ、盗聴や情報の改ざんなどを防止し、インターネット上で個人情報を安全に送信することができます。本サイトにはコミュニティページが存在します。サイト運営スタッフが常時書き込みチェックし管理に努めておりますが、コミュニティページで生じるどのような損害に関しても一切の責任を負わないものとします。</p>
<p class="pTit">個人情報の本人開示・訂正方法について</p>
<p class="pTxt">当サイトでは、本人から、当該本人が識別される保有個人データの開示を求められたときは、本人に対し、遅滞なく、当該保有個人データを開示しております。ただし、以下の場合にはその全部または一部を開示しない場合があります。</p>
<ul class="pList">
<li>・本人または第三者の生命、身体、財産その他の権利利益を害するおそれがある場合</li>
<li>・当サイトの業務の適正な実施に著しい支障を及ぼすおそれがある場合</li>
<li>・他の法令に違反することとなる場合<br>また、ご登録いただきましたお客様の個人情報について、お客様が変更・訂正・削除（以下「変更など」といいます。）をされたい場合には、変更などができるようになっております。</li>
</ul>
<p class="pTit">リンクについて</p>
<p class="pTxt">本サイトよりリンクを設定している他のサイトでの個人情報の取り扱いについては、当サイトでは責任を負うことができません。本サイトより他のサイトに移動される場合には、各リンク先サイトの個人情報取り扱いについてのプライバシーポリシーなどをご覧になることをおすすめいたします。本個人情報保護方針は本サイトによって収集された個人情報のみに適用されます。</p>
<p class="pTit">本個人情報保護方針の改訂について</p>
<p class="pTxt">当サイトは本個人情報保護方針の全部または一部を改訂することがあります。重要な変更がある場合には、本サイトのページ上において、分かりやすい方法でお知らせします。また、当初個人情報を収集した時点で本個人情報保護方針が述べていたものと異なった利用目的、方法で個人情報を使用する場合には、お客様にご連絡いたします。本サイトが異なった方法で個人情報を使用してよいかどうかの選択権はお客様が有しております。</p>
<p class="pTit">個人情報保護関連法令の遵守について</p>
<p class="pTxt">当サイトでは、個人情報の保護に関連する日本の法令その他の規範を遵守するとともに、本ポリシーの内容を継続的に見直し、その改善に努めています。</p>
<p class="pTit">個人情報の開示の手続きについて</p>
<p class="pTxt">個人情報の開示をお求めの場合は、当サイトお問い合わせ先にご連絡ください。</p>
<p class="pTit">本個人情報保護方針についてのお問い合わせ先</p>
<p class="pTxt">「バディダイブ」<br>
お問い合わせ先は<a href="/contact/">こちら</a></p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>