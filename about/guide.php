<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">

<section class="mypageCont">
<p class="tit">BuddyDiveの使い方</p>
<ul class="guideUl">
<li><p class="guideRegist"><img src="/common/img/about/ico_regist.png" alt="新規登録500ptプレゼント！"></p>
<p class="guideImg"><img src="/common/img/about/img_regist.png" alt="プロフィール スキル メディカル"></p>
<div class="guideTxt"><span class="guideTit">ダイバー登録</span>プロフィール、スキル、メディカルを登録して事前にダイビングセンターに伝えることでの受付をスムーズにし、緊急時に備えることができます。<br>
<a href="#REGIST">&#9660; ダイバー登録の方法</a>
<p class="orangeBtn small mt20"><a href="/diver/registration/?url=/about/about.php?">無料ダイバー登録</a></p>
</div>
</li>
<li>
<p class="guideImg"><img src="/common/img/about/img_reserve.png" alt="ダイビング予約"></p>
<div class="guideTxt"><span class="guideTit">ダイビング予約</span>お気に入りのダイビングポイントを選んでカンタン予約。バディの招待も忘れずに。ダイビング後にはポイントが2%付きます。<br>
<a href="/about/guide_reserve.php">&#9654; ご予約方法</a></p></div>
</li>
<li>
<p class="guideInvite"><img src="/common/img/about/ico_invite.png" alt="紹介したら500ptプレゼント！"></p>
<p class="guideInvited"><img src="/common/img/about/ico_invited.png" alt="紹介されたら500ptプレゼント！"></p>
<p class="guideInvitedRegist"><img src="/common/img/about/ico_invited_regist.png" alt="新規登録500ptプレゼント！"></p>
<p class="guideImg"><img src="/common/img/about/img_invite.png" alt="バディを招待"></p>
<div class="guideTxt"><span class="guideTit">バディを招待</span>招待ポイントプレゼント中！<br>
「招待した人」は招待<span class="cPink">500pt</span>。「招待された人」は新規登録<span class="cPink">500pt</span> + 招待<span class="cPink">500pt</span>がプレゼントされます。</p>
<p class="orangeBtn small mt20"><a href="/diver/invitation/">バディを招待</a></p>
</div>
</li>
</ul>
</section>

<section class="mypageCont" id="REGIST">
<p class="tit">ダイバー登録の方法</p>
<div class="guideBox">
<p class="guideTit"><span>01</span>『無料ダイバー登録』ボタンをクリック</p>
<p class="img"><img src="/common/img/about/guide/img_regist01.jpg" alt="無料ダイバー登録"></p>
<p class="txt">ナビゲーションまたはTOPページに設置してある『<a href="/diver/registration/">無料ダイバー登録</a>』をクリックしてください。</p>
</div>
<div class="guideBox">
<p class="guideTit"><span>02</span>『ダイバー登録』を入力</p>
<p class="img"><img src="/common/img/about/guide/img_regist02.jpg" alt="ダイバー登録"></p>
<ul class="txt mb10">
<li>1.ユーザー名、メールアドレス、パスワードを入力。</li>
<li>2.利用規約、個人情報保護方針、バディ潜水確認事項をご確認のうえ同意にチェック。</li>
</ul>
<p class="txt">※ユーザー名は重複していると登録できません。入力欄の右にあるチェックボタンで使用されているか確認ができます。</p>
</div>
<div class="guideBox">
<p class="guideTit"><span>03</span>『仮登録メール』の本登録用URLをクリック</p>
<p class="img"><img src="/common/img/about/guide/img_regist03.jpg" alt="『仮登録メール』の本登録用URLをクリック"></p>
<ul class="txt mb10">
<li>1.『件名：『BuddyDive』仮登録ありがとうございます。』というメールが届きます。</li>
<li>2.記載されている『<span class="cRed">本登録用URL</span>』をクリック。</li>
</ul>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">メールが届かない方は<a href="/about/faq/20151203.php">【よくある質問】ダイバー登録をしましたが仮登録メールが届きません。</a>をご参照ください。</span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>04</span>『プロフィール』を入力</p>
<p class="img"><img src="/common/img/about/guide/img_regist04.jpg" alt="『プロフィール』を入力"></p>
<p class="txt mb10">ダイバーのプロフィールを入力してください。</p>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">画像を登録するとバディが検索するときにみつけやすくなります。</span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>05</span>『スキルチェック』を入力</p>
<p class="img"><img src="/common/img/about/guide/img_regist05.jpg" alt="『スキルチェック』を入力"></p>
<ul class="txt mb10">
<li>1.各項目を『できる』か『できない』を選択。</li>
<li>2.最後の『私はすべてのスキルができます』か『私はできないスキルがあります』を上記内容に合わせて選択。</li>
</ul>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">『私はできないスキルがあります』を選択した場合は、バディダイビングのご予約をすることはできません。</span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>06</span>『メディカルチェック』を入力</p>
<p class="img"><img src="/common/img/about/guide/img_regist06.jpg" alt="『メディカルチェック』を入力"></p>
<ul class="txt">
<li>1.各項目を『はい』か『いいえ』を選択。</li>
<li>2.慢性的疾患の最後『私は上記項目に該当する項目は一つもありません』か『私は上記に該当する項目があるが、医師の診断許可書を持参しサービスを受けます』を上記内容に合わせて選択。</li>
<li>3.一時的疾患の最後にチェック。</li>
</ul>
</div>
<div class="guideBox">
<p class="guideTit"><span>07</span>ダイバー登録完了</p>
<p class="img"><img src="/common/img/about/guide/img_regist07.jpg" alt="ダイバー登録完了"></p>
<p class="txt">登録情報はダイバーページから変更することができます。</p>
</div>


<?php if($_SESSION['member_id']==""){ ?>
<p class="blueBtn mt20"><a href="/diver/registration/?url=<? echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];?>">無料ダイバー登録</a></p>
<?php }else{ ?>
<p class="blueBtn mt20"><a href="/diver/invitation/">バディを招待する</a></p>
<?php } ?>

</section>


</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>