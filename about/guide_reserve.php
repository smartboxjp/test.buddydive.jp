<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">
<section class="mypageCont">
<p class="tit">BuddyDiveのご予約方法</p>
<div class="guideBox">
<p class="guideTit"><span>01</span>『予約フォームへ』ボタンをクリック</p>
<p class="img"><img src="/common/img/about/guide/img_reserve01.jpg" alt="『予約フォームへ』ボタンをクリック"></p>
<p class="txt">ご希望のダイビングポイントから『予約フォームへ』ボタンをクリック。</p>
</div>
<div class="guideBox">
<p class="guideTit"><span>02</span>ログイン</p>
<p class="img"><img src="/common/img/about/guide/img_reserve02.jpg" alt="ログイン"></p>
<p class="txt">※すでにログインしている場合は自動的に次へ移動します</p>
</div>
<div class="guideBox">
<p class="guideTit"><span>03</span>『スタイル』『エントリー』を選択</p>
<p class="img"><img src="/common/img/about/guide/img_reserve03.jpg" alt="『スタイル』『エントリー』を選択"></p>
<ul class="txt mb10">
<li>『スタイル』とは、バディダイビング3本、バディダイビング2本、バディダイビング1本などです。</li>
<li>『エントリー』とは、ビーチやボート。ダイビングポイントによって異なります。</li>
</ul>
</div>
<div class="guideBox">
<p class="guideTit"><span>04</span>『日付』『タンク』『ポイント』を選択</p>
<p class="img"><img src="/common/img/about/guide/img_reserve04.jpg" alt="『日付』『タンク』『ポイント』を選択"></p>
<ul class="txt mb10">
<li>1.日付を選択（約3ヶ月先まで選択可能です）</li>
<li>2.タンクを選択（タンクによって差額が発生する差額が記載されています）</li>
<li>3.ポイントを使用するか選択（使用可能ポイントが足りない場合は、ボタンは表示されません）</li>
</ul>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">&#9314;の『スタイル』『エントリー』を入力後に、『日付』『タンク』『ポイント』は選択可能になります。</span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>05</span>『料金』『レンタルなど』『保険』の確認をして『予約する』をクリック</p>
<p class="img"><img src="/common/img/about/guide/img_reserve05.jpg" alt="『スキルチェック』を入力"></p>
<ul class="txt mb10">
<li>1.料金を確認する</li>
<li>2.レンタル器材、ガイド、ランチなどのリクエストがあれば入力</li>
<li>3.<span class="cRed">当日有効な保険に加入するにチェック</span></li>
</ul>
<div class="txt mb20"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">この保険は、参加者が参加者の過失により発生したトラブルに利用できる保険で「100万円以上の遭難対策費用」が含まれるものです。<span class="cRed">必ず保険への加入が必要です。</span><br>
<a href="/about/insurance.php" target="_blank">バディダイビング時に加入が必要な保険</a></span></p></div>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">予約完了メールが届きます。当日ダイビングをするには次項目のバディを選択とバディ自身のご予約が必要になります。<br>
<a href="/about/faq/20151203.php" target="_blank">【よくある質問】メールが届かいない場合はこちら</a></span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>06</span>『バディ』を選択</p>
<p class="img"><img src="/common/img/about/guide/img_reserve06.jpg" alt="『バディ』を選択"></p>
<ul class="txt mb20">
<li class="bold">&#9660; ダイバー登録済みのバディを探す</li>
<li>1.『バディのお名前』または『E−mail』を入力して『検索』をクリック</li>
<li>2.検索結果からバディを選択してクリック</li>
<li>3.『バディ選択完了』をクリック</li>
</ul>
<ul class="txt mb10">
<li class="bold">&#9660; ダイバー未登録のバディを招待する</li>
<li>1.『バディのお名前』と『E−mail』を入力</li>
<li>2.『招待』をクリック</li>
<li>3.入力したバディにメールが招待メールが送信されます</li>
</ul>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT"><a href="/about/faq/20151203.php" target="_blank">【よくある質問】メールが届かいない場合はこちら</a></span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>07</span>『ダイバーページ』からご予約確認</p>
<p class="img"><img src="/common/img/about/guide/img_reserve07.jpg" alt="『ダイバーページ』からご予約確認"></p>
<p class="txt mb10">ご自分の『<a href="/diver/" target="_blank">ダイバーページ</a>』からご予約の確認や取消ができます。<br>
招待したバディの予約状況や追加・変更をすることができます。</p>
<div class="txt"><p class="iconTxt"><span class="iconTxtI">!</span>
<span class="iconTxtT">招待したバディが予約になっていることを必ず確認してください。バディのご予約がない場合はダイビングに参加していただけません。</span></p></div>
</div>
<div class="guideBox">
<p class="guideTit"><span>08</span>ご予約当日</p>
<p class="img"><img src="/common/img/about/guide/img_reserve08.jpg" alt="ご予約当日"></p>
<p class="txt mb10">『<a href="/condition/index.php?<?php echo "s_yyyy=" . date("Y") . "&s_mm=" .  date("m") . "&s_dd=" . date("d");?>">今日の海況</a>』から海況をご確認いただけます。</p>
<ul class="txt">
<li class="bold indent">&#9660; クローズの場合</li>
<li>・ダイビングポイントから登録メールにクローズのお知らせが届きます。</li>
<li>・『<a href="/condition/index.php?<?php echo "s_yyyy=" . date("Y") . "&s_mm=" .  date("m") . "&s_dd=" . date("d");?>">今日の海況</a>』をご覧になって他のダイビングポイントへご予約をお申し込みください。</li>
<li>・または、ダイビングを中止してのんびり温泉にでも・・・</li>
</ul>
</div>
<div class="guideBox">
<p class="guideTit"><span>09</span>ご自分のダイビングログを確認する</p>
<p class="img"><img src="/common/img/about/guide/img_reserve09.jpg" alt="ご自分のダイビングログを確認する"></p>
<p class="txt">ダイビングが終了した翌日にはご自分の『<a href="/diver/" target="_blank">ダイバーページ</a>』を訪ねてみてください。潜ったダイビングポイント、日付等のログが表示され、ポイントも加算されています。</p>
</div>

<p class="blueBtn mt20"><a href="/divingpoint/">ダイビングポイントを探す</a></p>

</section>


</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>