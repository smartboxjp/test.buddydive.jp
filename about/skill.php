<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/header.php'); ?>
<article>
<div id="diverBox">



<section class="mypageCont">
<p class="tit">スキルチェック</p>
<p>安全にバディ潜水ができるためには、下記すべてのスキル項目に「できる」のチェックが必要です。ブランクがあり、できないスキルがある場合は、必ず下記スキルができるようになってからバディダイビングにご参加ください。</p>
    <table class="checkTable">
      <tr>
        <th class="tCenter bgLightGray">項目</th>
      </tr>
      <tr>
        <th>器材のセッティングと調整</th>
      </tr>
      <tr>
        <th>水面でBCDに落ち着いて給気</th>
      </tr>
      <tr>
        <th>水面でオーラル（口から）からBCDに給気</th>
      </tr>
      <tr>
        <th>水面で緊急にウエイト脱装</th>
      </tr>
      <tr>
        <th>水面でウエイト脱・装着</th>
      </tr>
      <tr>
        <th>適正ウエイトのチェック</th>
      </tr>
      <tr>
        <th>適正ウエイトでBCに頼らずスノーケリング遊泳</th>
      </tr>
      <tr>
        <th>水面下でスノーケルとレギュ交換</th>
      </tr>
      <tr>
        <th>スクーバユニット脱着 水面</th>
      </tr>
      <tr>
        <th>水深10m地点へフリー潜降</th>
      </tr>
      <tr>
        <th>いかなる水深からも安全なスピードで浮上</th>
      </tr>
      <tr>
        <th>潜行中に圧平衡（耳抜きなど）できない場合は落ち着いて対処</th>
      </tr>
      <tr>
        <th>潜降して暫く着底しない</th>
      </tr>
      <tr>
        <th>水中でレギュレタークリアー2種</th>
      </tr>
      <tr>
        <th>水中でレギュレータリカバリー</th>
      </tr>
      <tr>
        <th>全部に水の入ったマスククリアー</th>
      </tr>
      <tr>
        <th>落ちついてマスク脱着クリアー</th>
      </tr>
      <tr>
        <th>マスク無し呼吸で1分以上水中を泳ぐ</th>
      </tr>
      <tr>
        <th>水中を水平にゆっくり泳ぐ</th>
      </tr>
      <tr>
        <th>ホバーリング3分</th>
      </tr>
      <tr>
        <th>トリムの調整（中層で水平姿勢を維持）</th>
      </tr>
      <tr>
        <th>傷つきやすい水底の上を泳ぐ</th>
      </tr>
      <tr>
        <th>オーラルインフレーションでホバーリング</th>
      </tr>
      <tr>
        <th>自分の為に自分のバックアップ空気源で呼吸</th>
      </tr>
      <tr>
        <th>バディとバックアップ空気源を使って浮上</th>
      </tr>
      <tr>
        <th>水深9m以浅からコントロールされた緊急スイミングアセント</th>
      </tr>
      <tr>
        <th>フリーフローレギュからの呼吸</th>
      </tr>
      <tr>
        <th>スクーバユニット脱着 水中</th>
      </tr>
      <tr>
        <th>ウエイト脱着 水中</th>
      </tr>
      <tr>
        <th>こむらがえりの除去、自分とバディ</th>
      </tr>
      <tr>
        <th>エアーマネージメント50気圧残して浮上</th>
      </tr>
      <tr>
        <th>疲労ダイバーの水面曳航25m</th>
      </tr>
      <tr>
        <th>コンパスナビゲーションでの水面移動</th>
      </tr>
      <tr>
        <th>計画潜水を立てる</th>
      </tr>
      <tr>
        <th>バディと潜水前の器材や体調のチェック（プレダイブセーフティーチェック）</th>
      </tr>
      <tr>
        <th>ハンドシグナルを使って意思の疎通</th>
      </tr>
      <tr>
        <th>シグナルフロートを水面で立てる</th>
      </tr>
      <tr>
        <td colspan="2" class="lastCheck required">
					          <label id="label_skill_1_1">
						<span>私はすべてのスキルができます。</span>
          </label>
          <label id="label_skill_1_2">
						<span>私はできないスキルがあります。</span>
          </label>
        </td>
      </tr>
    </table>
    <p class="orangeBtn mt20"><a href="/diver/registration/">無料ダイバー登録</a></p>
    <p class="tCenter mt20">チェックするか否かの疑問がある方は、お気軽にサポートディスクまで<a href="/contact/">お問い合わせ</a>ください。</p>
</section>

</div>
</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/footer.php'); ?>