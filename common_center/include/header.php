<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common_center/include/common.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex,nofollow">
<title><?php echo $header_centername; ?> [ダイビングセンター]</title>

<link rel="stylesheet" href="/common_center/css/common.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="/mock_center/common/js/jquery-1.11.1.min.js"><\/script>')</script>
<script src="/common/js/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet" href="/common/js/sweetalert/sweetalert.css">
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/analytics.php'); ?>
</head>

<body>
<?php if($testserver) { echo '<div style="background:rgba(255,0,0,0.5); color:#fff; font-size:15px; text-align:center; position:fixed; z-index:9999; top:0; width:100%;">テストサーバーです。</div>' ; } ?>  
<header><p class="centerName"><?php if($page_file == 'login'){ echo ダイビングセンターログイン; }else{ echo $header_centername;} ?></p></header>
<nav>
<p>Buddy Dive<br>
Resavetion System</p>
<?php if($page_file != 'login'){ ?>
<ul>
  <li><a href="/dive_center/dashboard/"<?php if($page_file == 'dashboard'){ echo ' class="act"';}?>>ダッシュボード</a></li>
  <li><a href="/dive_center/reserve/"<?php if($page_file == 'reserve'){ echo ' class="act"';}?>>予約管理</a></li>
  <li><a href="/dive_center/sales/"<?php if($page_file == 'sales'){ echo ' class="act"';}?>>売上管理</a></li>
  <li><a href="/dive_center/stock_style/"<?php if($page_file == 'stock_style' or $page_file == 'stock_boat' or $page_file == 'stock_tank'){echo ' class="act"';}?>>在庫設定</a>
    <?php if($page_file == 'stock_style' or $page_file == 'stock_boat' or $page_file == 'stock_tank'){ ?>
      <ul>
        <li><a href="/dive_center/stock_style/"<?php if($page_file == 'stock_style'){echo ' class="act"';}?>>スタイル</a></li>
        <li><a href="/dive_center/stock_boat/"<?php if($page_file == 'stock_boat'){echo ' class="act"';}?>>エントリー</a></li>
        <li><a href="/dive_center/stock_tank/"<?php if($page_file == 'stock_tank'){echo ' class="act"';}?>>タンク</a></li>
      </ul>
    <?php } ?>
  </li>
  <li><a href="/dive_center/report/"<?php if($page_file == 'report'){echo ' class="act"';}?>>各種資料</a></li>
  <li><a href="/dive_center/contact/"<?php if($page_file == 'contact'){echo ' class="act"';}?>>お問い合わせ</a></li>
  <li><a href="/dive_center/login/logout.php">ログアウト</a></li>
</ul>
<?php } ?>
</nav>