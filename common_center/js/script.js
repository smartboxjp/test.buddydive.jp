// JavaScript Document

// 全角を半角に変換
function changeHalf() {
  $('.fm').change(function(){
    var txt  = $(this).val();
    var han = txt.replace(/[Ａ-Ｚａ-ｚ０-９－！"＃＄％＆'（）＝＜＞，．？＿［］｛｝＠＾～￥]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
    $(this).val(han);
  });
  $('.fm').change(function(){
    var txt  = $(this).val();
    var han = txt.replace(/[\u2212\uff0d\u30fc\u2012\u2013\u2014\u2015\u2500]/g, "-");
    $(this).val(han);
  });
};

// フォームのinputをフォーカス時に色変更
function addFocus(){
  $('input[type=text],textarea').focus(function(){
    $(this).addClass('focus');
  }).blur(function(){
    $(this).removeClass('focus');
  });
};

// ページロード時関数を実行
changeHalf();
addFocus();

// フォームをEnterキーで送信無効
$(function(){
  $("input"). keydown(function(e) {
    if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
      return false;
    } else {
      return true;
    }
  });
});

// プレースホルダー
$(function() {
  $('[placeholder]').ahPlaceholder({
    placeholderColor : 'silver',
    placeholderAttr : 'placeholder',
    likeApple : false
  });
});

// フォームバリデート
$(function(){
  var validation = $(".validateForm")
    .exValidation({
      firstValidate: true,
      rules: {
        company: "chkrequired",
        name: "chkrequired",
        phone: "chkrequired chktel",
        email: "chkrequired chkemail",
        email_confirm: "chkrequired chkretype-email",
        pref: "chkselect",
        contact_us: "chkradio",
        inquiry_type: "chkcheckbox",
        inquiry: "chkrequired",
        phone_corp: "chktel",
        email_corp: "chkemail",
        // 設定一覧
        // 〇〇〇〇: "chkrequired",         id〇〇〇〇を指定したinputを必須項目にします
        // 〇〇〇〇: "chktel",              id〇〇〇〇を指定したinputを電話番号型か判定します
        // 〇〇〇〇: "chkretype-□□□□",  id〇〇〇〇を指定したinputをid□□□□と同じ内容か判定します
        // 〇〇〇〇: "chkselect",           id〇〇〇〇を指定したselectを必須項目（select）にします
        // 〇〇〇〇: "chkradio",            id〇〇〇〇を指定したspanを必須項目（radio）にします
        // 〇〇〇〇: "chkcheckbox",         id〇〇〇〇を指定したspanを必須項目（checkbox）にします
      },
      customListener: "blur change", // エラー判定するタイミング
      // "blur"   カーソルが離れた時
      // "keyup"  1文字でも入力したらすぐ
      // "change" 変化があれば（selectやcheckbox,radioなど）
      // "focus"  カーソルを当てたらすぐ
      stepValidation: true, // エラーをリアルタイムで個別確認
      errTipCloseBtn: false, // エラーの閉じるボタン
      errTipPos: "left",  // 吹き出しが表示される位置（左右）
      // errHoverHide: true, // マウスオーバーで消える
      scrollToErr: false   // エラーまでスクロール
    });

  // 確認ボタンを押したとき
  $('.validateForm').on('submit', function () {
    // 入力エラーがある箇所まで画面をスクロール
    var targetTd = $('.err').parent('td').eq(0);
    var position = targetTd.offset().top;
    $('body,html').animate({scrollTop:position}, 500, 'swing');

    // 確認ボタン押したあと、再度関数を実行
    changeHalf();
    addFocus();
    emailChk();
  });

  // メールアドレス（確認用）でエラーメッセージが複数表示されてしまう対策
  function emailChk() {
    $('.chkretype-email').on('blur', function() {

      var msgErrRequired = $('#err_email_confirm .chkrequired');
      var msgErrType = $('#err_email_confirm .chkretype');

      if ($('.chkemail').val() !== '') {
        if ($(this).val() === '') {
          msgErrRequired.css('opacity', 0);
          msgErrType.text('入力内容が異なります。');
        }else if ($('.chkemail').val() !== $(this).val()) {
          msgErrRequired.css('opacity', 0);
          msgErrType.text('入力内容が異なります。');
        }
      }else {
        if ($('.chkemail').val() !== $(this).val()) {
          msgErrRequired.css('opacity', 0);
          msgErrType.text('入力内容が異なります。');
        }else {
          msgErrRequired.css('opacity', 1);
          msgErrType.text('必須項目です。ご入力ください。');
        }
      }
    });
  }

  emailChk();

});