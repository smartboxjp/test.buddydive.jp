<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>


<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
?>
<article>
		<?
      $sum_status_0 = 0;
      $sum_status_1 = 0;
      $sum_status_2 = 0;
      $sum_status_99 = 0;
      $sum_status_100 = 0;
      $sum_reserve_all_price = 0;
      
      //予約データ
      $sql_reserve = "SELECT member_id, dive_center_id, dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
      $sql_reserve .= " ,etc_price, reserve_tax, reserve_all_price ";
      $sql_reserve .= " FROM dive_reserve where yyyymmdd='".date("Y-m-d")."' ";
      $db_result_reserve = $common_dao->db_query($sql_reserve);
      if($db_result_reserve)
      {
        for($db_loop_reserve=0 ; $db_loop_reserve < count($db_result_reserve) ; $db_loop_reserve++)
        {
          //$arr_status[$db_result_reserve[$db_loop_reserve]["dive_center_id"]][$db_result_reserve[$db_loop_reserve]["status"]] = $db_result_reserve[$db_loop_reserve]["dive_reserve_id"];
					$arr_status[$db_result_reserve[$db_loop_reserve]["dive_center_id"]][$db_result_reserve[$db_loop_reserve]["status"]]+=1;
          $arr_status_reserve_all_price[$db_result_reserve[$db_loop_reserve]["dive_center_id"]] += $db_result_reserve[$db_loop_reserve]["reserve_all_price"];
        }
      }
    
    ?>
<section class="sectionBox">
<p class="tit">本日の予約状況</p>
<div class="box">
  <table class="listTable">
    <tr>
      <th>ポイント</th>
      <th>日付</th>
      <th>時間</th>
      <th>開催状況</th>
      <th>予約中</th>
      <th>受付済み</th>
      <th>精算済み</th>
      <th>中止</th>
      <th>不参加</th>
      <th>売上</th>
    </tr>
<?
	$arr_flag_open_wave_height[1] = "開催";
	$arr_flag_open_wave_height[2] = "条件付き";
	$arr_flag_open_wave_height[99] = "開催中止";
	
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw", "commission"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_id, ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center order by view_level ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_name[$dive_center_id] = $dive_center_name;
			$arr_dive_center_point[$dive_center_id] = $dive_center_point;
			$arr_dive_center_commission[$dive_center_id] = $commission;

			//リスト表示
			$sql_wave = "SELECT wave_height, flag_open, regi_date FROM dive_center_wave_height " ;
			$sql_wave .= " where left(regi_date, 10)='".date("Y-m-d")."' " ;
			$sql_wave .= " and dive_center_id='".$dive_center_id."' " ;
			$sql_wave .= " order by dive_center_wave_height_id desc";
			$sql_wave .= " limit 0, 1";
			$db_result_wave = $common_dao->db_query($sql_wave);

?>
    <tr>
      <td><? echo $dive_center_name;?></td>
      <td <? if(!$db_result_wave){ echo " class=\"error\"";}?>><? if($db_result_wave){ echo date("m/d", strtotime($db_result_wave[0]["regi_date"]));} else { echo "-";}?></td>
      <td <? if(!$db_result_wave){ echo " class=\"error\"";}?>><? if($db_result_wave){ echo date("H:i", strtotime($db_result_wave[0]["regi_date"]));} else { echo "-";}?></td>
      <td <? if(!$db_result_wave){ echo " class=\"error\"";}?>><? if($db_result_wave){ echo $arr_flag_open_wave_height[$db_result_wave[0]["flag_open"]];} else { echo "-";}?></td>
      <td <? if($arr_status[$dive_center_id]["0"]!=""){ echo " class=\"error\"";}?>><? if($arr_status[$dive_center_id]["0"]!=""){ echo $arr_status[$dive_center_id]["0"]."人"; $sum_status_0+=$arr_status[$dive_center_id]["0"];}?></td>
      <td align="right"><? if($arr_status[$dive_center_id]["1"]!=""){ echo $arr_status[$dive_center_id]["1"]."人"; $sum_status_1+=$arr_status[$dive_center_id]["1"];}?></td>
      <td align="right"><? if($arr_status[$dive_center_id]["2"]!=""){ echo $arr_status[$dive_center_id]["2"]."人"; $sum_status_2+=$arr_status[$dive_center_id]["2"];}?></td>
      <td align="right"><? if($arr_status[$dive_center_id]["99"]!=""){ echo $arr_status[$dive_center_id]["99"]."人"; $sum_status_99+=$arr_status[$dive_center_id]["99"];}?></td>
      <td align="right"><? if($arr_status[$dive_center_id]["100"]!=""){ echo $arr_status[$dive_center_id]["100"]."人"; $sum_status_100+=$arr_status[$dive_center_id]["100"];}?></td>
      <td align="right"><? if($arr_status_reserve_all_price[$dive_center_id]!=""){echo "¥".number_format($arr_status_reserve_all_price[$dive_center_id]); $sum_reserve_all_price+=$arr_status_reserve_all_price[$dive_center_id];}?></td>
    </tr>
<?
			
		}
	}
?>
    <tr class="sum">
      <td colspan="4" align="center">合計</td>
      <td align="right" class="error"><? echo number_format($sum_status_0);?>人</td>
      <td align="right"><? echo number_format($sum_status_1);?>人</td>
      <td align="right"><? echo number_format($sum_status_2);?>人</td>
      <td align="right"><? echo number_format($sum_status_99);?>人</td>
      <td align="right"><? echo number_format($sum_status_100);?>人</td>
      <td align="right">¥<? echo number_format($sum_reserve_all_price);?></td>
    </tr>
  </table>
</div>
</section>


<?
	$pattern_a = 0;
	$pattern_b = 0;
	$pattern_c = 0;
	$pattern_d = 0;
	
	$sql_member = "SELECT member_id ";
	$sql_member .= " FROM member where flag_hontouroku='1' ";
	$db_result_member = $common_dao->db_query($sql_member);
	if($db_result_member)
	{
		for($db_loop_member=0 ; $db_loop_member < count($db_result_member) ; $db_loop_member++)
		{
			//メディカル
			$medical_yes_no = "";
			$sql_member_medical = "SELECT yes_no ";
			$sql_member_medical .= " FROM member_medical where member_id='".$db_result_member[$db_loop_member]["member_id"]."' and medical_id=1 ";
			$db_result_member_medical = $common_dao->db_query($sql_member_medical);
			if($db_result_member_medical)
			{
				$medical_yes_no = $db_result_member_medical[0]["yes_no"];
			}
			
			//スキル
			$skill_yes_no = "";
			$sql_member_skill = "SELECT yes_no ";
			$sql_member_skill .= " FROM member_skill where member_id='".$db_result_member[$db_loop_member]["member_id"]."' and skill_id=1";
			$db_result_member_skill = $common_dao->db_query($sql_member_skill);
			if($db_result_member_skill)
			{
				$skill_yes_no = $db_result_member_skill[0]["yes_no"];
			}
			if($medical_yes_no=="1" && $skill_yes_no=="1")
			{
				$pattern_a++;
			}
			elseif($medical_yes_no=="2" && $skill_yes_no=="1")
			{
				$pattern_b++;
			}
			elseif($medical_yes_no=="1" && $skill_yes_no=="2")
			{
				$pattern_c++;
			}
			elseif($medical_yes_no=="2" && $skill_yes_no=="2")
			{
				$pattern_d++;
			}
		}
	}
?>
<section class="sectionBox">
  <p class="tit">ダイバー属性</p>
  <div class="box">
  <table class="listTable">
    <tr>
      <th>スキルできる・病歴該当なし</th>
      <th>スキルできる・要診断書</th>
      <th>スキルできない・病歴該当なし</th>
      <th>スキルできない・要診断書</th>
      <th>合計</th>
    </tr>
    <tr>
      <td align="right"><? echo number_format($pattern_a);?>人</td>
      <td align="right"><? echo number_format($pattern_b);?>人</td>
      <td align="right"><? echo number_format($pattern_c);?>人</td>
      <td align="right"><? echo number_format($pattern_d);?>人</td>
      <td align="right"><? echo number_format($pattern_a+$pattern_b+$pattern_c+$pattern_d);?>人</td>
    </tr>
  </table>
  </div>
</section>


<?
	/*予約情報 start */
	$sql = "SELECT yyyymmdd, dive_center_id, dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
	$sql .= " ,etc_price, used_point, reserve_tax, reserve_all_price ";
	$sql .= " FROM dive_reserve where left(yyyymmdd, 7)='".date("Y-m")."' ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			//日別
			$dd = substr($db_result[$db_loop]["yyyymmdd"], 8, 2);
			$arr_status[$dd][$db_result[$db_loop]["status"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			
			$arr_used_point[$dd] += $db_result[$db_loop]["used_point"];
			
			if($db_result[$db_loop]["status"]<2)
			{
				$arr_mikomi[$dd] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]==2)
			{
				$arr_uriage[$dd] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]<90)
			{
				$arr_total[$dd] += $db_result[$db_loop]["reserve_all_price"]*(0.01*$arr_dive_center_commission[$db_result[$db_loop]["dive_center_id"]]);
			}
			
			//センター別
			$dive_center_id = $db_result[$db_loop]["dive_center_id"];
			$arr_dive_center_status[$dive_center_id][$db_result[$db_loop]["status"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			
			$arr_dive_center_used_point[$dive_center_id] += $db_result[$db_loop]["used_point"];
			
			if($db_result[$db_loop]["status"]<2)
			{
				$arr_dive_center_mikomi[$dive_center_id] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]==2)
			{
				$arr_dive_center_uriage[$dive_center_id] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]<90)
			{
				$arr_dive_center_total[$dive_center_id] += $db_result[$db_loop]["reserve_all_price"]*(0.01*$arr_dive_center_commission[$db_result[$db_loop]["dive_center_id"]]);
			}
		}
	}
	/*予約情報 end */
	
	$sum_status_0 = 0;
	$sum_status_1 = 0;
	$sum_status_2 = 0;
	$sum_status_99 = 0;
	$sum_status_100 = 0;
	$sum_used_point = 0;
	$sum_mikomi = 0;
	$sum_uriage = 0;
	$sum_total = 0;
?>

<section class="sectionBox">
<p class="tit">今月の予約（日付別）</p>
<div class="box">
<table class="listTable">
  <tr>
    <th width="25%">日付</th>
    <th>予約中</th>
    <th>受付済み</th>
    <th>精算済み</th>
    <th>中止</th>
    <th>不参加</th>
    <th>ポイント</th>
    <th>売上見込み</th>
    <th>売上</th>
    <th>利用料</th>
  </tr>
  <?
	$s_yyyymm = date("Ym");
	$last_day = date('t', strtotime(substr($s_yyyymm, 0, 4)."-".substr($s_yyyymm, 5, 2)."-01"));
	
	for($loop_day = 1 ; $loop_day <= $last_day ; $loop_day++)
	{
		$dd = substr(($loop_day+100), 1, 2);
  ?>
  <tr>
    <th class="cellLink"><a href="/master/reserve/day.php?s_yyyymmdd=<? echo $s_yyyymm.substr(($loop_day+100), 1, 2);?>"><? echo intval(substr($s_yyyymm, 4, 2));?>月<? echo $loop_day;?>日（<? echo $common_connect->Fn_date_day($s_yyyymm.substr(($loop_day+100), 1, 2));?>）</a></th>
    <td align="right"><? if(count($arr_status[$dd]["0"])!=0) { echo number_format(count($arr_status[$dd]["0"]))."人"; $sum_status_0 += count($arr_status[$dd]["0"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["1"])!=0) { echo number_format(count($arr_status[$dd]["1"]))."人"; $sum_status_1 += count($arr_status[$dd]["1"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["2"])!=0) { echo number_format(count($arr_status[$dd]["2"]))."人"; $sum_status_2 += count($arr_status[$dd]["2"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["99"])!=0) { echo number_format(count($arr_status[$dd]["99"]))."人"; $sum_status_99 += count($arr_status[$dd]["99"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["100"])!=0) { echo number_format(count($arr_status[$dd]["100"]))."人"; $sum_status_100 += count($arr_status[$dd]["100"]);} ?></td>
    <td align="right"><? if($arr_used_point[$dd]!=0) {echo number_format($arr_used_point[$dd])."pt"; $sum_used_point += $arr_used_point[$dd];}?></td>
    <td align="right"><? if($arr_mikomi[$dd]!=0) {echo "¥".number_format($arr_mikomi[$dd]); $sum_mikomi += $arr_mikomi[$dd];}?></td>
    <td align="right"><? if($arr_uriage[$dd]!=0) {echo "¥".number_format($arr_uriage[$dd]); $sum_uriage += $arr_uriage[$dd];}?></td>
    <td align="right"><? if($arr_total[$dd]!=0) {echo "¥".number_format($arr_total[$dd]); $sum_total += $arr_total[$dd];}?></td>
  </tr>
  <?
	}
	?>
  <tr class="sum">
    <td align="center">合計</td>
    <td align="right"><? echo number_format($sum_status_0);?>人</td>
    <td align="right"><? echo number_format($sum_status_1);?>人</td>
    <td align="right"><? echo number_format($sum_status_2);?>人</td>
    <td align="right"><? echo number_format($sum_status_99);?>人</td>
    <td align="right"><? echo number_format($sum_status_100);?>人</td>
    <td align="right"><? echo number_format($sum_used_point);?>pt</td>
    <td align="right">¥<? echo number_format($sum_mikomi);?></td>
    <td align="right">¥<? echo number_format($sum_uriage);?></td>
    <td align="right">¥<? echo number_format($sum_total);?></td>
  </tr>
</table>
</section>


<?
	$sum_status_0 = 0;
	$sum_status_1 = 0;
	$sum_status_2 = 0;
	$sum_status_99 = 0;
	$sum_status_100 = 0;
	$sum_used_point = 0;
	$sum_mikomi = 0;
	$sum_uriage = 0;
	$sum_total = 0;
?>
<section class="sectionBox">
<p class="tit">今月の予約（ダイビングセンター別）</p>
<div class="box">
<table class="listTable">
  <tr>
    <th width="25%">ダイビングセンター</th>
    <th>予約中</th>
    <th>受付済み</th>
    <th>精算済み</th>
    <th>中止</th>
    <th>不参加</th>
    <th>ポイント</th>
    <th>売上見込み</th>
    <th>売上</th>
    <th>利用料</th>
  </tr>
  <?
	foreach($arr_dive_center_name as $key=>$value)
	{
  ?>
  <tr>
    <th class="cellLink"><a href="/master/sales/month.php?s_dive_center_id=<? echo $key;?>"><? echo $value;?></a></th>
    <td align="right"><? if(count($arr_dive_center_status[$key]["0"])!=0) { echo number_format(count($arr_dive_center_status[$key]["0"]))."人"; $sum_status_0 += count($arr_dive_center_status[$key]["0"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["1"])!=0) { echo number_format(count($arr_dive_center_status[$key]["1"]))."人"; $sum_status_1 += count($arr_dive_center_status[$key]["1"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["2"])!=0) { echo number_format(count($arr_dive_center_status[$key]["2"]))."人"; $sum_status_2 += count($arr_dive_center_status[$key]["2"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["99"])!=0) { echo number_format(count($arr_dive_center_status[$key]["99"]))."人"; $sum_status_99 += count($arr_dive_center_status[$key]["99"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["100"])!=0) { echo number_format(count($arr_dive_center_status[$key]["100"]))."人"; $sum_status_100 += count($arr_dive_center_status[$key]["100"]);} ?></td>
    <td align="right"><? if($arr_dive_center_used_point[$key]!=0) {echo number_format($arr_dive_center_used_point[$key])."pt"; $sum_used_point += $arr_dive_center_used_point[$key];}?></td>
    <td align="right"><? if($arr_dive_center_mikomi[$key]!=0) {echo "¥".number_format($arr_dive_center_mikomi[$key]); $sum_mikomi += $arr_dive_center_mikomi[$key];}?></td>
    <td align="right"><? if($arr_dive_center_uriage[$key]!=0) {echo "¥".number_format($arr_dive_center_uriage[$key]); $sum_uriage += $arr_dive_center_uriage[$key];}?></td>
    <td align="right"><? if($arr_dive_center_total[$key]!=0) {echo "¥".number_format($arr_dive_center_total[$key]); $sum_total += $arr_dive_center_total[$key];}?></td>
  </tr>
  <?
	}
	
  ?>
<tr>
  <tr class="sum">
    <td align="center">合計</td>
    <td align="right"><? echo number_format($sum_status_0);?>人</td>
    <td align="right"><? echo number_format($sum_status_1);?>人</td>
    <td align="right"><? echo number_format($sum_status_2);?>人</td>
    <td align="right"><? echo number_format($sum_status_99);?>人</td>
    <td align="right"><? echo number_format($sum_status_100);?>人</td>
    <td align="right"><? echo number_format($sum_used_point);?>pt</td>
    <td align="right">¥<? echo number_format($sum_mikomi);?></td>
    <td align="right">¥<? echo number_format($sum_uriage);?></td>
    <td align="right">¥<? echo number_format($sum_total);?></td>
  </tr>
</table>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>