<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>タンク登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($dive_center_id == "")
	{
		$common_connect -> Fn_javascript_back("ダイブセンターIDがありません。");
	}
	
	if($dive_center_tank_title == "")
	{
		$common_connect -> Fn_javascript_back("タイトルを入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//array
	$arr_db_field = array("dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open"));

	//基本情報
	if($dive_center_tank_id=="")
	{
		$db_insert = "insert into dive_center_tank ( ";
		$db_insert .= " dive_center_tank_id, dive_center_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', '".$dive_center_id."', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update dive_center_tank set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where dive_center_tank_id='".$dive_center_tank_id."' and dive_center_id='".$dive_center_id."' ";
	}

	$db_result = $common_dao->db_update($db_insert);

	if ($dive_center_tank_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$dive_center_tank_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
		
		//デフォルト在庫登録
		$max_month = 3;

		$yyyymmdd = date("Y-m-01");
		for ($i=0 ; $i < $max_month ; $i++)
		{
			$sql = "SELECT '".$yyyymmdd."' + INTERVAL $i MONTH as db_yyyymmdd";
			$db_result = $common_dao->db_query($sql);
			
			$yyyy = date("Y", strtotime($db_result[0]["db_yyyymmdd"]));
			$mm = date("m", strtotime($db_result[0]["db_yyyymmdd"]));
			
			//最後の日
			$last_day = date('t', strtotime($yyyy."-".$mm."-01"));
			
			//DBをチェックして無ければ生成
			$dbup = "insert into stock_tank (yyyymm, dive_center_tank_id, dive_center_id ";
			for($loop=1 ; $loop<=$last_day ; $loop++)
			{
				$dbup .= " ,s_".substr($loop+100, 1);
			}
			$dbup .= ") values ('".$yyyy.$mm."', '".$dive_center_tank_id."', '".$dive_center_id."' ";
			for($loop=1 ; $loop<=$last_day ; $loop++)
			{
				$dbup .= ", '".$dive_center_tank_stock."' ";
			}
			$dbup .= ")";

			$db_result = $common_dao->db_update($dbup);
		}
	}
	
	
	$common_connect-> Fn_javascript_move("タンク登録・修正しました", "dive_center_tank_list.php?dive_center_id=".$dive_center_id."&dive_center_tank_id=".$dive_center_tank_id);
?>
</body>
</html>