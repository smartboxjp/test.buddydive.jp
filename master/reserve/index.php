<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>


<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($s_yyyymm=="")
	{
		$s_yyyymm = date("Ym");
	}
?>
<article>

  <div class="listSelect">
    <form action="<? echo $_SERVER['PHP_SELF'];?>">
      <? $var = "s_yyyymm"; ?>
    <select name="<? echo $var;?>" id="<? echo $var;?>">
    <?
    $from_yyyymmdd = date('Y-m-d', strtotime("2015-11-01"));
    $to_yyyymmdd = date('Y-m-d', strtotime("+6 months", strtotime(date("Ymd"))));
    
    for($loop=$from_yyyymmdd ; date('Y-m-d', strtotime($loop)) <= $to_yyyymmdd ; $loop=$loop_yyyymmdd)
    {
      $sql_yyyymm="SELECT '$loop' + INTERVAL 1 MONTH as yyyymm"; 
      $db_result_yyyymm = $common_dao->db_query($sql_yyyymm);
      $loop_yyyymmdd = date('Y-m-d', strtotime($db_result_yyyymm[0]["yyyymm"]));
      $view_loop_yyyymmdd = date('Ym', strtotime($db_result_yyyymm[0]["yyyymm"]));
    ?>
      <option value="<?=$view_loop_yyyymmdd;?>" <? if($view_loop_yyyymmdd==$$var){ echo " selected ";} ?>><?=substr($view_loop_yyyymmdd, 0, 4)."年".substr($view_loop_yyyymmdd, 4, 2)."月";?></option>
    <?
      }
    ?>
    </select>
      <input type="submit" value="表示">
    </form>
  </div>

<?
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw", "commission"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_id, ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_commission[$dive_center_id] = $commission;
		}
	}

	/*予約情報 start */
	$sql = "SELECT yyyymmdd, dive_center_id, dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
	$sql .= " ,etc_price, used_point, reserve_tax, reserve_all_price ";
	$sql .= " FROM dive_reserve where left(yyyymmdd, 7)='".date("Y-m", strtotime($s_yyyymm."01"))."' ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			//日別
			$dd = substr($db_result[$db_loop]["yyyymmdd"], 8, 2);
			$arr_status[$dd][$db_result[$db_loop]["status"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			
			
			if($db_result[$db_loop]["status"]<2)
			{
				$arr_mikomi[$dd] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]==2)
			{
				$arr_uriage[$dd] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]<=2)
			{
				$arr_used_point[$dd] += $db_result[$db_loop]["used_point"];
			}
			if($db_result[$db_loop]["status"]<90)
			{
				$arr_total[$dd] += $db_result[$db_loop]["reserve_all_price"]*(0.01*$arr_dive_center_commission[$db_result[$db_loop]["dive_center_id"]]);
			}
			
		}
	}
	/*予約情報 end */
	
	$sum_status_0 = 0;
	$sum_status_1 = 0;
	$sum_status_2 = 0;
	$sum_status_99 = 0;
	$sum_status_100 = 0;
	$sum_used_point = 0;
	$sum_mikomi = 0;
	$sum_uriage = 0;
	$sum_total = 0;
?>

<section class="sectionBox">
<p class="tit"><? echo date("Y年m月", strtotime($s_yyyymm."01"));?>の予約（日付別）</p>
<div class="box">
<table class="listTable">
  <tr>
    <th width="25%">日付</th>
    <th>予約中</th>
    <th>受付済み</th>
    <th>精算済み</th>
    <th>中止</th>
    <th>不参加</th>
    <th>ポイント</th>
    <th>売上見込み</th>
    <th>売上</th>
    <th>利用料</th>
  </tr>
  <?
	$last_day = date('t', strtotime(substr($s_yyyymm, 0, 4)."-".substr($s_yyyymm, 4, 2)."-01"));
	
	for($loop_day = 1 ; $loop_day <= $last_day ; $loop_day++)
	{
		$dd = substr(($loop_day+100), 1, 2);
  ?>
  <tr>
    <th class="cellLink"><a href="/master/reserve/day.php?s_yyyymmdd=<? echo $s_yyyymm.substr(($loop_day+100), 1, 2);?>"><? echo intval(substr($s_yyyymm, 4, 2));?>月<? echo $loop_day;?>日（<? echo $common_connect->Fn_date_day($s_yyyymm.substr(($loop_day+100), 1, 2));?>）</a></th>
    <td align="right"><? if(count($arr_status[$dd]["0"])!=0) { echo number_format(count($arr_status[$dd]["0"]))."人"; $sum_status_0 += count($arr_status[$dd]["0"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["1"])!=0) { echo number_format(count($arr_status[$dd]["1"]))."人"; $sum_status_1 += count($arr_status[$dd]["1"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["2"])!=0) { echo number_format(count($arr_status[$dd]["2"]))."人"; $sum_status_2 += count($arr_status[$dd]["2"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["99"])!=0) { echo number_format(count($arr_status[$dd]["99"]))."人"; $sum_status_99 += count($arr_status[$dd]["99"]);} ?></td>
    <td align="right"><? if(count($arr_status[$dd]["100"])!=0) { echo number_format(count($arr_status[$dd]["100"]))."人"; $sum_status_100 += count($arr_status[$dd]["100"]);} ?></td>
    <td align="right"><? if($arr_used_point[$dd]!=0) {echo number_format($arr_used_point[$dd])."pt"; $sum_used_point += $arr_used_point[$dd];}?></td>
    <td align="right"><? if($arr_mikomi[$dd]!=0) {echo "¥".number_format($arr_mikomi[$dd]); $sum_mikomi += $arr_mikomi[$dd];}?></td>
    <td align="right"><? if($arr_uriage[$dd]!=0) {echo "¥".number_format($arr_uriage[$dd]); $sum_uriage += $arr_uriage[$dd];}?></td>
    <td align="right"><? if($arr_total[$dd]!=0) {echo "¥".number_format($arr_total[$dd]); $sum_total += $arr_total[$dd];}?></td>
  </tr>
  <?
	}
	?>
  <tr class="sum">
    <td align="center">合計</td>
    <td align="right"><? echo number_format($sum_status_0);?>人</td>
    <td align="right"><? echo number_format($sum_status_1);?>人</td>
    <td align="right"><? echo number_format($sum_status_2);?>人</td>
    <td align="right"><? echo number_format($sum_status_99);?>人</td>
    <td align="right"><? echo number_format($sum_status_100);?>人</td>
    <td align="right"><? echo number_format($sum_used_point);?>pt</td>
    <td align="right">¥<? echo number_format($sum_mikomi);?></td>
    <td align="right">¥<? echo number_format($sum_uriage);?></td>
    <td align="right">¥<? echo number_format($sum_total);?></td>
  </tr>
</table>
</section>




</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>