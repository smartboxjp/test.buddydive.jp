<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>メディカル登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	$cate_medical_id = $_GET['cate_medical_id'];

	//DBへ保存
	if ($cate_medical_id != "")
	{
		//商品情報削除
		$dbup = "delete from cate_medical where cate_medical_id = '$cate_medical_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("メディカルを削除しました。", "cate_medical_list.php#search_list");

?>
</body>
</html>