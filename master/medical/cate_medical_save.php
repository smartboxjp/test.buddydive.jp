<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>メディカル登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($cate_medical_title == "")
	{
		$common_connect -> Fn_javascript_back("タイトルを入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//array
	$arr_db_field = array("cate_medical_title");
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open"));

	//基本情報
	if($cate_medical_id=="")
	{
		$db_insert = "insert into cate_medical ( ";
		$db_insert .= " cate_medical_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update cate_medical set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where cate_medical_id='".$cate_medical_id."'";
	}

	$db_result = $common_dao->db_update($db_insert);
				
	if ($cate_medical_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$cate_medical_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	$db_result = $common_dao->db_update($dbup);
	
	$common_connect-> Fn_javascript_move("メディカル登録・修正しました", "cate_medical_list.php?cate_medical_id=".$cate_medical_id);
?>
</body>
</html>