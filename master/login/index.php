<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>

<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input_email("admin_id");
			err_check_count += check_input_pw("admin_pw");
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<div style='color:#F00;'>正しく入力してください。</div>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<div style='color:#F00;'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00;'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<div style='color:#F00;'>６文字以上入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						
	});
	
//-->
</script>

<article>


<section>
  <div class="sectionBox">
    <div class="box">
    <form action="/master/login/login_check.php" name="form_regist" id="form_regist" method="post">
      <table class="listTable">
        <tr>
        <th width="200">ID</th>
          <td>
						<? $var = "admin_id";?>
            <input name="<?=$var;?>" id="<?=$var;?>" type="text" placeholder="ID" class="w200">
            <label id="err_<?=$var;?>"></label>
          </td>
        </tr>
        <tr>
        <th>パスワード</th>
          <td>
						<? $var = "admin_pw";?>
            <input name="<?=$var;?>" id="<?=$var;?>" type="password" placeholder="パスワード" class="w200">
            <label id="err_<?=$var;?>"></label>
          </td>
        </tr>
      </table>
      <div class="btnBox">
        <p class="btn">
					<? $var = "form_confirm";?>
          <input name="<?=$var;?>" id="<?=$var;?>"  type="submit" value="管理者ログイン" />
        </p>
      </div>
    </form>
    </div>
  </div>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>