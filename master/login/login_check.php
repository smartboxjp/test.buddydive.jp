<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>
<?

	if ($admin_id == "" or $admin_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	else
	{
		$sql = "select admin_name, admin_id, admin_level from admin where admin_id ='".$admin_id."' and admin_pw='".$admin_pw."' ";


		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$db_admin_id = $db_result[0]["admin_id"];
			$db_admin_name = $db_result[0]["admin_name"];
			$db_admin_level = $db_result[0]["admin_level"];
		}
		else
		{
			$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
		}
		
		if ($db_admin_id == $admin_id)
		{
			//管理者の場合
			session_start();
			$_SESSION['admin_id']=$db_admin_id;
			$_SESSION['admin_name']=$db_admin_name;
			$_SESSION['admin_level']=$db_admin_level;
			$common_connect->Fn_redirect(global_ssl."/master/dashboard/");

		}
	}
?>
</body>
</html>