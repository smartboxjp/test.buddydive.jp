<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();

?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>


<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	
	/* スタイル start */
	$arr_db_field = array("dive_center_style_id", "dive_center_id", "dive_center_style_title", "dive_center_style_stock", "dive_center_style_price");
		
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_style where dive_center_id='".$dive_center_id."'";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_style_title[$dive_center_style_id] = $dive_center_style_title;
			$arr_dive_center_style_price[$dive_center_style_id] = $dive_center_style_price;
		}
	}
	/* スタイル end */
	
	
	/* ボート start */
	$arr_db_field_boat = array("dive_center_boat_id", "dive_center_boat_title", "dive_center_boat_stock", "dive_center_boat_price");
		
	$sql = "SELECT dive_center_boat_id, ";
	foreach($arr_db_field_boat as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_boat where dive_center_id='".$dive_center_id."'";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field_boat as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_boat_title[$dive_center_boat_id] = $dive_center_boat_title;
			$arr_dive_center_boat_price[$dive_center_boat_id] = $dive_center_boat_price;
		}
	}
	/* ボート end */
	
	/* タンク start */
	$arr_db_field_tank = array("dive_center_tank_id", "dive_center_tank_title", "dive_center_tank_stock", "dive_center_tank_price");
		
	$sql = "SELECT dive_center_tank_id, ";
	foreach($arr_db_field_tank as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center_tank where dive_center_id='".$dive_center_id."' ";
	$sql .= " and flag_open=1 ";
	$sql .= " order by view_level, up_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field_tank as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_tank_title[$dive_center_tank_id] = $dive_center_tank_title;
			$arr_dive_center_tank_price[$dive_center_tank_id] = $dive_center_tank_price;
		}
	}
	/* タンク end */
	

?>
<article>


<?
	$sum_etc_price = 0;
	$sum_used_point = 0;
	$sum_reserve_tax = 0;
	$sum_reserve_all_price = 0;
	
	//予約データ
	$sql = "SELECT dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
	$sql .= " ,etc_price, used_point, reserve_tax, reserve_all_price ";
	$sql .= " FROM dive_reserve where dive_center_id='".$dive_center_id."' and yyyymmdd='".date("Y-m-d", strtotime($s_yyyymmdd))."' ";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_status[$db_result[$db_loop]["status"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_style_id[$db_result[$db_loop]["reserve_style_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_boat_id[$db_result[$db_loop]["reserve_boat_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_tank_id[$db_result[$db_loop]["reserve_1_tank_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_tank_id[$db_result[$db_loop]["reserve_2_tank_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			$arr_reserve_tank_id[$db_result[$db_loop]["reserve_3_tank_id"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			
			if($db_result[$db_loop]["reserve_1_tank_id"]!="0")
			{
				$arr_reserve_tank_count[$db_result[$db_loop]["reserve_1_tank_id"]] ++;
			}
			if($db_result[$db_loop]["reserve_2_tank_id"]!="0")
			{
				$arr_reserve_tank_count[$db_result[$db_loop]["reserve_2_tank_id"]] ++;
			}
			if($db_result[$db_loop]["reserve_3_tank_id"]!="0")
			{
				$arr_reserve_tank_count[$db_result[$db_loop]["reserve_3_tank_id"]] ++;
			}
			
			$sum_etc_price += $db_result[$db_loop]["etc_price"];
			$sum_used_point += $db_result[$db_loop]["used_point"];
			$sum_reserve_tax += $db_result[$db_loop]["reserve_tax"];
			$sum_reserve_all_price += $db_result[$db_loop]["reserve_all_price"];
		}
	}

?>

<section class="sectionBox">
  <p class="tit"><? echo date('Y/m/d', strtotime($s_yyyymmdd))."（".$common_connect->Fn_date_day($s_yyyymmdd)."）";?>の売上</p>
  <div class="box">
    <table class="listTable">
      <tr>
        <th colspan="2">項目</th>
        <th width="15%">単価</th>
        <th width="15%">数量</th>
        <th width="15%">金額</th>
      </tr>
<?
	//スタイル
	$db_result_style = $common_center->Fn_dive_center_style ($common_dao, $dive_center_id) ;
	if($db_result_style)
	{
		for($db_loop_style=0 ; $db_loop_style < count($db_result_style) ; $db_loop_style++)
		{
			$dive_center_style_id = $db_result_style[$db_loop_style]["dive_center_style_id"];
			$arr_dive_center_style_id[$db_loop_style] = $dive_center_style_id;
			$arr_dive_center_style_title[$dive_center_style_id] = $db_result_style[$db_loop_style]["dive_center_style_title"];
			$arr_dive_center_style_stock[$dive_center_style_id] = $db_result_style[$db_loop_style]["dive_center_style_stock"];
			$arr_dive_center_style_price[$dive_center_style_id] = $db_result_style[$db_loop_style]["dive_center_style_price"];
		}
	}
?>
      <tr>
        <td rowspan="<? echo count($arr_dive_center_style_title);?>">スタイル</td>
        <? $loop = 0;?>
        <td><? echo $arr_dive_center_style_title[$arr_dive_center_style_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($arr_dive_center_style_price[$arr_dive_center_style_id[$loop]]);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($arr_dive_center_style_price[$arr_dive_center_style_id[$loop]]*count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?></td>
      </tr>
      <?
      for($loop = 1 ; $loop<count($arr_dive_center_style_id) ; $loop++)
			{
				$dive_center_style_price = $arr_dive_center_style_price[$arr_dive_center_style_id[$loop]];
			?>
      <tr>
        <td><? echo $arr_dive_center_style_title[$arr_dive_center_style_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_style_price);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($dive_center_style_price*count($arr_reserve_style_id[$arr_dive_center_style_id[$loop]]));?></td>
      </tr>
      <?
			}
			?>
      
      
      
<?
	//エントリー
	$db_result_boat = $common_center->Fn_dive_center_boat ($common_dao, $dive_center_id) ;
	if($db_result_boat)
	{
		for($db_loop_boat=0 ; $db_loop_boat < count($db_result_boat) ; $db_loop_boat++)
		{
			$dive_center_boat_id = $db_result_boat[$db_loop_boat]["dive_center_boat_id"];
			$arr_dive_center_boat_id[$db_loop_boat] = $dive_center_boat_id;
			$arr_dive_center_boat_title[$dive_center_boat_id] = $db_result_boat[$db_loop_boat]["dive_center_boat_title"];
			$arr_dive_center_boat_stock[$dive_center_boat_id] = $db_result_boat[$db_loop_boat]["dive_center_boat_stock"];
			$arr_dive_center_boat_price[$dive_center_boat_id] = $db_result_boat[$db_loop_boat]["dive_center_boat_price"];
		}
	}
?>
      <tr>
        <td rowspan="<? echo count($arr_dive_center_boat_title);?>">エントリー</td>
        <? $loop = 0;?>
        <td><? echo $arr_dive_center_boat_title[$arr_dive_center_boat_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_boat_price);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($dive_center_boat_price*count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?></td>
      </tr>
      <?
      for($loop = 1 ; $loop<count($arr_dive_center_boat_id) ; $loop++)
			{
				$dive_center_boat_price = $arr_dive_center_boat_price[$arr_dive_center_boat_id[$loop]];
			?>
      <tr>
        <td><? echo $arr_dive_center_boat_title[$arr_dive_center_boat_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_boat_price);?></td>
        <td align="right"><? echo number_format(count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?>人</td>
        <td align="right">¥<? echo number_format($dive_center_boat_price*count($arr_reserve_boat_id[$arr_dive_center_boat_id[$loop]]));?></td>
      </tr>
      <?
			}
			?>
      
      
      

<?
	//タンク
	$db_result_tank = $common_center->Fn_dive_center_tank ($common_dao, $dive_center_id) ;
	if($db_result_tank)
	{
		for($db_loop_tank=0 ; $db_loop_tank < count($db_result_tank) ; $db_loop_tank++)
		{
			$dive_center_tank_id = $db_result_tank[$db_loop_tank]["dive_center_tank_id"];
			$arr_dive_center_tank_id[$db_loop_tank] = $dive_center_tank_id;
			$arr_dive_center_tank_title[$dive_center_tank_id] = $db_result_tank[$db_loop_tank]["dive_center_tank_title"];
			$arr_dive_center_tank_stock[$dive_center_tank_id] = $db_result_tank[$db_loop_tank]["dive_center_tank_stock"];
			$arr_dive_center_tank_price[$dive_center_tank_id] = $db_result_tank[$db_loop_tank]["dive_center_tank_price"];
		}
	}
?>
      <tr>
        <td rowspan="<? echo count($arr_dive_center_tank_title);?>">タンク</td>
        <? $loop = 0;?>
        <td><? echo $arr_dive_center_tank_title[$arr_dive_center_tank_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_tank_price);?></td>
        <td align="right"><? echo number_format(($arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]));?>本</td>
        <td align="right">¥<? echo number_format($dive_center_tank_price*$arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]);?></td>
      </tr>
      <?
      for($loop = 1 ; $loop<count($arr_dive_center_tank_id) ; $loop++)
			{
				$dive_center_tank_price = $arr_dive_center_tank_price[$arr_dive_center_tank_id[$loop]];
			?>
      <tr>
        <td><? echo $arr_dive_center_tank_title[$arr_dive_center_tank_id[$loop]];?></td>
        <td align="right">¥<? echo number_format($dive_center_tank_price);?></td>
        <td align="right"><? echo number_format($arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]);?>本</td>
        <td align="right">¥<? echo number_format($dive_center_tank_price*$arr_reserve_tank_count[$arr_dive_center_tank_id[$loop]]);?></td>
      </tr>
      <?
			}
			?>
      <tr class="sum">
        <td colspan="4" align="center">使用されたポイント</td>
        <td align="right"><? echo number_format($sum_used_point);?>pt</td>
      </tr>
      <tr class="sum">
        <td colspan="4" align="center">その他金額</td>
        <td align="right">¥<? echo number_format($sum_etc_price);?></td>
      </tr>
      <tr class="sum">
        <td colspan="4" align="center">小計</td>
        <td align="right">¥<? echo number_format($sum_reserve_all_price-$sum_reserve_tax);?></td>
      </tr>
      <tr>
        <td colspan="4" align="center">消費税</td>
        <td align="right">¥<? echo number_format($sum_reserve_tax);?></td>
      </tr>
      <tr>
        <td colspan="4" align="center">合計</td>
        <td align="right">¥<? echo number_format($sum_reserve_all_price);?></td>
      </tr>
    </table>
  </div>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>