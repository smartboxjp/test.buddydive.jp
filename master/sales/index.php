<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>


<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($s_yyyymm=="")
	{
		$s_yyyymm = date("Ym");
	}
?>
<article>

  <div class="listSelect">
    <form action="<? echo $_SERVER['PHP_SELF'];?>">
      <? $var = "s_yyyymm"; ?>
      <select name="<? echo $var;?>" id="<? echo $var;?>">
      <?
			$from_yyyymmdd = date('Y-m-d', strtotime("2015-11-01"));
			$to_yyyymmdd = date('Y-m-d', strtotime("+6 months", strtotime(date("Ymd"))));
			
			for($loop=$from_yyyymmdd ; date('Y-m-d', strtotime($loop)) <= $to_yyyymmdd ; $loop=$loop_yyyymmdd)
			{
				$sql_yyyymm="SELECT '$loop' + INTERVAL 1 MONTH as yyyymm"; 
				$db_result_yyyymm = $common_dao->db_query($sql_yyyymm);
				$loop_yyyymmdd = date('Y-m-d', strtotime($db_result_yyyymm[0]["yyyymm"]));
				$view_loop_yyyymmdd = date('Ym', strtotime($db_result_yyyymm[0]["yyyymm"]));
      ?>
        <option value="<?=$view_loop_yyyymmdd;?>" <? if($view_loop_yyyymmdd==$$var){ echo " selected ";} ?>><?=substr($view_loop_yyyymmdd, 0, 4)."年".substr($view_loop_yyyymmdd, 4, 2)."月";?></option>
      <?
        }
      ?>
      </select>
      <input type="submit" value="表示">
    </form>
    
  </div>




<?
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw", "commission"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
	$sql = "SELECT dive_center_id, ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center order by view_level ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			$arr_dive_center_name[$dive_center_id] = $dive_center_name;
			$arr_dive_center_point[$dive_center_id] = $dive_center_point;
			$arr_dive_center_commission[$dive_center_id] = $commission;
		}
	}
	
	
	/*予約情報 start */
	$sql = "SELECT yyyymmdd, dive_center_id, dive_reserve_id, reserve_style_id, reserve_boat_id, reserve_1_tank_id, reserve_2_tank_id, reserve_3_tank_id, status ";
	$sql .= " ,etc_price, used_point, reserve_tax, reserve_all_price ";
	$sql .= " FROM dive_reserve where left(yyyymmdd, 7)='".date("Y-m", strtotime($s_yyyymm."01"))."' ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			//センター別
			$dive_center_id = $db_result[$db_loop]["dive_center_id"];
			$arr_dive_center_status[$dive_center_id][$db_result[$db_loop]["status"]][$db_result[$db_loop]["dive_reserve_id"]] = $db_result[$db_loop]["dive_reserve_id"];
			
			
			if($db_result[$db_loop]["status"]<2)
			{
				$arr_dive_center_mikomi[$dive_center_id] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]==2)
			{
				$arr_dive_center_uriage[$dive_center_id] += $db_result[$db_loop]["reserve_all_price"];
			}
			if($db_result[$db_loop]["status"]<=2)
			{
				$arr_dive_center_used_point[$dive_center_id] += $db_result[$db_loop]["used_point"];
			}
			if($db_result[$db_loop]["status"]<90)
			{
				$arr_dive_center_total[$dive_center_id] += $db_result[$db_loop]["reserve_all_price"]*(0.01*$arr_dive_center_commission[$db_result[$db_loop]["dive_center_id"]]);
			}
		}
	}
	/*予約情報 end */
	

	$sum_status_0 = 0;
	$sum_status_1 = 0;
	$sum_status_2 = 0;
	$sum_status_99 = 0;
	$sum_status_100 = 0;
	$sum_used_point = 0;
	$sum_mikomi = 0;
	$sum_uriage = 0;
	$sum_total = 0;
?>

<div align="right"><a href="index_csv.php?<? echo $_SERVER['QUERY_STRING']; ?>">CSVダウンロード</a></div>
<section class="sectionBox">
<p class="tit"><? echo date("Y年m月", strtotime($s_yyyymm."01"));?>の予約（ダイビングセンター別）</p>
<div class="box">
<table class="listTable">
  <tr>
    <th width="25%">ダイビングセンター</th>
    <th>予約中</th>
    <th>受付済み</th>
    <th>精算済み</th>
    <th>中止</th>
    <th>不参加</th>
    <th>ポイント</th>
    <th>売上見込み</th>
    <th>売上</th>
    <th>利用料</th>
  </tr>
  <?
	foreach($arr_dive_center_name as $key=>$value)
	{
  ?>
  <tr>
    <th class="cellLink"><a href="month.php?s_dive_center_id=<? echo $key;?>&s_yyyymm=<? echo $s_yyyymm;?>"><? echo $value;?></a></th>
    <td align="right"><? if(count($arr_dive_center_status[$key]["0"])!=0) { echo number_format(count($arr_dive_center_status[$key]["0"]))."人"; $sum_status_0 += count($arr_dive_center_status[$key]["0"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["1"])!=0) { echo number_format(count($arr_dive_center_status[$key]["1"]))."人"; $sum_status_1 += count($arr_dive_center_status[$key]["1"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["2"])!=0) { echo number_format(count($arr_dive_center_status[$key]["2"]))."人"; $sum_status_2 += count($arr_dive_center_status[$key]["2"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["99"])!=0) { echo number_format(count($arr_dive_center_status[$key]["99"]))."人"; $sum_status_99 += count($arr_dive_center_status[$key]["99"]);} ?></td>
    <td align="right"><? if(count($arr_dive_center_status[$key]["100"])!=0) { echo number_format(count($arr_dive_center_status[$key]["100"]))."人"; $sum_status_100 += count($arr_dive_center_status[$key]["100"]);} ?></td>
    <td align="right"><? if($arr_dive_center_used_point[$key]!=0) {echo number_format($arr_dive_center_used_point[$key])."pt"; $sum_used_point += $arr_dive_center_used_point[$key];}?></td>
    <td align="right"><? if($arr_dive_center_mikomi[$key]!=0) {echo "¥".number_format($arr_dive_center_mikomi[$key]); $sum_mikomi += $arr_dive_center_mikomi[$key];}?></td>
    <td align="right"><? if($arr_dive_center_uriage[$key]!=0) {echo "¥".number_format($arr_dive_center_uriage[$key]); $sum_uriage += $arr_dive_center_uriage[$key];}?></td>
    <td align="right"><? if($arr_dive_center_total[$key]!=0) {echo "¥".number_format($arr_dive_center_total[$key]); $sum_total += $arr_dive_center_total[$key];}?></td>
  </tr>
  <?
	}
	
  ?>
<tr>
  <tr class="sum">
    <td align="center">合計</td>
    <td align="right"><? echo number_format($sum_status_0);?>人</td>
    <td align="right"><? echo number_format($sum_status_1);?>人</td>
    <td align="right"><? echo number_format($sum_status_2);?>人</td>
    <td align="right"><? echo number_format($sum_status_99);?>人</td>
    <td align="right"><? echo number_format($sum_status_100);?>人</td>
    <td align="right"><? echo number_format($sum_used_point);?>pt</td>
    <td align="right">¥<? echo number_format($sum_mikomi);?></td>
    <td align="right">¥<? echo number_format($sum_uriage);?></td>
    <td align="right">¥<? echo number_format($sum_total);?></td>
  </tr>
</table>
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>