<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ダイブセンター登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
	}
	
	if($dive_center_name == "")
	{
		$common_connect -> Fn_javascript_back("タイトルを入力してください。");
	}
	
	if($check_up=="")
	{
		$sql = "select dive_center_id from dive_center where dive_center_id ='$dive_center_id'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			if($dive_center_id == $db_result[0]["dive_center_id"])
			{
				$common_connect -> Fn_javascript_back("既に登録されているダイブセンターIDです。");
			}
		}
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//array
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "commission", "cate_area_id");
	$arr_db_field = array_merge($arr_db_field, array("comment_1", "comment_2", "comment_3", "comment_4", "comment_list", "google_map_1", "google_map_2"));
	$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw", "flag_boat", "flag_beach", "flag_guide"));
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open"));

	//基本情報
	if($check_up=="")
	{
		$db_insert = "insert into dive_center ( ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update dive_center set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where dive_center_id='".$dive_center_id."'";
	}

	$db_result = $common_dao->db_update($db_insert);
	
	if ($dive_center_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$dive_center_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}

	//Folder生成
	$save_dir = $global_path.global_dive_center_dir.$dive_center_id."/";
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $dive_center_id, $text_img_1, "");
	$dbup = "update dive_center set ";
	for($loop = 1 ; $loop<2 ; $loop++)
	{
		$dbup .= " img_".$loop."='".$fname_new_name[$loop]."',";
	}
	$dbup .= " up_date='$datetime' where dive_center_id='".$dive_center_id."'";
	
	$db_result = $common_dao->db_update($dbup);

	
	$common_connect-> Fn_javascript_move("ダイブセンター登録・修正しました", "dive_center_list.php?dive_center_id=".$dive_center_id);
?>
</body>
</html>