<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonCenter.php";
	$common_center = new CommonCenter();

  $meta_title = "ダイブセンター管理";
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>

<script language="javascript"> 
	function fnChangeSel(j) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './dive_center_del.php?dive_center_id='+j;
		} 
	}
	
	function fnImgDel(i, j, k) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './img_one_del.php?contents_id='+i+'&img='+j+'&img_name='+k;
		} 
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_id("dive_center_id");
			err_check_count += check_input("dive_center_name");
			err_check_count += check_input("dive_center_point");
			
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_id($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkID($('#'+$str).val())==false)
			{
				err ="<br /><span style='color:#F00'>英数半角で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<3)
			{
				err ="<br /><span style='color:#F00'>3文字以上で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		//英数半角
		function checkID(value){
			if(value.match(/[^0-9a-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 
		
	});
//-->
</script>

<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
		
	if($dive_center_id!="")
	{

		$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "commission");
		$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw"));
		$arr_db_field = array_merge($arr_db_field, array("img_1", "cate_area_id"));
		$arr_db_field = array_merge($arr_db_field, array("comment_1", "comment_2", "comment_3", "comment_4", "comment_list", "google_map_1", "google_map_2"));
		$arr_db_field = array_merge($arr_db_field, array("flag_boat", "flag_beach", "flag_guide"));
		$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT dive_center_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM dive_center where dive_center_id='".$dive_center_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}


  $sql = "SELECT cate_area_id, cate_area_name FROM cate_area order by view_level";
  
  $db_result = $common_dao->db_query($sql);
  if($db_result)
  {
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
      $arr_cate_area_id[$db_result[$db_loop]["cate_area_id"]] = $db_result[$db_loop]["cate_area_name"];
    }
  }
?>

<article>

<section class="sectionBox">
  <p class="tit">ダイブセンター登録・編集</p>
    <form action="./dive_center_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
		<?php $var = "check_up";?>
    <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="<? echo $_GET["dive_center_id"];?>" />
      <table class="editTable">
        <tr>
          <td colspan="2"><p><a href="<?=$_SERVER["PHP_SELF"];?>">ダイブセンター新規登録</a></p></td>
        </tr>
        <tr>
          <th>ダイブセンターID</th>
          <td>
          		<?php $var = "dive_center_id";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="<? if($$var!=""){ echo "hidden";} else { echo "text";}?>" value="<?php echo $$var;?>" />
            <?
              if($$var!="")
              {
                echo $$var."<p style=\"color:red\">変更不可</p>";
              }
              else
              {
                echo "<p>3文字以上の英語小文字、数字のみ入力可能</p>";
              }
            ?>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>ダイブセンター名</th>
          <td>
            <?php $var = "dive_center_name";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputLong" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>ダイビングポイント</th>
          <td>
            <?php $var = "dive_center_point";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputLong" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>メール</th>
          <td>
            <?php $var = "dive_center_email";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputLong" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>パスワード</th>
          <td>
            <?php $var = "dive_center_login_pw";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputLong" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>コミッション</th>
          <td>
            <?php $var = "commission";?>
            <? if($$var == "") { $$var = 15;}?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputLong" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>画像</th>
          <td>
              <? 
                  $var = "img_1";
              ?>
                  <input type="file" name="<?=$var;?>" size="50"> 
                  <input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>"><br>
              <?
                  if ($$var != "")
                  {
                      echo "<a href='".global_ssl."/".global_dive_center_dir.$dive_center_id."/".$$var."' target='_blank'>".global_ssl."/".global_dive_center_dir.$dive_center_id."/".$$var."</a><br />";
                      echo "<a href='/".global_dive_center_dir.$dive_center_id."/".$$var."' target='_blank'><img src='/".global_dive_center_dir.$dive_center_id."/".$$var."?d=".date(his)."' width=250 border=0></a>";
                      echo "<br /><a href=\"#\" onClick='fnImgDel(\"".$dive_center_id."\", \"".$$var."\", \"".$var."\");'>上のファイル削除</a>";
                  }
              ?>
          </td>
        </tr>
        <tr>
          <th>基本情報</th>
          <td>
            <?php $var = "comment_1";?>
            <textarea name="<?php echo $var;?>" id="<?php echo $var;?>"><?php echo $$var;?></textarea>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>その他・レンタル等</th>
          <td>
            <?php $var = "comment_2";?>
            <textarea name="<?php echo $var;?>" id="<?php echo $var;?>"><?php echo $$var;?></textarea>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>ダイビングポイント情報</th>
          <td>
            <?php $var = "comment_3";?>
            <textarea name="<?php echo $var;?>" id="<?php echo $var;?>"><?php echo $$var;?></textarea>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>アクセス</th>
          <td>
            <?php $var = "comment_4";?>
            <textarea name="<?php echo $var;?>" id="<?php echo $var;?>"><?php echo $$var;?></textarea>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>一覧表示料金</th>
          <td>
            <?php $var = "comment_list";?>
            <textarea name="<?php echo $var;?>" id="<?php echo $var;?>"><?php echo $$var;?></textarea>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>googlemap</th>
          <td>
          		緯度：
            <?php $var = "google_map_1";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputMid" />
            <label id="err_<?php echo $var;?>"></label>
            <br />
            軽度：
            <?php $var = "google_map_2";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputMid" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>エリア</th>
          <td>
            <?php $var = "cate_area_id";?>
            <select name="<? echo $var;?>" id="<? echo $var;?>">
            <?
            foreach($arr_cate_area_id as $key => $value)
            {
            ?>
              <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
            <?
            }
            ?>
            </select>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>表示順位</th>
          <td>
            <?php $var = "view_level";?>
            <?
							if($$var=="")
							{
								$$var = 10;
							}
						?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputShort" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>ボート有無</th>
          <td>
            <?php
							$var = "flag_boat";
							$arr_flag_boat[] = "無し";
							$arr_flag_boat[] = "有り";
						?>
            
            <select name="<? echo $var;?>" id="<? echo $var;?>">
            <?
            foreach($arr_flag_boat as $key => $value)
            {
            ?>
              <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
            <?
            }
            ?>
            </select>
          </td>
        </tr>
        <tr>
          <th>ビーチ有無</th>
          <td>
            <?php
							$var = "flag_beach";
							$arr_flag_beach[] = "無し";
							$arr_flag_beach[] = "有り";
						?>
            
            <select name="<? echo $var;?>" id="<? echo $var;?>">
            <?
            foreach($arr_flag_beach as $key => $value)
            {
            ?>
              <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
            <?
            }
            ?>
            </select>
          </td>
        </tr>
        <tr>
          <th>ガイド有無</th>
          <td>
            <?php
							$var = "flag_guide";
							$arr_flag_guide[] = "無し";
							$arr_flag_guide[] = "有り";
						?>
            
            <select name="<? echo $var;?>" id="<? echo $var;?>">
            <?
            foreach($arr_flag_guide as $key => $value)
            {
            ?>
              <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
            <?
            }
            ?>
            </select>
          </td>
        </tr>
        <tr>
          <th>ステータス</th>
          <td>
            <?php $var = "flag_open";?>
            <?php if($$var == "") {$$var = "1";}?>
            <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="1" <? if($flag_open==1){echo "checked" ;}?>> 公開　</label>　
            <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="0" <? if($flag_open!=1){echo "checked" ;}?>> 非公開</label>
          </td>
        </tr>
        <tr>
          <td colspan="2" class="tCenter btn"><input type="submit" value="この内容で保存" id="form_confirm"></td>
        </tr>
      </table>
  </form>
</section>

<section class="sectionBox">
  <p class="tit">ダイブセンター検索</p>
	<form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
    <table class="editTable">
      <tr>
        <th>キーワード</th>
        <td>
          <?php $var = "s_keyword";?>
          <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="inputLong" />
        </td>
      </tr>
      <tr>
        <th>ステータス</th>
        <td>
          <?
            $arr_flag_open[1] = "公開";
            $arr_flag_open[0] = "非公開";
          ?>
          <? $var = "s_flag_open";?>
          <select name="<?=$var;?>" id="<?=$var;?>">
            <option value="" selected>---</option>
          <? 
          foreach($arr_flag_open as $key => $value)
          {
          ?>
            <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?></option>
          <?
          }
          ?>
          </select>
          <label id="err_<?php echo $var;?>"></label>
        </td>
      </tr>
      <tr>
        <th>エリア</th>
        <td>
          <? $var = "s_cate_area_id";?>
          <select name="<?=$var;?>" id="<?=$var;?>">
            <option value="" selected>---</option>
          <? 
          foreach($arr_cate_area_id as $key => $value)
          {
          ?>
            <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?></option>
          <?
          }
          ?>
          </select>
          <label id="err_<?php echo $var;?>"></label>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="tCenter btn">
          <input type="submit" value="検索する" />
        </td>
      </tr>
    </table>
  </form>
</section>

<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (dive_center_name like '%".$s_keyword."%' or dive_center_point like '%".$s_keyword."%' ) ";
	}
  if($s_flag_open != "")
  {
    $where .= " and flag_open='".$s_flag_open."' ";
  }
  if($s_cate_area_id != "")
  {
    $where .= " and cate_area_id='".$s_cate_area_id."' ";
  }


	//合計
	$sql_count = "SELECT count(dive_center_id) as all_count FROM dive_center where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("dive_center_id", "dive_center_name", "dive_center_point", "img_1");
	$arr_db_field = array_merge($arr_db_field, array("dive_center_email", "dive_center_login_pw"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM dive_center where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by up_date desc";
	}
	$sql .= " limit $offset,$view_count";
	
?>
	すべて：<?=$all_count;?>
<section class="sectionBox">
  <p class="tit">ダイブセンターリスト</p>
  <div class="box">
    <table class="listTable">
      <tr align="center">
        <th>編集</th>
        <th>管理</th>
        <th>画像</th>
        <th>ダイブセンター名</th>
        <th>スタイル</th>
        <th>エントリー</th>
        <th>タンク</th>
      </tr>
<?
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <tr align="center">
        <td></td>
        <td></td>
        <td><a href="?order_name=dive_center_id&order=desc">▼</a>｜<a href="?order_name=dive_center_id">▲</a></td>
        <td><a href="?order_name=dive_center_name&order=desc">▼</a>｜<a href="?order_name=dive_center_name">▲</a></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
<?php
  for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
  {
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
    <tr align="center">
      <td>
        <a href="?dive_center_id=<?php echo $dive_center_id;?>">編集</a> 
        <br /><a href="#" onClick='fnChangeSel("<?php echo $dive_center_id;?>");'>削除</a>
        <br /><a href="/divingpoint/detail.php?dive_center_id=<?php echo $dive_center_id;?>" target="_blank">詳細</a>
      </td>
      <td>
      	<a href="/master/dive_center_style/dive_center_style_list.php?dive_center_id=<?php echo $dive_center_id;?>">スタイル</a><br />
      	<a href="/master/dive_center_boat/dive_center_boat_list.php?dive_center_id=<?php echo $dive_center_id;?>">エントリー</a><br />
      	<a href="/master/dive_center_tank/dive_center_tank_list.php?dive_center_id=<?php echo $dive_center_id;?>">タンク</a><br />
      </td>
      <td>
				<?php echo $dive_center_id;?><br />
				<?php
					if($img_1!="")
					{
						echo "<img src='/".global_dive_center_dir.$dive_center_id."/".$img_1."' width=80 border=0>";
					}
					?>
      </td>
      <td><?php echo $dive_center_name;?></td>
      <td>
        <?php
				//スタイル
				$db_result_style = $common_center->Fn_dive_center_style ($common_dao, $dive_center_id) ;
				if($db_result_style)
				{
					for($db_loop_style=0 ; $db_loop_style < count($db_result_style) ; $db_loop_style++)
					{
						echo $db_result_style[$db_loop_style]["dive_center_style_title"]."（&yen;".number_format($db_result_style[$db_loop_style]["dive_center_style_price"]);
						if($db_result_style[$db_loop_style]["possible_point"]!="0")
						{
							echo " ポイント：".number_format($db_result_style[$db_loop_style]["possible_point"]);
						}
						echo "）<br />";
					}
				}
				?>
      </td>
      <td>
        <?php
				//エントリー
				$db_result_boat = $common_center->Fn_dive_center_boat ($common_dao, $dive_center_id) ;
				if($db_result_boat)
				{
					for($db_loop_boat=0 ; $db_loop_boat < count($db_result_boat) ; $db_loop_boat++)
					{
						echo $db_result_boat[$db_loop_boat]["dive_center_boat_title"]."（&yen;".number_format($db_result_boat[$db_loop_boat]["dive_center_boat_price"])."）<br />";
					}
				}
				?>
      </td>
      <td>
        <?php
				//タンク
				$db_result_tank = $common_center->Fn_dive_center_tank ($common_dao, $dive_center_id) ;
				if($db_result_tank)
				{
					for($db_loop_tank=0 ; $db_loop_tank < count($db_result_tank) ; $db_loop_tank++)
					{
						echo $db_result_tank[$db_loop_tank]["dive_center_tank_title"]."（&yen;".number_format($db_result_tank[$db_loop_tank]["dive_center_tank_price"])."）<br />";
					}
				}
				?>
      </td>
    </tr>
<?
	}
?>
<?
	}
?>
    </table>
    <div style="text-align:center;">
      <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
    </div><!-- //center-->
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>