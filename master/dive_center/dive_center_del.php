<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ダイブセンター登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	$dive_center_id = $_GET['dive_center_id'];

	//DBへ保存
	if ($dive_center_id != "")
	{
		//商品情報削除
		$dbup = "delete from dive_center where dive_center_id = '$dive_center_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("ダイブセンターを削除しました。", "dive_center_list.php#search_list");

?>
</body>
</html>