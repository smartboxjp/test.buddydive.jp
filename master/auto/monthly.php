<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>在庫自動更新</title>
</head>

<body>
<?
	//$dive_center_id = "test1";
	date_default_timezone_set('Asia/Tokyo');
	$start_yyyymm = date("Y-m-01");
	
	Fn_stock_up($common_dao, "boat");
	Fn_stock_up($common_dao, "style");
	Fn_stock_up($common_dao, "tank");
	
	function Fn_stock_up($common_dao, $str)
	{
		$arr_db_field = array("dive_center_".$str."_id", "dive_center_id", "dive_center_".$str."_title", "dive_center_".$str."_stock");
		$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM dive_center_".$str." ";
		// where dive_center_id='".$dive_center_id."'";
		//$sql .= " and flag_open=1 ";//管理ページは見れる
		$sql .= " order by view_level ";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
			{
				foreach($arr_db_field as $val)
				{
					$$val = $db_result[$db_loop][$val];
				}
				
				for($loop_yyyymm = 0 ; $loop_yyyymm<3 ; $loop_yyyymm++)
				{
					$check_yyyymm = date("Y-m-01", strtotime($start_yyyymm. "  ".$loop_yyyymm. "month"));
					
					$dive_center_str_id = "dive_center_".$str."_id"; 
					$dive_center_str_stock = "dive_center_".$str."_stock"; 
					
					//データがあるかをチェック
					$sql_count="select yyyymm FROM stock_".$str." where yyyymm = '".date("Ym", strtotime($check_yyyymm))."' and dive_center_id='".$dive_center_id."' and dive_center_".$str."_id='".$$dive_center_str_id."'";
		
					$db_result_count = $common_dao->db_query($sql_count);
					if(!$db_result_count)
					{
						$last_day = date('t', strtotime(substr($yyyymm, 0, 4)."-".substr($yyyymm, 5, 2)."-01"));
						
						$sql_insert = "insert into stock_".$str." (yyyymm, dive_center_id, dive_center_".$str."_id ";
						for($schDay = 1; $schDay <= $last_day; $schDay ++) 
						{
							$sql_insert .= ", s_".substr($schDay+100, 1) ; 
						}
						$sql_insert .= ", regi_date, up_date) values ('".date("Ym", strtotime($check_yyyymm))."', '".$dive_center_id."', '".$$dive_center_str_id."' ";
						for($schDay = 1; $schDay <= $last_day; $schDay ++) 
						{
							$sql_insert .= ", ".$$dive_center_str_stock ; 
						}
						$sql_insert .= ", now(), now() )";
	
						$common_dao->db_update($sql_insert);
						//echo $sql_insert."<hr />";
					}
	
				}
			}
		}
	}
?>
</body>
</html>