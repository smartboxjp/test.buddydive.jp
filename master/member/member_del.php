<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ダイバー削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($member_id == "")
	{
		$common_connect -> Fn_javascript_back("ユーザーIDがありません。");
	}
	
	//DBへ保存
	if ($member_id != "")
	{
		//商品情報削除
		$dbup = "delete from member where member_id = '$member_id'";
		$db_result = $common_dao->db_update($dbup);
		
		$dbup = "delete from member_buddy where member_id = '$member_id' or buddy_id = '$member_id'";
		$db_result = $common_dao->db_update($dbup);
		
		$dbup = "delete from member_medical where member_id = '$member_id'";
		$db_result = $common_dao->db_update($dbup);
		
		$dbup = "delete from member_message where member_id = '$member_id'";
		$db_result = $common_dao->db_update($dbup);
		
		$dbup = "delete from member_point where member_id = '$member_id'";
		$db_result = $common_dao->db_update($dbup);
		
		$dbup = "delete from member_skill where member_id = '$member_id'";
		$db_result = $common_dao->db_update($dbup);
		
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_member_dir.$member_id))
		{
			$common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_member_dir.$member_id);
		}
	}
	
	$common_connect -> Fn_javascript_move("ダイバーを削除しました。", "list.php?member_id=".$member_id."#search_list");

?>
</body>
</html>