<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();

	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
header("Content-Type: application/vnd.ms-excel;"); 
header("Content-Disposition: attachment; filename=csv_".date("YmdHi").".csv"); 
header("Content-Transfer-Encoding: text/plain"); 
header("Pragma: no-cache"); 
header("Expires: 0"); 
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (user_name like '%".$s_keyword."%' or member_email like '%".$s_keyword."%' or member_name_1 like '%".$s_keyword."%' or member_name_2 like '%".$s_keyword."%' or member_name_kana like '%".$s_keyword."%' or tel like '%".$s_keyword."%' or phone like '%".$s_keyword."%' or address like '%".$s_keyword."%' or emergency_name like '%".$s_keyword."%' or emergency_relationship like '%".$s_keyword."%' or emergency_tel like '%".$s_keyword."%'  )";
	}
	
	if($s_pref != "")
	{
		$where .= " and pref = '".$s_pref."'";
	}
	
	
	

	
	//リスト表示
	$arr_db_field = array("member_id", "user_name", "member_email", "member_login_pw", "temp_key");
	$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana", "member_point"));
	$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
	$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
	$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count"));
	$arr_db_field = array_merge($arr_db_field, array("flag_hontouroku", "hontouroku_start_date", "hontouroku_finish_date"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM member where 1 ".$where ;
	
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by up_date desc";
	}
	
	$enter = "\r\n";
	$csv_list = "";
	$csv_list .= "管理ID,名前,ユーザー名,スキル,メディカル,ポイント,最終ダイブ日,登録状況,本登録開始,本登録完了,登録日,修正日".$enter;

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);

		for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			
			$csv_list .= "\"".$member_id."\",\"".$member_name_1." ".$member_name_2."\",\"".$user_name."\",";
			
			$skill_id = $common_member->Fn_member_skill($common_dao, $member_id);
			
	
			if($skill_id=="1") { $csv_list .= "\"できる\"";}
			elseif($skill_id=="2") { $csv_list .= "\"できない\"";}
			
			$csv_list .= ",";
			$medical_id = $common_member->Fn_member_medical($common_dao, $member_id);

			if($medical_id=="1") { $csv_list .= "\"該当なし\"";}
			elseif($medical_id=="2") { $csv_list .= "\"要診断書\"";}
      
			$csv_list .= ",";
			$csv_list .= "\"".number_format($member_point)."\",";
			
			$last_reserve = $common_member->Fn_member_last_dive($common_dao, $member_id);
			$csv_list .= "\"".str_replace("-", "/", $last_reserve)."\",";
		
			if($flag_hontouroku=="1")
			{
				if($skill_id>0 && $medical_id>0)
				{
					$csv_list .= "\"本登録完了\",";
				}
				else
				{
					$csv_list .= "\"本登録開始\",";
				}
			}
			else
			{
				$csv_list .= "\"仮登録\",";
			}
			
      $csv_list .= "\"".$hontouroku_start_date."\",\"".$hontouroku_finish_date."\",";
      $csv_list .= "\"".$regi_date."\",\"".$up_date."\"".$enter;
		}
	}
	
	echo mb_convert_encoding($csv_list, 'SJIS', 'UTF-8');
?>
