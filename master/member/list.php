<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMember.php";
	$common_member = new CommonMember();
	
  $meta_title = "ダイバー管理";
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>

<script language="javascript"> 
	function fnChangeSel(i) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './member_del.php?member_id='+i;
		} 
	}
</script>

<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>

<article>


<section class="sectionBox">
  <p class="tit">ダイバー検索</p>
	<form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
  <table class="editTable">
    <tr>
      <th>都道府県</th>
      <td>
				<? $var = "s_pref";?>
				<?
        $arr_pref[] = "北海道";
        $arr_pref[] = "青森県";
        $arr_pref[] = "岩手県";
        $arr_pref[] = "宮城県";
        $arr_pref[] = "秋田県";
        $arr_pref[] = "山形県";
        $arr_pref[] = "福島県";
        $arr_pref[] = "茨城県";
        $arr_pref[] = "栃木県";
        $arr_pref[] = "群馬県";
        $arr_pref[] = "埼玉県";
        $arr_pref[] = "千葉県";
        $arr_pref[] = "東京都";
        $arr_pref[] = "神奈川県";
        $arr_pref[] = "新潟県";
        $arr_pref[] = "富山県";
        $arr_pref[] = "石川県";
        $arr_pref[] = "福井県";
        $arr_pref[] = "山梨県";
        $arr_pref[] = "長野県";
        $arr_pref[] = "岐阜県";
        $arr_pref[] = "静岡県";
        $arr_pref[] = "愛知県";
        $arr_pref[] = "三重県";
        $arr_pref[] = "滋賀県";
        $arr_pref[] = "京都府";
        $arr_pref[] = "大阪府";
        $arr_pref[] = "兵庫県";
        $arr_pref[] = "奈良県";
        $arr_pref[] = "和歌山県";
        $arr_pref[] = "鳥取県";
        $arr_pref[] = "島根県";
        $arr_pref[] = "岡山県";
        $arr_pref[] = "広島県";
        $arr_pref[] = "山口県";
        $arr_pref[] = "徳島県";
        $arr_pref[] = "香川県";
        $arr_pref[] = "愛媛県";
        $arr_pref[] = "高知県";
        $arr_pref[] = "福岡県";
        $arr_pref[] = "佐賀県";
        $arr_pref[] = "長崎県";
        $arr_pref[] = "熊本県";
        $arr_pref[] = "大分県";
        $arr_pref[] = "宮崎県";
        $arr_pref[] = "鹿児島県";
        $arr_pref[] = "沖縄県";
        ?>
        <select name="<? echo $var;?>" id="<? echo $var;?>">
        <option value="">都道府県</option>
        <?
        foreach($arr_pref as $key => $value)
        {
        ?>
          <option value="<? echo $value;?>" <? if($$var==$value) { echo " selected ";}?>><? echo $value;?></option>
        <?
        }
        ?>
        </select>
      </td>
    </tr>
    <tr>
      <th>キーワード</th>
      <td>
        <?php $var = "s_keyword";?>
        <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="inputLong" />
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellpadding="5" cellspacing="1">
    <tr>
      <td class="tCenter btn">
        <input type="submit" value="検索する" />
      </td>
    </tr>
  </table>
  </form>
</section>

<?php
	
	$view_count=300;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (user_name like '%".$s_keyword."%' or member_email like '%".$s_keyword."%' or member_name_1 like '%".$s_keyword."%' or member_name_2 like '%".$s_keyword."%' or member_name_kana like '%".$s_keyword."%' or tel like '%".$s_keyword."%' or phone like '%".$s_keyword."%' or address like '%".$s_keyword."%' or emergency_name like '%".$s_keyword."%' or emergency_relationship like '%".$s_keyword."%' or emergency_tel like '%".$s_keyword."%'  )";
	}
	
	if($s_pref != "")
	{
		$where .= " and pref = '".$s_pref."'";
	}
	
	
	

	//合計
	$sql_count = "SELECT count(member_id) as all_count FROM member where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("member_id", "user_name", "member_email", "member_login_pw", "temp_key");
	$arr_db_field = array_merge($arr_db_field, array("member_name_1", "member_name_2", "member_name_kana", "member_point"));
	$arr_db_field = array_merge($arr_db_field, array("birth_yyyy", "birth_mm", "birth_dd", "sex", "tel", "phone", "blood"));
	$arr_db_field = array_merge($arr_db_field, array("pref", "address", "emergency_name", "emergency_relationship", "img_1"));
	$arr_db_field = array_merge($arr_db_field, array("emergency_tel", "diving_rank", "diving_group", "diving_count", "flag_hontouroku"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM member where 1 ".$where ;
	
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by up_date desc";
	}
	$sql .= " limit $offset,$view_count";
	
?>
	すべて：<?=$all_count;?>
	<div align="right"><a href="list_csv.php">CSVダウンロード</a></div>
  <section class="sectionBox">
  <p class="tit">ダイバーリスト</p>
  <div class="box">
    <table class="listTable">
      <tr>
        <th>編集</th>
        <th>
					管理ID<br />
				</th>
        <th>写真</th>
        <th>名前<br />ユーザー名</th>
        <th>スキル</th>
        <th>メディカル</th>
        <th>ポイント</th>
        <th>最終ダイブ日</th>
        <th>登録状況</th>
        <th>登録日<br />修正日</th>
      </tr>
<?
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
          <tr align="center">
            <td>ソート</td>
            <td><a href="?order_name=member_id&order=desc">▼</a>｜<a href="?order_name=member_id">▲</a></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><a href="?order_name=member_point&order=desc">▼</a>｜<a href="?order_name=member_point">▲</a></td>
            <td></td>
            <td><a href="?order_name=flag_hontouroku&order=desc">▼</a>｜<a href="?order_name=flag_hontouroku">▲</a></td>
            <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
          </tr>
<?php
  for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
  {
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
      <tr>
        <th>
          <a href="./member_detail.php?s_member_id=<?php echo $member_id;?>">詳細</a>　
          <a href="#" onClick='fnChangeSel("<?php echo $member_id;?>");'>削除</a>
        </th>
        <td><?php echo $member_id;?></td>
        <td>
				<?php
					if($img_1!="")
					{
						echo "<img src='/".global_member_dir.$member_id."/".$img_1."' width=50 border=0>";
					}
				?>
        </td>
        <td><?php echo $member_name_1." ".$member_name_2;?><br /><?php echo $user_name;?></td>
				<?php 
				$skill_id = $common_member->Fn_member_skill($common_dao, $member_id);
				?>
        <td <? if($skill_id!="1") { echo " class='error' ";}?>>
				<?
          if($skill_id=="1") { echo "できる";}
          elseif($skill_id=="2") { echo "できない";}
				?>
        </td>
				<?
          $medical_id = $common_member->Fn_member_medical($common_dao, $member_id);
        ?>
        <td <? if($medical_id!="1") { echo " class='error' ";}?>>
        <?
          if($medical_id=="1") { echo "該当なし";}
          elseif($medical_id=="2") { echo "要診断書";}
        ?>
        </td>
        <td><?php echo number_format($member_point);?></td>
        <td>
				<?php
					$last_reserve = $common_member->Fn_member_last_dive($common_dao, $member_id);
					echo str_replace("-", "/", $last_reserve);
				?>
        </td>
				
				
				<?php
				if($flag_hontouroku=="1")
				{
					if($skill_id>0 && $medical_id>0)
					{
						$hontouroku_view = "本登録完了";
					}
					else
					{
						$hontouroku_view =  "本登録開始";
					}
				}
				else
				{
					$hontouroku_view =  "仮登録";
				}
				?>
        <td <? if($hontouroku_view!="本登録完了") { echo " class='error' ";}?>>
					<? echo $hontouroku_view;?>
        </td>
        <td><?php echo $regi_date."<br />".$up_date;?></td>
      </tr>
<?
	}
?>
<?
	}
?>
    </table>
    <div style="text-align:center;">
      <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
    </div><!-- //center-->
  </section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>