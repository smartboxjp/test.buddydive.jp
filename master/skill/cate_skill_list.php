<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

  $meta_title = "スキル管理";
?>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/header.php'); ?>

<script language="javascript"> 
	function fnChangeSel(i, j) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './cate_skill_del.php?shop_id='+i+'&cate_skill_id='+j;
		} 
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("cate_skill_title");
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
	});
//-->
</script>

<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
		
	if($cate_skill_id!="")
	{

		$arr_db_field = array("cate_skill_id", "cate_skill_title");
		$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT cate_skill_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM cate_skill where cate_skill_id='".$cate_skill_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
?>

<article>

<section class="sectionBox">
  <p class="tit">スキル登録・編集</p>
    <form action="./cate_skill_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
    <?php $var = "shop_id";?>
      <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="<?php echo $$var;?>" />
      <table class="editTable">
        <tr>
          <td colspan="2"><p><a href="<?=$_SERVER["PHP_SELF"];?>">スキル新規登録</a></p></td>
        </tr>
        <tr>
          <th>スキルID</th>
          <td>
            <?php if($cate_skill_id=="") {echo "自動生成";} else { echo $cate_skill_id;}?>
            <input name="cate_skill_id" type="hidden" value="<?php echo $cate_skill_id;?>" />
          </td>
        </tr>
        <tr>
          <th>スキル名</th>
          <td>
            <?php $var = "cate_skill_title";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="inputLong" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>表示順位</th>
          <td>
            <?php $var = "view_level";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<? if($$var==""){echo "10";} else {echo $$var;}?>" class="inputShort" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th>ステータス</th>
          <td>
            <?php $var = "flag_open";?>
            <?php if($$var == "") {$$var = "1";}?>
            <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="1" <? if($flag_open==1){echo "checked" ;}?>> 公開　</label>　
            <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="0" <? if($flag_open!=1){echo "checked" ;}?>> 非公開</label>
          </td>
        </tr>
        <tr>
          <td colspan="2" class="tCenter btn"><input type="submit" value="この内容で作成" id="form_confirm"></td>
        </tr>
      </table>
  </form>
</section>

<section class="sectionBox">
  <p class="tit">スキル検索</p>
	<form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
    <table class="editTable">
      <tr>
        <th>キーワード</th>
        <td>
          <?php $var = "s_keyword";?>
          <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="inputLong" />
        </td>
      </tr>
      <tr>
        <th>ステータス</th>
        <td>
          <?
            $arr_flag_open[1] = "公開";
            $arr_flag_open[0] = "非公開";
          ?>
          <? $var = "s_flag_open";?>
          <select name="<?=$var;?>" id="<?=$var;?>">
            <option value="" selected>---</option>
          <? 
          foreach($arr_flag_open as $key => $value)
          {
          ?>
            <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?></option>
          <?
          }
          ?>
          </select>
          <label id="err_<?php echo $var;?>"></label>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="tCenter btn">
          <input type="submit" value="検索する" />
        </td>
      </tr>
    </table>
  </form>
</section>

<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (cate_skill_title like '%".$s_keyword."%') ";
	}
	if($s_flag_open != "")
	{
		$where .= " and flag_open='".$s_flag_open."' ";
	}

	//合計
	$sql_count = "SELECT count(cate_skill_id) as all_count FROM cate_skill where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("cate_skill_id", "cate_skill_title");
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM cate_skill where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by view_level, up_date desc";
	}
	$sql .= " limit $offset,$view_count";
	
?>
	すべて：<?=$all_count;?>
<section class="sectionBox">
  <p class="tit">スキルリスト</p>
  <div class="box">
    <table class="listTable">
      <tr align="center">
        <th>編集</th>
        <th>スキルID</th>
        <th>スキル名</th>
        <th>表示順位</th>
        <th>公開有無</th>
        <th>登録日<br />修正日</th>
      </tr>
<?
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <tr align="center">
        <td>ソート</td>
        <td><a href="?order_name=cate_skill_id&order=desc">▼</a>｜<a href="?order_name=cate_skill_id">▲</a></td>
        <td><a href="?order_name=cate_skill_title&order=desc">▼</a>｜<a href="?order_name=cate_skill_title">▲</a></td>
        <td><a href="?order_name=view_level&order=desc">▼</a>｜<a href="?order_name=view_level">▲</a></td>
        <td><a href="?order_name=flag_open&order=desc">▼</a>｜<a href="?order_name=flag_open">▲</a></td>
        <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
      </tr>
<?php
  for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
  {
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
    <tr align="center">
      <td>
        <a href="?cate_skill_id=<?php echo $cate_skill_id;?>">編集</a>　
        <a href="#" onClick='fnChangeSel("<?php echo $shop_id;?>", "<?php echo $cate_skill_id;?>");'>削除</a>
      </td>
      <td><?php echo $cate_skill_id;?></td>
      <td><?php echo $cate_skill_title;?></td>
      <td><?php echo $view_level;?></td>
      <td>
        <?php
        if($flag_open==1)
        {
          $open_check = "公開";
        }
        else
        {
          $open_check = "非公開";
        }
        echo $open_check;
        ?>
      </td>
      <td><?php echo $regi_date."<br />".$up_date;?></td>
    </tr>
<?
	}
?>
<?
	}
?>
    </table>
    <div style="text-align:center;">
      <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
    </div><!-- //center-->
</section>

</article>

<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/master/include/footer.php'); ?>