<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ダイブセンタースタイル登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	$dive_center_style_id = $_GET['dive_center_style_id'];

	//DBへ保存
	if ($dive_center_style_id != "")
	{
		//商品情報削除
		$dbup = "delete from dive_center_style where dive_center_style_id = '$dive_center_style_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("ダイブセンタースタイルを削除しました。", "dive_center_style_list.php#search_list");

?>
</body>
</html>