<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>スタイル登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($dive_center_id == "")
	{
		$common_connect -> Fn_javascript_back("ダイブセンターIDがありません。");
	}
	
	//DBへ保存
	if ($dive_center_style_id != "" && $dive_center_id != "")
	{
		//商品情報削除
		$dbup = "delete from dive_center_style where dive_center_style_id = '$dive_center_style_id' and dive_center_id = '$dive_center_id'";
		$db_result = $common_dao->db_update($dbup);
		
		//在庫削除
		$dbup = "delete from stock_style where dive_center_style_id = '$dive_center_style_id' and dive_center_id = '$dive_center_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("スタイルを削除しました。", "dive_center_style_list.php?dive_center_id=".$dive_center_id."#search_list");

?>
</body>
</html>