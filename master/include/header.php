<?php require_once($_SERVER["DOCUMENT_ROOT"]. "/common_center/include/common.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex,nofollow">
<title><? echo $meta_title;?>[バディダイブ管理画面]</title>

<link rel="stylesheet" href="/common_center/css/common.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php require_once ($_SERVER['DOCUMENT_ROOT'] . '/common/include/analytics.php'); ?>
</head>

<body>
<header><p class="centerName">バディダイブ管理画面</p></header>
<nav>
<p>Buddy Diving<br>
Resavetion System</p>
<?php if($page_file != 'login'){ ?>
  <ul>
    <li><a href="/master/dashboard/"<?php if($page_file == 'dashboard'){ echo ' class="act"';}?>>ダッシュボード</a></li>
    <li><a href="/master/reserve/"<?php if($page_file == 'reserve'){ echo ' class="act"';}?>>予約管理</a></li>
    <li><a href="/master/sales/"<?php if($page_file == 'sales'){ echo ' class="act"';}?>>売上管理</a></li>
    <li><a href="/master/member/list.php"<?php if($page_file == 'member'){ echo ' class="act"';}?>>ユーザー管理</a></li>
    <? if($_SESSION['admin_level'] == "2") { ?>
    <li><a href="/master/dive_center/dive_center_list.php"<?php if($page_file == 'dive_center'){ echo ' class="act"';}?>>ダイビングセンター</a></li>
    <li><a href="/master/skill/cate_skill_list.php"<?php if($page_file == 'skill'){ echo ' class="act"';}?>>スキル管理</a></li>
    <li><a href="/master/medical/cate_medical_list.php"<?php if($page_file == 'medical'){ echo ' class="act"';}?>>メディカル管理</a></li>
    <? } ?>
    <li><a href="/master/login/logout.php">ログアウト</a></li>
  </ul>
<?php } ?>
</nav>