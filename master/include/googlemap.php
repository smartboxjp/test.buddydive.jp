<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	$google_map_1 = $_GET["google_map_1"];
	$google_map_2 = $_GET["google_map_2"];
	$kubun = $_GET["kubun"];
	
	if ($google_map_1=="") { $google_map_1 = "26.212353";}
	if ($google_map_2=="") { $google_map_2 = "127.680777";}
	
?>
<!DOCTYPE html>

<html lang="ja-JP">
<head>
<meta charset="UTF-8">
<title>Google Map</title>
	<link rel="stylesheet" type="text/css" href="/_css/base.css">
	<style type="text/css"><!--
		.tbl	{ background-color:#cccccc; }
		.tbl_t	{ background-color:#7FA47F; color:#ffffff; }
		.td_t	{ background-color:#dbe8db; vertical-align:middle; font-weight:bold;}
		.td_t2	{ background-color:#dbe8db; vertical-align:middle; }
		.td_d	{ background-color:#ffffff; }
	--></style>

	<script src="http://maps.google.com/maps/api/js?v=3&sensor=false" type="text/javascript" charset="UTF-8"></script>
	</script>

    <script type="text/javascript">
    //<![CDATA[

	
		var map;
		var google_map_1	= <?=$google_map_1;?>;
		var google_map_2	= <?=$google_map_2;?>;
		
		// 初期化。bodyのonloadでinit()を指定することで呼び出してます
		function init() {
	
			// Google Mapで利用する初期設定用の変数
			var latlng = new google.maps.LatLng(google_map_1, google_map_2);
			var opts = {
				zoom: 17,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: latlng
			};
			
			// getElementById("map")の"map"は、body内の<div id="map">より
			map = new google.maps.Map(document.getElementById("map"), opts);
	
			var markerOpts = {
			position: new google.maps.LatLng(google_map_1, google_map_2),
			map: map
			//,title: infoHtml
			};
			
			var marker = new google.maps.Marker(markerOpts);
			google.maps.event.addListener(map, 'click', mylistener);
			
		}

	
		function mylistener(event) {
			document.save.google_map_1.value = jfncRoundDecimal(event.latLng.lat(), 6);
			document.save.google_map_2.value = jfncRoundDecimal(event.latLng.lng(), 6);
			
		}

		/*** 座標を親画面にセット ***/
		function jfncSetXY() {
			if ( window.opener != window.self ) {
			<?
				if($kubun=="parking")
				{
			?>
				window.opener.document.save.google_parking_1.value = document.save.google_map_1.value;
				window.opener.document.save.google_parking_2.value = document.save.google_map_2.value;
			<?
				}
				else
				{
			?>
				window.opener.document.save.google_map_1.value = document.save.google_map_1.value;
				window.opener.document.save.google_map_2.value = document.save.google_map_2.value;
			<?
				}
			?>
				window.close();
			}
		}

		/*** 少数点指定桁以下四捨五入 ***/
		function jfncRoundDecimal( val, decimal ) {
			var n = 1;
			for ( var i=0; i<decimal; i++ ){
				n *= 10;
			}

			val *= n;
			val =  Math.round( val );
			val /= n;

			return val;
		}
    //]]>
    </script>
</head>
<body onLoad="init();" onContextMenu="return false;" topmargin="0" leftmargin="0">

	<center>
		<div id="map" style="width:580px; height:450px; cursor:pointer;">
			<table id=tblloading height="100%" align=center>
				<tr>
					<td id="loadingMsg">Now loading... </td>
				</tr>
			</table>
		</div>


		<br />
    <form name="save" style="margin:0px;">
		<table class="tbl" border="0" cellspacing="1" cellpadding="4">
			<colgroup>
				<col width="70">
				<col width="80">
				<col width="130">
			</colgroup>
			<tr>
				<th class="td_t text06" colspan="3" align="center" height="25" style="color:blue;">
					<b>※地図上をクリックすると座標が取得できます</b>
				</th>
			</tr>
			<tr>
				<th class="td_t text06" nowrap>緯度（y）</th>
				<td class="td_d text06" nowrap><input type="text" name="google_map_1" id="google_map_1" value="<?=$google_map_1;?>" style="text-align:right; width:100%;" class="text06"></td>
				<td class="td_d text06" nowrap><span id="latFull"></span></td>
			</tr>
			<tr>
				<th class="td_t text06" nowrap>経度（x）</th>
				<td class="td_d text06" nowrap><input type="text" name="google_map_2" id="google_map_2" value="<?=$google_map_2;?>" style="text-align:right; width:100%;" class="text06"></td>
				<td class="td_d text06" nowrap><span id="lonFull"></span></td>
			</tr>
			<tr>
				<td class="td_t text06" colspan="3" align="center">
					<input type="button" name="btnUpdate" value="確定" onClick="jfncSetXY();" style="width:70px; letter-spacing:1em;">　
					<input type="button" name="btnUpdate" value="キャンセル" onClick="window.close();" style="width:70px">
				</td>
			</tr>
		</table>
    </form>

	</center>

</body>
</html>